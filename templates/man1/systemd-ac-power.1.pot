# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-02-20 20:33+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux fedora-38 fedora-rawhide
#, no-wrap
msgid "SYSTEMD-AC-POWER"
msgstr ""

#. type: TH
#: archlinux fedora-38 fedora-rawhide
#, no-wrap
msgid "systemd 253"
msgstr ""

#. type: TH
#: archlinux fedora-38 fedora-rawhide
#, no-wrap
msgid "systemd-ac-power"
msgstr ""

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux fedora-38 fedora-rawhide
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide
msgid ""
"systemd-ac-power - Report whether we are connected to an external power "
"source"
msgstr ""

#. type: SH
#: archlinux fedora-38 fedora-rawhide
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide
msgid "B<systemd-ac-power> [OPTIONS...]"
msgstr ""

#. type: SH
#: archlinux fedora-38 fedora-rawhide
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide
msgid ""
"B<systemd-ac-power> may be used to check whether the system is running on AC "
"power or not\\&. By default it will simply return success (if we can detect "
"that we are running on AC power) or failure, with no output\\&. This can be "
"useful for example to debug I<ConditionACPower=> (see B<systemd.unit>(5))\\&."
msgstr ""

#. type: SH
#: archlinux fedora-38 fedora-rawhide
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide
msgid "The following options are understood:"
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide
msgid "B<-v>, B<--verbose>"
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide
msgid "Show result as text instead of just returning success or failure\\&."
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide
msgid "B<-h>, B<--help>"
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide
msgid "Print a short help text and exit\\&."
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide
msgid "B<--version>"
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide
msgid "Print a short version string and exit\\&."
msgstr ""

#. type: SH
#: archlinux fedora-38 fedora-rawhide
#, no-wrap
msgid "EXIT STATUS"
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide
msgid ""
"On success (running on AC power), 0 is returned, a non-zero failure code "
"otherwise\\&."
msgstr ""

#. type: SH
#: archlinux fedora-38 fedora-rawhide
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide
msgid "B<systemd>(1)"
msgstr ""
