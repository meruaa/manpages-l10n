# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-02-15 19:26+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ZSTDGREP"
msgstr ""

#. type: TH
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "January 2022"
msgstr ""

#. type: TH
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "zstd 1.5.2"
msgstr ""

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<zstdgrep> - print lines matching a pattern in zstandard-compressed files"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<zstdgrep> [I<grep-flags>] [--] I<pattern> [I<files> ...]"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid ""
"B<zstdgrep> runs B<grep (1)> on files, or B<stdin> if no files argument is "
"given, after decompressing them with B<zstdcat (1)>."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The grep-flags and pattern arguments are passed on to B<grep (1)>. If an B<-"
"e> flag is found in the B<grep-flags>, B<zstdgrep> will not look for a "
"pattern argument."
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid ""
"Note that modern B<grep> alternatives such as B<ripgrep> (B<rg>) support "
"B<zstd>-compressed files out of the box, and can prove better alternatives "
"than B<zstdgrep> notably for unsupported complex pattern searches. Note "
"though that such alternatives may also feature some minor command line "
"differences."
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "EXIT STATUS"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"In case of missing arguments or missing pattern, 1 will be returned, "
"otherwise 0."
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<zstd (1)>"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "AUTHORS"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Thomas Klausner I<wiz@NetBSD.org>"
msgstr ""

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "December 2020"
msgstr ""

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "zstd 1.4.8"
msgstr ""

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"B<zstdgrep> runs B<grep (1)> on files or stdin, if no files argument is "
"given, after decompressing them with B<zstdcat (1)>."
msgstr ""

#. type: TH
#: debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "February 2023"
msgstr ""

#. type: TH
#: debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "zstd 1.5.4"
msgstr ""

#. type: Plain text
#: debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid "B<zstdgrep> [I<grep-flags>] [--] I<pattern> [I<files> \\|.\\|.\\|.]"
msgstr ""

#. type: Plain text
#: debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"B<zstdgrep> runs B<grep>(1) on files, or B<stdin> if no files argument is "
"given, after decompressing them with B<zstdcat>(1)."
msgstr ""

#. type: Plain text
#: debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"The I<grep-flags> and I<pattern> arguments are passed on to B<grep>(1). If "
"an B<-e> flag is found in the I<grep-flags>, B<zstdgrep> will not look for a "
"I<pattern> argument."
msgstr ""

#. type: Plain text
#: debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"Note that modern B<grep> alternatives such as B<ripgrep> (B<rg>(1)) support "
"B<zstd>-compressed files out of the box, and can prove better alternatives "
"than B<zstdgrep> notably for unsupported complex pattern searches. Note "
"though that such alternatives may also feature some minor command line "
"differences."
msgstr ""

#. type: Plain text
#: debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid "B<zstd>(1)"
msgstr ""

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "May 2021"
msgstr ""

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "zstd 1.5.0"
msgstr ""
