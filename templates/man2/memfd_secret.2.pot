# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-02-20 20:16+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "memfd_secret"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"memfd_secret - create an anonymous RAM-based file to access secret memory "
"regions"
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#include E<lt>sys/syscall.hE<gt>>      /* Definition of B<SYS_*> constants */\n"
"B<#include E<lt>unistd.hE<gt>>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "B<int syscall(SYS_memfd_secret, unsigned int >I<flags>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"I<Note>: glibc provides no wrapper for B<memfd_secret>(), necessitating the "
"use of B<syscall>(2)."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"B<memfd_secret>()  creates an anonymous RAM-based file and returns a file "
"descriptor that refers to it.  The file provides a way to create and access "
"memory regions with stronger protection than usual RAM-based files and "
"anonymous memory mappings.  Once all open references to the file are closed, "
"it is automatically released.  The initial size of the file is set to 0.  "
"Following the call, the file size should be set using B<ftruncate>(2)."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"The memory areas backing the file created with B<memfd_secret>(2)  are "
"visible only to the processes that have access to the file descriptor.  The "
"memory region is removed from the kernel page tables and only the page "
"tables of the processes holding the file descriptor map the corresponding "
"physical memory.  (Thus, the pages in the region can't be accessed by the "
"kernel itself, so that, for example, pointers to the region can't be passed "
"to system calls.)"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"The following values may be bitwise ORed in I<flags> to control the behavior "
"of B<memfd_secret>():"
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "B<FD_CLOEXEC>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Set the close-on-exec flag on the new file descriptor, which causes the "
"region to be removed from the process on B<execve>(2).  See the description "
"of the B<O_CLOEXEC> flag in B<open>(2)"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"As its return value, B<memfd_secret>()  returns a new file descriptor that "
"refers to an anonymous file.  This file descriptor is opened for both "
"reading and writing (B<O_RDWR>)  and B<O_LARGEFILE> is set for the file "
"descriptor."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"With respect to B<fork>(2)  and B<execve>(2), the usual semantics apply for "
"the file descriptor created by B<memfd_secret>().  A copy of the file "
"descriptor is inherited by the child produced by B<fork>(2)  and refers to "
"the same file.  The file descriptor is preserved across B<execve>(2), unless "
"the close-on-exec flag has been set."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"The memory region is locked into memory in the same way as with B<mlock>(2), "
"so that it will never be written into swap, and hibernation is inhibited for "
"as long as any B<memfd_secret>()  descriptions exist.  However the "
"implementation of B<memfd_secret>()  will not try to populate the whole "
"range during the B<mmap>(2)  call that attaches the region into the "
"process's address space; instead, the pages are only actually allocated as "
"they are faulted in.  The amount of memory allowed for memory mappings of "
"the file descriptor obeys the same rules as B<mlock>(2)  and cannot exceed "
"B<RLIMIT_MEMLOCK>."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"On success, B<memfd_secret>()  returns a new file descriptor.  On error, -1 "
"is returned and I<errno> is set to indicate the error."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "I<flags> included unknown bits."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "B<EMFILE>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"The per-process limit on the number of open file descriptors has been "
"reached."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"The system-wide limit on the total number of open files has been reached."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "B<ENOMEM>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "There was insufficient memory to create a new anonymous file."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "B<ENOSYS>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"B<memfd_secret>()  is not implemented on this architecture, or has not been "
"enabled on the kernel command-line with B<secretmem_enable>=1."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "The B<memfd_secret>()  system call first appeared in Linux 5.14."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "The B<memfd_secret>()  system call is Linux-specific."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"The B<memfd_secret>()  system call is designed to allow a user-space process "
"to create a range of memory that is inaccessible to anybody else - kernel "
"included.  There is no 100% guarantee that kernel won't be able to access "
"memory ranges backed by B<memfd_secret>()  in any circumstances, but "
"nevertheless, it is much harder to exfiltrate data from these regions."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "B<memfd_secret>()  provides the following protections:"
msgstr ""

#. type: IP
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "\\[bu]"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Enhanced protection (in conjunction with all the other in-kernel attack "
"prevention systems)  against ROP attacks.  Absence of any in-kernel "
"primitive for accessing memory backed by B<memfd_secret>()  means that one-"
"gadget ROP attack can't work to perform data exfiltration.  The attacker "
"would need to find enough ROP gadgets to reconstruct the missing page table "
"entries, which significantly increases difficulty of the attack, especially "
"when other protections like the kernel stack size limit and address space "
"layout randomization are in place."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Prevent cross-process user-space memory exposures.  Once a region for a "
"B<memfd_secret>()  memory mapping is allocated, the user can't accidentally "
"pass it into the kernel to be transmitted somewhere.  The memory pages in "
"this region cannot be accessed via the direct map and they are disallowed in "
"get_user_pages."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Harden against exploited kernel flaws.  In order to access memory areas "
"backed by B<memfd_secret>(), a kernel-side attack would need to either walk "
"the page tables and create new ones, or spawn a new privileged user-space "
"process to perform secrets exfiltration using B<ptrace>(2)."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"The way B<memfd_secret>()  allocates and locks the memory may impact overall "
"system performance, therefore the system call is disabled by default and "
"only available if the system administrator turned it on using \"secretmem."
"enable=y\" kernel parameter."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"To prevent potential data leaks of memory regions backed by "
"B<memfd_secret>()  from a hybernation image, hybernation is prevented when "
"there are active B<memfd_secret>()  users."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"B<fcntl>(2), B<ftruncate>(2), B<mlock>(2), B<memfd_create>(2), B<mmap>(2), "
"B<setrlimit>(2)"
msgstr ""

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-10-30"
msgstr ""

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.02"
msgstr ""

#. type: IP
#: opensuse-tumbleweed
#, no-wrap
msgid "\\(bu"
msgstr ""
