# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-02-20 20:32+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "strcpy"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "stpcpy, strcpy, strcat - copy or catenate a string"
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>string.hE<gt>>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"B<char *stpcpy(char *restrict >I<dst>B<, const char *restrict >I<src>B<);>\n"
"B<char *strcpy(char *restrict >I<dst>B<, const char *restrict >I<src>B<);>\n"
"B<char *strcat(char *restrict >I<dst>B<, const char *restrict >I<src>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "B<stpcpy>():"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    Since glibc 2.10:\n"
"        _POSIX_C_SOURCE E<gt>= 200809L\n"
"    Before glibc 2.10:\n"
"        _GNU_SOURCE\n"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "B<stpcpy>()"
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "B<strcpy>()"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"These functions copy the string pointed to by I<src>, into a string at the "
"buffer pointed to by I<dst>.  The programmer is responsible for allocating a "
"destination buffer large enough, that is, I<strlen(src) + 1>.  For the "
"difference between the two functions, see RETURN VALUE."
msgstr ""

#. type: TQ
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "B<strcat>()"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"This function catenates the string pointed to by I<src>, after the string "
"pointed to by I<dst> (overwriting its terminating null byte).  The "
"programmer is responsible for allocating a destination buffer large enough, "
"that is, I<strlen(dst) + strlen(src) + 1>."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "An implementation of these functions might be:"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"char *\n"
"stpcpy(char *restrict dst, const char *restrict src)\n"
"{\n"
"    char  *p;\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid ""
"    p = mempcpy(dst, src, strlen(src));\n"
"    *p = \\[aq]\\e0\\[aq];\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    return p;\n"
"}\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"char *\n"
"strcpy(char *restrict dst, const char *restrict src)\n"
"{\n"
"    stpcpy(dst, src);\n"
"    return dst;\n"
"}\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"char *\n"
"strcat(char *restrict dst, const char *restrict src)\n"
"{\n"
"    stpcpy(dst + strlen(dst), src);\n"
"    return dst;\n"
"}\n"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"This function returns a pointer to the terminating null byte of the copied "
"string."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "These functions return I<dst>."
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr ""

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr ""

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr ""

#. type: tbl table
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"B<stpcpy>(),\n"
"B<strcpy>(),\n"
"B<strcat>()"
msgstr ""

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr ""

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid "POSIX.1-2001, POSIX.1-2008, C99, SVr4, 4.3BSD."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "CAVEATS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "The strings I<src> and I<dst> may not overlap."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"If the destination buffer is not large enough, the behavior is undefined.  "
"See B<_FORTIFY_SOURCE> in B<feature_test_macros>(7)."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"B<strcat>()  can be very inefficient.  Read about E<.UR https://www."
"joelonsoftware.com/\\:2001/12/11/\\:back-to-basics/> Shlemiel the\\ painter "
"E<.UE .>"
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"#include E<lt>err.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>string.hE<gt>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"int\n"
"main(void)\n"
"{\n"
"    char    *p;\n"
"    char    *buf1;\n"
"    char    *buf2;\n"
"    size_t  len, maxsize;\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    maxsize = strlen(\"Hello \") + strlen(\"world\") + strlen(\"!\") + 1;\n"
"    buf1 = malloc(sizeof(*buf1) * maxsize);\n"
"    if (buf1 == NULL)\n"
"        err(EXIT_FAILURE, \"malloc()\");\n"
"    buf2 = malloc(sizeof(*buf2) * maxsize);\n"
"    if (buf2 == NULL)\n"
"        err(EXIT_FAILURE, \"malloc()\");\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    p = buf1;\n"
"    p = stpcpy(p, \"Hello \");\n"
"    p = stpcpy(p, \"world\");\n"
"    p = stpcpy(p, \"!\");\n"
"    len = p - buf1;\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    printf(\"[len = %zu]: \", len);\n"
"    puts(buf1);  // \"Hello world!\"\n"
"    free(buf1);\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    strcpy(buf2, \"Hello \");\n"
"    strcat(buf2, \"world\");\n"
"    strcat(buf2, \"!\");\n"
"    len = strlen(buf2);\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    printf(\"[len = %zu]: \", len);\n"
"    puts(buf2);  // \"Hello world!\"\n"
"    free(buf2);\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""

#. #-#-#-#-#  archlinux: strcpy.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  debian-bullseye: strcpy.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  debian-unstable: strcpy.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  fedora-38: strcpy.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  fedora-rawhide: strcpy.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  mageia-cauldron: strcpy.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  opensuse-leap-15-5: strcpy.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  opensuse-tumbleweed: strcpy.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "B<strdup>(3), B<string>(3), B<wcscpy>(3), B<string_copying>(7)"
msgstr ""

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "STRCPY"
msgstr ""

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "2019-03-06"
msgstr ""

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "GNU"
msgstr ""

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "strcpy, strncpy - copy a string"
msgstr ""

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "B<char *strcpy(char *>I<dest>B<, const char *>I<src>B<);>\n"
msgstr ""

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "B<char *strncpy(char *>I<dest>B<, const char *>I<src>B<, size_t >I<n>B<);>\n"
msgstr ""

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"The B<strcpy>()  function copies the string pointed to by I<src>, including "
"the terminating null byte (\\(aq\\e0\\(aq), to the buffer pointed to by "
"I<dest>.  The strings may not overlap, and the destination string I<dest> "
"must be large enough to receive the copy.  I<Beware of buffer overruns!> "
"(See BUGS.)"
msgstr ""

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"The B<strncpy>()  function is similar, except that at most I<n> bytes of "
"I<src> are copied.  B<Warning>: If there is no null byte among the first "
"I<n> bytes of I<src>, the string placed in I<dest> will not be null-"
"terminated."
msgstr ""

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"If the length of I<src> is less than I<n>, B<strncpy>()  writes additional "
"null bytes to I<dest> to ensure that a total of I<n> bytes are written."
msgstr ""

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "A simple implementation of B<strncpy>()  might be:"
msgstr ""

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid ""
"char *\n"
"strncpy(char *dest, const char *src, size_t n)\n"
"{\n"
"    size_t i;\n"
msgstr ""

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid ""
"    for (i = 0; i E<lt> n && src[i] != \\(aq\\e0\\(aq; i++)\n"
"        dest[i] = src[i];\n"
"    for ( ; i E<lt> n; i++)\n"
"        dest[i] = \\(aq\\e0\\(aq;\n"
msgstr ""

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid ""
"    return dest;\n"
"}\n"
msgstr ""

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"The B<strcpy>()  and B<strncpy>()  functions return a pointer to the "
"destination string I<dest>."
msgstr ""

#. type: tbl table
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid ""
"B<strcpy>(),\n"
"B<strncpy>()"
msgstr ""

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
msgid "POSIX.1-2001, POSIX.1-2008, C89, C99, SVr4, 4.3BSD."
msgstr ""

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"Some programmers consider B<strncpy>()  to be inefficient and error prone.  "
"If the programmer knows (i.e., includes code to test!)  that the size of "
"I<dest> is greater than the length of I<src>, then B<strcpy>()  can be used."
msgstr ""

#. type: Plain text
#: debian-bullseye
msgid ""
"One valid (and intended) use of B<strncpy>()  is to copy a C string to a "
"fixed-length buffer while ensuring both that the buffer is not overflowed "
"and that unused bytes in the destination buffer are zeroed out (perhaps to "
"prevent information leaks if the buffer is to be written to media or "
"transmitted to another process via an interprocess communication technique)."
msgstr ""

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"If there is no terminating null byte in the first I<n> bytes of I<src>, "
"B<strncpy>()  produces an unterminated string in I<dest>.  If I<buf> has "
"length I<buflen>, you can force termination using something like the "
"following:"
msgstr ""

#. type: Plain text
#: debian-bullseye
#, no-wrap
msgid ""
"if (buflen E<gt> 0) {\n"
"    strncpy(buf, str, buflen - 1);\n"
"    buf[buflen - 1]= \\(aq\\e0\\(aq;\n"
"}\n"
msgstr ""

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"(Of course, the above technique ignores the fact that, if I<src> contains "
"more than I<buflen\\ -\\ 1> bytes, information is lost in the copying to "
"I<dest>.)"
msgstr ""

#. type: SS
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "strlcpy()"
msgstr ""

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"Some systems (the BSDs, Solaris, and others) provide the following function:"
msgstr ""

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "    size_t strlcpy(char *dest, const char *src, size_t size);\n"
msgstr ""

#.  http://static.usenix.org/event/usenix99/full_papers/millert/millert_html/index.html
#.      "strlcpy and strlcat - consistent, safe, string copy and concatenation"
#.      1999 USENIX Annual Technical Conference
#.  https://lwn.net/Articles/506530/
#. type: Plain text
#: debian-bullseye
msgid ""
"This function is similar to B<strncpy>(), but it copies at most I<size-1> "
"bytes to I<dest>, always adds a terminating null byte, and does not pad the "
"destination with (further) null bytes.  This function fixes some of the "
"problems of B<strcpy>()  and B<strncpy>(), but the caller must still handle "
"the possibility of data loss if I<size> is too small.  The return value of "
"the function is the length of I<src>, which allows truncation to be easily "
"detected: if the return value is greater than or equal to I<size>, "
"truncation occurred.  If loss of data matters, the caller I<must> either "
"check the arguments before the call, or test the function return value.  "
"B<strlcpy>()  is not present in glibc and is not standardized by POSIX, but "
"is available on Linux via the I<libbsd> library."
msgstr ""

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "BUGS"
msgstr ""

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"If the destination string of a B<strcpy>()  is not large enough, then "
"anything might happen.  Overflowing fixed-length string buffers is a "
"favorite cracker technique for taking complete control of the machine.  Any "
"time a program reads or copies data into a buffer, the program first needs "
"to check that there's enough space.  This may be unnecessary if you can show "
"that overflow is impossible, but be careful: programs can get changed over "
"time, in ways that may make the impossible possible."
msgstr ""

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"B<bcopy>(3), B<memccpy>(3), B<memcpy>(3), B<memmove>(3), B<stpcpy>(3), "
"B<stpncpy>(3), B<strdup>(3), B<string>(3), B<wcscpy>(3), B<wcsncpy>(3)"
msgstr ""

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr ""

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"One valid (and intended) use of B<strncpy>()  is to copy a C string to a "
"fixed-length buffer while ensuring both that the buffer is not overflowed "
"and that unused bytes in the target buffer are zeroed out (perhaps to "
"prevent information leaks if the buffer is to be written to media or "
"transmitted to another process via an interprocess communication technique)."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid ""
"strncpy(buf, str, buflen - 1);\n"
"if (buflen E<gt> 0)\n"
"    buf[buflen - 1]= \\(aq\\e0\\(aq;\n"
msgstr ""

#.  http://static.usenix.org/event/usenix99/full_papers/millert/millert_html/index.html
#.      "strlcpy and strlcat - consistent, safe, string copy and concatenation"
#.      1999 USENIX Annual Technical Conference
#.  https://lwn.net/Articles/506530/
#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This function is similar to B<strncpy>(), but it copies at most I<size-1> "
"bytes to I<dest>, always adds a terminating null byte, and does not pad the "
"target with (further) null bytes.  This function fixes some of the problems "
"of B<strcpy>()  and B<strncpy>(), but the caller must still handle the "
"possibility of data loss if I<size> is too small.  The return value of the "
"function is the length of I<src>, which allows truncation to be easily "
"detected: if the return value is greater than or equal to I<size>, "
"truncation occurred.  If loss of data matters, the caller I<must> either "
"check the arguments before the call, or test the function return value.  "
"B<strlcpy>()  is not present in glibc and is not standardized by POSIX, but "
"is available on Linux via the I<libbsd> library."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-12-21"
msgstr ""

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.02"
msgstr ""

#. type: Plain text
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    p = mempcpy(dst, src, strlen(src));\n"
"    *p = \\(aq\\e0\\(aq;\n"
msgstr ""
