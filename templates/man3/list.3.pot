# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-02-20 20:14+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "LIST"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-10-30"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. LIST_FOREACH_FROM,
#. LIST_FOREACH_SAFE,
#. LIST_FOREACH_FROM_SAFE,
#. LIST_PREV,
#. LIST_SWAP
#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"LIST_EMPTY, LIST_ENTRY, LIST_FIRST, LIST_FOREACH, LIST_HEAD, "
"LIST_HEAD_INITIALIZER, LIST_INIT, LIST_INSERT_AFTER, LIST_INSERT_BEFORE, "
"LIST_INSERT_HEAD, LIST_NEXT, LIST_REMOVE - implementation of a doubly linked "
"list"
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>sys/queue.hE<gt>>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "B<LIST_ENTRY(TYPE);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"B<LIST_HEAD(HEADNAME, TYPE);>\n"
"B<LIST_HEAD LIST_HEAD_INITIALIZER(LIST_HEAD >I<head>B<);>\n"
"B<void LIST_INIT(LIST_HEAD *>I<head>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "B<int LIST_EMPTY(LIST_HEAD *>I<head>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"B<void LIST_INSERT_HEAD(LIST_HEAD *>I<head>B<,>\n"
"B<                        struct TYPE *>I<elm>B<, LIST_ENTRY >I<NAME>B<);>\n"
"B<void LIST_INSERT_BEFORE(struct TYPE *>I<listelm>B<,>\n"
"B<                        struct TYPE *>I<elm>B<, LIST_ENTRY >I<NAME>B<);>\n"
"B<void LIST_INSERT_AFTER(struct TYPE *>I<listelm>B<,>\n"
"B<                        struct TYPE *>I<elm>B<, LIST_ENTRY >I<NAME>B<);>\n"
msgstr ""

#.  .BI "struct TYPE *LIST_PREV(struct TYPE *" elm ", LIST_HEAD *" head ,
#.  .BI "                        struct TYPE, LIST_ENTRY " NAME );
#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"B<struct TYPE *LIST_FIRST(LIST_HEAD *>I<head>B<);>\n"
"B<struct TYPE *LIST_NEXT(struct TYPE *>I<elm>B<, LIST_ENTRY >I<NAME>B<);>\n"
msgstr ""

#.  .BI "LIST_FOREACH_FROM(struct TYPE *" var ", LIST_HEAD *" head ", LIST_ENTRY " NAME );
#.  .PP
#.  .BI "LIST_FOREACH_SAFE(struct TYPE *" var ", LIST_HEAD *" head ,
#.  .BI "                        LIST_ENTRY " NAME ", struct TYPE *" temp_var );
#.  .BI "LIST_FOREACH_FROM_SAFE(struct TYPE *" var ", LIST_HEAD *" head ,
#.  .BI "                        LIST_ENTRY " NAME ", struct TYPE *" temp_var );
#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "B<LIST_FOREACH(struct TYPE *>I<var>B<, LIST_HEAD *>I<head>B<, LIST_ENTRY >I<NAME>B<);>\n"
msgstr ""

#.  .PP
#.  .BI "void LIST_SWAP(LIST_HEAD *" head1 ", LIST_HEAD *" head2 ,
#.  .BI "                        struct TYPE, LIST_ENTRY " NAME );
#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "B<void LIST_REMOVE(struct TYPE *>I<elm>B<, LIST_ENTRY >I<NAME>B<);>\n"
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "These macros define and operate on doubly linked lists."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"In the macro definitions, I<TYPE> is the name of a user-defined structure, "
"that must contain a field of type I<LIST_ENTRY>, named I<NAME>.  The "
"argument I<HEADNAME> is the name of a user-defined structure that must be "
"declared using the macro B<LIST_HEAD>()."
msgstr ""

#. type: SS
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "Creation"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"A list is headed by a structure defined by the B<LIST_HEAD>()  macro.  This "
"structure contains a single pointer to the first element on the list.  The "
"elements are doubly linked so that an arbitrary element can be removed "
"without traversing the list.  New elements can be added to the list after an "
"existing element, before an existing element, or at the head of the list.  A "
"I<LIST_HEAD> structure is declared as follows:"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "LIST_HEAD(HEADNAME, TYPE) head;\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"where I<struct HEADNAME> is the structure to be defined, and I<struct TYPE> "
"is the type of the elements to be linked into the list.  A pointer to the "
"head of the list can later be declared as:"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "struct HEADNAME *headp;\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "(The names I<head> and I<headp> are user selectable.)"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"B<LIST_ENTRY>()  declares a structure that connects the elements in the list."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"B<LIST_HEAD_INITIALIZER>()  evaluates to an initializer for the list I<head>."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "B<LIST_INIT>()  initializes the list referenced by I<head>."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"B<LIST_EMPTY>()  evaluates to true if there are no elements in the list."
msgstr ""

#. type: SS
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "Insertion"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"B<LIST_INSERT_HEAD>()  inserts the new element I<elm> at the head of the "
"list."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"B<LIST_INSERT_BEFORE>()  inserts the new element I<elm> before the element "
"I<listelm>."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"B<LIST_INSERT_AFTER>()  inserts the new element I<elm> after the element "
"I<listelm>."
msgstr ""

#. type: SS
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "Traversal"
msgstr ""

#.  .PP
#.  .BR LIST_PREV ()
#.  returns the previous element in the list, or NULL if this is the first.
#.  List
#.  .I head
#.  must contain element
#.  .IR elm .
#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"B<LIST_FIRST>()  returns the first element in the list, or NULL if the list "
"is empty."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"B<LIST_NEXT>()  returns the next element in the list, or NULL if this is the "
"last."
msgstr ""

#.  .PP
#.  .BR LIST_FOREACH_FROM ()
#.  behaves identically to
#.  .BR LIST_FOREACH ()
#.  when
#.  .I var
#.  is NULL, else it treats
#.  .I var
#.  as a previously found LIST element and begins the loop at
#.  .I var
#.  instead of the first element in the LIST referenced by
#.  .IR head .
#.  .PP
#.  .BR LIST_FOREACH_SAFE ()
#.  traverses the list referenced by
#.  .I head
#.  in the forward direction, assigning each element in turn to
#.  .IR var .
#.  However, unlike
#.  .BR LIST_FOREACH ()
#.  here it is permitted to both remove
#.  .I var
#.  as well as free it from within the loop safely without interfering with the
#.  traversal.
#.  .PP
#.  .BR LIST_FOREACH_FROM_SAFE ()
#.  behaves identically to
#.  .BR LIST_FOREACH_SAFE ()
#.  when
#.  .I var
#.  is NULL, else it treats
#.  .I var
#.  as a previously found LIST element and begins the loop at
#.  .I var
#.  instead of the first element in the LIST referenced by
#.  .IR head .
#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"B<LIST_FOREACH>()  traverses the list referenced by I<head> in the forward "
"direction, assigning each element in turn to I<var>."
msgstr ""

#. type: SS
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "Removal"
msgstr ""

#.  .SS Other features
#.  .BR LIST_SWAP ()
#.  swaps the contents of
#.  .I head1
#.  and
#.  .IR head2 .
#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "B<LIST_REMOVE>()  removes the element I<elm> from the list."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"B<LIST_EMPTY>()  returns nonzero if the list is empty, and zero if the list "
"contains at least one entry."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"B<LIST_FIRST>(), and B<LIST_NEXT>()  return a pointer to the first or next "
"I<TYPE> structure, respectively."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"B<LIST_HEAD_INITIALIZER>()  returns an initializer that can be assigned to "
"the list I<head>."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Not in POSIX.1, POSIX.1-2001, or POSIX.1-2008.  Present on the BSDs (LIST "
"macros first appeared in 4.4BSD)."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"B<LIST_FOREACH>()  doesn't allow I<var> to be removed or freed within the "
"loop, as it would interfere with the traversal.  B<LIST_FOREACH_SAFE>(), "
"which is present on the BSDs but is not present in glibc, fixes this "
"limitation by allowing I<var> to safely be removed from the list and freed "
"from within the loop without interfering with the traversal."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"#include E<lt>stddef.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>sys/queue.hE<gt>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"struct entry {\n"
"    int data;\n"
"    LIST_ENTRY(entry) entries;              /* List */\n"
"};\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "LIST_HEAD(listhead, entry);\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"int\n"
"main(void)\n"
"{\n"
"    struct entry *n1, *n2, *n3, *np;\n"
"    struct listhead head;                   /* List head */\n"
"    int i;\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "    LIST_INIT(&head);                       /* Initialize the list */\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    n1 = malloc(sizeof(struct entry));      /* Insert at the head */\n"
"    LIST_INSERT_HEAD(&head, n1, entries);\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    n2 = malloc(sizeof(struct entry));      /* Insert after */\n"
"    LIST_INSERT_AFTER(n1, n2, entries);\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    n3 = malloc(sizeof(struct entry));      /* Insert before */\n"
"    LIST_INSERT_BEFORE(n2, n3, entries);\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    i = 0;                                  /* Forward traversal */\n"
"    LIST_FOREACH(np, &head, entries)\n"
"        np-E<gt>data = i++;\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    LIST_REMOVE(n2, entries);               /* Deletion */\n"
"    free(n2);\n"
"                                            /* Forward traversal */\n"
"    LIST_FOREACH(np, &head, entries)\n"
"        printf(\"%i\\en\", np-E<gt>data);\n"
"                                            /* List deletion */\n"
"    n1 = LIST_FIRST(&head);\n"
"    while (n1 != NULL) {\n"
"        n2 = LIST_NEXT(n1, entries);\n"
"        free(n1);\n"
"        n1 = n2;\n"
"    }\n"
"    LIST_INIT(&head);\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""

#.  SRC END
#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "B<insque>(3), B<queue>(7)"
msgstr ""

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.02"
msgstr ""
