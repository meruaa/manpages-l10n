# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-02-20 20:37+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "SYSTEMD-VCONSOLE-SETUP\\&.SERVICE"
msgstr ""

#. type: TH
#: archlinux fedora-38 fedora-rawhide
#, no-wrap
msgid "systemd 253"
msgstr ""

#. type: TH
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "systemd-vconsole-setup.service"
msgstr ""

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"systemd-vconsole-setup.service, systemd-vconsole-setup - Configure the "
"virtual consoles"
msgstr ""

#. type: SH
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "systemd-vconsole-setup\\&.service"
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "B</usr/lib/systemd/systemd-vconsole-setup> [TTY]"
msgstr ""

#. type: SH
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"systemd-vconsole-setup sets up and configures either all virtual consoles, "
"or \\(em if the optional I<TTY> parameter is provided \\(em a specific "
"one\\&. When the system is booting up, it\\*(Aqs called by B<systemd-"
"udevd>(8)  during VT console subsystem initialization\\&. Also, B<systemd-"
"localed.service>(8)  invokes it as needed when language or console changes "
"are made\\&. Internally, this program calls B<loadkeys>(1)  and "
"B<setfont>(8)\\&."
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"Execute B<systemctl restart systemd-vconsole-setup\\&.service> in order to "
"apply any manual changes made to /etc/vconsole\\&.conf\\&."
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"See B<vconsole.conf>(5)  for information about the configuration files and "
"kernel command line options understood by this program\\&."
msgstr ""

#. type: SH
#: archlinux fedora-38 fedora-rawhide
#, no-wrap
msgid "CREDENTIALS"
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide
msgid ""
"B<systemd-vconsole-setup> supports the service credentials logic as "
"implemented by I<LoadCredential=>/I<SetCredential=> (see B<systemd.exec>(1)  "
"for details)\\&. The following credentials are used when passed in:"
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide
msgid "I<vconsole\\&.keymap>, I<vconsole\\&.keymap_toggle>"
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide
msgid ""
"The keymap (and toggle keymap) to apply\\&. The matching options in "
"vconsole\\&.conf and on the kernel command line take precedence over these "
"credentials\\&."
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide
msgid ""
"Note the relationship to the I<firstboot\\&.keymap> credential understood by "
"B<systemd-firstboot.service>(8): both ultimately affect the same setting, "
"but I<firstboot\\&.keymap> is written into /etc/vconsole\\&.conf on first "
"boot (if not already configured), and then read from there by B<systemd-"
"vconsole-setup>, while I<vconsole\\&.keymap> is read on every boot, and is "
"not persisted to disk (but any configuration in vconsole\\&.conf will take "
"precedence if present)\\&."
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide
msgid ""
"I<vconsole\\&.font>, I<vconsole\\&.font_map>, I<vconsole\\&.font_unimap>"
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide
msgid ""
"The console font settings to apply\\&. The matching options in vconsole\\&."
"conf and on the kernel command line take precedence over these "
"credentials\\&."
msgstr ""

#. type: SH
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"B<systemd>(1), B<vconsole.conf>(5), B<loadkeys>(1), B<setfont>(8), B<systemd-"
"localed.service>(8)"
msgstr ""

#. type: TH
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "systemd 252"
msgstr ""

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "systemd 249"
msgstr ""
