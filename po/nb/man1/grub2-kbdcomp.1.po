# Norwegian bokmål translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.11.0\n"
"POT-Creation-Date: 2023-02-20 20:09+0100\n"
"PO-Revision-Date: 2021-09-03 20:09+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Norwegian bokmål <>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "GRUB-KBDCOMP"
msgstr "GRUB-KBDCOMP"

#. type: TH
#: fedora-38 fedora-rawhide opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "February 2017"
msgid "February 2023"
msgstr "Februar 2017"

#. type: TH
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "grub-kbdcomp ()"
msgstr "grub-kbdcomp ()"

#. type: TH
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Brukerkommandoer"

#. type: SH
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAVN"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, fuzzy
#| msgid "Make GRUB keyboard layout file."
msgid "grub-kbdcomp - generate a GRUB keyboard layout file"
msgstr "Lag tastatur-utformingsfil for GRUB."

#. type: SH
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "OVERSIKT"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "B<grub-kbdcomp> I<\\,-o OUTPUT CKBMAP_ARGUMENTS\\/>..."
msgstr "B<grub-kbdcomp> I<\\,-o UTDATA CKBMAP-ARGUMENTER\\/>..."

#. type: SH
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESKRIVELSE"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"grub-kbdcomp processes a X keyboard layout description in B<keymaps>(5)  "
"format into a format that can be used by GRUB's B<keymap> command."
msgstr ""

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "Make GRUB keyboard layout file."
msgstr "Lag tastatur-utformingsfil for GRUB."

#. type: TP
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "print this message and exit"
msgstr "skriv ut denne meldinga og avslutt"

#. type: TP
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "print the version information and exit"
msgstr "skriv ut versjon og avslutt"

#. type: TP
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-o>, B<--output>=I<\\,FILE\\/>"
msgstr "B<-o>, B<--output>=I<\\,FIL\\/>"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "save output in FILE [required]"
msgstr "lagre utdata i valgt FIL (påkrevet)"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "grub-kbdcomp generates a keyboard layout for GRUB using ckbcomp"
msgstr ""
"B<grub-kbdcomp> lager en tastaturutforming for GRUB ved hjelp av B<ckbcomp>."

#. type: SH
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "RAPPORTERING AV FEIL"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "Report bugs to E<lt>bug-grub@gnu.orgE<gt>."
msgstr "Rapporter feil til E<lt>bug-grub@gnu.orgE<gt>."

#. type: SH
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SE OGSÅ"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "B<grub-mklayout>(8)"
msgstr "B<grub-mklayout>(8)"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"The full documentation for B<grub-kbdcomp> is maintained as a Texinfo "
"manual.  If the B<info> and B<grub-kbdcomp> programs are properly installed "
"at your site, the command"
msgstr ""
"Den fullstendige dokumentasjonen for B<grub-kbdcomp> opprettholdes som en "
"Texinfo manual. Dersom B<info> og B<grub-kbdcomp> programmene er riktig "
"installert på ditt sted burde kommandoen"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "B<info grub-kbdcomp>"
msgstr "B<info grub-kbdcomp>"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "should give you access to the complete manual."
msgstr "gi deg tilgang til hele manualen."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "October 2022"
msgstr "Oktober 2022"
