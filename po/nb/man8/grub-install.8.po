# Norwegian bokmål translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.11.0\n"
"POT-Creation-Date: 2023-02-15 18:53+0100\n"
"PO-Revision-Date: 2021-09-03 20:09+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Norwegian bokmål <>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "GRUB-INSTALL"
msgstr "GRUB-INSTALL"

#. type: TH
#: archlinux
#, fuzzy, no-wrap
#| msgid "February 2017"
msgid "February 2023"
msgstr "Februar 2017"

#. type: TH
#: archlinux
#, fuzzy, no-wrap
#| msgid "GRUB 2:2.06.r403.g7259d55ff-1"
msgid "GRUB 2:2.06.r456.g65bc45963-1"
msgstr "GRUB 2:2.06.r403.g7259d55ff-1"

#. type: TH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "System Administration Utilities"
msgstr "Systemadministrasjonsverktøy"

#. type: SH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "NAME"
msgstr "NAVN"

#. type: Plain text
#: archlinux debian-bullseye
msgid "grub-install - install GRUB to a device"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr "OVERSIKT"

#. type: Plain text
#: archlinux debian-bullseye
msgid ""
"B<grub-install> [I<\\,OPTION\\/>...] [I<\\,OPTION\\/>] [I<\\,"
"INSTALL_DEVICE\\/>]"
msgstr ""
"B<grub-install> [I<\\,VALG\\/>...] [I<\\,VALG\\/>] [I<\\,"
"INSTALLASJONSENHET\\/>]"

#. type: SH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESKRIVELSE"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "Install GRUB on your drive."
msgstr "Installer GRUB på lagringsenheten."

#. type: TP
#: archlinux debian-bullseye
#, no-wrap
msgid "B<--compress>=I<\\,no\\/>|xz|gz|lzo"
msgstr "B<--compress>=I<\\,no\\/>|xz|gz|lzo"

#. type: Plain text
#: archlinux debian-bullseye
msgid "compress GRUB files [optional]"
msgstr "komprimer GRUB-filer (valgfritt)"

#. type: TP
#: archlinux debian-bullseye
#, no-wrap
msgid "B<--disable-shim-lock>"
msgstr "B<--disable-shim-lock>"

#. type: Plain text
#: archlinux debian-bullseye
msgid "disable shim_lock verifier"
msgstr ""

#. type: TP
#: archlinux debian-bullseye
#, no-wrap
msgid "B<--dtb>=I<\\,FILE\\/>"
msgstr "B<--dtb>=I<\\,FIL\\/>"

#. type: Plain text
#: archlinux debian-bullseye
msgid "embed a specific DTB"
msgstr "bygg inn spesifikk DTB"

#. type: TP
#: archlinux debian-bullseye
#, no-wrap
msgid "B<-d>, B<--directory>=I<\\,DIR\\/>"
msgstr "B<-d>, B<--directory>=I<\\,MAPPE\\/>"

#. type: Plain text
#: archlinux debian-bullseye
msgid ""
"use images and modules under DIR [default=/usr/lib/grub/E<lt>platformE<gt>]"
msgstr ""
"use images and modules under DIR [default=/usr/lib/grub/E<lt>platformE<gt>]"

#. type: TP
#: archlinux debian-bullseye
#, no-wrap
msgid "B<--fonts>=I<\\,FONTS\\/>"
msgstr "B<--fonts>=I<\\,SKRIFTTYPER\\/>"

#. type: Plain text
#: archlinux debian-bullseye
msgid "install FONTS [default=unicode]"
msgstr "installer SKRIFTTYPER (standard=unicode)"

#. type: TP
#: archlinux debian-bullseye
#, no-wrap
msgid "B<--install-modules>=I<\\,MODULES\\/>"
msgstr "B<--install-modules>=I<\\,MODULER\\/>"

#. type: Plain text
#: archlinux debian-bullseye
msgid "install only MODULES and their dependencies [default=all]"
msgstr ""
"bare installer valgte MODULER og ting de er avhengige av (standard=alle)"

#. type: TP
#: archlinux debian-bullseye
#, no-wrap
msgid "B<-k>, B<--pubkey>=I<\\,FILE\\/>"
msgstr "B<-k>, B<--pubkey>=I<\\,FIL\\/>"

#. type: Plain text
#: archlinux debian-bullseye
msgid "embed FILE as public key for signature checking"
msgstr "bruk valgt FIL som offentlig nøkkel for signaturkontroll"

#. type: TP
#: archlinux debian-bullseye
#, no-wrap
msgid "B<--locale-directory>=I<\\,DIR\\/> use translations under DIR"
msgstr "B<--locale-directory>=I<\\,DIR\\/> bruk oversettelser i valgt mappe (DIR)"

#. type: Plain text
#: archlinux debian-bullseye
msgid "[default=/usr/share/locale]"
msgstr "[standard=/usr/share/locale]"

#. type: TP
#: archlinux debian-bullseye
#, no-wrap
msgid "B<--locales>=I<\\,LOCALES\\/>"
msgstr "B<--locales>=I<\\,REGIONER\\/>"

#. type: Plain text
#: archlinux debian-bullseye
msgid "install only LOCALES [default=all]"
msgstr "bare installer valgte REGIONER (standard=alle)"

#. type: TP
#: archlinux debian-bullseye
#, no-wrap
msgid "B<--modules>=I<\\,MODULES\\/>"
msgstr "B<--modules>=I<\\,MODULER\\/>"

#. type: Plain text
#: archlinux debian-bullseye
msgid "pre-load specified modules MODULES"
msgstr "last inn valgte MODULER på forhånd"

#. type: TP
#: archlinux debian-bullseye
#, no-wrap
msgid "B<--sbat>=I<\\,FILE\\/>"
msgstr "B<--sbat>=I<\\,FIL\\/>"

#. type: Plain text
#: archlinux debian-bullseye
msgid "SBAT metadata"
msgstr ""

#. type: TP
#: archlinux debian-bullseye
#, no-wrap
msgid "B<--themes>=I<\\,THEMES\\/>"
msgstr "B<--themes>=I<\\,TEMAER\\/>"

#. type: Plain text
#: archlinux debian-bullseye
msgid "install THEMES [default=starfield]"
msgstr "installer TEMAER (standard=starfield)"

#. type: TP
#: archlinux debian-bullseye
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: archlinux debian-bullseye
msgid "print verbose messages."
msgstr "skriv ut detaljerte meldinger"

#. type: TP
#: archlinux debian-bullseye
#, no-wrap
msgid "B<--allow-floppy>"
msgstr "B<--allow-floppy>"

#. type: Plain text
#: archlinux debian-bullseye
msgid ""
"make the drive also bootable as floppy (default for fdX devices). May break "
"on some BIOSes."
msgstr ""
"la enheten starte opp i diskettmodus (standard for fdX-enheter). Dette "
"fungerer ikke med alle BIOS-er."

#. type: TP
#: archlinux debian-bullseye
#, no-wrap
msgid "B<--boot-directory>=I<\\,DIR\\/>"
msgstr "B<--boot-directory>=I<\\,MAPPE\\/>"

#. type: Plain text
#: archlinux
msgid ""
"install GRUB images under the directory DIR/grub instead of the I<\\,/boot/"
"grub\\/> directory"
msgstr ""
"installer GRUB-bilder under mappa  MAPPE/grub i stedet for mappa I<\\,/boot/"
"grub\\/>"

#. type: TP
#: archlinux debian-bullseye
#, no-wrap
msgid "B<--bootloader-id>=I<\\,ID\\/>"
msgstr "B<--bootloader-id>=I<\\,ID\\/>"

#. type: Plain text
#: archlinux debian-bullseye
msgid "the ID of bootloader. This option is only available on EFI and Macs."
msgstr ""
"oppstartslasterens ID. Dette valget er bare tilgjengelig på EFI og Mac."

#. type: TP
#: archlinux debian-bullseye
#, no-wrap
msgid "B<--core-compress>=I<\\,xz\\/>|none|auto"
msgstr "B<--core-compress>=I<\\,xz\\/>|none|auto"

#. type: Plain text
#: archlinux debian-bullseye
msgid "choose the compression to use for core image"
msgstr "velg komprimeringsmetoden som skal brukes på kjernebildet"

#. type: TP
#: archlinux debian-bullseye
#, no-wrap
msgid "B<--disk-module>=I<\\,MODULE\\/>"
msgstr "B<--disk-module>=I<\\,MODUL\\/>"

#. type: Plain text
#: archlinux debian-bullseye
msgid ""
"disk module to use (biosdisk or native). This option is only available on "
"BIOS target."
msgstr ""
"diskmoduler som skal brukes (biosdisk eller innebygd). Dette valget er bare "
"tilgjengelig på BIOS-mål."

#. type: TP
#: archlinux debian-bullseye
#, no-wrap
msgid "B<--efi-directory>=I<\\,DIR\\/>"
msgstr "B<--efi-directory>=I<\\,MAPPE\\/>"

#. type: Plain text
#: archlinux debian-bullseye
msgid "use DIR as the EFI System Partition root."
msgstr "bruk valgt MAPPE som rot av EFI-systempartisjon."

#. type: TP
#: archlinux debian-bullseye
#, no-wrap
msgid "B<--force>"
msgstr "B<--force>"

#. type: Plain text
#: archlinux debian-bullseye
msgid "install even if problems are detected"
msgstr "ikke stans installasjonen selv om problemer oppstår"

#. type: TP
#: archlinux debian-bullseye
#, no-wrap
msgid "B<--force-file-id>"
msgstr "B<--force-file-id>"

#. type: Plain text
#: archlinux debian-bullseye
msgid "use identifier file even if UUID is available"
msgstr "bruk identifikasjonsfil selv hvis UUID er tilgjengelig"

#. type: TP
#: archlinux debian-bullseye
#, no-wrap
msgid "B<--label-bgcolor>=I<\\,COLOR\\/>"
msgstr "B<--label-bgcolor>=I<\\,FARGE\\/>"

#. type: Plain text
#: archlinux debian-bullseye
msgid "use COLOR for label background"
msgstr "bruk valgt FARGE på etikett-forgrunnen"

#. type: TP
#: archlinux debian-bullseye
#, no-wrap
msgid "B<--label-color>=I<\\,COLOR\\/>"
msgstr "B<--label-color>=I<\\,FARGE\\/>"

#. type: Plain text
#: archlinux debian-bullseye
msgid "use COLOR for label"
msgstr "bruk valgt FARGE på etikett"

#. type: TP
#: archlinux debian-bullseye
#, no-wrap
msgid "B<--label-font>=I<\\,FILE\\/>"
msgstr "B<--label-font>=I<\\,FIL\\/>"

#. type: Plain text
#: archlinux debian-bullseye
msgid "use FILE as font for label"
msgstr "bruk valgt FIL som skrifttype på etikett"

#. type: Plain text
#: archlinux debian-bullseye
msgid "B<--macppc-directory>=I<\\,DIR\\/> use DIR for PPC MAC install."
msgstr ""
"B<--macppc-directory>=I<\\,DIR\\/> bruk valgt MAPPE til PPC MAC-installasjon."

#. type: TP
#: archlinux debian-bullseye
#, no-wrap
msgid "B<--no-bootsector>"
msgstr "B<--no-bootsector>"

#. type: Plain text
#: archlinux debian-bullseye
msgid "do not install bootsector"
msgstr "ikke installer oppstartssektor"

#. type: TP
#: archlinux debian-bullseye
#, no-wrap
msgid "B<--no-nvram>"
msgstr "B<--no-nvram>"

#. type: Plain text
#: archlinux debian-bullseye
msgid ""
"don't update the `boot-device'/`Boot*' NVRAM variables. This option is only "
"available on EFI and IEEE1275 targets."
msgstr ""
"ikke oppdater NVRAM-variablene «boot-device» og «Boot*». Dette valget er "
"bare tilgjengelig på EFI- og IEEE1275-mål."

#. type: TP
#: archlinux debian-bullseye
#, no-wrap
msgid "B<--no-rs-codes>"
msgstr "B<--no-rs-codes>"

#. type: Plain text
#: archlinux debian-bullseye
msgid ""
"Do not apply any reed-solomon codes when embedding core.img. This option is "
"only available on x86 BIOS targets."
msgstr ""
"Ikke bruk reed-solomon-koder ved innbygging av core.img. Dette valget er "
"bare tilgjengelig på x86-BIOS-mål."

#. type: TP
#: archlinux debian-bullseye
#, no-wrap
msgid "B<--product-version>=I<\\,STRING\\/>"
msgstr "B<--product-version>=I<\\,STReNG\\/>"

#. type: Plain text
#: archlinux debian-bullseye
msgid "use STRING as product version"
msgstr "bruk STRENG som produktversjon"

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "B<--recheck>"
msgstr "B<--recheck>"

#. type: Plain text
#: archlinux debian-bullseye
msgid "delete device map if it already exists"
msgstr "slett enhetskart hvis det finnes allerede"

#. type: TP
#: archlinux debian-bullseye
#, no-wrap
msgid "B<--removable>"
msgstr "B<--removable>"

#. type: Plain text
#: archlinux debian-bullseye
msgid ""
"the installation device is removable. This option is only available on EFI."
msgstr ""
"installasjonsenheten kan fjernes. Dette valget er bare tilgjengelig for EFI."

#. type: TP
#: archlinux debian-bullseye
#, no-wrap
msgid "B<-s>, B<--skip-fs-probe>"
msgstr "B<-s>, B<--skip-fs-probe>"

#. type: Plain text
#: archlinux debian-bullseye
msgid "do not probe for filesystems in DEVICE"
msgstr "ikke undersøk filsystemer på valgt ENHET"

#. type: TP
#: archlinux debian-bullseye
#, no-wrap
msgid "B<--target>=I<\\,TARGET\\/>"
msgstr "B<--target>=I<\\,MÅL\\/>"

#. type: Plain text
#: archlinux
#, fuzzy
#| msgid ""
#| "install GRUB for TARGET platform [default=i386-pc]; available targets: "
#| "arm-coreboot, arm-efi, arm-uboot, arm64-efi, i386-coreboot, i386-efi, "
#| "i386-ieee1275, i386-multiboot, i386-pc, i386-qemu, i386-xen, i386-"
#| "xen_pvh, ia64-efi, mips-arc, mips-qemu_mips, mipsel-arc, mipsel-loongson, "
#| "mipsel-qemu_mips, powerpc-ieee1275, riscv32-efi, riscv64-efi, sparc64-"
#| "ieee1275, x86_64-efi, x86_64-xen"
msgid ""
"install GRUB for TARGET platform [default=x86_64-efi]; available targets: "
"arm-coreboot, arm-efi, arm-uboot, arm64-efi, i386-coreboot, i386-efi, i386-"
"ieee1275, i386-multiboot, i386-pc, i386-qemu, i386-xen, i386-xen_pvh, ia64-"
"efi, mips-arc, mips-qemu_mips, mipsel-arc, mipsel-loongson, mipsel-"
"qemu_mips, powerpc-ieee1275, riscv32-efi, riscv64-efi, sparc64-ieee1275, "
"x86_64-efi, x86_64-xen"
msgstr ""
"installer GRUB på valgt MÅLplattform (standard=i386-pc). Tilgjengelige mål: "
"arm-coreboot, arm-efi, arm-uboot, arm64-efi, i386-coreboot, i386-efi, i386-"
"ieee1275, i386-multiboot, i386-pc, i386-qemu, i386-xen, i386-xen_pvh, ia64-"
"efi, mips-arc, mips-qemu_mips, mipsel-arc, mipsel-loongson, mipsel-"
"qemu_mips, powerpc-ieee1275, riscv32-efi, riscv64-efi, sparc64-ieee1275, "
"x86_64-efi, x86_64-xen"

#. type: TP
#: archlinux debian-bullseye
#, no-wrap
msgid "-?, B<--help>"
msgstr "-?, B<--help>"

#. type: Plain text
#: archlinux debian-bullseye
msgid "give this help list"
msgstr "vis denne hjelpelista"

#. type: TP
#: archlinux debian-bullseye
#, no-wrap
msgid "B<--usage>"
msgstr "B<--usage>"

#. type: Plain text
#: archlinux debian-bullseye
msgid "give a short usage message"
msgstr "vis en kortfattet bruksanvisning"

#. type: TP
#: archlinux debian-bullseye
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: archlinux debian-bullseye
msgid "print program version"
msgstr "skriv ut programversjon"

#. type: Plain text
#: archlinux debian-bullseye
msgid ""
"Mandatory or optional arguments to long options are also mandatory or "
"optional for any corresponding short options."
msgstr ""
"Valg er enten obligatoriske både for fullstendige valg og tilsvarende "
"forkortede valg."

#. type: Plain text
#: archlinux
msgid ""
"INSTALL_DEVICE must be system device filename.  grub-install copies GRUB "
"images into I<\\,/boot/grub\\/>.  On some platforms, it may also install "
"GRUB into the boot sector."
msgstr ""
"INSTALLASJONSENHETen må være et enhetsnavn fra systemet. Bruk B<grub-"
"install>(8) for å kopiere GRUB-bilder til I<\\,/boot/grub\\/>. Dette "
"installerer også GRUB i oppstartssektoren på enkelte plattformer."

#. type: SH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "REPORTING BUGS"
msgstr "RAPPORTERING AV FEIL"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "Report bugs to E<lt>bug-grub@gnu.orgE<gt>."
msgstr "Rapporter feil til E<lt>bug-grub@gnu.orgE<gt>."

#. type: SH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr "SE OGSÅ"

#. type: Plain text
#: archlinux debian-bullseye
msgid "B<grub-mkconfig>(8), B<grub-mkimage>(1), B<grub-mkrescue>(1)"
msgstr "B<grub-mkconfig>(8), B<grub-mkimage>(1), B<grub-mkrescue>(1)"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid ""
"The full documentation for B<grub-install> is maintained as a Texinfo "
"manual.  If the B<info> and B<grub-install> programs are properly installed "
"at your site, the command"
msgstr ""
"Den fullstendige dokumentasjonen for B<grub-install> opprettholdes som en "
"Texinfo manual. Dersom B<info> og B<grub-install> programmene er riktig "
"installert på ditt sted burde kommandoen"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "B<info grub-install>"
msgstr "B<info grub-install>"

#. type: Plain text
#: archlinux debian-bullseye
msgid "should give you access to the complete manual."
msgstr "gi deg tilgang til hele manualen."

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "November 2022"
msgstr "November 2022"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "GRUB 2.06-3~deb11u5"
msgstr "GRUB 2.06-3~deb11u5"

#. type: Plain text
#: debian-bullseye
msgid ""
"install GRUB images under the directory DIR/grub instead of the boot/grub "
"directory"
msgstr ""
"installer GRUB-bilder under mappa  DIR/grub i stedet for mappa boot/grub"

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<--force-extra-removable>"
msgstr "B<--force-extra-removable>"

#. type: Plain text
#: debian-bullseye
#, fuzzy
#| msgid ""
#| "the installation device is removable. This option is only available on "
#| "EFI."
msgid ""
"force installation to the removable media path also. This option is only "
"available on EFI."
msgstr ""
"installasjonsenheten kan fjernes. Dette valget er bare tilgjengelig for EFI."

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<--no-uefi-secure-boot>"
msgstr "B<--no-uefi-secure-boot>"

#. type: Plain text
#: debian-bullseye
msgid ""
"do not install an image usable with UEFI Secure Boot, even if the system was "
"currently started using it. This option is only available on EFI."
msgstr ""

#. type: Plain text
#: debian-bullseye
msgid ""
"install GRUB for TARGET platform [default=i386-pc]; available targets: arm-"
"coreboot, arm-efi, arm-uboot, arm64-efi, i386-coreboot, i386-efi, i386-"
"ieee1275, i386-multiboot, i386-pc, i386-qemu, i386-xen, i386-xen_pvh, ia64-"
"efi, mips-arc, mips-qemu_mips, mipsel-arc, mipsel-loongson, mipsel-"
"qemu_mips, powerpc-ieee1275, riscv32-efi, riscv64-efi, sparc64-ieee1275, "
"x86_64-efi, x86_64-xen"
msgstr ""
"installer GRUB på valgt MÅLplattform (standard=i386-pc). Tilgjengelige mål: "
"arm-coreboot, arm-efi, arm-uboot, arm64-efi, i386-coreboot, i386-efi, i386-"
"ieee1275, i386-multiboot, i386-pc, i386-qemu, i386-xen, i386-xen_pvh, ia64-"
"efi, mips-arc, mips-qemu_mips, mipsel-arc, mipsel-loongson, mipsel-"
"qemu_mips, powerpc-ieee1275, riscv32-efi, riscv64-efi, sparc64-ieee1275, "
"x86_64-efi, x86_64-xen"

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<--uefi-secure-boot>"
msgstr "B<--uefi-secure-boot>"

#. type: Plain text
#: debian-bullseye
msgid ""
"install an image usable with UEFI Secure Boot.  This option is only "
"available on EFI and if the grub-efi-amd64-signed package is installed."
msgstr ""

#. type: Plain text
#: debian-bullseye
msgid ""
"INSTALL_DEVICE must be system device filename.  grub-install copies GRUB "
"images into boot/grub.  On some platforms, it may also install GRUB into the "
"boot sector."
msgstr ""
"INSTALLASJONSENHETen må være et enhetsnavn fra systemet. Bruk B<grub-"
"install>(8) for å kopiere GRUB-bilder til boot/grub. Dette installerer også "
"GRUB i oppstartssektoren på enkelte plattformer."

#. type: TH
#: debian-unstable
#, no-wrap
msgid "January 2022"
msgstr "Januar 2022"

#. type: TH
#: debian-unstable
#, fuzzy, no-wrap
#| msgid "B<grub-install>(1)"
msgid "grub-install (GNU GRUB 0.97)"
msgstr "B<grub-install>(1)"

#. type: Plain text
#: debian-unstable
#, fuzzy
#| msgid "Install GRUB on your drive."
msgid "grub-install - install GRUB on your drive"
msgstr "Installer GRUB på lagringsenheten."

#. type: Plain text
#: debian-unstable
#, fuzzy
#| msgid "B<grub-set-default> [I<\\,OPTION\\/>] I<\\,MENU_ENTRY\\/>"
msgid "B<grub-install> [I<\\,OPTION\\/>] I<\\,install_device\\/>"
msgstr "B<grub-set-default> [I<\\,VALG\\/>] I<\\,MENYOPPFØRING\\/>"

#. type: TP
#: debian-unstable
#, no-wrap
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: debian-unstable
msgid "print this message and exit"
msgstr "skriv ut denne meldinga og avslutt"

#. type: TP
#: debian-unstable
#, no-wrap
msgid "B<-v>, B<--version>"
msgstr "B<-v>, B<--version>"

#. type: Plain text
#: debian-unstable
msgid "print the version information and exit"
msgstr "skriv ut versjon og avslutt"

#. type: TP
#: debian-unstable
#, fuzzy, no-wrap
#| msgid "B<--boot-directory>=I<\\,DIR\\/>"
msgid "B<--root-directory>=I<\\,DIR\\/>"
msgstr "B<--boot-directory>=I<\\,MAPPE\\/>"

#. type: Plain text
#: debian-unstable
#, fuzzy
#| msgid ""
#| "install GRUB images under the directory DIR/grub instead of the boot/grub "
#| "directory"
msgid ""
"install GRUB images under the directory DIR instead of the root directory"
msgstr ""
"installer GRUB-bilder under mappa  DIR/grub i stedet for mappa boot/grub"

#. type: TP
#: debian-unstable
#, fuzzy, no-wrap
#| msgid "B<--dtb>=I<\\,FILE\\/>"
msgid "B<--grub-shell>=I<\\,FILE\\/>"
msgstr "B<--dtb>=I<\\,FIL\\/>"

#. type: Plain text
#: debian-unstable
#, fuzzy
#| msgid "use FILE as font for label"
msgid "use FILE as the grub shell"
msgstr "bruk valgt FIL som skrifttype på etikett"

#. type: TP
#: debian-unstable
#, fuzzy, no-wrap
#| msgid "B<--allow-floppy>"
msgid "B<--no-floppy>"
msgstr "B<--allow-floppy>"

#. type: Plain text
#: debian-unstable
#, fuzzy
#| msgid "do not create any files"
msgid "do not probe any floppy drive"
msgstr "Ikke opprett filer."

#. type: TP
#: debian-unstable
#, fuzzy, no-wrap
#| msgid "B<--force>"
msgid "B<--force-lba>"
msgstr "B<--force>"

#. type: Plain text
#: debian-unstable
msgid "force GRUB to use LBA mode even for a buggy BIOS"
msgstr ""

#. type: Plain text
#: debian-unstable
#, fuzzy
#| msgid "delete device map if it already exists"
msgid "probe a device map even if it already exists"
msgstr "slett enhetskart hvis det finnes allerede"

#. type: Plain text
#: debian-unstable
msgid "INSTALL_DEVICE can be a GRUB device name or a system device filename."
msgstr ""

#. type: Plain text
#: debian-unstable
msgid ""
"grub-install copies GRUB images into the DIR/boot directory specfied by B<--"
"root-directory>, and uses the grub shell to install grub into the boot "
"sector."
msgstr ""

#. type: Plain text
#: debian-unstable
#, fuzzy
#| msgid "B<grub-reboot>(8), B<grub-editenv>(1)"
msgid "B<grub>(8), B<update-grub>(8)."
msgstr "B<grub-reboot>(8), B<grub-editenv>(1)"

#. type: Plain text
#: debian-unstable
#, fuzzy
#| msgid "should give you access to the complete manual."
msgid ""
"should give you access to the complete manual.  You may need to install the "
"B<grub-legacy-doc> package."
msgstr "gi deg tilgang til hele manualen."
