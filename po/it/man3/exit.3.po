# Italian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Giulio Daprelà <giulio@pluto.it>, 2005-2006.
# Elisabetta Galli <lab@kkk.it>, 2007.
# Marco Curreli <marcocurreli@tiscali.it>, 2013-2015, 2018, 2020, 2021.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.0.0\n"
"POT-Creation-Date: 2023-02-20 20:04+0100\n"
"PO-Revision-Date: 2021-05-01 21:47+0200\n"
"Last-Translator: Marco Curreli <marcocurreli@tiscali.it>\n"
"Language-Team: Italian <pluto-ildp@lists.pluto.it>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.04.0\n"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<exit>()"
msgid "exit"
msgstr "B<exit>()"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 febbraio 2023"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOME"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "exit - cause normal process termination"
msgstr "exit - causa un termine normale del processo"

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINTASSI"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>stdlib.hE<gt>>\n"
msgstr "B<#include E<lt>stdlib.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<noreturn void exit(int >I<status>B<);>\n"
msgid "B<[[noreturn]] void exit(int >I<status>B<);>\n"
msgstr "B<noreturn void exit(int >I<status>B<);>\n"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIZIONE"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The B<exit>()  function causes normal process termination and the least "
"significant byte of I<status> (i.e., I<status & 0xFF>) is returned to the "
"parent (see B<wait>(2))."
msgstr ""
"La funzione B<exit>() provoca il termine normale dei processi e il byte meno "
"significativo di I<status> (i.e., I<status & 0xFF>) è restituito al genitore "
"(vedere B<wait>(2))."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"All functions registered with B<atexit>(3)  and B<on_exit>(3)  are called, "
"in the reverse order of their registration.  (It is possible for one of "
"these functions to use B<atexit>(3)  or B<on_exit>(3)  to register an "
"additional function to be executed during exit processing; the new "
"registration is added to the front of the list of functions that remain to "
"be called.)  If one of these functions does not return (e.g., it calls "
"B<_exit>(2), or kills itself with a signal), then none of the remaining "
"functions is called, and further exit processing (in particular, flushing of "
"B<stdio>(3)  streams) is abandoned.  If a function has been registered "
"multiple times using B<atexit>(3)  or B<on_exit>(3), then it is called as "
"many times as it was registered."
msgstr ""
"Tutte le funzioni registrate con B<atexit>(3) e B<on_exit>(3) sono chiamate, "
"in ordine inverso rispetto alla loro registrazione. (È possibile per una di "
"queste funzioni usare B<atexit>(3) o B<on_exit>(3) per registrare una "
"funzione aggiuntiva da eseguire durante il processo di uscita; la nuova "
"registrazione è aggiunta all'inizio dell'elenco di funzioni che rimane da "
"chiamare). Se una di queste funzioni non restituisce niente (per esempio, "
"chiama B<_exit>(2), o si termina da sola mandandosi un segnale), non viene "
"chiamata nessuna delle funzioni rimanenti, e la successiva uscita in "
"elaborazione (in particolare, lo svuotamento degli stream B<stdio>(3)) viene "
"abbandonata. Se una funzione è stata registrata più volte usando "
"B<atexit>(3) o B<on_exit>(3), verrà chiamata tante volte quante sono le "
"registrazioni."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"All open B<stdio>(3)  streams are flushed and closed.  Files created by "
"B<tmpfile>(3)  are removed."
msgstr ""
"Tutti gli stream B<stdio>(3) aperti sono svuotati e chiusi. I file creati "
"con B<tmpfile>(3) sono rimossi."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The C standard specifies two constants, B<EXIT_SUCCESS> and B<EXIT_FAILURE>, "
"that may be passed to B<exit>()  to indicate successful or unsuccessful "
"termination, respectively."
msgstr ""
"Il C standard specifica due costanti, B<EXIT_SUCCESS> e B<EXIT_FAILURE>, che "
"possono essere passate a B<exit>() per indicare rispettivamente il successo "
"o l'insuccesso di una chiusura."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALORE RESTITUITO"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The B<exit>()  function does not return."
msgstr "La funzione B<exit>() non restituisce nulla."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATTRIBUTI"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Per la spiegazione dei termini usati in questa sezione, vedere "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interfaccia"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Attributo"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valore"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<exit>()"
msgstr "B<exit>()"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Thread safety"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "MT-Unsafe race:exit"
msgstr "MT-Unsafe race:exit"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The B<exit>()  function uses a global variable that is not protected, so it "
"is not thread-safe."
msgstr ""
"La funzione B<exit>() usa una variabile globale che non è protetta, quindi "
"non è thread-safe."

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, fuzzy
#| msgid "POSIX.1-2001, POSIX.1-2008, C89, C99, SVr4, 4.3BSD."
msgid "POSIX.1-2001, POSIX.1-2008, C99, SVr4, 4.3BSD."
msgstr "POSIX.1-2001, POSIX.1-2008, C89, C99, SVr4, 4.3BSD."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTE"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The behavior is undefined if one of the functions registered using "
"B<atexit>(3)  and B<on_exit>(3)  calls either B<exit>()  or B<longjmp>(3).  "
"Note that a call to B<execve>(2)  removes registrations created using "
"B<atexit>(3)  and B<on_exit>(3)."
msgstr ""
"Il comportamento è indefinito se una delle funzioni registrate usando "
"B<atexit>(3) e B<on_exit>(3) chiama B<exit>() o B<longjmp>(3). Da notare che "
"una chiamata a B<execve>(2) rimuove le registrazioni create con B<atexit>(3) "
"e B<on_exit>(3)."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The use of B<EXIT_SUCCESS> and B<EXIT_FAILURE> is slightly more portable (to "
"non-UNIX environments) than the use of 0 and some nonzero value like 1 or "
"-1.  In particular, VMS uses a different convention."
msgstr ""
"L'uso di B<EXIT_SUCCESS> e B<EXIT_FAILURE> è leggermente più portabile "
"(verso ambienti non-Unix) dell'uso di 0 e di qualche valore diverso da zero "
"come 1 o -1. In particolare, VMS usa una convenzione differente."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"BSD has attempted to standardize exit codes (which some C libraries such as "
"the GNU C library have also adopted); see the file I<E<lt>sysexits.hE<gt>>."
msgstr ""
"BSD ha tentato di standardizzare i codici di uscita (che anche alcune "
"librerie C, come la libreria GNU C, hanno adottato); vedere il file "
"I<E<lt>sysexits.hE<gt>>."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"After B<exit>(), the exit status must be transmitted to the parent process.  "
"There are three cases:"
msgstr ""
"Dopo B<exit>(), lo stato di uscita deve essere trasmesso al processo "
"genitore. Ci sono tre casi:"

#. type: IP
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "\\[bu]"
msgstr "\\[bu]"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If the parent has set B<SA_NOCLDWAIT>, or has set the B<SIGCHLD> handler to "
"B<SIG_IGN>, the status is discarded and the child dies immediately."
msgstr ""
"Se il genitore ha impostato B<SA_NOCLDWAIT>, o ha impostato l'handler "
"B<SIGCHLD> a B<SIG_IGN>, lo stato è scartato e il processo figlio muore "
"immediatamente."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If the parent was waiting on the child, it is notified of the exit status "
"and the child dies immediately."
msgstr ""
"Se il genitore stava aspettando il processo figlio, gli viene notificato lo "
"stato di uscita e il processo figlio muore immediatamente."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Otherwise, the child becomes a \"zombie\" process: most of the process "
"resources are recycled, but a slot containing minimal information about the "
"child process (termination status, resource usage statistics) is retained in "
"process table.  This allows the parent to subsequently use B<waitpid>(2)  "
"(or similar) to learn the termination status of the child; at that point the "
"zombie process slot is released."
msgstr ""
"Altrimenti, il processo figlio diviene un processo \"zombie\": la maggior "
"parte delle risorse di processo vengono riciclate, ma uno slot contenente "
"informazioni minimali sul processo figlio (stato di interruzione, "
"statistiche sull'uso di risorse) è conservato nella tabella dei processi. "
"Questo consente al genitore di usare susseguentemente B<waitpid>(2) (o "
"simile) per avere informazioni sullo stato di interruzione del processo "
"figlio; a questo punto lo slot del processo zombie viene rilasciato."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If the implementation supports the B<SIGCHLD> signal, this signal is sent to "
"the parent.  If the parent has set B<SA_NOCLDWAIT>, it is undefined whether "
"a B<SIGCHLD> signal is sent."
msgstr ""
"Se l'implementazione supporta il segnale B<SIGCHLD>, questo segnale è "
"inviato al genitore. Se il genitore ha impostato B<SA_NOCLDWAIT>, non è "
"definito se il segnale B<SIGCHLD> è inviato."

#. type: SS
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Signals sent to other processes"
msgstr "Segnali inviati ad altri processi"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If the exiting process is a session leader and its controlling terminal is "
"the controlling terminal of the session, then each process in the foreground "
"process group of this controlling terminal is sent a B<SIGHUP> signal, and "
"the terminal is disassociated from this session, allowing it to be acquired "
"by a new controlling process."
msgstr ""
"Se il processo in uscita è un leader di sessione e il suo terminale di "
"controllo è il terminale di controllo della sessione, allora a ciascun "
"processo nel gruppo dei processi in primo piano di questo terminale di "
"controllo viene mandato un segnale B<SIGHUP>, e il terminale è disassociato "
"da questa sessione, permettendone l'acquisizione da parte di un nuovo "
"processo controllante."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If the exit of the process causes a process group to become orphaned, and if "
"any member of the newly orphaned process group is stopped, then a B<SIGHUP> "
"signal followed by a B<SIGCONT> signal will be sent to each process in this "
"process group.  See B<setpgid>(2)  for an explanation of orphaned process "
"groups."
msgstr ""
"Se l'uscita del processo fa sì che un gruppo di processo divenga orfano, e "
"se un qualunque membro del nuovo gruppo del processo orfano viene fermato, "
"allora verrà mandato un segnale B<SIGHUP> seguito da un segnale B<SIGCONT> a "
"ciascun processo in questo gruppo di processo. Si veda B<setpgid>(2) per una "
"spiegazione dei gruppi di processi orfani."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Except in the above cases, where the signalled processes may be children of "
"the terminating process, termination of a process does I<not> in general "
"cause a signal to be sent to children of that process.  However, a process "
"can use the B<prctl>(2)  B<PR_SET_PDEATHSIG> operation to arrange that it "
"receives a signal if its parent terminates."
msgstr ""
"Al di fuori dei casi di cui sopra, dove i processi segnalati possono essere "
"figli del processo che viene interrotto, l'interruzione di un processo "
"generalmente I<non> provoca l'invio di un segnale al figlio di quel "
"processo. Comunque, un processo può usare l'operazione B<prctl>(2) "
"B<PR_SET_PDEATHSIG> per fare in modo che riceva un segnale se il suo "
"genitore termina."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VEDERE ANCHE"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<_exit>(2), B<get_robust_list>(2), B<setpgid>(2), B<wait>(2), B<atexit>(3), "
"B<on_exit>(3), B<tmpfile>(3)"
msgstr ""
"B<_exit>(2), B<get_robust_list>(2), B<setpgid>(2), B<wait>(2), B<atexit>(3), "
"B<on_exit>(3), B<tmpfile>(3)"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "EXIT"
msgstr "EXIT"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "2020-02-09"
msgstr "9 febbraio 2020"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manuale del programmatore di Linux"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "B<void exit(int >I<status>B<);>\n"
msgstr "B<void exit(int >I<status>B<);>\n"

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORME A"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
msgid "POSIX.1-2001, POSIX.1-2008, C89, C99, SVr4, 4.3BSD."
msgstr "POSIX.1-2001, POSIX.1-2008, C89, C99, SVr4, 4.3BSD."

#. type: IP
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "\\(bu"
msgstr "\\(bu"

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "COLOPHON"

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Questa pagina fa parte del rilascio 5.10 del progetto Linux I<man-pages>. "
"Una descrizione del progetto, le istruzioni per la segnalazione degli "
"errori, e l'ultima versione di questa pagina si trovano su \\%https://www."
"kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15 settembre 2017"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"The B<exit>()  function causes normal process termination and the value of "
"I<status & 0377> is returned to the parent (see B<wait>(2))."
msgstr ""
"La funzione  B<exit>()  provoca provoca il termine normale dei processi e il "
"valore di I<status & 0377> è restituito al genitore (see B<wait>(2))."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"BSD has attempted to standardize exit codes; see the file I<E<lt>sysexits."
"hE<gt>>."
msgstr ""
"BSD ha tentato di standardizzare i codici di uscita; vedere il file "
"I<E<lt>sysexits.hE<gt>>."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Questa pagina fa parte del rilascio 4.16 del progetto Linux I<man-pages>. "
"Una descrizione del progetto, le istruzioni per la segnalazione degli "
"errori, e l'ultima versione di questa pagina si trovano su \\%https://www."
"kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-12-15"
msgstr "15 dicembre 2022"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.02"
msgstr "Linux man-pages 6.02"
