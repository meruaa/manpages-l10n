# Ukrainian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Yuri Chornoivan <yurchor@ukr.net>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2022-09-09 17:07+0200\n"
"PO-Revision-Date: 2022-09-09 21:08+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <trans-uk@lists.fedoraproject.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 20.12.0\n"

#. type: TH
#: archlinux
#, no-wrap
msgid "RSH"
msgstr "RSH"

#. type: TH
#: archlinux
#, no-wrap
msgid "July 2022"
msgstr "липень 2022 року"

#. type: TH
#: archlinux
#, no-wrap
msgid "GNU inetutils 2.3"
msgstr "GNU inetutils 2.3"

#. type: TH
#: archlinux
#, no-wrap
msgid "User Commands"
msgstr "Команди користувача"

#. type: SH
#: archlinux
#, no-wrap
msgid "NAME"
msgstr "НАЗВА"

#. type: Plain text
#: archlinux
msgid "rsh - Remote shell client"
msgstr "rsh — клієнт віддаленої оболонки"

#. type: SH
#: archlinux
#, no-wrap
msgid "SYNOPSIS"
msgstr "КОРОТКИЙ ОПИС"

#. type: Plain text
#: archlinux
msgid ""
"B<rsh> [I<\\,OPTION\\/>...] [I<\\,USER@\\/>]I<\\,HOST \\/>[I<\\,COMMAND \\/"
">[I<\\,ARG\\/>...]]"
msgstr ""
"B<rsh> [I<\\,ПАРАМЕТР\\/>...] [I<\\,КОРИСТУВАЧ@\\/>]I<\\,ВУЗОЛ \\/>[I<\\,"
"КОМАНДА \\/>[I<\\,АРГУМЕНТ\\/>...]]"

#. type: SH
#: archlinux
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИС"

#. type: Plain text
#: archlinux
msgid "remote shell"
msgstr "віддалена оболонка"

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-4>, B<--ipv4>"
msgstr "B<-4>, B<--ipv4>"

#. type: Plain text
#: archlinux
msgid "use only IPv4"
msgstr "використовувати лише IPv4"

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-6>, B<--ipv6>"
msgstr "B<-6>, B<--ipv6>"

#. type: Plain text
#: archlinux
msgid "use only IPv6"
msgstr "використовувати лише IPv6"

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-8>, B<--8-bit>"
msgstr "B<-8>, B<--8-bit>"

#. type: Plain text
#: archlinux
msgid "allows an eight-bit input data path at all times"
msgstr "безумовно дозволяє шлях восьмибітових вхідних даних"

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-d>, B<--debug>"
msgstr "B<-d>, B<--debug>"

#. type: Plain text
#: archlinux
msgid "turns on socket debugging (see setsockopt(2))"
msgstr "вмикає діагностику сокета (див. setsockopt(2))"

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-e>, B<--escape>=I<\\,CHAR\\/>"
msgstr "B<-e>, B<--escape>=I<\\,СИМВОЛ\\/>"

#. type: Plain text
#: archlinux
msgid "allows user specification of the escape character (``~'' by default)"
msgstr ""
"уможливлює задання користувачем символу екранування (типовим символом є "
"``~'')"

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-l>, B<--user>=I<\\,USER\\/>"
msgstr "B<-l>, B<--user>=I<\\,КОРИСТУВАЧ\\/>"

#. type: Plain text
#: archlinux
msgid "run as USER on the remote system"
msgstr "запустити від імені КОРИСТУВАЧА на віддаленій системі"

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-n>, B<--no-input>"
msgstr "B<-n>, B<--no-input>"

#. type: Plain text
#: archlinux
msgid "use I<\\,/dev/null\\/> as input"
msgstr "скористатися I<\\,/dev/null\\/>, як вхідними даними"

#. type: TP
#: archlinux
#, no-wrap
msgid "-?, B<--help>"
msgstr "-?, B<--help>"

#. type: Plain text
#: archlinux
msgid "give this help list"
msgstr "показати ці довідкові дані"

#. type: TP
#: archlinux
#, no-wrap
msgid "B<--usage>"
msgstr "B<--usage>"

#. type: Plain text
#: archlinux
msgid "give a short usage message"
msgstr "отримати коротке повідомлення щодо користування"

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: archlinux
msgid "print program version"
msgstr "вивести дані щодо версії програми"

#. type: Plain text
#: archlinux
msgid ""
"Mandatory or optional arguments to long options are also mandatory or "
"optional for any corresponding short options."
msgstr ""
"Обов’язкові і додаткові аргументи до довгих форм запису параметрів є також "
"обов’язковими або додатковими для всіх відповідних скорочених форм запису."

#. type: SH
#: archlinux
#, no-wrap
msgid "AUTHOR"
msgstr "АВТОР"

#. type: Plain text
#: archlinux
msgid "Written by many authors."
msgstr "Має багато авторів."

#. type: SH
#: archlinux
#, no-wrap
msgid "REPORTING BUGS"
msgstr "ЗВІТИ ПРО ВАДИ"

#. type: Plain text
#: archlinux
msgid "Report bugs to E<lt>bug-inetutils@gnu.orgE<gt>."
msgstr "Про вади слід повідомляти за адресою E<lt>bug-inetutils@gnu.orgE<gt>."

#. type: SH
#: archlinux
#, no-wrap
msgid "COPYRIGHT"
msgstr "АВТОРСЬКІ ПРАВА"

#. type: Plain text
#: archlinux
msgid ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Авторські права \\(co 2022 Free Software Foundation, Inc.  Ліцензія GPLv3+: "
"GNU GPL версії 3 або пізнішої E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: archlinux
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Це вільне програмне забезпечення: Ви можете вільно змінювати і "
"розповсюджувати його. БЕЗ ЖОДНИХ ГАРАНТІЙ, в межах, дозволених законом."

#. type: SH
#: archlinux
#, no-wrap
msgid "SEE ALSO"
msgstr "ДИВ. ТАКОЖ"

#. type: Plain text
#: archlinux
msgid "rshd(1)"
msgstr "rshd(1)"

#. type: Plain text
#: archlinux
msgid ""
"The full documentation for B<rsh> is maintained as a Texinfo manual.  If the "
"B<info> and B<rsh> programs are properly installed at your site, the command"
msgstr ""
"Повноцінну документацію з B<rsh> можна знайти у довіднику Texinfo. Якщо у "
"вашій системі встановлено B<info> та B<rsh>, за допомогою команди"

#. type: Plain text
#: archlinux
msgid "B<info rsh>"
msgstr "B<info rsh>"

#. type: Plain text
#: archlinux
msgid "should give you access to the complete manual."
msgstr "ви зможете отримати доступ до повноцінного підручника."
