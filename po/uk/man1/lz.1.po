# Ukrainian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Yuri Chornoivan <yurchor@ukr.net>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2022-04-08 18:11+0200\n"
"PO-Revision-Date: 2022-11-06 20:33+0200\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <trans-uk@lists.fedoraproject.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 20.12.0\n"

#. type: TH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "LZ"
msgstr "LZ"

#. type: TH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "Wed Feb 23 00:00:00 EET 2000"
msgstr "Середа, 23 лютого 00:00:00 EET 2000 року"

#. type: TH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "Mtools Users Manual"
msgstr "Підручник користувача Mtools"

#. type: SH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "NAME"
msgstr "НАЗВА"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "lz - gunzips and shows a listing of a gzip'd tar'd archive"
msgstr "lz виконує gunzip і показує список стисненого gzip архіву tar"

#. type: SH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr "КОРОТКИЙ ОПИС"

#.  The command line
#. type: Plain text
#: debian-bullseye debian-unstable
msgid "B<lz> I<file>"
msgstr "B<lz> I<файл>"

#. type: SH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИС"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"B<lz> provides a listing of a gzip'd tar'd archive, that is a B<tar>(1) "
"archive compressed with the B<gzip>(1) utility.  It is not strictly "
"necessary on Debian GNU/Linux, because the GNU B<tar>(1) program provides "
"the same capability with the command"
msgstr ""
"B<lz> надає список вмісту архіву tar запакованого gzip, тобто архіву "
"B<tar>(1), стискання якого виконано за допомогою програми B<gzip>(1). Ця "
"програма не є обов'язковою для Debian GNU/Linux, оскільки сама програма GNU "
"B<tar>(1) надає ті самі можливості, якщо скористатися форматом команди"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "B<tar -tzf> I<file>"
msgstr "B<tar -tzf> I<файл>"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"but this utility is provided in the mtools package for other platforms and "
"is retained here for completeness."
msgstr ""
"але цей інструмент надається у форматі пакунка mtools для інших платформа і "
"зберігається тут заради повноти."

#. type: SH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "AUTHOR"
msgstr "АВТОР"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"Robert King (Robert.King@mailbox.gu.edu.au) wrote this page for the I<Debian/"
"GNU> mtools package."
msgstr ""
"Цю сторінку підручника написано Robert King (Robert.King@mailbox.gu.edu.au) "
"для пакунка mtools I<Debian/GNU>."

#. type: SH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr "ДИВ. ТАКОЖ"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "B<mtools>(1), B<gzip>(1), B<tar>(1), B<uz>(1)."
msgstr "B<mtools>(1), B<gzip>(1), B<tar>(1), B<uz>(1)."
