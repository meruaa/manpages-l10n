# Ukrainian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Yuri Chornoivan <yurchor@ukr.net>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-02-15 18:59+0100\n"
"PO-Revision-Date: 2022-08-06 19:18+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <trans-uk@lists.fedoraproject.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 20.12.0\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "LSNS"
msgstr "LSNS"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-07-20"
msgstr "20 липня 2022 року"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "util-linux 2.38.1"
msgstr "util-linux 2.38.1"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "System Administration"
msgstr "Керування системою"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "НАЗВА"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "lsns - list namespaces"
msgstr "lsns — виведення списку просторів назв"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "КОРОТКИЙ ОПИС"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<lsns> [options] I<namespace>"
msgstr "B<lsns> [параметри] I<простір назв>"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИС"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<lsns> lists information about all the currently accessible namespaces or "
"about the given I<namespace>. The I<namespace> identifier is an inode number."
msgstr ""
"B<lsns> виводить список відомостей щодо усіх поточних доступних просторів "
"назв або щодо вказаного I<простору-назв>. Ідентифікатор I<простір-назв> є "
"номером inode."

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"The default output is subject to change. So whenever possible, you should "
"avoid using default outputs in your scripts. Always explicitly define "
"expected output mode (B<--tree> or B<--list>) and columns by using the B<--"
"output> option together with a columns list in environments where a stable "
"output is required."
msgstr ""
"Типовий формат виведення може бути змінено авторами програми. Тому, коли це "
"можливо, вам слід уникати обробки типових виведених даних у ваших скриптах. "
"Завжди явно визначайте очікуваний режим виведення (B<--tree> або B<--list>) "
"і стовпчики за допомогою параметра B<--output> разом зі списком стовпчиків у "
"середовищах, де потрібне виведення стабільного набору даних."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The B<NSFS> column, printed when B<net> is specified for the B<--type> "
"option, is special; it uses multi-line cells. Use the option B<--nowrap> to "
"switch to \",\"-separated single-line representation."
msgstr ""
"Стовпчик B<NSFS>, який програма виводить, якщо вказано аргумент B<net> для "
"параметра B<--type>, є особливим; у ньому використано багаторядкові комірки. "
"Скористайтеся параметром B<--nowrap> для перемикання на однорядкове "
"представлення із відокремленням записів «,»."

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Note that B<lsns> reads information directly from the I</proc> filesystem "
"and for non-root users it may return incomplete information. The current I</"
"proc> filesystem may be unshared and affected by a PID namespace (see "
"B<unshare --mount-proc> for more details). B<lsns> is not able to see "
"persistent namespaces without processes where the namespace instance is held "
"by a bind mount to /proc/I<pid>/ns/I<type>."
msgstr ""
"Зауважте, що B<lsns> читає дані безпосередньо з файлової системи I</proc>, а "
"для відмінних від root користувачів може бути повернуто неповні дані. "
"Поточну версію файлової системи I</proc> може бути виведено зі спільного "
"користування і на нього може впливати простір назв PID (див. B<unshare --"
"mount-proc>, щоб дізнатися більше). B<lsns> не може отримувати дані щодо "
"сталих просторів назв без процесів, де екземпляр простору назв утримується "
"монтуванням прив'язки до /proc/I<pid>/ns/I<тип>."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "ПАРАМЕТРИ"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<-J>, B<--json>"
msgstr "B<-J>, B<--json>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Use JSON output format."
msgstr "Bикористати формат виведення JSON."

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<-l>, B<--list>"
msgstr "B<-l>, B<--list>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Use list output format."
msgstr "Використовувати формат виведення списком."

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<-n>, B<--noheadings>"
msgstr "B<-n>, B<--noheadings>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Do not print a header line."
msgstr "Не виводити рядок заголовка."

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<-o>, B<--output> I<list>"
msgstr "B<-o>, B<--output> I<список>"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Specify which output columns to print. Use B<--help> to get a list of all "
"supported columns."
msgstr ""
"Визначити, які стовпчики слід використовувати для виведення. Скористайтеся "
"параметром B<--help>, щоб переглянути список підтримуваних стовпчиків."

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The default list of columns may be extended if I<list> is specified in the "
"format B<+>I<list> (e.g., B<lsns -o +PATH>)."
msgstr ""
"Типовий список стовпчиків може бути розширено, якщо I<список> вказано у "
"форматі B<+>I<список> (наприклад B<lsns -o +PATH>)."

#. #-#-#-#-#  archlinux: lsns.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  debian-bullseye: lsns.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  debian-unstable: lsns.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  fedora-38: lsns.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: lsns.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: lsns.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-15-5: lsns.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: lsns.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--output-all>"
msgstr "B<--output-all>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Output all available columns."
msgstr "Вивести список усіх доступних стовпчиків."

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<-p>, B<--task> I<PID>"
msgstr "B<-p>, B<--task> I<PID>"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "Display only the namespaces held by the process with this I<PID>."
msgstr "Вивести лише простори назв, які утримуються процесом із цим I<pid>."

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<-r>, B<--raw>"
msgstr "B<-r>, B<--raw>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Use the raw output format."
msgstr "Використовувати формат виведення без обробки."

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<-t>, B<--type> I<type>"
msgstr "B<-t>, B<--type> I<тип>"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Display the specified I<type> of namespaces only. The supported types are "
"B<mnt>, B<net>, B<ipc>, B<user>, B<pid>, B<uts>, B<cgroup> and B<time>. This "
"option may be given more than once."
msgstr ""
"Вивести дані лише щодо вказаного I<типу> просторів назв. Підтримуваними "
"типами є B<mnt>, B<net>, B<ipc>, B<user>, B<pid>, B<uts>, B<cgroup> і "
"B<time>. Цей параметр може бути вказано декілька разів."

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<-u>, B<--notruncate>"
msgstr "B<-u>, B<--notruncate>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Do not truncate text in columns."
msgstr "Не обрізати текст у стовпчиках."

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<-W>, B<--nowrap>"
msgstr "B<-W>, B<--nowrap>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Do not use multi-line text in columns."
msgstr "Не використовувати багаторядковий текст у стовпчиках."

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "B<-T>, B<--tree> I<rel>"
msgstr "B<-T>, B<--tree> I<відносний>"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Use tree-like output format. If B<process> is given as I<rel>, print process "
"tree(s) in each name space. This is default when B<--tree> is not specified. "
"If B<parent> is given, print tree(s) constructed by the parent/child "
"relationship. If B<owner> is given, print tree(s) constructed by the owner/"
"owned relationship. B<owner> is used as default when I<rel> is omitted."
msgstr ""
"Використати деревоподібний формат виведення. Якщо аргументом I<відносний> є "
"B<process>, вивести ієрархії процесів у кожному з просторів назв. Це типовий "
"варіант, якщо не вказано B<--tree>. Якщо вказано B<parent>, вивести "
"ієрархії, які побудовано за співвідношенням батьківський/дочірній запис. "
"Якщо вказано B<owner>, вивести ієрархії, які побудовано за співвідношенням "
"власник-власність. B<owner> буде використано як типовий варіант, якщо не "
"вказано аргумент I<відносний>."

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Display help text and exit."
msgstr "Вивести текст довідки і завершити роботу."

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Print version and exit."
msgstr "Вивести дані щодо версії і завершити роботу."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "AUTHORS"
msgstr "АВТОРИ"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ДИВ. ТАКОЖ"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"B<nsenter>(1), B<unshare>(1), B<clone>(2), B<namespaces>(7), B<ioctl_ns>(2), "
"B<ip-netns>(8)"
msgstr ""
"B<nsenter>(1), B<unshare>(1), B<clone>(2), B<namespaces>(7), B<ioctl_ns>(2), "
"B<ip-netns>(8)"

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "ЗВІТИ ПРО ВАДИ"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "For bug reports, use the issue tracker at"
msgstr "Для звітування про вади використовуйте систему стеження помилками на"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "AVAILABILITY"
msgstr "ДОСТУПНІСТЬ"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The B<lsns> command is part of the util-linux package which can be "
"downloaded from"
msgstr "B<lsns> є частиною пакунка util-linux, який можна отримати з"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "December 2015"
msgstr "Грудень 2015 року"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "util-linux"
msgstr "util-linux"

#. type: Plain text
#: debian-bullseye
msgid "B<lsns> [options] [I<namespace>]"
msgstr "B<lsns> [параметри] [I<простір назв>]"

#. type: Plain text
#: debian-bullseye
msgid ""
"B<lsns> lists information about all the currently accessible namespaces or "
"about the given I<namespace>.  The I<namespace> identifier is an inode "
"number."
msgstr ""
"B<lsns> виводить список відомостей щодо усіх поточних доступних просторів "
"назв або щодо вказаного I<простору-назв>. Ідентифікатор I<простір-назв> є "
"номером inode."

#. type: Plain text
#: debian-bullseye
msgid ""
"The default output is subject to change.  So whenever possible, you should "
"avoid using default outputs in your scripts.  Always explicitly define "
"expected columns by using the B<--output> option together with a columns "
"list in environments where a stable output is required."
msgstr ""
"Типовий формат виведення може бути змінено авторами програми. Тому, коли це "
"можливо, вам слід уникати обробки типових виведених даних у ваших скриптах. "
"Завжди явно визначайте очікувані стовпчики за допомогою параметра B<--"
"output> разом зі списком стовпчиків у середовищах, де потрібне виведення "
"стабільного набору даних."

#. type: Plain text
#: debian-bullseye
msgid ""
"Note that B<lsns> reads information directly from the /proc filesystem and "
"for non-root users it may return incomplete information.  The current /proc "
"filesystem may be unshared and affected by a PID namespace (see B<unshare --"
"mount-proc> for more details).  B<lsns> is not able to see persistent "
"namespaces without processes where the namespace instance is held by a bind "
"mount to /proc/I<pid>/ns/I<type>."
msgstr ""
"Зауважте, що B<lsns> читає дані безпосередньо з файлової системи /proc, а "
"для відмінних від root користувачів може бути повернуто неповні дані. "
"Поточну версію файлової системи I</proc> може бути виведено зі спільного "
"користування і на нього може впливати простір назв PID (див. B<unshare --"
"mount-proc>, щоб дізнатися більше). B<lsns> не може отримувати дані щодо "
"сталих просторів назв без процесів, де екземпляр простору назв утримується "
"монтуванням прив'язки до /proc/I<pid>/ns/I<тип>."

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-J>,B< --json>"
msgstr "B<-J>, B<--json>"

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-l>,B< --list>"
msgstr "B<-l>,B< --list>"

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-n>,B< --noheadings>"
msgstr "B<-n>, B<--noheadings>"

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-o>,B< --output >I<list>"
msgstr "B<-o>, B<--output> I<список>"

#. type: Plain text
#: debian-bullseye
msgid ""
"Specify which output columns to print.  Use B<--help> to get a list of all "
"supported columns."
msgstr ""
"Визначити, які стовпчики слід використовувати для виведення. Скористайтеся "
"параметром B<--help>, щоб переглянути список підтримуваних стовпчиків."

#. type: Plain text
#: debian-bullseye
msgid ""
"The default list of columns may be extended if I<list> is specified in the "
"format B<+>I<list>B< (e.g., lsns -o +PATH).>"
msgstr ""
"Типовий список стовпчиків може бути розширено, якщо I<список> вказано у "
"форматі B<+>I<список> (наприклад B<lsns -o +PATH>)."

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-p>,B< --task >I<pid>"
msgstr "B<-p>,B< --task >I<pid>"

#. type: Plain text
#: debian-bullseye
msgid "Display only the namespaces held by the process with this I<pid>."
msgstr "Вивести лише простори назв, які утримуються процесом із цим I<pid>."

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-r>,B< --raw>"
msgstr "B<-r>,B< --raw>"

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-t>,B< --type >I<type>"
msgstr "B<-t>,B< --type >I<тип>"

#. type: Plain text
#: debian-bullseye
msgid ""
"Display the specified I<type> of namespaces only.  The supported types are "
"B<mnt>, B<net>, B<ipc>, B<user>, B<pid>, B<uts>, B<cgroup> and B<time>.  "
"This option may be given more than once."
msgstr ""
"Вивести дані лише щодо вказаного I<типу> просторів назв. Підтримуваними "
"типами є B<mnt>, B<net>, B<ipc>, B<user>, B<pid>, B<uts>, B<cgroup> і "
"B<time>. Цей параметр може бути вказано декілька разів."

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-u>,B< --notruncate>"
msgstr "B<-u>, B<--notruncate>"

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-W>,B< --nowrap>"
msgstr "B<-W>, B<--nowrap>"

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-V>,B< --version>"
msgstr "B<-V>,B< --version>"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "Display version information and exit."
msgstr "Вивести дані щодо версії і завершити роботу."

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-h>,B< --help>"
msgstr "B<-h>,B< --help>"

#. type: Plain text
#: debian-bullseye
#, no-wrap
msgid "Karel Zak E<lt>kzak@redhat.comE<gt>\n"
msgstr "Karel Zak E<lt>kzak@redhat.comE<gt>\n"

#. type: Plain text
#: debian-bullseye
msgid "B<nsenter>(1), B<unshare>(1), B<clone>(2), B<namespaces>(7)"
msgstr "B<nsenter>(1), B<unshare>(1), B<clone>(2), B<namespaces>(7)"

#. type: Plain text
#: debian-bullseye
msgid ""
"The lsns command is part of the util-linux package and is available from "
"https://www.kernel.org/pub/linux/utils/util-linux/."
msgstr ""
"Програма lsns є частиною пакунка util-linux і доступна за адресою https://"
"www.kernel.org/pub/linux/utils/util-linux/."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2022-02-14"
msgstr "14 лютого 2022 року"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "util-linux 2.37.4"
msgstr "util-linux 2.37.4"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"The default output is subject to change. So whenever possible, you should "
"avoid using default outputs in your scripts. Always explicitly define "
"expected columns by using the B<--output> option together with a columns "
"list in environments where a stable output is required."
msgstr ""
"Типовий формат виведення може бути змінено авторами програми. Тому, коли це "
"можливо, вам слід уникати обробки типових виведених даних у ваших скриптах. "
"Завжди явно визначайте очікувані стовпчики за допомогою параметра B<--"
"output> разом зі списком стовпчиків у середовищах, де потрібне виведення "
"стабільного набору даних."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"B<nsenter>(1), B<unshare>(1), B<clone>(2), B<namespaces>(7), B<ioctl_ns(2)>"
msgstr ""
"B<nsenter>(1), B<unshare>(1), B<clone>(2), B<namespaces>(7), B<ioctl_ns(2)>"
