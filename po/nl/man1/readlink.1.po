# Dutch translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Mario Blättermann <mario.blaettermann@gmail.com>, 2019.
# Luc Castermans <luc.castermans@gmail.com>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 2.15\n"
"POT-Creation-Date: 2023-02-15 19:08+0100\n"
"PO-Revision-Date: 2022-08-21 20:28+0200\n"
"Last-Translator: Luc Castermans <luc.castermans@gmail.com>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.12.3\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "READLINK"
msgstr "READLINK"

#. type: TH
#: archlinux
#, no-wrap
msgid "November 2022"
msgstr "November 2022"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "GNU coreutils 9.1"
msgstr "GNU coreutils 9.1"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Opdrachten voor gebruikers"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAAM"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "readlink - print resolved symbolic links or canonical file names"
msgstr ""
"readlink - toon gevonden symbolische koppelingen of complete bestandsnamen"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SAMENVATTING"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<readlink> [I<\\,OPTION\\/>]... I<\\,FILE\\/>..."
msgstr "B<readlink> [I<\\,OPTIE\\/>]... I<\\,BESTAND\\/>..."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHRIJVING"

#.  Add any additional description here
#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Note B<realpath>(1) is the preferred command to use for canonicalization "
"functionality."
msgstr ""
"Let op: B<realpath>(1) is het voorkeur commando voor complete "
"functionaliteit."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Print value of a symbolic link or canonical file name"
msgstr ""
"Toont de waarde van een symbolische koppeling of een volledige bestandsnaam."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-f>, B<--canonicalize>"
msgstr "B<-f>, B<--canonicalize>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"canonicalize by following every symlink in every component of the given name "
"recursively; all but the last component must exist"
msgstr ""
"compleet maken door elke koppeling in elke component van het pad recursief "
"te volgen; alle behalve laatste component moeten bestaan"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-e>, B<--canonicalize-existing>"
msgstr "B<-e>, B<--canonicalize-existing>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"canonicalize by following every symlink in every component of the given name "
"recursively, all components must exist"
msgstr ""
"compleet maken door elke koppeling in elke component van het pad recursief "
"te volgen; alle componenten moeten bestaan"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-m>, B<--canonicalize-missing>"
msgstr "B<-m>, B<--canonicalize-missing>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"canonicalize by following every symlink in every component of the given name "
"recursively, without requirements on components existence"
msgstr ""
"compleet maken door elke koppeling in elke component van het pad recursief "
"te volgen; geen van de componenten hoeft te bestaan"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-n>, B<--no-newline>"
msgstr "B<-n>, B<--no-newline>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "do not output the trailing delimiter"
msgstr "geen scheidingsteken achtervoegen"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<-q>, B<--quiet>"
msgstr "B<-q>, B<--quiet>"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-s>, B<--silent>"
msgstr "B<-s>, B<--silent>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "suppress most error messages (on by default)"
msgstr "de meeste foutmeldingen onderdrukken (standaard)"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "report error messages"
msgstr "meer foutmeldingen tonen"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-z>, B<--zero>"
msgstr "B<-z>, B<--zero>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "end each output line with NUL, not newline"
msgstr "elke regel afsluiten met NUL-byte, niet met nieuwe regel"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "display this help and exit"
msgstr "toon de helptekst en stop"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "output version information and exit"
msgstr "toon programmaversie en stop"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTEUR"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Written by Dmitry V. Levin."
msgstr "Geschreven door Dmitry V. Levin."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "RAPPORTEREN VAN BUGS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"Online hulp bij GNU coreutils: E<lt>https://www.gnu.org/software/coreutils/"
"E<gt>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"Meld alle vertaalfouten op E<lt>https://translationproject.org/team/nl."
"htmlE<gt>"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "COPYRIGHT"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  Licentie GPLv3+: GNU "
"GPL versie 3 of later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Dit is vrije software: u mag het vrijelijk wijzigen en verder verspreiden. "
"Deze software kent GEEN GARANTIE, voor zover de wet dit toestaat."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ZIE OOK"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "B<readlink>(2), B<realpath>(1), B<realpath>(3)"
msgstr "B<readlink>(2), B<realpath>(1), B<realpath>(3)"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Full documentation E<lt>https://www.gnu.org/software/coreutils/readlinkE<gt>"
msgstr ""
"Volledige documentatie op: E<lt>https://www.gnu.org/software/coreutils/"
"readlinkE<gt>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"or available locally via: info \\(aq(coreutils) readlink invocation\\(aq"
msgstr "of lokaal via: info \\(aq(coreutils) readlink invocation\\(aq"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "September 2020"
msgstr "September 2020"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "GNU coreutils 8.32"
msgstr "GNU coreutils 8.32"

#.  Add any additional description here
#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"Note realpath(1) is the preferred command to use for canonicalization "
"functionality."
msgstr ""
"Let op: realpath(1) is het voorkeur commando voor complete functionaliteit."

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  Licentie GPLv3+: GNU "
"GPL versie 3 of later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "readlink(2), realpath(1), realpath(3)"
msgstr "readlink(2), realpath(1), realpath(3)"

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "September 2022"
msgstr "September 2022"

#. type: TH
#: fedora-38 fedora-rawhide
#, no-wrap
msgid "January 2023"
msgstr "Januari 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "April 2022"
msgstr "April 2022"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "October 2021"
msgstr "Oktober 2021"
