# Dutch translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Jos Boersema <joshb@xs4all.nl>, 2001.
# Mario Blättermann <mario.blaettermann@gmail.com>, 2019.
# Luc Castermans <luc.castermans@gmail.com>, 2021-2023.
#
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 2.15\n"
"POT-Creation-Date: 2023-02-20 20:39+0100\n"
"PO-Revision-Date: 2023-02-19 11:48+0100\n"
"Last-Translator: Luc Castermans <luc.castermans@gmail.com>\n"
"Language-Team: Dutch - The Netherlands <kde-i18n-nl@kde.org>\n"
"Language: nl_NL\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1)\n"
"X-Generator: Gtranslator 42.0\n"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "unlink"
msgstr "unlink"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 februari 2023"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pagina's 6.03"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAAM"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "unlink, unlinkat - delete a name and possibly the file it refers to"
msgstr ""
"unlink, unlinkat - verwijder een naam en mogelijk het bestand waarnaar het "
"wijst"

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHEEK"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Standard C bibliotheek  (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SAMENVATTING"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>unistd.hE<gt>>\n"
msgstr "B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<int unlink(const char *>I<pathname>B<);>\n"
msgstr "B<int unlink(const char *>I<padnaam>B<);>\n"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#include E<lt>fcntl.hE<gt>           >/* Definition of B<AT_*> constants */\n"
"B<#include E<lt>unistd.hE<gt>>\n"
msgstr ""
"B<#include E<lt>fcntl.hE<gt>           >/* Definitie van  B<AT_*> constanten */\n"
"B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<int unlinkat(int >I<dirfd>B<, const char *>I<pathname>B<, int >I<flags>B<);>\n"
msgstr "B<int unlinkat(int >I<mapbi>B<, const char *>I<padnaam>B<, int >I<vlaggen>B<);>\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr "Feature Test Macro´s eisen in  glibc (zie B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<unlinkat>():"
msgstr "B<unlinkat>():"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    Since glibc 2.10:\n"
"        _POSIX_C_SOURCE E<gt>= 200809L\n"
"    Before glibc 2.10:\n"
"        _ATFILE_SOURCE\n"
msgstr ""
"    Vanaf glibc 2.10:\n"
"        _POSIX_C_SOURCE E<gt>= 200809L\n"
"    Voor glibc 2.10:\n"
"        _ATFILE_SOURCE\n"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHRIJVING"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<unlink>()  deletes a name from the filesystem.  If that name was the last "
"link to a file and no processes have the file open, the file is deleted and "
"the space it was using is made available for reuse."
msgstr ""
"B<unlink>() verwijderd een naam uit een bestandssysteem. Als die naam de "
"laatste koppeling was van een bestand en geen enkel proces heeft het bestand "
"open, dan wordt het bestand verwijderd en de ruimte die het innam wordt "
"vrijgemaakt om hergebruikt te worden."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If the name was the last link to a file but any processes still have the "
"file open, the file will remain in existence until the last file descriptor "
"referring to it is closed."
msgstr ""
"Als de naam de laatste koppeling was naar het bestand maar er zijn nog "
"processen die het bestand nog steeds open hebben, dan zal het bestand "
"blijven bestaan totdat de laatste bestandindicator die ernaar verwijst "
"gesloten is."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "If the name referred to a symbolic link, the link is removed."
msgstr ""
"Als de naam wijst naar een symbolische koppeling dan wordt die koppeling "
"verwijderd."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If the name referred to a socket, FIFO, or device, the name for it is "
"removed but processes which have the object open may continue to use it."
msgstr ""
"Als de naam wijst naar een `socket', een fifo of een apparaat dan wordt de "
"naam ervoor verwijderd maar processen die het voorwerp open hebben mogen het "
"blijven gebruiken."

#. type: SS
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "unlinkat()"
msgstr "unlinkat()"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The B<unlinkat>()  system call operates in exactly the same way as either "
"B<unlink>()  or B<rmdir>(2)  (depending on whether or not I<flags> includes "
"the B<AT_REMOVEDIR> flag)  except for the differences described here."
msgstr ""
"De B<unlinkat>() systeem aanroep werkt op exact dezelfde manier zoals "
"B<unlink>()  of B<rmdir>(2)  (afhankelijk van of de I<flags> wel  of niet de "
"B<AT_REMOVEDIR> vlag bevat) behalve voor de verschillen zoals hier "
"beschreven."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If the pathname given in I<pathname> is relative, then it is interpreted "
"relative to the directory referred to by the file descriptor I<dirfd> "
"(rather than relative to the current working directory of the calling "
"process, as is done by B<unlink>()  and B<rmdir>(2)  for a relative "
"pathname)."
msgstr ""
"Als de padnaam gegeven in I<padnaam> is relatief, dan wordt deze "
"geïnterpreteerd relatief aan de map zoals gerefereerd door de bestands "
"beschrijving I<dirfd> (liever dan relatief aan de huidige werkmap van het "
"aanroepende proces, zoals gedaan door B<unlink>() en B<rmdir>(2) voor een "
"relatieve padnaam)."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If the pathname given in I<pathname> is relative and I<dirfd> is the special "
"value B<AT_FDCWD>, then I<pathname> is interpreted relative to the current "
"working directory of the calling process (like B<unlink>()  and B<rmdir>(2))."
msgstr ""
"Als de padnaam gegeven in I<padnaam> relatief is en I<dirfd> is de speciale "
"waarde B<AT_FDCWD>, dan zal I<padnaam> geïnterpreteerd worden relatief aan "
"de huidige werk map van het aanroepende proces (zoals B<unlink>() en "
"B<rmdir>(2)."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If the pathname given in I<pathname> is absolute, then I<dirfd> is ignored."
msgstr ""
"Als de padnaam opgegeven in I<padnaam> absoluut is, dan wordt I<mapbi> "
"genegeerd."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"I<flags> is a bit mask that can either be specified as 0, or by ORing "
"together flag values that control the operation of B<unlinkat>().  "
"Currently, only one such flag is defined:"
msgstr ""
"I<vlaggen> is een bit masker dat ofwel gespecificeerd worden als 0, of door "
"de logische OF-bewerking op de waarden van de vlag die de operatie van "
"B<unlinkat>() bepalen. Op dit moment is alleen een zo´n vlag gedefinieerd: "

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<AT_REMOVEDIR>"
msgstr "B<AT_REMOVEDIR>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"By default, B<unlinkat>()  performs the equivalent of B<unlink>()  on "
"I<pathname>.  If the B<AT_REMOVEDIR> flag is specified, then performs the "
"equivalent of B<rmdir>(2)  on I<pathname>."
msgstr ""
"Standaard zal B<unlinkat>() het equivalent van B<unlink>() op I<padnaam> "
"uitvoeren. Indien de B<AT_REMOVEDIR> vlag werd gezet dan werkt het "
"equivalent van B<rmdir>(2) op I<padnaam>."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "See B<openat>(2)  for an explanation of the need for B<unlinkat>()."
msgstr "Zie B<openat>(2) voor een uitleg over het gebruik van B<unlinkat>()."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "EIND WAARDE"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"On success, zero is returned.  On error, -1 is returned, and I<errno> is set "
"to indicate the error."
msgstr ""
"Bij succes wordt nul teruggegeven. Bij falen wordt -1 teruggegeven en wordt "
"I<errno> overeenkomstig gezet."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "FOUTEN"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EACCES>"
msgstr "B<EACCES>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Write access to the directory containing I<pathname> is not allowed for the "
"process's effective UID, or one of the directories in I<pathname> did not "
"allow search permission.  (See also B<path_resolution>(7).)"
msgstr ""
"Schrijf toegang in de map die I<padnaam> bevat wordt niet toegestaan voor "
"het geldende uid van het proces, of een van de mappen in I<padnaam> liet "
"zoek (voer-uit) toestemming niet toe. (Zie ook B<path_resolution>(7).)"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EBUSY>"
msgstr "B<EBUSY>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The file I<pathname> cannot be unlinked because it is being used by the "
"system or another process; for example, it is a mount point or the NFS "
"client software created it to represent an active but otherwise nameless "
"inode (\"NFS silly renamed\")."
msgstr ""
"Het bestand I<padnaam> kan niet ontkoppeld worden omdat het in gebruik is "
"door het systeem of door een ander proces; bijvoorbeeld, het is een "
"koppelpunt of  NFS client software maakte het aan om een actieve maar anders "
"naamloze inode te vertegenwoordigen (\"NFS silly renamed\")."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "I<pathname> points outside your accessible address space."
msgstr "I<padnaam> wijst buiten de voor u toegankelijke adresruimte."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EIO>"
msgstr "B<EIO>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "An I/O error occurred."
msgstr "Een Invoer/Uitvoer fout trad op."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EISDIR>"
msgstr "B<EISDIR>"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"I<pathname> refers to a directory.  (This is the non-POSIX value returned "
"since Linux 2.1.132.)"
msgstr ""
"I<padnaam> wijst naar een map. (Dit is de niet-POSIX waarde teruggegeven "
"sinds Linux 2.1.132.)"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ELOOP>"
msgstr "B<ELOOP>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Too many symbolic links were encountered in translating I<pathname>."
msgstr ""
"Teveel symbolische koppelingen werden tegengekomen bij het vertalen van "
"I<padnaam>."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ENAMETOOLONG>"
msgstr "B<ENAMETOOLONG>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "I<pathname> was too long."
msgstr "I<padnaam> was te lang."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOENT>"
msgstr "B<ENOENT>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"A component in I<pathname> does not exist or is a dangling symbolic link, or "
"I<pathname> is empty."
msgstr ""
"Een deel in I<padnaam> bestaat niet of is een loshangende symbolische "
"koppeling, of I<padnaam> is leeg."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOMEM>"
msgstr "B<ENOMEM>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Insufficient kernel memory was available."
msgstr "Onvoldoende kernelgeheugen voorhanden."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOTDIR>"
msgstr "B<ENOTDIR>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"A component used as a directory in I<pathname> is not, in fact, a directory."
msgstr "Een onderdeel gebruikt als map in I<padnaam> is in feite geen map."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM>"
msgstr "B<EPERM>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The system does not allow unlinking of directories, or unlinking of "
"directories requires privileges that the calling process doesn't have.  "
"(This is the POSIX prescribed error return; as noted above, Linux returns "
"B<EISDIR> for this case.)"
msgstr ""
"Het systeem staat ontkoppelen van mappen niet toe, of het ontkoppelen van "
"mappen behoeft privileges die het aanroepende proces niet heeft.  (Dit is de "
"voorgeschreven POSIX fout waarde; zoals hierboven beschreven zal Linux in "
"dit geval B<EISDIR> terug geven.)"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM> (Linux only)"
msgstr "B<EPERM> (alleen Linux)"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The filesystem does not allow unlinking of files."
msgstr "Het bestandssysteem staat ontkoppeling van bestanden niet toe."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM> or B<EACCES>"
msgstr "B<EPERM> of B<EACCES>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The directory containing I<pathname> has the sticky bit (B<S_ISVTX>)  set "
"and the process's effective UID is neither the UID of the file to be deleted "
"nor that of the directory containing it, and the process is not privileged "
"(Linux: does not have the B<CAP_FOWNER> capability)."
msgstr ""
"De map waar I<padnaam> in zit heeft het sticky-bit (B<S_ISVTX>) aan staan en "
"het geldende UID van het proces is noch het UID van het bestand dat "
"verwijderd zou worden noch dat van de map waar het in zit en het proces is "
"niet geprivilegieerd (Linux: heeft niet de B<CAP_FOWNER> capaciteit). "

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The file to be unlinked is marked immutable or append-only.  (See "
"B<ioctl_iflags>(2).)"
msgstr ""
"Het te ontkoppelen bestand is gemarkeerd als onveranderlijk of alleen-"
"toevoegen. (Zie B<ioctl_iflags>(2).)"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EROFS>"
msgstr "B<EROFS>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "I<pathname> refers to a file on a read-only filesystem."
msgstr ""
"I<padnaam> verwijst naar een bestand op een alleen-lezen bestandsysteem."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The same errors that occur for B<unlink>()  and B<rmdir>(2)  can also occur "
"for B<unlinkat>().  The following additional errors can occur for "
"B<unlinkat>():"
msgstr ""
"Dezelfde fouten die optreden in B<unlink>()  en B<rmdir>(2)  kunnen ook "
"optreden in B<unlinkat>().  De volgende additionele fouten kunnen optreden "
"in B<unlinkat>():"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EBADF>"
msgstr "B<EBADF>"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"I<pathname> is relative but I<dirfd> is neither B<AT_FDCWD> nor a valid file "
"descriptor."
msgstr ""
"I<padnaam> is relatief en I<mapbi> is noch B<AT_FDCWD>  noch een geldige "
"bestandsindicator."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "An invalid flag value was specified in I<flags>."
msgstr "Een ongeldige vlag werd opgegeven in I<vlaggen>."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"I<pathname> refers to a directory, and B<AT_REMOVEDIR> was not specified in "
"I<flags>."
msgstr ""
"I<mapbi> wijst naar een map, en B<AT_REMOVEDIR> werd in I<vlaggen> niet "
"opgegeven."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"I<pathname> is relative and I<dirfd> is a file descriptor referring to a "
"file other than a directory."
msgstr ""
"I<padnaam> is relatief en I<mapbi> is een bestandsindicator die naar een "
"bestand wijst die geen map is."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIES"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"B<unlinkat>()  was added in Linux 2.6.16; library support was added in glibc "
"2.4."
msgstr ""
"B<unlinkat>() werd toegevoegd in Linux 2.6.16; bibliotheek ondersteuning "
"werd toegevoegd in glibc 2.4."

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "VOLDOET AAN"

#.  SVr4 documents additional error
#.  conditions EINTR, EMULTIHOP, ETXTBSY, ENOLINK.
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<unlink>(): SVr4, 4.3BSD, POSIX.1-2001, POSIX.1-2008."
msgstr "B<unlink>(): SVr4, 4.3BSD, POSIX.1-2001, POSIX.1-2008."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<unlinkat>(): POSIX.1-2008."
msgstr "B<unlinkat>(): POSIX.1-2008."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "OPMERKINGEN"

#. type: SS
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "glibc notes"
msgstr "glibc opmerkingen"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"On older kernels where B<unlinkat>()  is unavailable, the glibc wrapper "
"function falls back to the use of B<unlink>()  or B<rmdir>(2).  When "
"I<pathname> is a relative pathname, glibc constructs a pathname based on the "
"symbolic link in I</proc/self/fd> that corresponds to the I<dirfd> argument."
msgstr ""
"Op oudere kernels waar B<unlinkat>() niet beschikbaar is, valt de glibc "
"omwikkel functie terug op het gebruik van B<unlink> of B<rmdir>(2). Wanneer "
"I<padnaam> een relatieve padnaam is, dan construeert glibc een padnaam "
"gebaseerd op de symbolische koppeling in I</proc/self/fd> overeenkomende met "
"het I<dirfd> argument."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "BUGS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Infelicities in the protocol underlying NFS can cause the unexpected "
"disappearance of files which are still being used."
msgstr ""
"Ongelukkigheden in het protocol waar NFS op is gebaseerd kunnen het "
"onverwacht verdwijnen van bestanden veroorzaken die nog steeds gebruikt "
"worden."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ZIE OOK"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<rm>(1), B<unlink>(1), B<chmod>(2), B<link>(2), B<mknod>(2), B<open>(2), "
"B<rename>(2), B<rmdir>(2), B<mkfifo>(3), B<remove>(3), "
"B<path_resolution>(7), B<symlink>(7)"
msgstr ""
"B<rm>(1), B<unlink>(1), B<chmod>(2), B<link>(2), B<mknod>(2), B<open>(2), "
"B<rename>(2), B<rmdir>(2), B<mkfifo>(3), B<remove>(3), "
"B<path_resolution>(7), B<symlink>(7)"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "UNLINK"
msgstr "UNLINK"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15 september 2017"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Linux Programmeurs Handleiding"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid ""
"B<#include E<lt>fcntl.hE<gt>           >/* Definition of AT_* constants */\n"
"B<#include E<lt>unistd.hE<gt>>\n"
msgstr ""
"B<#include E<lt>fcntl.hE<gt>           >/* Definitie van AT_* constanten */\n"
"B<#include E<lt>unistd.hE<gt>>\n"

#. type: TP
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Since glibc 2.10:"
msgstr "Vanaf glibc 2.10:"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "_POSIX_C_SOURCE\\ E<gt>=\\ 200809L"
msgstr "_POSIX_C_SOURCE\\ E<gt>=\\ 200809L"

#. type: TP
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Before glibc 2.10:"
msgstr "Vóór glibc 2.10:"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "_ATFILE_SOURCE"
msgstr "_ATFILE_SOURCE"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"On success, zero is returned.  On error, -1 is returned, and I<errno> is set "
"appropriately."
msgstr ""
"Bij succes wordt nul teruggegeven. Bij falen wordt -1 teruggegeven en wordt "
"I<errno> overeenkomstig gezet."

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"I<pathname> refers to a directory.  (This is the non-POSIX value returned by "
"Linux since 2.1.132.)"
msgstr ""
"I<padnaam> wijst naar een map. (Dit is de niet-POSIX waarde teruggegeven "
"door Linux sinds 2.1.132.)"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "I<dirfd> is not a valid file descriptor."
msgstr "I<mapbi> is geen geldige bestandsindicator."

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"B<unlinkat>()  was added to Linux in kernel 2.6.16; library support was "
"added to glibc in version 2.4."
msgstr ""
"B<unlinkat>() werd toegevoegd in de Linux kernel 2.6.16; bibliotheek "
"ondersteuning werd toegevoegd in glibc versie 2.4."

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "VOLDOET AAN"

#. type: SS
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Glibc notes"
msgstr "Glibc-opmerkingen"

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "COLOFON"

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Deze pagina is onderdeel van release 5.10 van het Linux I<man-pages>-"
"project. Een beschrijving van het project, informatie over het melden van "
"bugs en de nieuwste versie van deze pagina zijn op \\%https://www.kernel.org/"
"doc/man-pages/ te vinden."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Deze pagina is onderdeel van release 4.16 van het Linux I<man-pages>-"
"project. Een beschrijving van het project, informatie over het melden van "
"bugs en de nieuwste versie van deze pagina zijn op \\%https://www.kernel.org/"
"doc/man-pages/ te vinden."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-12-04"
msgstr "4 december 2022"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.02"
msgstr "Linux man-pagina's 6.02"
