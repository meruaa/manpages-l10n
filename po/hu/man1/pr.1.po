# Hungarian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Nagy Viktor <chaos@inf.elte.hu>, 2001.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-02-15 19:06+0100\n"
"PO-Revision-Date: 2021-05-18 20:32+0200\n"
"Last-Translator: Nagy Viktor <chaos@inf.elte.hu>\n"
"Language-Team: Hungarian <>\n"
"Language: hu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.12.0\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "PR"
msgstr "PR"

#. type: TH
#: archlinux
#, no-wrap
msgid "November 2022"
msgstr "2022 november"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "GNU coreutils 9.1"
msgstr "GNU coreutils 9.1"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Felhasználói parancsok"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NÉV"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "pr - convert text files for printing"
msgstr "pr - szövegfájlokat konvertál nyomtatáshoz"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÖSSZEGZÉS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<pr> [I<\\,OPTION\\/>]... [I<\\,FILE\\/>]..."
msgstr "B<pr> [I<\\,KAPCSOLÓ\\/>]... [I<\\,FÁJL\\/>]..."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "LEÍRÁS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Paginate or columnate FILE(s) for printing."
msgstr "A FÁJLOK oldalakra tördelése vagy oszlopokba rendezése nyomtatáshoz."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "With no FILE, or when FILE is -, read standard input."
msgstr "Ha a FÁJL nincs megadva, vagy -, akkor a szabványos bemenetet olvassa."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Mandatory arguments to long options are mandatory for short options too."
msgstr ""
"A hosszú kapcsolók kötelező argumentumai a rövid kapcsolókhoz is kötelezők."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "+FIRST_PAGE[:LAST_PAGE], B<--pages>=I<\\,FIRST_PAGE[\\/>:LAST_PAGE]"
msgstr "+ELSŐ_OLDAL[:UTOLSÓ_OLDAL], B<--pages>=I<\\,ELSŐ_OLDAL[\\/>:UTOLSÓ_OLDAL]"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "begin [stop] printing with page FIRST_[LAST_]PAGE"
msgstr "a nyomtatás elkezdése [befejezése] az ELSŐ_[UTOLSÓ]_OLDALON"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-COLUMN>, B<--columns>=I<\\,COLUMN\\/>"
msgstr "B<-OSZLOP>, B<--columns>=I<\\,OSZLOP\\/>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"output COLUMN columns and print columns down, unless B<-a> is used. Balance "
"number of lines in the columns on each page"
msgstr ""
"OSZLOP darab oszlop előállítása és az oszlopok nyomtatása, lefelé, hacsak a -"
"a nincs megadva. A sorok számának kiegyensúlyozása az oszlopokban minden "
"egyes oldalon."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-a>, B<--across>"
msgstr "B<-a>, B<--across>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "print columns across rather than down, used together with B<-COLUMN>"
msgstr ""
"oszlopok kiírása vízszintesen és nem függőlegesen, a B<-OSZLOP> kapcsolóval "
"együtt használatos"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-c>, B<--show-control-chars>"
msgstr "B<-c>, B<--show-control-chars>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "use hat notation (^G) and octal backslash notation"
msgstr "kalapos (^G) és oktális visszaper jelölés használata"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-d>, B<--double-space>"
msgstr "B<-d>, B<--double-space>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "double space the output"
msgstr "a kimenet dupla kitöltése"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-D>, B<--date-format>=I<\\,FORMAT\\/>"
msgstr "B<-D>, B<--date-format>=I<\\,FORMÁTUM\\/>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "use FORMAT for the header date"
msgstr "a FORMÁTUM használata a fejléc dátumához"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-e[CHAR[WIDTH]]>, B<--expand-tabs>[=I<\\,CHAR[WIDTH]\\/>]"
msgstr "B<-e[KAR[SZÉLESSÉG]]>, B<--expand-tabs>[=I<\\,KAR[SZÉLESSÉG]\\/>]"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "expand input CHARs (TABs) to tab WIDTH (8)"
msgstr "a bemeneti KARAKTEREK (TAB-ok) kiterjesztése a tab SZÉLESSÉGÉRE (8)"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-F>, B<-f>, B<--form-feed>"
msgstr "B<-F>, B<-f>, B<--form-feed>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"use form feeds instead of newlines to separate pages (by a 3-line page "
"header with B<-F> or a 5-line header and trailer without B<-F>)"
msgstr ""
"lapdobások használata új sorok helyett az oldalak elválasztására (egy 3 "
"soros oldalfejléccel a B<-F> vagy egy 5 soros fejléccel és bevezetővel az B<-"
"F> nélkül)"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-h>, B<--header>=I<\\,HEADER\\/>"
msgstr "B<-h>, B<--header>=I<\\,FEJLÉC\\/>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"use a centered HEADER instead of filename in page header, B<-h> \"\" prints "
"a blank line, don't use B<-h>\"\""
msgstr ""
"egy középre igazított FEJLÉC használata a fájlnév helyett az oldalfejlécben, "
"a B<-h> \"\" egy üres sort nyomtat, ne használja a B<-h>\"\" kapcsolót"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-i[CHAR[WIDTH]]>, B<--output-tabs>[=I<\\,CHAR[WIDTH]\\/>]"
msgstr "B<-i[KAR[SZÉLESSÉG]]>, B<--output-tabs>[=I<\\,KAR[SZÉLESSÉG]\\/>]"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "replace spaces with CHARs (TABs) to tab WIDTH (8)"
msgstr "a szóközök cseréje KARAKTEREKRE (TAB-okra) a tab SZÉLESSÉGÉIG (8)"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-J>, B<--join-lines>"
msgstr "B<-J>, B<--join-lines>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"merge full lines, turns off B<-W> line truncation, no column alignment, B<--"
"sep-string>[=I<\\,STRING\\/>] sets separators"
msgstr ""
"teljes sorok összefésülése, kikapcsolja a B<-W> sorcsonkítást, nincs "
"oszlopigazítás, a B<--sep-string>[=I<\\,KARAKTERLÁNC\\/>] beállítja az "
"elválasztókat"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-l>, B<--length>=I<\\,PAGE_LENGTH\\/>"
msgstr "B<-l>, B<--length>=I<\\,OLDALHOSSZ\\/>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"set the page length to PAGE_LENGTH (66) lines (default number of lines of "
"text 56, and with B<-F> 63).  implies B<-t> if PAGE_LENGTH E<lt>= 10"
msgstr ""
"beállítja az oldalhosszúságot OLDALHOSSZ (66) sorra (a szövegsorok "
"alapértelmezett száma 56, és a B<-F> esetén 63). a B<-t> használata "
"következik belőle, ha az OLDALHOSSZ E<lt>= 10"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-m>, B<--merge>"
msgstr "B<-m>, B<--merge>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"print all files in parallel, one in each column, truncate lines, but join "
"lines of full length with B<-J>"
msgstr ""
"az összes fájl párhuzamos nyomtatása, egyet minden oszlopba, a sorokat "
"csonkítja, de a teljes hosszúságú sorokat a B<-J> használatakor "
"összekapcsolja"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-n[SEP[DIGITS]]>, B<--number-lines>[=I<\\,SEP[DIGITS]\\/>]"
msgstr "B<-n[ELV[SZÁMJEGYEK]>, B<--number-lines>[=I<\\,ELV[SZÁMJEGYEK]\\/>]"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"number lines, use DIGITS (5) digits, then SEP (TAB), default counting starts "
"with 1st line of input file"
msgstr ""
"sorok számozása, SZÁMJEGYEK (5) darab számjegy, majd az ELVÁLASZTÓ (TAB) "
"használatával, a számolás alapértelmezésben a bemeneti fájl első sorával "
"kezdődik."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-N>, B<--first-line-number>=I<\\,NUMBER\\/>"
msgstr "B<-N>, B<--first-line-number>=I<\\,SZÁM\\/>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"start counting with NUMBER at 1st line of first page printed (see "
"+FIRST_PAGE)"
msgstr ""
"a számolás kezdése a SZÁMMAL az első kinyomtatott oldal első sorában (lásd "
"+ELSŐ_OLDAL)"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-o>, B<--indent>=I<\\,MARGIN\\/>"
msgstr "B<-o>, B<--indent>=I<\\,MARGÓ\\/>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"offset each line with MARGIN (zero) spaces, do not affect B<-w> or B<-W>, "
"MARGIN will be added to PAGE_WIDTH"
msgstr ""
"minden egyes sor eltolása MARGÓ (nulla) szóközzel, nincs hatással a B<-w> "
"vagy B<-W> kapcsolókra, a MARGÓ az OLDAL_SZÉLESSÉGHEZ lesz hozzáadva."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-r>, B<--no-file-warnings>"
msgstr "B<-r>, B<--no-file-warnings>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "omit warning when a file cannot be opened"
msgstr "figyelmeztetés kihagyása, ha egy fájl nem nyitható meg"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-s[CHAR]>, B<--separator>[=I<\\,CHAR\\/>]"
msgstr "B<-s[KARAKTER]>, B<--separator>[=I<\\,KARAKTER\\/>]"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"separate columns by a single character, default for CHAR is the "
"E<lt>TABE<gt> character without B<-w> and 'no char' with B<-w>.  B<-s[CHAR]> "
"turns off line truncation of all 3 column options (B<-COLUMN>|-a B<-COLUMN>|-"
"m) except B<-w> is set"
msgstr ""
"oszlopok elválasztása egyetlen karakterrel, a KARAKTER alapértelmezetten "
"E<lt>TABE<gt> karakter a -w nélkül és „semmi” a B<-w> esetén. A B<-"
"s[KARAKTER]> kikapcsolja a sorcsonkítást mind a 3 oszlopkapcsolóhoz (B<-"
"OSZLOP>|-a B<-OSZLOP>|-m), kivéve ha a B<-w> be van állítva"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-S[STRING]>, B<--sep-string>[=I<\\,STRING\\/>]"
msgstr "B<-S[KARAKTERLÁNC]>, B<--sep-string>[=I<\\,KARAKTERLÁNC\\/>]"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"separate columns by STRING, without B<-S>: Default separator E<lt>TABE<gt> "
"with B<-J> and E<lt>spaceE<gt> otherwise (same as B<-S>\" \"), no effect on "
"column options"
msgstr ""
"az oszlopok elválasztása a KARAKTERLÁNCCAL, a B<-S> nélkül: az "
"alapértelmezett elválasztó a E<lt>TABE<gt> a B<-J> kapcsolóval és "
"E<lt>szóközE<gt> egyébként (ugyanaz, mint a B<-S>\" \") nincs hatással az "
"oszlopkapcsolókra"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-t>, B<--omit-header>"
msgstr "B<-t>, B<--omit-header>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "omit page headers and trailers; implied if PAGE_LENGTH E<lt>= 10"
msgstr ""
"oldalfejlécek és befejezők kihagyása használata az OLDALHOSSZ E<lt>= 10-ből "
"következik"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-T>, B<--omit-pagination>"
msgstr "B<-T>, B<--omit-pagination>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"omit page headers and trailers, eliminate any pagination by form feeds set "
"in input files"
msgstr ""
"oldalfejlécek és befejezők kihagyása, a bemeneti fájlokban található "
"lapdobások által beállított oldalakra tördelés megszüntetése"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-v>, B<--show-nonprinting>"
msgstr "B<-v>, B<--show-nonprinting>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "use octal backslash notation"
msgstr "oktális visszaper jelölés használata"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-w>, B<--width>=I<\\,PAGE_WIDTH\\/>"
msgstr "B<-w>, B<--width>=I<\\,OLDAL_SZÉLESSÉG\\/>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"set page width to PAGE_WIDTH (72) characters for multiple text-column output "
"only, B<-s[char]> turns off (72)"
msgstr ""
"az oldalszélesség beállítása OLDAL_SZÉLESSÉG (72) karakterre, csak több "
"szöveges oszlopot tartalmazó kimenethez, az B<-s[karakter]> kikapcsolja (72)"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-W>, B<--page-width>=I<\\,PAGE_WIDTH\\/>"
msgstr "B<-W>, B<--page-width>=I<\\,OLDAL_SZÉLESSÉG\\/>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"set page width to PAGE_WIDTH (72) characters always, truncate lines, except "
"B<-J> option is set, no interference with B<-S> or B<-s>"
msgstr ""
"az oldalszélesség beállítása OLDAL_SZÉLESSÉG (72) karakterre mindig, a sorok "
"csonkítása, kivéve ha a B<-J> kapcsoló be van állítva, nincs interferencia a "
"B<-S> vagy B<-s> kapcsolókkal"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "display this help and exit"
msgstr "ezen súgó megjelenítése és kilépés"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "output version information and exit"
msgstr "verzióinformációk megjelenítése és kilépés"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "SZERZŐ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Written by Pete TerMaat and Roland Huebner."
msgstr "Írta Pete TerMaat és Roland Huebner."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "HIBÁK JELENTÉSE"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"A(z) GNU coreutils online súgója: E<lt>https://www.gnu.org/software/"
"coreutils/E<gt>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "SZERZŐI JOG"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  A licenc GPLv3+: a GNU "
"GPL 3. vagy újabb változata: E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Ez egy szabad szoftver, terjesztheti és/vagy módosíthatja. NINCS GARANCIA, a "
"törvény által engedélyezett mértékig."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "LÁSD MÉG"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Full documentation E<lt>https://www.gnu.org/software/coreutils/prE<gt>"
msgstr ""
"Teljes dokumentáció E<lt>https://www.gnu.org/software/coreutils/prE<gt>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "or available locally via: info \\(aq(coreutils) pr invocation\\(aq"
msgstr "vagy helyileg elérhető: info \\(aq(coreutils) pr invocation\\(aq"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "September 2020"
msgstr "2020 szeptember"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "GNU coreutils 8.32"
msgstr "GNU coreutils 8.32"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  A licenc GPLv3+: a GNU "
"GPL 3. vagy újabb változata: E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "September 2022"
msgstr "2022 szeptember"

#. type: TH
#: fedora-38 fedora-rawhide
#, no-wrap
msgid "January 2023"
msgstr "2023. január"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "April 2022"
msgstr "2022 április"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "October 2021"
msgstr "2021 október"
