# Brazilian Portuguese translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Marcelo M. de Abreu <mmabreu@terra.com.br>, 2001.
# André Luiz Fassone <lonely_wolf@ig.com.br>, 2001.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-02-20 20:10+0100\n"
"PO-Revision-Date: 2001-05-31 17:26+0200\n"
"Last-Translator: André Luiz Fassone <lonely_wolf@ig.com.br>\n"
"Language-Team: Brazilian Portuguese <debian-l10n-portuguese@lists.debian."
"org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 20.04.1\n"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<inet_ntop>()"
msgid "inet_ntop"
msgstr "B<inet_ntop>()"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 fevereiro 2023"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOME"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy
msgid "inet_ntop - convert IPv4 and IPv6 addresses from binary to text form"
msgstr "inet_ntop - analisa estruturas de endereço de rede"

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Biblioteca C Padrão (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSE"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>arpa/inet.hE<gt>>\n"
msgstr "B<#include E<lt>arpa/inet.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "B<const char *inet_ntop(int >I<af>B<, const void *>I<src>B<,>\n"
#| "B<                      char *>I<dst>B<, socklen_t >I<size>B<);>\n"
msgid ""
"B<const char *inet_ntop(int >I<af>B<, const void *restrict >I<src>B<,>\n"
"B<                      char >I<dst>B<[restrict .>I<size>B<], socklen_t >I<size>B<);>\n"
msgstr ""
"B<const char *inet_ntop(int >I<af>B<, const void *>I<src>B<,>\n"
"B<                      char *>I<dst>B<, socklen_t >I<size>B<);>\n"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIÇÃO"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "This function converts the network address structure I<src> in the I<af> "
#| "address family into a character string, which is copied to a character "
#| "buffer I<dst>, which is I<cnt> bytes long."
msgid ""
"This function converts the network address structure I<src> in the I<af> "
"address family into a character string.  The resulting string is copied to "
"the buffer pointed to by I<dst>, which must be a non-null pointer.  The "
"caller specifies the number of bytes available in this buffer in the "
"argument I<size>."
msgstr ""
"Esta função converte a estrutura de endereço de rede I<scr> na família de "
"endereço I<af> em uma seqüencia, a qual é copiada no trecho de memória "
"alocado I<dst>, que tem comprimento de I<cnt> bytes."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<inet_ntop>()  extends the B<inet_ntoa>(3)  function to support multiple "
"address families, B<inet_ntoa>(3)  is now considered to be deprecated in "
"favor of B<inet_ntop>().  The following address families are currently "
"supported:"
msgstr ""
"B<inet_ntop>() estende a função B<inet_ntoa>(3) para suportar múltiplas "
"famílias de endereço; B<inet_ntoa>(3) é agora considerada a ser depreciada "
"em favor de B<inet_ntop>(). As seguintes famílias de endereço são atualmente "
"suportadas:"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<AF_INET>"
msgstr "B<AF_INET>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "I<src> points to a I<struct in_addr> (network byte order format)  which "
#| "is converted to an IPv4 network address in the dotted-quad format, "
#| "\"I<ddd.ddd.ddd.ddd>\".  The buffer I<dst> must be at least "
#| "B<INET_ADDRSTRLEN> bytes long."
msgid ""
"I<src> points to a I<struct in_addr> (in network byte order)  which is "
"converted to an IPv4 network address in the dotted-decimal format, \"I<ddd."
"ddd.ddd.ddd>\".  The buffer I<dst> must be at least B<INET_ADDRSTRLEN> bytes "
"long."
msgstr ""
"I<src> aponta para uma I<struct in_addr> (formato de ordem de byte da rede) "
"a qual é convertida para um endereço de rede IPv4 no formato da quadra "
"pontuada, 'I<ddd.ddd.ddd.ddd>'. O trecho de memória I<dst> deve ter no "
"mínimo B<INET_ADDRSTRLEN> bytes de comprimento."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<AF_INET6>"
msgstr "B<AF_INET6>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "I<src> points to a I<struct in6_addr> (network byte order format)  which "
#| "is converted to a representation of this address in the most appropriate "
#| "IPv6 network address format for this address.  The buffer I<dst> must be "
#| "at least B<INET6_ADDRSTRLEN> bytes long."
msgid ""
"I<src> points to a I<struct in6_addr> (in network byte order)  which is "
"converted to a representation of this address in the most appropriate IPv6 "
"network address format for this address.  The buffer I<dst> must be at least "
"B<INET6_ADDRSTRLEN> bytes long."
msgstr ""
"I src aponta para uma I<struct in6_addr> (formato de ordem de byte da rede) "
"a qual é convertida para uma representação desse endereço no formato de "
"endereço de rede IPv6 mais apropriado para ele. O trecho de memória I<dst> "
"deve ter pelo menos B<INET6_ADDRSTRLEN> bytes de comprimento."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALOR DE RETORNO"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<inet_ntop> returns a non-null pointer to I<dst>.  NULL is returned if "
#| "there was an error, with I<errno> set to B<EAFNOSUPPORT> if I<af> was not "
#| "set to a valid address family, or to B<ENOSPC> if the converted address "
#| "string would exceed the size of I<dst> given by the I<cnt> argument."
msgid ""
"On success, B<inet_ntop>()  returns a non-null pointer to I<dst>.  NULL is "
"returned if there was an error, with I<errno> set to indicate the error."
msgstr ""
"B<inet_ntop> retorna um ponteiro não nulo para I<dst>. NULO é retornado se "
"houve um erro, com I<errno> definido para B<EAFNOSUPPORT> se I<af> não foi "
"definido para uma família válida de endereço, ou para B<ENOSPC> se a string "
"do endereço convertido excederia o tamanho de I<dst> dado pelo argumento "
"I<cnt>."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERROS"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EAFNOSUPPORT>"
msgstr "B<EAFNOSUPPORT>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy
#| msgid "I<fd> is not a valid file descriptor."
msgid "I<af> was not a valid address family."
msgstr "I<fd> não é válido como descritor de arquivos."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOSPC>"
msgstr "B<ENOSPC>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The converted address string would exceed the size given by I<size>."
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATRIBUTOS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Para uma explicação dos termos usados nesta seção, consulte B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interface"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Atributo"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valor"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<inet_ntop>()"
msgstr "B<inet_ntop>()"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Thread safety"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe locale"
msgstr "MT-Safe locale"

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "PADRÕES"

#.  2.1.3: size_t, 2.1.91: socklen_t
#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"POSIX.1-2001, POSIX.1-2008.  Note that RFC\\ 2553 defines a prototype where "
"the last argument I<size> is of type I<size_t>.  Many systems follow RFC\\ "
"2553.  glibc 2.0 and 2.1 have I<size_t>, but 2.2 and later have I<socklen_t>."
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "BUGS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy
#| msgid "B<AF_INET6> converts IPv6-mapped IPv4 addresses into an IPv6 format."
msgid "B<AF_INET6> converts IPv4-mapped IPv6 addresses into an IPv6 format."
msgstr ""
"B<AF_INET6> converte endereços IPv4 mapeados para IPv6 para um formato IPv6."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "EXEMPLOS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "See B<inet_pton>(3)."
msgstr "Veja B<inet_pton>(3)."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VEJA TAMBÉM"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<getnameinfo>(3), B<inet>(3), B<inet_pton>(3)"
msgstr "B<getnameinfo>(3), B<inet>(3), B<inet_pton>(3)"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "INET_NTOP"
msgstr ""

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "2020-06-09"
msgstr "9 junho 2020"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manual do Programador do Linux"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid ""
"B<const char *inet_ntop(int >I<af>B<, const void *>I<src>B<,>\n"
"B<                      char *>I<dst>B<, socklen_t >I<size>B<);>\n"
msgstr ""
"B<const char *inet_ntop(int >I<af>B<, const void *>I<src>B<,>\n"
"B<                      char *>I<dst>B<, socklen_t >I<size>B<);>\n"

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "DE ACORDO COM"

#.  2.1.3: size_t, 2.1.91: socklen_t
#. type: Plain text
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"POSIX.1-2001, POSIX.1-2008.  Note that RFC\\ 2553 defines a prototype where "
"the last argument I<size> is of type I<size_t>.  Many systems follow RFC\\ "
"2553.  Glibc 2.0 and 2.1 have I<size_t>, but 2.2 and later have I<socklen_t>."
msgstr ""

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "COLOFÃO"

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Esta página faz parte da versão 5.10 do projeto Linux I<man-pages>. Uma "
"descrição do projeto, informações sobre relatórios de bugs e a versão mais "
"recente desta página podem ser encontradas em \\%https://www.kernel.org/doc/"
"man-pages/."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15 setembro 2017"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "EXAMPLE"
msgstr "EXEMPLO"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Esta página faz parte da versão 4.16 do projeto Linux I<man-pages>. Uma "
"descrição do projeto, informações sobre relatórios de bugs e a versão mais "
"recente desta página podem ser encontradas em \\%https://www.kernel.org/doc/"
"man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-12-15"
msgstr "15 dezembro 2022"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.02"
msgstr "Linux man-pages 6.02"
