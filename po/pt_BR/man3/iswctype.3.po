# Brazilian Portuguese translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Felipe M Pereira <Felipe.Pereira@ic.unicamp.br>, 2001.
# André Luiz Fassone <lonely_wolf@ig.com.br>, 2001.
# Rafael Fontenelle <rafaelff@gnome.org>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-02-20 20:12+0100\n"
"PO-Revision-Date: 2021-06-28 12:21-0300\n"
"Last-Translator: Rafael Fontenelle <rafaelff@gnome.org>\n"
"Language-Team: Brazilian Portuguese <debian-l10n-portuguese@lists.debian."
"org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1)\n"
"X-Generator: Gtranslator 40.0\n"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<iswctype>()"
msgid "iswctype"
msgstr "B<iswctype>()"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-12-15"
msgstr "15 dezembro 2022"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOME"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "iswctype - wide-character classification"
msgstr "iswctype - classificação de caractere largo"

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Biblioteca C Padrão (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSE"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>wctype.hE<gt>>\n"
msgstr "B<#include E<lt>wctype.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<int iswctype(wint_t >I<wc>B<, wctype_t >I<desc>B<);>\n"
msgstr "B<int iswctype(wint_t >I<wc>B<, wctype_t >I<desc>B<);>\n"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIÇÃO"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "If I<wc> is a wide character having the character property designated by "
#| "I<desc> (or in other words: belongs to the character class designated by "
#| "I<desc>), the B<iswctype>()  function returns nonzero.  Otherwise, it "
#| "returns zero.  If I<wc> is B<WEOF>, zero is returned."
msgid ""
"If I<wc> is a wide character having the character property designated by "
"I<desc> (or in other words: belongs to the character class designated by "
"I<desc>), then the B<iswctype>()  function returns nonzero.  Otherwise, it "
"returns zero.  If I<wc> is B<WEOF>, zero is returned."
msgstr ""
"Se I<wc> é um caractere largo tendo a propriedade de caractere designada por "
"I<desc> (em outras palavras: pertence à classe de caracteres designada por "
"I<desc>), a função B<iswctype>() retorna um valor diferente de zero. Caso "
"contrário, ela retorna zero. Se I<wc> é B<WEOF>, zero é retornado."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"I<desc> must be a character property descriptor returned by the "
"B<wctype>(3)  function."
msgstr ""
"I<desc> deve ser um descritor de propriedade de caractere retornado pela "
"função B<wctype>(3)."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALOR DE RETORNO"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The B<iswctype>()  function returns nonzero if the I<wc> has the designated "
"property.  Otherwise, it returns 0."
msgstr ""
"A função B<iswctype>() retorna um valor diferente de zero se I<wc>() possui "
"a propriedade designada. Caso contrário ela retorna 0."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATRIBUTOS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Para uma explicação dos termos usados nesta seção, consulte B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interface"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Atributo"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valor"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<iswctype>()"
msgstr "B<iswctype>()"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Thread safety"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "PADRÕES"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "POSIX.1-2001, POSIX.1-2008, C99."
msgstr "POSIX.1-2001, POSIX.1-2008, C99."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTAS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The behavior of B<iswctype>()  depends on the B<LC_CTYPE> category of the "
"current locale."
msgstr ""
"O comportamento de B<iswctype>() depende da categoria B<LC_CTYPE> da "
"localidade atual."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VEJA TAMBÉM"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<iswalnum>(3), B<iswalpha>(3), B<iswblank>(3), B<iswcntrl>(3), "
"B<iswdigit>(3), B<iswgraph>(3), B<iswlower>(3), B<iswprint>(3), "
"B<iswpunct>(3), B<iswspace>(3), B<iswupper>(3), B<iswxdigit>(3), B<wctype>(3)"
msgstr ""
"B<iswalnum>(3), B<iswalpha>(3), B<iswblank>(3), B<iswcntrl>(3), "
"B<iswdigit>(3), B<iswgraph>(3), B<iswlower>(3), B<iswprint>(3), "
"B<iswpunct>(3), B<iswspace>(3), B<iswupper>(3), B<iswxdigit>(3), B<wctype>(3)"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "ISWCTYPE"
msgstr "ISWCTYPE"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "2015-08-08"
msgstr "8 agosto 2015"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "GNU"
msgstr "GNU"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manual do Programador do Linux"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"If I<wc> is a wide character having the character property designated by "
"I<desc> (or in other words: belongs to the character class designated by "
"I<desc>), the B<iswctype>()  function returns nonzero.  Otherwise, it "
"returns zero.  If I<wc> is B<WEOF>, zero is returned."
msgstr ""
"Se I<wc> é um caractere largo tendo a propriedade de caractere designada por "
"I<desc> (em outras palavras: pertence à classe de caracteres designada por "
"I<desc>), a função B<iswctype>() retorna um valor diferente de zero. Caso "
"contrário, ela retorna zero. Se I<wc> é B<WEOF>, zero é retornado."

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "DE ACORDO COM"

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "COLOFÃO"

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Esta página faz parte da versão 5.10 do projeto Linux I<man-pages>. Uma "
"descrição do projeto, informações sobre relatórios de bugs e a versão mais "
"recente desta página podem ser encontradas em \\%https://www.kernel.org/doc/"
"man-pages/."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Esta página faz parte da versão 4.16 do projeto Linux I<man-pages>. Uma "
"descrição do projeto, informações sobre relatórios de bugs e a versão mais "
"recente desta página podem ser encontradas em \\%https://www.kernel.org/doc/"
"man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.02"
msgstr "Linux man-pages 6.02"
