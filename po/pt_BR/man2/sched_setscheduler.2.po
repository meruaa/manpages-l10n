# Brazilian Portuguese translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# André Luiz Fassone <lonely_wolf@ig.com.br>, 2001.
# Marcelo Pereira da Silva <marcelo@pereira.com>, 2001.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-02-20 20:28+0100\n"
"PO-Revision-Date: 2001-06-02 19:20-0300\n"
"Last-Translator: Marcelo Pereira da Silva <marcelo@pereira.com>\n"
"Language-Team: Brazilian Portuguese <debian-l10n-portuguese@lists.debian."
"org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Virtaal 1.0.0-beta1\n"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "sched_setscheduler"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-10-30"
msgstr "30 outubro 2022"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOME"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "sched_setscheduler, sched_getscheduler - set and get scheduling algorithm/"
#| "parameters"
msgid ""
"sched_setscheduler, sched_getscheduler - set and get scheduling policy/"
"parameters"
msgstr ""
"sched_setscheduler, sched_getscheduler - seleciona e obtém os parâmetros/"
"algorítmos de agendamento"

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Biblioteca C Padrão (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSE"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>sched.hE<gt>>\n"
msgstr "B<#include E<lt>sched.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "B<int sched_setscheduler(pid_t >I<pid>B<, int >I<policy>B<,>\n"
#| "B<                       const struct sched_param *>I<param>B<);>\n"
msgid ""
"B<int sched_setscheduler(pid_t >I<pid>B<, int >I<policy>B<,>\n"
"B<                       const struct sched_param *>I<param>B<);>\n"
"B<int sched_getscheduler(pid_t >I<pid>B<);>\n"
msgstr ""
"B<int sched_setscheduler(pid_t >I<pid>B<, int >I<policy>B<,>\n"
"B<                       const struct sched_param *>I<param>B<);>\n"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIÇÃO"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<sched_getscheduler> queries the scheduling policy currently applied to "
#| "the process identified by I<pid>. If I<pid> equals zero, the policy of "
#| "the calling process will be retrieved."
msgid ""
"The B<sched_setscheduler>()  system call sets both the scheduling policy and "
"parameters for the thread whose ID is specified in I<pid>.  If I<pid> equals "
"zero, the scheduling policy and parameters of the calling thread will be set."
msgstr ""
"B<sched_getscheduler> questiona a política de agendamento atualmente "
"aplicada para o processo identificado por I<pid>. Se I<pid> é igual a zero, "
"a política do processo solicitante será recuperada."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The scheduling parameters are specified in the I<param> argument, which is a "
"pointer to a structure of the following form:"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"struct sched_param {\n"
"    ...\n"
"    int sched_priority;\n"
"    ...\n"
"};\n"
msgstr ""
"struct sched_param {\n"
"    ...\n"
"    int sched_priority;\n"
"    ...\n"
"};\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"In the current implementation, the structure contains only one field, "
"I<sched_priority>.  The interpretation of I<param> depends on the selected "
"policy."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Currently, Linux supports the following \"normal\" (i.e., non-real-time) "
"scheduling policies as values that may be specified in I<policy>:"
msgstr ""

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<SCHED_OTHER>"
msgstr "B<SCHED_OTHER>"

#.  In the 2.6 kernel sources, SCHED_OTHER is actually called
#.  SCHED_NORMAL.
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "the standard round-robin time-sharing policy;"
msgstr ""

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<SCHED_BATCH>"
msgstr "B<SCHED_BATCH>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "for \"batch\" style execution of processes; and"
msgstr ""

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<SCHED_IDLE>"
msgstr "B<SCHED_IDLE>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "for running I<very> low priority background jobs."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "For each of the above policies, I<param-E<gt>sched_priority> must be 0."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Various \"real-time\" policies are also supported, for special time-critical "
"applications that need precise control over the way in which runnable "
"threads are selected for execution.  For the rules governing when a process "
"may use these policies, see B<sched>(7).  The real-time policies that may be "
"specified in I<policy> are:"
msgstr ""

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<SCHED_FIFO>"
msgstr "B<SCHED_FIFO>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "a first-in, first-out policy; and"
msgstr ""

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<SCHED_RR>"
msgstr "B<SCHED_RR>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "a round-robin policy."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For each of the above policies, I<param-E<gt>sched_priority> specifies a "
"scheduling priority for the thread.  This is a number in the range returned "
"by calling B<sched_get_priority_min>(2)  and B<sched_get_priority_max>(2)  "
"with the specified I<policy>.  On Linux, these system calls return, "
"respectively, 1 and 99."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Since Linux 2.6.32, the B<SCHED_RESET_ON_FORK> flag can be ORed in I<policy> "
"when calling B<sched_setscheduler>().  As a result of including this flag, "
"children created by B<fork>(2)  do not inherit privileged scheduling "
"policies.  See B<sched>(7)  for details."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<sched_getscheduler> queries the scheduling policy currently applied to "
#| "the process identified by I<pid>. If I<pid> equals zero, the policy of "
#| "the calling process will be retrieved."
msgid ""
"B<sched_getscheduler>()  returns the current scheduling policy of the thread "
"identified by I<pid>.  If I<pid> equals zero, the policy of the calling "
"thread will be retrieved."
msgstr ""
"B<sched_getscheduler> questiona a política de agendamento atualmente "
"aplicada para o processo identificado por I<pid>. Se I<pid> é igual a zero, "
"a política do processo solicitante será recuperada."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALOR DE RETORNO"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "On success, B<sched_setscheduler> returns zero.  On success, "
#| "B<sched_getscheduler> returns the policy for the process (a non-negative "
#| "integer).  On error, -1 is returned, I<errno> is set appropriately."
msgid ""
"On success, B<sched_setscheduler>()  returns zero.  On success, "
"B<sched_getscheduler>()  returns the policy for the thread (a nonnegative "
"integer).  On error, both calls return -1, and I<errno> is set to indicate "
"the error."
msgstr ""
"Em caso de sucesso, B<sched_setscheduler> devolve 0. Em caso de sucesso, "
"B<sched_getscheduler> devolve a política para o processo (um inteiro não "
"negativo). Caso contrário, -1 é devolvido e I<errno> é selecionado "
"adequadamente."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERROS"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Invalid arguments: I<pid> is negative or I<param> is NULL."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"(B<sched_setscheduler>())  I<policy> is not one of the recognized policies."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"(B<sched_setscheduler>())  I<param> does not make sense for the specified "
"I<policy>."
msgstr ""

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM>"
msgstr "B<EPERM>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The calling thread does not have appropriate privileges."
msgstr ""

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ESRCH>"
msgstr "B<ESRCH>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy
#| msgid "The process whose ID is I<pid> could not be found."
msgid "The thread whose ID is I<pid> could not be found."
msgstr "O processo cujo ID é I<pid> não pôde ser encontrado."

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "PADRÕES"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"POSIX.1-2001, POSIX.1-2008 (but see BUGS below).  The B<SCHED_BATCH> and "
"B<SCHED_IDLE> policies are Linux-specific."
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTAS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Further details of the semantics of all of the above \"normal\" and \"real-"
"time\" scheduling policies can be found in the B<sched>(7)  manual page.  "
"That page also describes an additional policy, B<SCHED_DEADLINE>, which is "
"settable only via B<sched_setattr>(2)."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"POSIX systems on which B<sched_setscheduler>()  and B<sched_getscheduler>()  "
"are available define B<_POSIX_PRIORITY_SCHEDULING> in I<E<lt>unistd.hE<gt>>."
msgstr ""
"Sistemas POSIX nos quais estão disponíveis B<sched_setscheduler>() e "
"B<sched_getscheduler>() definem B<_POSIX_PRIORITY_SCHEDULING> na "
"I<E<lt>unistd.hE<gt>>."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"POSIX.1 does not detail the permissions that an unprivileged thread requires "
"in order to call B<sched_setscheduler>(), and details vary across systems.  "
"For example, the Solaris 7 manual page says that the real or effective user "
"ID of the caller must match the real user ID or the save set-user-ID of the "
"target."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The scheduling policy and parameters are in fact per-thread attributes on "
"Linux.  The value returned from a call to B<gettid>(2)  can be passed in the "
"argument I<pid>.  Specifying I<pid> as 0 will operate on the attributes of "
"the calling thread, and passing the value returned from a call to "
"B<getpid>(2)  will operate on the attributes of the main thread of the "
"thread group.  (If you are using the POSIX threads API, then use "
"B<pthread_setschedparam>(3), B<pthread_getschedparam>(3), and "
"B<pthread_setschedprio>(3), instead of the B<sched_*>(2)  system calls.)"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "BUGS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"POSIX.1 says that on success, B<sched_setscheduler>()  should return the "
"previous scheduling policy.  Linux B<sched_setscheduler>()  does not conform "
"to this requirement, since it always returns 0 on success."
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VEJA TAMBÉM"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<chrt>(1), B<nice>(2), B<sched_get_priority_max>(2), "
"B<sched_get_priority_min>(2), B<sched_getaffinity>(2), B<sched_getattr>(2), "
"B<sched_getparam>(2), B<sched_rr_get_interval>(2), B<sched_setaffinity>(2), "
"B<sched_setattr>(2), B<sched_setparam>(2), B<sched_yield>(2), "
"B<setpriority>(2), B<capabilities>(7), B<cpuset>(7), B<sched>(7)"
msgstr ""
"B<chrt>(1), B<nice>(2), B<sched_get_priority_max>(2), "
"B<sched_get_priority_min>(2), B<sched_getaffinity>(2), B<sched_getattr>(2), "
"B<sched_getparam>(2), B<sched_rr_get_interval>(2), B<sched_setaffinity>(2), "
"B<sched_setattr>(2), B<sched_setparam>(2), B<sched_yield>(2), "
"B<setpriority>(2), B<capabilities>(7), B<cpuset>(7), B<sched>(7)"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "SCHED_SETSCHEDULER"
msgstr "SCHED_SETSCHEDULER"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15 setembro 2017"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manual do Programador do Linux"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid ""
"B<int sched_setscheduler(pid_t >I<pid>B<, int >I<policy>B<,>\n"
"B<                       const struct sched_param *>I<param>B<);>\n"
msgstr ""
"B<int sched_setscheduler(pid_t >I<pid>B<, int >I<policy>B<,>\n"
"B<                       const struct sched_param *>I<param>B<);>\n"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "B<int sched_getscheduler(pid_t >I<pid>B<);>\n"
msgstr "B<int sched_getscheduler(pid_t >I<pid>B<);>\n"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, fuzzy
#| msgid ""
#| "On success, B<sched_setscheduler> returns zero.  On success, "
#| "B<sched_getscheduler> returns the policy for the process (a non-negative "
#| "integer).  On error, -1 is returned, I<errno> is set appropriately."
msgid ""
"On success, B<sched_setscheduler>()  returns zero.  On success, "
"B<sched_getscheduler>()  returns the policy for the thread (a nonnegative "
"integer).  On error, both calls return -1, and I<errno> is set appropriately."
msgstr ""
"Em caso de sucesso, B<sched_setscheduler> devolve 0. Em caso de sucesso, "
"B<sched_getscheduler> devolve a política para o processo (um inteiro não "
"negativo). Caso contrário, -1 é devolvido e I<errno> é selecionado "
"adequadamente."

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "DE ACORDO COM"

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "COLOFÃO"

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Esta página faz parte da versão 5.10 do projeto Linux I<man-pages>. Uma "
"descrição do projeto, informações sobre relatórios de bugs e a versão mais "
"recente desta página podem ser encontradas em \\%https://www.kernel.org/doc/"
"man-pages/."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Esta página faz parte da versão 4.16 do projeto Linux I<man-pages>. Uma "
"descrição do projeto, informações sobre relatórios de bugs e a versão mais "
"recente desta página podem ser encontradas em \\%https://www.kernel.org/doc/"
"man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.02"
msgstr "Linux man-pages 6.02"
