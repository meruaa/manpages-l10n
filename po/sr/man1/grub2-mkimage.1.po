# Serbian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.11.0\n"
"POT-Creation-Date: 2023-02-20 20:09+0100\n"
"PO-Revision-Date: 2022-07-23 16:26+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Serbian <>\n"
"Language: sr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#. type: TH
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "GRUB-MKIMAGE"
msgstr "GRUB-MKIMAGE"

#. type: TH
#: fedora-38 fedora-rawhide opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "February 2023"
msgstr "Фебруара 2023"

#. type: TH
#: fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "GRUB 2.06"
msgstr "ГРУБ 2.06"

#. type: TH
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Корисничке наредбе"

#. type: SH
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "НАЗИВ"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "grub-mkimage - make a bootable image of GRUB"
msgstr "grub-mkimage - прави подизну слику ГРУБ-а"

#. type: SH
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "УВОД"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"B<grub-mkimage> [I<\\,OPTION\\/>...] [I<\\,OPTION\\/>]... [I<\\,MODULES\\/>]"
msgstr ""
"B<grub-mkimage> [I<\\,ОПЦИЈА\\/>...] [I<\\,ОПЦИЈА\\/>]... [I<\\,МОДУЛИ\\/>]"

#. type: SH
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИС"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "Make a bootable image of GRUB."
msgstr "Прави подизну слику ГРУБ-а."

#. type: TP
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-c>, B<--config>=I<\\,FILE\\/>"
msgstr "B<-c>, B<--config>=I<\\,ДАТОТЕКА\\/>"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "embed FILE as an early config"
msgstr "уграђује ДАТОТЕКУ као раније подешавање"

#. type: TP
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-C>, B<--compression=>(xz|none|auto)"
msgstr "B<-C>, B<--compression=>(xz|none|auto)"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "choose the compression to use for core image"
msgstr "бира сажимање за слику језгра"

#. type: TP
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--disable-shim-lock>"
msgstr "B<--disable-shim-lock>"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "disable shim_lock verifier"
msgstr "искључује „shim_lock“ проверивача"

#. type: TP
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-d>, B<--directory>=I<\\,DIR\\/>"
msgstr "B<-d>, B<--directory>=I<\\,ДИР\\/>"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"use images and modules under DIR [default=/usr/lib/grub/E<lt>platformE<gt>]"
msgstr ""
"користи слике и модуле под ДИР [основно=/usr/lib/grub/E<lt>платформаE<gt>]"

#. type: TP
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-D>, B<--dtb>=I<\\,FILE\\/>"
msgstr "B<-D>, B<--dtb>=I<\\,ДАТОТЕКА\\/>"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "embed FILE as a device tree (DTB)"
msgstr "уграђује ДАТОТЕКУ као стабло уређаја (ДТБ)"

#. type: TP
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-k>, B<--pubkey>=I<\\,FILE\\/>"
msgstr "B<-k>, B<--pubkey>=I<\\,ДАТОТЕКА\\/>"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, fuzzy
#| msgid "embed FILE as public key for signature checking"
msgid "embed FILE as public key for PGP signature checking"
msgstr "уграђује ДАТОТЕКУ као јавни кључ за проверавање потписа"

#. type: TP
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-m>,                              B<--memdisk>=I<\\,FILE\\/>"
msgstr "B<-m>,                              B<--memdisk>=I<\\,ДАТОТЕКА\\/>"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, fuzzy
#| msgid "use FILE as memdisk"
msgid "embed FILE as a memdisk image"
msgstr "користи ДАТОТЕКУ као меморијски диск"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "Implies `-p (memdisk)/boot/grub' and overrides"
msgstr ""

#. type: TP
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "any prefix supplied previously, but the prefix"
msgstr ""

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "itself can be overridden by later options"
msgstr ""

#. type: TP
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-n>, B<--note>"
msgstr "B<-n>, B<--note>"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "add NOTE segment for CHRP IEEE1275"
msgstr "додаје подеок НАПОМЕНА за ЦХРП ИЕЕЕ1275"

#. type: TP
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-o>, B<--output>=I<\\,FILE\\/>"
msgstr "B<-o>, B<--output>=I<\\,ДАТОТЕКА\\/>"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "output a generated image to FILE [default=stdout]"
msgstr "шаље створену слику у ДАТОТЕКУ [основно=stdout]"

#. type: TP
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-O>, B<--format>=I<\\,FORMAT\\/>"
msgstr "B<-O>, B<--format>=I<\\,ЗАПИС\\/>"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"generate an image in FORMAT available formats: i386-coreboot, i386-"
"multiboot, i386-pc, i386-xen_pvh, i386-pc-pxe, i386-pc-eltorito, i386-efi, "
"i386-ieee1275, i386-qemu, x86_64-efi, i386-xen, x86_64-xen, mipsel-yeeloong-"
"flash, mipsel-fuloong2f-flash, mipsel-loongson-elf, powerpc-ieee1275, "
"sparc64-ieee1275-raw, sparc64-ieee1275-cdcore, sparc64-ieee1275-aout, ia64-"
"efi, mips-arc, mipsel-arc, mipsel-qemu_mips-elf, mips-qemu_mips-flash, "
"mipsel-qemu_mips-flash, mips-qemu_mips-elf, arm-uboot, arm-coreboot-"
"vexpress, arm-coreboot-veyron, arm-efi, arm64-efi, riscv32-efi, riscv64-efi"
msgstr ""
"Ствара слику у ЗАПИСУ. Доступни записи: i386-coreboot, i386-multiboot, i386-"
"pc, i386-xen_pvh, i386-pc-pxe, i386-pc-eltorito, i386-efi, i386-ieee1275, "
"i386-qemu, x86_64-efi, i386-xen, x86_64-xen, mipsel-yeeloong-flash, mipsel-"
"fuloong2f-flash, mipsel-loongson-elf, powerpc-ieee1275, sparc64-ieee1275-"
"raw, sparc64-ieee1275-cdcore, sparc64-ieee1275-aout, ia64-efi, mips-arc, "
"mipsel-arc, mipsel-qemu_mips-elf, mips-qemu_mips-flash, mipsel-qemu_mips-"
"flash, mips-qemu_mips-elf, arm-uboot, arm-coreboot-vexpress, arm-coreboot-"
"veyron, arm-efi, arm64-efi, riscv32-efi, riscv64-efi"

#. type: TP
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-p>, B<--prefix>=I<\\,DIR\\/>"
msgstr "B<-p>, B<--prefix>=I<\\,ДИР\\/>"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "set prefix directory"
msgstr "поставља директоријум префикса"

#. type: TP
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-s>, B<--sbat>=I<\\,FILE\\/>"
msgstr "B<-s>, B<--sbat>=I<\\,ДАТОТЕКА\\/>"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "SBAT metadata"
msgstr "СБАТ метаподаци"

#. type: TP
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-S>, B<--appended-signature-size>=I<\\,SIZE\\/>"
msgstr "B<-S>, B<--appended-signature-size>=I<\\,ВЕЛИЧИНА\\/>"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "Add a note segment reserving SIZE bytes for an appended signature"
msgstr ""

#. type: TP
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "print verbose messages."
msgstr "исписује опширне поруке."

#. type: TP
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-x>, B<--x509>=I<\\,FILE\\/>"
msgstr "B<-x>, B<--x509>=I<\\,ДАТОТЕКА\\/>"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, fuzzy
#| msgid "embed FILE as public key for signature checking"
msgid "embed FILE as an x509 certificate for appended signature checking"
msgstr "уграђује ДАТОТЕКУ као јавни кључ за проверавање потписа"

#. type: TP
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "-?, B<--help>"
msgstr "-?, B<--help>"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "give this help list"
msgstr "приказује овај списак помоћи"

#. type: TP
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--usage>"
msgstr "B<--usage>"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "give a short usage message"
msgstr "приказује кратку поруку коришћења"

#. type: TP
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "print program version"
msgstr "исписује издање програма"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"Mandatory or optional arguments to long options are also mandatory or "
"optional for any corresponding short options."
msgstr ""
"Обавезни или изборни аргументи за дуге опције су такође обавезни или изборни "
"за било које одговарајуће кратке опције."

#. type: SH
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "ПРИЈАВЉИВАЊЕ ГРЕШАКА"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "Report bugs to E<lt>bug-grub@gnu.orgE<gt>."
msgstr "Грешке пријавите на: E<lt>bug-grub@gnu.orgE<gt>."

#. type: SH
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ВИДИТЕ ТАКОЂЕ"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "B<grub-install>(8), B<grub-mkrescue>(1), B<grub-mknetdir>(8)"
msgstr "B<grub-install>(8), B<grub-mkrescue>(1), B<grub-mknetdir>(8)"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"The full documentation for B<grub-mkimage> is maintained as a Texinfo "
"manual.  If the B<info> and B<grub-mkimage> programs are properly installed "
"at your site, the command"
msgstr ""
"Потпуна документација за B<grub-mkimage> је одржавана као Тексинфо "
"упутство.  Ако су B<info> и B<grub-mkimage> исправно инсталирани на вашем "
"сајту, наредба"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "B<info grub-mkimage>"
msgstr "B<info grub-mkimage>"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "should give you access to the complete manual."
msgstr "треба да вам да приступ потпуном упутству."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "October 2022"
msgstr "Октобра 2022"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "grub-mkimage (GRUB2) 2.06"
msgstr "grub-mkimage (ГРУБ2) 2.06"

#. type: Plain text
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"use images and modules under DIR [default=/usr/share/grub2/"
"E<lt>platformE<gt>]"
msgstr ""
"користи слике и модуле под ДИР [основно=/usr/share/grub2/E<lt>платформаE<gt>]"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "GRUB2 2.06"
msgstr "ГРУБ2 2.06"
