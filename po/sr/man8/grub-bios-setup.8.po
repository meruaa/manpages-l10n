# Serbian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.11.0\n"
"POT-Creation-Date: 2023-02-15 18:53+0100\n"
"PO-Revision-Date: 2022-07-23 17:29+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Serbian <>\n"
"Language: sr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "GRUB-BIOS-SETUP"
msgstr "GRUB-BIOS-SETUP"

#. type: TH
#: archlinux debian-unstable
#, no-wrap
msgid "February 2023"
msgstr "Фебруара 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "GRUB 2:2.06.r456.g65bc45963-1"
msgstr "ГРУБ 2:2.06.r456.g65bc45963-1"

#. type: TH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "System Administration Utilities"
msgstr "Помагала за администрацију система"

#. type: SH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "NAME"
msgstr "НАЗИВ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "grub-bios-setup - set up a device to boot using GRUB"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr "УВОД"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "B<grub-bios-setup> [I<\\,OPTION\\/>...] I<\\,DEVICE\\/>"
msgstr "B<grub-bios-setup> [I<\\,ОПЦИЈА\\/>...] I<\\,УРЕЂАЈ\\/>"

#. type: SH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИС"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "Set up images to boot from DEVICE."
msgstr "Подешава слике за подизање из УРЕЂАЈА."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid ""
"You should not normally run this program directly.  Use grub-install instead."
msgstr ""
"Не би требало директно да покренете овај програм.  Уместо тога користите "
"„grub-install“."

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "B<-a>, B<--allow-floppy>"
msgstr "B<-a>, B<--allow-floppy>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid ""
"make the drive also bootable as floppy (default for fdX devices). May break "
"on some BIOSes."
msgstr ""
"чини диск такође подизним као флопи (основно за фдИкс уређаје). Може да се "
"оштети на неким БИОС-има."

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "B<-b>, B<--boot-image>=I<\\,FILE\\/>"
msgstr "B<-b>, B<--boot-image>=I<\\,ДАТОТЕКА\\/>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "use FILE as the boot image [default=boot.img]"
msgstr "користи ДАТОТЕКУ као подизну слику [основно=boot.img]"

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "B<-c>, B<--core-image>=I<\\,FILE\\/>"
msgstr "B<-c>, B<--core-image>=I<\\,ДАТОТЕКА\\/>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "use FILE as the core image [default=core.img]"
msgstr "користи ДАТОТЕКУ као кључну слику [основно=core.img]"

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "B<-d>, B<--directory>=I<\\,DIR\\/>"
msgstr "B<-d>, B<--directory>=I<\\,ДИР\\/>"

#. type: Plain text
#: archlinux
msgid "use GRUB files in the directory DIR [default=//boot/grub]"
msgstr "користи датотеке ГРУБ-а у директоријуму ДИР [основно=//boot/grub]"

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "B<-f>, B<--force>"
msgstr "B<-f>, B<--force>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "install even if problems are detected"
msgstr "инсталира чак и ако су откривени проблеми"

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "B<-m>, B<--device-map>=I<\\,FILE\\/>"
msgstr "B<-m>, B<--device-map>=I<\\,ДАТОТЕКА\\/>"

#. type: Plain text
#: archlinux
msgid "use FILE as the device map [default=//boot/grub/device.map]"
msgstr "користи ДАТОТЕКУ као карту уређаја [основно=//boot/grub/device.map]"

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "B<--no-rs-codes>"
msgstr "B<--no-rs-codes>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid ""
"Do not apply any reed-solomon codes when embedding core.img. This option is "
"only available on x86 BIOS targets."
msgstr ""
"Не примењује никакво мудро читање шифре када уграђује „core.img“. Ова опција "
"је доступна једнино на метама x86 БИОС-а."

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "B<-s>, B<--skip-fs-probe>"
msgstr "B<-s>, B<--skip-fs-probe>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "do not probe for filesystems in DEVICE"
msgstr "не испробава системе датотека у УРЕЂАЈУ"

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "print verbose messages."
msgstr "исписује опширне поруке."

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "-?, B<--help>"
msgstr "-?, B<--help>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "give this help list"
msgstr "приказује овај списак помоћи"

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "B<--usage>"
msgstr "B<--usage>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "give a short usage message"
msgstr "приказује кратку поруку коришћења"

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "print program version"
msgstr "исписује издање програма"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid ""
"Mandatory or optional arguments to long options are also mandatory or "
"optional for any corresponding short options."
msgstr ""
"Обавезни или изборни аргументи за дуге опције су такође обавезни или изборни "
"за било које одговарајуће кратке опције."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "DEVICE must be an OS device (e.g. I<\\,/dev/sda\\/>)."
msgstr "УРЕЂАЈ мора бити уређај ОС-а (нпр. I<\\,/dev/sda\\/>)."

#. type: SH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "REPORTING BUGS"
msgstr "ПРИЈАВЉИВАЊЕ ГРЕШАКА"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "Report bugs to E<lt>bug-grub@gnu.orgE<gt>."
msgstr "Грешке пријавите на: E<lt>bug-grub@gnu.orgE<gt>."

#. type: SH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr "ВИДИТЕ ТАКОЂЕ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "B<grub-install>(8), B<grub-mkimage>(1), B<grub-mkrescue>(1)"
msgstr "B<grub-install>(8), B<grub-mkimage>(1), B<grub-mkrescue>(1)"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid ""
"The full documentation for B<grub-bios-setup> is maintained as a Texinfo "
"manual.  If the B<info> and B<grub-bios-setup> programs are properly "
"installed at your site, the command"
msgstr ""
"Потпуна документација за B<grub-bios-setup> је одржавана као Тексинфо "
"упутство.  Ако су B<info> и B<grub-bios-setup> исправно инсталирани на вашем "
"сајту, наредба"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "B<info grub-bios-setup>"
msgstr "B<info grub-bios-setup>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "should give you access to the complete manual."
msgstr "треба да вам да приступ потпуном упутству."

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "November 2022"
msgstr "Новембар 2022"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "GRUB 2.06-3~deb11u5"
msgstr "ГРУБ 2.06-3~deb11u5"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "use GRUB files in the directory DIR [default=/boot/grub]"
msgstr "користи датотеке ГРУБ-а у директоријуму ДИР [основно=/boot/grub]"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "use FILE as the device map [default=/boot/grub/device.map]"
msgstr "користи ДАТОТЕКУ као карту уређаја [основно=/boot/grub/device.map]"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "GRUB 2.06-8"
msgstr "ГРУБ 2.06-8"
