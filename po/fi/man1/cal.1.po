# Finnish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Jaakko Puurunen <jaakko.puurunen@iki.fi>, 1998.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-02-15 18:44+0100\n"
"PO-Revision-Date: 1998-04-10 11:01+0200\n"
"Last-Translator: Jaakko Puurunen <jaakko.puurunen@iki.fi>\n"
"Language-Team: Finnish <>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "CAL"
msgstr "CAL"

#. type: TH
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "2022-05-11"
msgstr "11. toukokuuta 2022"

#. type: TH
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "util-linux 2.38.1"
msgstr "util-linux 2.38.1"

#. type: TH
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Käyttäjän sovellukset"

#. type: SH
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NIMI"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "cal - display a calendar"
msgstr "cal - tulostaa kalenterin"

#. type: SH
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "YLEISKATSAUS"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "B<cal> [options] [[[I<day>] I<month>] I<year>]"
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "B<cal> [options] [I<timestamp>|I<monthname>]"
msgstr ""

#. type: SH
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "KUVAUS"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"B<cal> displays a simple calendar. If no arguments are specified, the "
"current month is displayed."
msgstr ""
"B<cal> tulostaa yksinkertaisen kalenterin.  Ilman argumentteja tulostetaan "
"kuluva kuukausi."

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"The I<month> may be specified as a number (1-12), as a month name or as an "
"abbreviated month name according to the current locales."
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"Two different calendar systems are used, Gregorian and Julian. These are "
"nearly identical systems with Gregorian making a small adjustment to the "
"frequency of leap years; this facilitates improved synchronization with "
"solar events like the equinoxes. The Gregorian calendar reform was "
"introduced in 1582, but its adoption continued up to 1923. By default B<cal> "
"uses the adoption date of 3 Sept 1752. From that date forward the Gregorian "
"calendar is displayed; previous dates use the Julian calendar system. 11 "
"days were removed at the time of adoption to bring the calendar in sync with "
"solar events. So Sept 1752 has a mix of Julian and Gregorian dates by which "
"the 2nd is followed by the 14th (the 3rd through the 13th are absent)."
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"Optionally, either the proleptic Gregorian calendar or the Julian calendar "
"may be used exclusively. See B<--reform> below."
msgstr ""

#. type: SH
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "VALITSIMET"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "B<-1>, B<--one>"
msgstr "B<-1>, B<--one>"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "Display single month output. (This is the default.)"
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "B<-3>, B<--three>"
msgstr "B<-3>, B<--three>"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "Display three months spanning the date."
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "B<-n , --months> I<number>"
msgstr "B<-n , --months> I<määrä>"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"Display I<number> of months, starting from the month containing the date."
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "B<-S, --span>"
msgstr "B<-S, --span>"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "Display months spanning the date."
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "B<-s>, B<--sunday>"
msgstr "B<-s>, B<--sunday>"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "Display Sunday as the first day of the week."
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "B<-m>, B<--monday>"
msgstr "B<-m>, B<--monday>"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "Display Monday as the first day of the week."
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "B<-v>, B<--vertical>"
msgstr "B<-v>, B<--vertical>"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "Display using a vertical layout (aka B<ncal>(1) mode)."
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "B<--iso>"
msgstr "B<--iso>"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"Display the proleptic Gregorian calendar exclusively. This option does not "
"affect week numbers and the first day of the week. See B<--reform> below."
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "B<-j>, B<--julian>"
msgstr "B<-j>, B<--julian>"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"Use day-of-year numbering for all calendars. These are also called ordinal "
"days. Ordinal days range from 1 to 366. This option does not switch from the "
"Gregorian to the Julian calendar system, that is controlled by the B<--"
"reform> option."
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"Sometimes Gregorian calendars using ordinal dates are referred to as Julian "
"calendars. This can be confusing due to the many date related conventions "
"that use Julian in their name: (ordinal) julian date, julian (calendar) "
"date, (astronomical) julian date, (modified) julian date, and more. This "
"option is named julian, because ordinal days are identified as julian by the "
"POSIX standard. However, be aware that B<cal> also uses the Julian calendar "
"system. See B<DESCRIPTION> above."
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "B<--reform> I<val>"
msgstr "B<--reform> I<arvo>"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"This option sets the adoption date of the Gregorian calendar reform. "
"Calendar dates previous to reform use the Julian calendar system. Calendar "
"dates after reform use the Gregorian calendar system. The argument I<val> "
"can be:"
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"I<1752> - sets 3 September 1752 as the reform date (default). This is when "
"the Gregorian calendar reform was adopted by the British Empire."
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"I<gregorian> - display Gregorian calendars exclusively. This special "
"placeholder sets the reform date below the smallest year that B<cal> can "
"use; meaning all calendar output uses the Gregorian calendar system. This is "
"called the proleptic Gregorian calendar, because dates prior to the calendar "
"system\\(cqs creation use extrapolated values."
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"I<iso> - alias of I<gregorian>. The ISO 8601 standard for the representation "
"of dates and times in information interchange requires using the proleptic "
"Gregorian calendar."
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"I<julian> - display Julian calendars exclusively. This special placeholder "
"sets the reform date above the largest year that B<cal> can use; meaning all "
"calendar output uses the Julian calendar system."
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, fuzzy
#| msgid "DESCRIPTION"
msgid "See B<DESCRIPTION> above."
msgstr "KUVAUS"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "B<-y>, B<--year>"
msgstr "B<-y>, B<--year>"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, fuzzy
#| msgid "Display a calendar for the current year."
msgid "Display a calendar for the whole year."
msgstr "Tulosta kuluvan vuoden kalenteri."

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "B<-Y, --twelve>"
msgstr "B<-Y, --twelve>"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, fuzzy
#| msgid "Display a calendar for the current year."
msgid "Display a calendar for the next twelve months."
msgstr "Tulosta kuluvan vuoden kalenteri."

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "B<-w>, B<--week>[=I<number>]"
msgstr "B<-w>, B<--week>[=I<määrä>]"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"Display week numbers in the calendar (US or ISO-8601). See the B<NOTES> "
"section for more details."
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "B<--color>[=I<when>]"
msgstr "B<--color>[=I<milloin>]"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"Colorize the output. The optional argument I<when> can be B<auto>, B<never> "
"or B<always>. If the I<when> argument is omitted, it defaults to B<auto>. "
"The colors can be disabled; for the current built-in default see the B<--"
"help> output. See also the B<COLORS> section."
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "Display help text and exit."
msgstr "Näytä tämä ohje ja poistu."

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "Print version and exit."
msgstr "Tulosta versiotiedot ja poistu."

#. type: SH
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "PARAMETERS"
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "B<Single digits-only parameter (e.g., \\(aqcal 2020\\(aq)>"
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"Specifies the I<year> to be displayed; note the year must be fully "
"specified: B<cal 89> will not display a calendar for 1989."
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"B<Single string parameter (e.g., \\(aqcal tomorrow\\(aq or \\(aqcal "
"August\\(aq)>"
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"Specifies I<timestamp> or a I<month name> (or abbreviated name) according to "
"the current locales."
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"The special placeholders are accepted when parsing timestamp, \"now\" may be "
"used to refer to the current time, \"today\", \"yesterday\", \"tomorrow\" "
"refer to of the current day, the day before or the next day, respectively."
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"The relative date specifications are also accepted, in this case \"+\" is "
"evaluated to the current time plus the specified time span. Correspondingly, "
"a time span that is prefixed with \"-\" is evaluated to the current time "
"minus the specified time span, for example \\(aq+2days\\(aq. Instead of "
"prefixing the time span with \"+\" or \"-\", it may also be suffixed with a "
"space and the word \"left\" or \"ago\" (for example \\(aq1 week ago\\(aq)."
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "B<Two parameters (e.g., \\(aqcal 11 2020\\(aq)>"
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "Denote the I<month> (1 - 12) and I<year>."
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "B<Three parameters (e.g., \\(aqcal 25 11 2020\\(aq)>"
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"Denote the I<day> (1-31), I<month and year>, and the day will be highlighted "
"if the calendar is displayed on a terminal. If no parameters are specified, "
"the current month\\(cqs calendar is displayed."
msgstr ""

#. type: SH
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "HUOMAUTUKSET"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"A year starts on January 1. The first day of the week is determined by the "
"locale or the B<--sunday> and B<--monday> options."
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"The week numbering depends on the choice of the first day of the week. If it "
"is Sunday then the customary North American numbering is used, where 1 "
"January is in week number 1. If it is Monday (B<-m>) then the ISO 8601 "
"standard week numbering is used, where the first Thursday is in week number "
"1."
msgstr ""

#. type: SH
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "COLORS"
msgstr "VÄRIT"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"The output colorization is implemented by B<terminal-colors.d>(5) "
"functionality.  Implicit coloring can be disabled by an empty file"
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid "B<touch /etc/terminal-colors.d/cal.disable>"
msgid "I</etc/terminal-colors.d/cal.disable>"
msgstr "B<touch /etc/terminal-colors.d/cal.disable>"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "for the B<cal> command or for all tools by"
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "I</etc/terminal-colors.d/disable>"
msgstr "I</etc/terminal-colors.d/disable>"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"The user-specific I<$XDG_CONFIG_HOME/terminal-colors.d> or I<$HOME/.config/"
"terminal-colors.d> overrides the global setting."
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"Note that the output colorization may be enabled by default, and in this "
"case I<terminal-colors.d> directories do not have to exist yet."
msgstr ""

#. type: SH
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIA"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "A B<cal> command appeared in Version 6 AT&T UNIX."
msgstr "B<cal> esiintyi ensimmäisen kerran AT&T UNIX Versiossa 6."

#. type: SH
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "BUGIT"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"The default B<cal> output uses 3 September 1752 as the Gregorian calendar "
"reform date. The historical reform dates for the other locales, including "
"its introduction in October 1582, are not implemented."
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"Alternative calendars, such as the Umm al-Qura, the Solar Hijri, the "
"Ge\\(cqez, or the lunisolar Hindu, are not supported."
msgstr ""

#. type: SH
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "VIRHEISTÄ ILMOITTAMINEN"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "For bug reports, use the issue tracker at"
msgstr ""

#. type: SH
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "AVAILABILITY"
msgstr "SAATAVUUS"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"The B<cal> command is part of the util-linux package which can be downloaded "
"from"
msgstr ""

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2022-02-14"
msgstr "14. helmikuuta 2022"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "util-linux 2.37.4"
msgstr "util-linux 2.37.4"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"Display week numbers in the calendar (US or ISO-8601). See NOTES section for "
"more details."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid "Display version information and exit."
msgstr "Näytä ohjelman versiotiedot ja poistu."

#. type: Plain text
#: opensuse-leap-15-5
msgid "Implicit coloring can be disabled as follows:"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<touch /etc/terminal-colors.d/cal.disable>"
msgstr "B<touch /etc/terminal-colors.d/cal.disable>"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"See B<terminal-colors.d>(5) for more details about colorization "
"configuration."
msgstr ""
