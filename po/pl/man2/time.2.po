# Polish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Przemek Borys <pborys@dione.ids.pl>, 1999.
# Andrzej Krzysztofowicz <ankry@green.mf.pg.gda.pl>, 2002.
# Michał Kułach <michal.kulach@gmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-02-20 20:38+0100\n"
"PO-Revision-Date: 2021-03-09 18:57+0100\n"
"Last-Translator: Michał Kułach <michal.kulach@gmail.com>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<time>"
msgid "time"
msgstr "B<time>"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2022-12-29"
msgstr "29 grudnia 2022 r."

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAZWA"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "time - get time in seconds"
msgstr "time - pobranie czasu w sekundach"

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SKŁADNIA"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>time.hE<gt>>\n"
msgstr "B<#include E<lt>time.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<time_t time(time_t *>I<tloc>B<);>\n"
msgid "B<time_t time(time_t *_Nullable >I<tloc>B<);>\n"
msgstr "B<time_t time(time_t *>I<tloc>B<);>\n"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "OPIS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<time>()  returns the time as the number of seconds since the Epoch, "
"1970-01-01 00:00:00 +0000 (UTC)."
msgstr ""
"B<time>() zwraca czas jako liczbę sekund od epoki, tj. 1970-01-01 00:00:00 "
"+0000 (UTC)."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If I<tloc> is non-NULL, the return value is also stored in the memory "
"pointed to by I<tloc>."
msgstr ""
"Jeśli I<tloc> nie jest równe NULL, to zwracana wartość jest również "
"zapisywana w pamięci wskazywanej przez I<tloc>."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "WARTOŚĆ ZWRACANA"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "On success, the value of time in seconds since the Epoch is returned.  On "
#| "error, I<((time_t)\\ -1)> is returned, and I<errno> is set appropriately."
msgid ""
"On success, the value of time in seconds since the Epoch is returned.  On "
"error, I<((time_t)\\ -1)> is returned, and I<errno> is set to indicate the "
"error."
msgstr ""
"W przypadku pomyślnego zakończenia, zwracana jest wartość liczbowa dla czasu "
"w sekundach od Epoki. W przypadku błędu, zwracane jest ((time_t)\\ -1) i "
"odpowiednio ustawiane I<errno>."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "BŁĘDY"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "I<tloc> points outside your accessible address space (but see BUGS)."
msgstr ""
"I<tloc> wskazuje poza dostępną dla użytkownika przestrzeń adresową (ale zob. "
"BŁĘDY)."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"On systems where the C library B<time>()  wrapper function invokes an "
"implementation provided by the B<vdso>(7)  (so that there is no trap into "
"the kernel), an invalid address may instead trigger a B<SIGSEGV> signal."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDY"

#.  Under 4.3BSD, this call is obsoleted by
#.  .BR gettimeofday (2).
#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, fuzzy
#| msgid ""
#| "SVr4, 4.3BSD, C89, C99, POSIX.1-2001.  POSIX does not specify any error "
#| "conditions."
msgid ""
"SVr4, 4.3BSD, C99, POSIX.1-2001.  POSIX does not specify any error "
"conditions."
msgstr ""
"SVr4, 4.3BSD, C89, C99, POSIX.1-2001. POSIX nie określa żadnych sytuacji "
"błędnych."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "UWAGI"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "POSIX.1 defines I<seconds since the Epoch> as a value to be interpreted "
#| "as the number of seconds between a specified time and the Epoch, "
#| "according to a formula for conversion from UTC equivalent to conversion "
#| "on the na�ve basis that leap seconds are ignored and all years divisible "
#| "by 4 are leap years.  This value is not the same as the actual number of "
#| "seconds between the time and the Epoch, because of leap seconds and "
#| "because clocks are not required to be synchronised to a standard "
#| "reference.  The intention is that the interpretation of seconds since the "
#| "Epoch values be consistent; see POSIX.1 Annex B 2.2.2 for further "
#| "rationale."
msgid ""
"POSIX.1 defines I<seconds since the Epoch> using a formula that approximates "
"the number of seconds between a specified time and the Epoch.  This formula "
"takes account of the facts that all years that are evenly divisible by 4 are "
"leap years, but years that are evenly divisible by 100 are not leap years "
"unless they are also evenly divisible by 400, in which case they are leap "
"years.  This value is not the same as the actual number of seconds between "
"the time and the Epoch, because of leap seconds and because system clocks "
"are not required to be synchronized to a standard reference.  The intention "
"is that the interpretation of seconds since the Epoch values be consistent; "
"see POSIX.1-2008 Rationale A.4.15 for further rationale."
msgstr ""
"POSIX.1 definiuje I<liczbę sekund od Epoki> jako wartość, które ma być "
"interpretowana jako liczba sekund pomiędzy zadanym czasem a Epoką, obliczona "
"jako różnica czasów UTC z wykorzystaniem uproszczonego wzoru, w którym "
"ignorowane są sekundy przestępne, a wszystkie lata podzielne przez 4 są "
"przestępne. Wartość ta nie jest tym samym, co rzeczywista liczba sekund "
"pomiędzy zadanym czasem a Epoką, gdyż istnieją sekundy przestępne oraz "
"zegary nie muszą być synchronizowane z czasem standardowym. Ma to na celu "
"spójną interpretację dla liczby sekund od Epoki; dodatkowe uzasadnienie "
"można znaleźć w POSIX.1 Annex B 2.2.2."

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"On Linux, a call to B<time>()  with I<tloc> specified as NULL cannot fail "
"with the error B<EOVERFLOW>, even on ABIs where I<time_t> is a signed 32-bit "
"integer and the clock reaches or exceeds 2**31 seconds (2038-01-19 03:14:08 "
"UTC, ignoring leap seconds).  (POSIX.1 permits, but does not require, the "
"B<EOVERFLOW> error in the case where the seconds since the Epoch will not "
"fit in I<time_t>.)  Instead, the behavior on Linux is undefined when the "
"system time is out of the I<time_t> range.  Applications intended to run "
"after 2038 should use ABIs with I<time_t> wider than 32 bits."
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "BŁĘDY"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Error returns from this system call are indistinguishable from successful "
"reports that the time is a few seconds I<before> the Epoch, so the C library "
"wrapper function never sets I<errno> as a result of this call."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The I<tloc> argument is obsolescent and should always be NULL in new code.  "
"When I<tloc> is NULL, the call cannot fail."
msgstr ""

#. type: SS
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "C library/kernel differences"
msgstr "Różnice biblioteki C/jądra"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"On some architectures, an implementation of B<time>()  is provided in the "
"B<vdso>(7)."
msgstr ""
"Na niektórych architekturach, implementacja B<time>() jest zapewniona w "
"B<vdso>(7)."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ZOBACZ TAKŻE"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<date>(1), B<gettimeofday>(2), B<ctime>(3), B<ftime>(3), B<time>(7), "
"B<vdso>(7)"
msgstr ""
"B<date>(1), B<gettimeofday>(2), B<ctime>(3), B<ftime>(3), B<time>(7), "
"B<vdso>(7)"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "TIME"
msgstr "TIME"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15 września 2017 r."

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Podręcznik programisty Linuksa"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "B<#include E<lt>time.hE<gt>>"
msgstr "B<#include E<lt>time.hE<gt>>"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "B<time_t time(time_t *>I<tloc>B<);>"
msgstr "B<time_t time(time_t *>I<tloc>B<);>"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"On success, the value of time in seconds since the Epoch is returned.  On "
"error, I<((time_t)\\ -1)> is returned, and I<errno> is set appropriately."
msgstr ""
"W przypadku pomyślnego zakończenia, zwracana jest wartość liczbowa dla czasu "
"w sekundach od Epoki. W przypadku błędu, zwracane jest ((time_t)\\ -1) i "
"odpowiednio ustawiane I<errno>."

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "ZGODNE Z"

#.  Under 4.3BSD, this call is obsoleted by
#.  .BR gettimeofday (2).
#. type: Plain text
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"SVr4, 4.3BSD, C89, C99, POSIX.1-2001.  POSIX does not specify any error "
"conditions."
msgstr ""
"SVr4, 4.3BSD, C89, C99, POSIX.1-2001. POSIX nie określa żadnych sytuacji "
"błędnych."

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"On Linux, a call to B<time>()  with I<tloc> specified as NULL cannot fail "
"with the error B<EOVERFLOW>, even on ABIs where I<time_t> is a signed 32-bit "
"integer and the clock ticks past the time 2**31 (2038-01-19 03:14:08 UTC, "
"ignoring leap seconds).  (POSIX.1 permits, but does not require, the "
"B<EOVERFLOW> error in the case where the seconds since the Epoch will not "
"fit in I<time_t>.)  Instead, the behavior on Linux is undefined when the "
"system time is out of the I<time_t> range.  Applications intended to run "
"after 2038 should use ABIs with I<time_t> wider than 32 bits."
msgstr ""

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "O STRONIE"

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Angielska wersja tej strony pochodzi z wydania 5.10 projektu Linux I<man-"
"pages>. Opis projektu, informacje dotyczące zgłaszania błędów oraz najnowszą "
"wersję oryginału można znaleźć pod adresem \\%https://www.kernel.org/doc/man-"
"pages/."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Angielska wersja tej strony pochodzi z wydania 4.16 projektu Linux I<man-"
"pages>. Opis projektu, informacje dotyczące zgłaszania błędów oraz najnowszą "
"wersję oryginału można znaleźć pod adresem \\%https://www.kernel.org/doc/man-"
"pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-12-03"
msgstr "3 grudnia 2022 r."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.02"
msgstr "Linux man-pages 6.02"
