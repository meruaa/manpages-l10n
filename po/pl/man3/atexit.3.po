# Polish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Adam Byrtek <alpha@irc.pl>, 1998.
# Andrzej Krzysztofowicz <ankry@green.mf.pg.gda.pl>, 2001.
# Robert Luberda <robert@debian.org>, 2013, 2019.
# Michał Kułach <michal.kulach@gmail.com>, 2016.
msgid ""
msgstr ""
"Project-Id-Version: manpages-pl\n"
"POT-Creation-Date: 2023-02-20 19:59+0100\n"
"PO-Revision-Date: 2019-08-16 21:41+0100\n"
"Last-Translator: Robert Luberda <robert@debian.org>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<atexit>()"
msgid "atexit"
msgstr "B<atexit>()"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 lutego 2023 r."

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAZWA"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "atexit - register a function to be called at normal process termination"
msgstr ""
"atexit - rejestracja funkcji wywoływanej po normalnym zakończeniu procesu"

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SKŁADNIA"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>stdlib.hE<gt>>\n"
msgstr "B<#include E<lt>stdlib.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<int atexit(void (*>I<function>B<)(void));>\n"
msgstr "B<int atexit(void (*>I<funkcja>B<)(void));>\n"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "OPIS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The B<atexit>()  function registers the given I<function> to be called at "
"normal process termination, either via B<exit>(3)  or via return from the "
"program's I<main>().  Functions so registered are called in the reverse "
"order of their registration; no arguments are passed."
msgstr ""
"Funkcja B<atexit>() powoduje, że podana I<funkcja> będzie wywołana po "
"normalnym zakończeniu pracy procesu, które może nastąpić przez B<exit>(3) "
"lub przez powrót z funkcji I<main>() procesu. Funkcje zarejestrowane w ten "
"sposób są wywoływane w kolejności odwrotnej do ich rejestracji; nie są im "
"przekazywane żadne argumenty."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The same function may be registered multiple times: it is called once for "
"each registration."
msgstr ""
"Tę samą funkcję można zarejestrować kilka razy: zostanie wywołana tyle razy, "
"ile razy została zarejestrowana."

#.  POSIX.1-2001, POSIX.1-2008
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"POSIX.1 requires that an implementation allow at least B<ATEXIT_MAX> (32) "
"such functions to be registered.  The actual limit supported by an "
"implementation can be obtained using B<sysconf>(3)."
msgstr ""
"POSIX.1 wymaga, aby implementacja pozwalała na zarejestrowanie co najmniej "
"B<ATEXIT_MAX> (32) takich funkcji. Bieżące ograniczenie obsługiwane przez "
"implementację można odczytać za pomocą funkcji B<sysconf>(3)."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"When a child process is created via B<fork>(2), it inherits copies of its "
"parent's registrations.  Upon a successful call to one of the B<exec>(3)  "
"functions, all registrations are removed."
msgstr ""
"Kiedy proces potomny jest tworzony za pomocą B<fork>(2), dziedziczy kopie "
"zarejestrowań funkcji  rodzica. Po pomyślnym wywołaniu jednej z funkcji "
"B<exec>(3), wszystkie zarejestrowane funkcje są usuwane."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "WARTOŚĆ ZWRACANA"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The B<atexit>()  function returns the value 0 if successful; otherwise it "
"returns a nonzero value."
msgstr ""
"Funkcja B<atexit>() zwraca wartość 0, jeśli zakończy się pomyślnie. W "
"przeciwnym wypadku zwraca wartość niezerową."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATRYBUTY"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Informacje o pojęciach używanych w tym rozdziale można znaleźć w podręczniku "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interfejs"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Atrybut"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Wartość"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<atexit>()"
msgstr "B<atexit>()"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Bezpieczeństwo wątkowe"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDY"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, fuzzy
#| msgid "POSIX.1-2001, POSIX.1-2008, C89, C99, SVr4, 4.3BSD."
msgid "POSIX.1-2001, POSIX.1-2008, C99, SVr4, 4.3BSD."
msgstr "POSIX.1-2001, POSIX.1-2008, C89, C99, SVr4, 4.3BSD."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "UWAGI"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Functions registered using B<atexit>()  (and B<on_exit>(3))  are not called "
"if a process terminates abnormally because of the delivery of a signal."
msgstr ""
"Funkcje zarejestrowane przez B<atexit>() (i B<on_exit>(3)) nie są "
"uruchamiane w przypadku nienormalnego zakończenia procesu po otrzymaniu "
"sygnału."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If one of the registered functions calls B<_exit>(2), then any remaining "
"functions are not invoked, and the other process termination steps performed "
"by B<exit>(3)  are not performed."
msgstr ""
"Jeśli jedna z zarejestrowanych funkcji wywoła B<_exit>(2), to pozostałe "
"zarejestrowane funkcje nie będą uruchamiane i żadne inne kroki kończenia "
"procesu nie będą podejmowane przez B<exit>(3)."

#. #-#-#-#-#  archlinux: atexit.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  POSIX.1-2001, POSIX.1-2008
#.  This can happen on OpenBSD 4.2 for example, and is documented
#.  as occurring on FreeBSD as well.
#.  glibc does "the Right Thing" -- invocation of the remaining
#.  exit handlers carries on as normal.
#. type: Plain text
#. #-#-#-#-#  debian-bullseye: atexit.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  POSIX.1-2001, POSIX.1-2008
#.  This can happen on OpenBSD 4.2 for example, and is documented
#.  as occurring on FreeBSD as well.
#.  Glibc does "the Right Thing" -- invocation of the remaining
#.  exit handlers carries on as normal.
#. type: Plain text
#. #-#-#-#-#  debian-unstable: atexit.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  POSIX.1-2001, POSIX.1-2008
#.  This can happen on OpenBSD 4.2 for example, and is documented
#.  as occurring on FreeBSD as well.
#.  glibc does "the Right Thing" -- invocation of the remaining
#.  exit handlers carries on as normal.
#. type: Plain text
#. #-#-#-#-#  fedora-38: atexit.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  POSIX.1-2001, POSIX.1-2008
#.  This can happen on OpenBSD 4.2 for example, and is documented
#.  as occurring on FreeBSD as well.
#.  glibc does "the Right Thing" -- invocation of the remaining
#.  exit handlers carries on as normal.
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: atexit.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  POSIX.1-2001, POSIX.1-2008
#.  This can happen on OpenBSD 4.2 for example, and is documented
#.  as occurring on FreeBSD as well.
#.  glibc does "the Right Thing" -- invocation of the remaining
#.  exit handlers carries on as normal.
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: atexit.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  POSIX.1-2001, POSIX.1-2008
#.  This can happen on OpenBSD 4.2 for example, and is documented
#.  as occurring on FreeBSD as well.
#.  glibc does "the Right Thing" -- invocation of the remaining
#.  exit handlers carries on as normal.
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-15-5: atexit.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  POSIX.1-2001, POSIX.1-2008
#.  This can happen on OpenBSD 4.2 for example, and is documented
#.  as occurring on FreeBSD as well.
#.  Glibc does "the Right Thing" -- invocation of the remaining
#.  exit handlers carries on as normal.
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: atexit.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  POSIX.1-2001, POSIX.1-2008
#.  This can happen on OpenBSD 4.2 for example, and is documented
#.  as occurring on FreeBSD as well.
#.  Glibc does "the Right Thing" -- invocation of the remaining
#.  exit handlers carries on as normal.
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"POSIX.1 says that the result of calling B<exit>(3)  more than once (i.e., "
"calling B<exit>(3)  within a function registered using B<atexit>())  is "
"undefined.  On some systems (but not Linux), this can result in an infinite "
"recursion; portable programs should not invoke B<exit>(3)  inside a function "
"registered using B<atexit>()."
msgstr ""
"POSIX.1 określa, że wynik wywołania B<exit>(3) więcej niż raz (np. wywołanie "
"B<exit>(3) przez funkcję zarejestrowaną przez B<atexit>()) jest "
"niezdefiniowany. Na niektórych systemach (ale nie pod Linuksem) może to "
"spowodować nieskończoną rekurencję. Programy przenośne nie powinny wywoływać "
"B<exit>(3) z ciała funkcji rejestrowanej przez B<atexit>()."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The B<atexit>()  and B<on_exit>(3)  functions register functions on the same "
"list: at normal process termination, the registered functions are invoked in "
"reverse order of their registration by these two functions."
msgstr ""
"Funkcje B<atexit>() i B<on_exit>(3) rejestrują funkcje, używając tej samej "
"listy: podczas normalnego zakończenia procesu, zarejestrowane funkcje są "
"uruchamiane w kolejności odwrotnej do kolejności ich rejestracji przez "
"którąkolwiek z dwu powyższych funkcji."

#.  In glibc, things seem to be handled okay
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"According to POSIX.1, the result is undefined if B<longjmp>(3)  is used to "
"terminate execution of one of the functions registered using B<atexit>()."
msgstr ""
"Zgodnie z POSIX.1 wynik jest niezdefiniowany, jeśli B<longjmp>(3) jest "
"używane do zakończenia wykonywania funkcji zarejestrowanej za pomocą "
"B<atexit>()."

#. type: SS
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Linux notes"
msgstr "Uwagi linuksowe"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Since glibc 2.2.3, B<atexit>()  (and B<on_exit>(3))  can be used within a "
"shared library to establish functions that are called when the shared "
"library is unloaded."
msgstr ""
"Od wersji 2.2.3 biblioteki glibc funkcji B<atexit>() (oraz B<on_exit>(3)) "
"można użyć w obrębie biblioteki współdzielonej do zarejestrowania funkcji "
"wywoływanych podczas wyładowywania biblioteki współdzielonej."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "PRZYKŁADY"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
msgstr ""
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"void\n"
"bye(void)\n"
"{\n"
"    printf(\"That was all, folks\\en\");\n"
"}\n"
msgstr ""
"void\n"
"bye(void)\n"
"{\n"
"    printf(\"I to by było wszystko\\en\");\n"
"}\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"int\n"
"main(void)\n"
"{\n"
"    long a;\n"
"    int i;\n"
msgstr ""
"int\n"
"main(void)\n"
"{\n"
"    long a;\n"
"    int i;\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"    a = sysconf(_SC_ATEXIT_MAX);\n"
"    printf(\"ATEXIT_MAX = %ld\\en\", a);\n"
msgstr ""
"    a = sysconf(_SC_ATEXIT_MAX);\n"
"    printf(\"ATEXIT_MAX = %ld\\en\", a);\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"    i = atexit(bye);\n"
"    if (i != 0) {\n"
"        fprintf(stderr, \"cannot set exit function\\en\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""
"    i = atexit(bye);\n"
"    if (i != 0) {\n"
"        fprintf(stderr, \"nie można ustawić funkcji wyjścia\\en\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. #-#-#-#-#  archlinux: atexit.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  debian-bullseye: atexit.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  debian-unstable: atexit.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  fedora-38: atexit.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  fedora-rawhide: atexit.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  mageia-cauldron: atexit.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  opensuse-leap-15-5: atexit.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  opensuse-tumbleweed: atexit.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ZOBACZ TAKŻE"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<_exit>(2), B<dlopen>(3), B<exit>(3), B<on_exit>(3)"
msgstr "B<_exit>(2), B<dlopen>(3), B<exit>(3), B<on_exit>(3)"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "ATEXIT"
msgstr "ATEXIT"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "2020-06-09"
msgstr "9 czerwca 2020 r."

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Podręcznik programisty Linuksa"

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "ZGODNE Z"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
msgid "POSIX.1-2001, POSIX.1-2008, C89, C99, SVr4, 4.3BSD."
msgstr "POSIX.1-2001, POSIX.1-2008, C89, C99, SVr4, 4.3BSD."

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "O STRONIE"

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Angielska wersja tej strony pochodzi z wydania 5.10 projektu Linux I<man-"
"pages>. Opis projektu, informacje dotyczące zgłaszania błędów oraz najnowszą "
"wersję oryginału można znaleźć pod adresem \\%https://www.kernel.org/doc/man-"
"pages/."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15 września 2017 r."

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "EXAMPLE"
msgstr "PRZYKŁAD"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Angielska wersja tej strony pochodzi z wydania 4.16 projektu Linux I<man-"
"pages>. Opis projektu, informacje dotyczące zgłaszania błędów oraz najnowszą "
"wersję oryginału można znaleźć pod adresem \\%https://www.kernel.org/doc/man-"
"pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-12-15"
msgstr "15 grudnia 2022 r."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.02"
msgstr "Linux man-pages 6.02"
