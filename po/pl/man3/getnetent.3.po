# Polish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Andrzej Krzysztofowicz <ankry@green.mf.pg.gda.pl>, 2002.
# Robert Luberda <robert@debian.org>, 2014, 2019.
# Michał Kułach <michal.kulach@gmail.com>, 2016.
msgid ""
msgstr ""
"Project-Id-Version: manpages-pl\n"
"POT-Creation-Date: 2023-02-20 20:07+0100\n"
"PO-Revision-Date: 2019-08-16 21:21+0100\n"
"Last-Translator: Robert Luberda <robert@debian.org>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "getnetent"
msgstr "getnetent"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-12-15"
msgstr "15 grudnia 2022 r."

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAZWA"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"getnetent, getnetbyname, getnetbyaddr, setnetent, endnetent - get network "
"entry"
msgstr ""
"getnetent, getnetbyname, getnetbyaddr, setnetent, endnetent - odczytanie "
"wpisu dotyczącego sieci"

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SKŁADNIA"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>netdb.hE<gt>>\n"
msgstr "B<#include E<lt>netdb.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<struct netent *getnetent(void);>\n"
msgstr "B<struct netent *getnetent(void);>\n"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"B<struct netent *getnetbyname(const char *>I<name>B<);>\n"
"B<struct netent *getnetbyaddr(uint32_t >I<net>B<, int >I<type>B<);>\n"
msgstr ""
"B<struct netent *getnetbyname(const char *>I<name>B<);>\n"
"B<struct netent *getnetbyaddr(uint32_t >I<net>B<, int >I<type>B<);>\n"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"B<void setnetent(int >I<stayopen>B<);>\n"
"B<void endnetent(void);>\n"
msgstr ""
"B<void setnetent(int >I<stayopen>B<);>\n"
"B<void endnetent(void);>\n"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "OPIS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The B<getnetent>()  function reads the next entry from the networks database "
"and returns a I<netent> structure containing the broken-out fields from the "
"entry.  A connection is opened to the database if necessary."
msgstr ""
"Funkcja B<getnetent>() odczytuje następny wpis z bazy danych sieci i zwraca "
"strukturę I<netent> zawierającą pola powstałe z rozłożenia pól wpisu. "
"Połączenie do bazy danych jest otwierane, jeśli jest to potrzebne."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The B<getnetbyname>()  function returns a I<netent> structure for the entry "
"from the database that matches the network I<name>."
msgstr ""
"Funkcja B<getnetbyname>() zwraca strukturę I<netent> zawierającą ten wpis z "
"bazy danych, który odpowiada sieci I<name>."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The B<getnetbyaddr>()  function returns a I<netent> structure for the entry "
"from the database that matches the network number I<net> of type I<type>.  "
"The I<net> argument must be in host byte order."
msgstr ""
"Funkcja B<getnetbyaddr>() zwraca strukturę I<netent> zawierającą ten wpis z "
"bazy danych, który odpowiada sieci o numerze I<net> rodzaju I<type>. "
"Argument I<net> musi być w porządku bajtów lokalnego komputera (host byte "
"order)."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The B<setnetent>()  function opens a connection to the database, and sets "
"the next entry to the first entry.  If I<stayopen> is nonzero, then the "
"connection to the database will not be closed between calls to one of the "
"B<getnet*>()  functions."
msgstr ""
"Funkcja B<setnetent>() otwiera połączenie do bazy danych i ustawia wskaźnik "
"kolejnego wpisu na pierwszy wpis. Jeśli I<stayopen> jest niezerowy, to "
"połączenie do bazy danych nie będzie zamykane pomiędzy wywołaniami funkcji "
"B<getnet*>()."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The B<endnetent>()  function closes the connection to the database."
msgstr "Funkcja B<endnetent>() zamyka połączenie do bazy danych."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The I<netent> structure is defined in I<E<lt>netdb.hE<gt>> as follows:"
msgstr ""
"Struktura I<netent> jest zdefiniowana w I<E<lt>netdb.hE<gt>> następująco:"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"struct netent {\n"
"    char      *n_name;     /* official network name */\n"
"    char     **n_aliases;  /* alias list */\n"
"    int        n_addrtype; /* net address type */\n"
"    uint32_t   n_net;      /* network number */\n"
"}\n"
msgstr ""
"struct netent {\n"
"    char      *n_name;     /* oficjalna nazwa sieci */\n"
"    char     **n_aliases;  /* lista aliasów */\n"
"    int        n_addrtype; /* rodzaj adresu sieci */\n"
"    uint32_t   n_net;      /* numer sieci */\n"
"}\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The members of the I<netent> structure are:"
msgstr "Polami struktury I<netent> są:"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I<n_name>"
msgstr "I<n_name>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The official name of the network."
msgstr "Oficjalna nazwa sieci."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I<n_aliases>"
msgstr "I<n_aliases>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "A NULL-terminated list of alternative names for the network."
msgstr "Zakończona NULL-em lista alternatywnych nazw tej sieci."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I<n_addrtype>"
msgstr "I<n_addrtype>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The type of the network number; always B<AF_INET>."
msgstr "Rodzaj numeru sieci; zawsze B<AF_INET>."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I<n_net>"
msgstr "I<n_net>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The network number in host byte order."
msgstr "Numer sieci w porządku bajtów hosta."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "WARTOŚĆ ZWRACANA"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The B<getnetent>(), B<getnetbyname>(), and B<getnetbyaddr>()  functions "
"return a pointer to a statically allocated I<netent> structure, or a null "
"pointer if an error occurs or the end of the file is reached."
msgstr ""
"Funkcje B<getnetent>(), B<getnetbyname>() i B<getnetbyaddr>() zwracają "
"wskaźnik do statycznej struktury I<netent> lub wskaźnik null, gdy wystąpi "
"błąd lub napotkany zostanie koniec pliku."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "PLIKI"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I</etc/networks>"
msgstr "I</etc/networks>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "networks database file"
msgstr "plik bazy danych z sieciami"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATRYBUTY"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Informacje o pojęciach używanych w tym rozdziale można znaleźć w podręczniku "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interfejs"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Atrybut"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Wartość"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<getnetent>()"
msgstr "B<getnetent>()"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Bezpieczeństwo wątkowe"

#. type: tbl table
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "race:netentbuf env locale"
msgid ""
"MT-Unsafe race:netent\n"
"race:netentbuf env locale"
msgstr "race:netentbuf env locale"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<getnetbyname>()"
msgstr "B<getnetbyname>()"

#. type: tbl table
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "MT-Unsafe race:netbyname\n"
msgid ""
"MT-Unsafe race:netbyname\n"
"env locale"
msgstr "MT-Unsafe race:netbyname\n"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<getnetbyaddr>()"
msgstr "B<getnetbyaddr>()"

#. type: tbl table
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "MT-Unsafe race:netbyaddr\n"
msgid ""
"MT-Unsafe race:netbyaddr\n"
"locale"
msgstr "MT-Unsafe race:netbyaddr\n"

#. type: tbl table
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"B<setnetent>(),\n"
"B<endnetent>()"
msgstr ""
"B<setnetent>(),\n"
"B<endnetent>()"

#. type: tbl table
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "MT-Unsafe race:netent env\n"
msgid ""
"MT-Unsafe race:netent env\n"
"locale"
msgstr "MT-Unsafe race:netent env\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"In the above table, I<netent> in I<race:netent> signifies that if any of the "
"functions B<setnetent>(), B<getnetent>(), or B<endnetent>()  are used in "
"parallel in different threads of a program, then data races could occur."
msgstr ""
"W powyższej tabeli, I<netent> w I<race:netent> oznacza, że jeśli któraś z "
"funkcji B<setnetent>(), B<getnetent>() lub B<endnetent>() jest używana "
"równolegle w różnych wątkach programu, może nastąpić sytuacja wyścigu danych."

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDY"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "POSIX.1-2001, POSIX.1-2008, 4.3BSD."
msgstr "POSIX.1-2001, POSIX.1-2008, 4.3BSD."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "UWAGI"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "In glibc versions before 2.2, the I<net> argument of B<getnetbyaddr>()  "
#| "was of type I<long>."
msgid ""
"Before glibc 2.2, the I<net> argument of B<getnetbyaddr>()  was of type "
"I<long>."
msgstr ""
"W glibc aż do wersji 2.2 argument I<net> funkcji B<getnetbyaddr>() był typu "
"I<long>."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ZOBACZ TAKŻE"

#.  .BR networks (5)
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<getnetent_r>(3), B<getprotoent>(3), B<getservent>(3)"
msgstr "B<getnetent_r>(3), B<getprotoent>(3), B<getservent>(3)"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "RFC\\ 1101"
msgstr "RFC\\ 1101"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "GETNETENT"
msgstr "GETNETENT"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "2020-12-21"
msgstr "21 grudnia 2020 r."

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "GNU"
msgstr "GNU"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Podręcznik programisty Linuksa"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "B<struct netent *getnetbyname(const char *>I<name>B<);>\n"
msgstr "B<struct netent *getnetbyname(const char *>I<name>B<);>\n"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "B<struct netent *getnetbyaddr(uint32_t >I<net>B<, int >I<type>B<);>\n"
msgstr "B<struct netent *getnetbyaddr(uint32_t >I<net>B<, int >I<type>B<);>\n"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "B<void setnetent(int >I<stayopen>B<);>\n"
msgstr "B<void setnetent(int >I<stayopen>B<);>\n"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "B<void endnetent(void);>\n"
msgstr "B<void endnetent(void);>\n"

#. type: tbl table
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "MT-Unsafe race:netent\n"
msgstr "MT-Unsafe race:netent\n"

#. type: tbl table
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid ".br\n"
msgstr ".br\n"

#. type: tbl table
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "race:netentbuf env locale"
msgstr "race:netentbuf env locale"

#. type: tbl table
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "MT-Unsafe race:netbyname\n"
msgstr "MT-Unsafe race:netbyname\n"

#. type: tbl table
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "env locale"
msgstr "env locale"

#. type: tbl table
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "MT-Unsafe race:netbyaddr\n"
msgstr "MT-Unsafe race:netbyaddr\n"

#. type: tbl table
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "locale"
msgstr "locale"

#. type: tbl table
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "B<setnetent>(),\n"
msgstr "B<setnetent>(),\n"

#. type: tbl table
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "B<endnetent>()"
msgstr "B<endnetent>()"

#. type: tbl table
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "MT-Unsafe race:netent env\n"
msgstr "MT-Unsafe race:netent env\n"

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "ZGODNE Z"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"In glibc versions before 2.2, the I<net> argument of B<getnetbyaddr>()  was "
"of type I<long>."
msgstr ""
"W glibc aż do wersji 2.2 argument I<net> funkcji B<getnetbyaddr>() był typu "
"I<long>."

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "O STRONIE"

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Angielska wersja tej strony pochodzi z wydania 5.10 projektu Linux I<man-"
"pages>. Opis projektu, informacje dotyczące zgłaszania błędów oraz najnowszą "
"wersję oryginału można znaleźć pod adresem \\%https://www.kernel.org/doc/man-"
"pages/."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15 września 2017 r."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"The B<getnetent>(), B<getnetbyname>()  and B<getnetbyaddr>()  functions "
"return a pointer to a statically allocated I<netent> structure, or a null "
"pointer if an error occurs or the end of the file is reached."
msgstr ""
"Funkcje B<getnetent>(), B<getnetbyname>() i B<getnetbyaddr>() zwracają "
"wskaźnik do statycznej struktury I<netent> lub wskaźnik null, gdy wystąpi "
"błąd lub napotkany zostanie koniec pliku."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Angielska wersja tej strony pochodzi z wydania 4.16 projektu Linux I<man-"
"pages>. Opis projektu, informacje dotyczące zgłaszania błędów oraz najnowszą "
"wersję oryginału można znaleźć pod adresem \\%https://www.kernel.org/doc/man-"
"pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.02"
msgstr "Linux man-pages 6.02"
