# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Julien Cristau <jcristau@debian.org>, 2006, 2007, 2008.
# Florentin Duneau <fduneau@gmail.com>, 2009.
# David Prévot <david@tilapin.org>, 2011.
msgid ""
msgstr ""
"Project-Id-Version: at 3.1.10\n"
"POT-Creation-Date: 2023-02-15 18:42+0100\n"
"PO-Revision-Date: 2011-07-21 17:42+0200\n"
"Last-Translator: Florentin Duneau <fduneau@gmail.com>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.0\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "AT.ALLOW"
msgstr "AT.ALLOW"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Sep 1997"
msgstr "21 mars 2006"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manuel du programmeur Linux"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "at.allow, at.deny - determine who can submit jobs via at or batch"
msgstr "at.allow, at.deny - Déterminer qui peut utiliser le mécanisme B<at>"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The I</etc/at.allow> and I</etc/at.deny> files determine which user can "
"submit commands for later execution via B<at>(1)  or B<batch>(1)B<.>"
msgstr ""
"Les fichiers I</etc/at.allow> et I</etc/at.deny> déterminent quels "
"utilisateurs peuvent utiliser le mécanisme B<at>(1) ou B<batch>(1) pour "
"mémoriser des commandes à exécuter ultérieurement."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The format of the files is a list of usernames, one on each line.  "
"Whitespace is not permitted."
msgstr ""
"Ces fichiers contiennent une liste de noms d'utilisateurs, un sur chaque "
"ligne. Les espaces sont interdits."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If the file I</etc/at.allow> exists, only usernames mentioned in it are "
"allowed to use B<at>."
msgstr ""
"Si le fichier I</etc/at.allow> existe, seuls les utilisateurs dont les noms "
"sont mentionnés dans ce fichier peuvent utiliser B<at>."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If I</etc/at.allow> does not exist, I</etc/at.deny> is checked, every "
"username not mentioned in it is then allowed to use B<at>."
msgstr ""
"Si I</etc/at.allow> n'existe pas, B<at> vérifie si I</etc/at.deny> existe, "
"et tous les utilisateurs non mentionnés dans ce fichier ont alors le droit "
"d'invoquer B<at>."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "An empty I</etc/at.deny> means that every user may use B<at>."
msgstr ""
"Un fichier I</etc/at.deny> vide signifie que tous les utilisateurs "
"pourraient appeler B<at>."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "If neither exists, only the superuser is allowed to use at."
msgstr ""
"Si aucun de ces deux fichiers n'existe, seul le superutilisateur a le droit "
"d'appeler B<at>."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<at>(1), B<cron>(8), B<crontab>(1), B<atd>(8)."
msgstr "B<at>(1), B<cron>(8), B<crontab>(1), B<atd>(8)."
