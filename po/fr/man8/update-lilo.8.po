# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Florentin Duneau <fduneau@gmail.com>, 2006, 2007.
# Luc Froidefond <luc.froidefond@free.fr>, 2006.
# David Prévot <david@tilapin.org>, 2011-2014.
msgid ""
msgstr ""
"Project-Id-Version: fr\n"
"POT-Creation-Date: 2021-12-05 11:35+0100\n"
"PO-Revision-Date: 2021-01-15 17:32+0100\n"
"Last-Translator: Jean-Paul Guillonneau <guillonneau.jeanpaul@free.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.5\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#. type: ds C+
#: debian-unstable
#, no-wrap
msgid "C\\v'-.1v'\\h'-1p'\\s-2+\\h'-1p'+\\s0\\v'.1v'\\h'-1p'"
msgstr "C\\v'-.1v'\\h'-1p'\\s-2+\\h'-1p'+\\s0\\v'.1v'\\h'-1p'"

#.  ========================================================================
#. type: IX
#: debian-unstable
#, no-wrap
msgid "Title"
msgstr "Titre"

#.  ========================================================================
#. type: IX
#: debian-unstable
#, no-wrap
msgid "UPDATE-LILO 8"
msgstr "UPDATE-LILO 8"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "UPDATE-LILO"
msgstr "UPDATE-LILO"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "2013-06-07"
msgstr "7 juin 2013"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "24.0"
msgstr "24.0"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "lilo documentation"
msgstr "Documentation de lilo"

#. type: SH
#: debian-unstable
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: debian-unstable
msgid "update-lilo - execute lilo in special debconf mode"
msgstr "update-lilo - Exécuter lilo en mode spécifique à debconf"

#. type: IX
#: debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: IX
#: debian-unstable
#, no-wrap
msgid "Header"
msgstr "En-tête"

#. type: Plain text
#: debian-unstable
msgid "\\&B<update-lilo>"
msgstr "\\&B<update-lilo>"

#. type: IX
#: debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: debian-unstable
msgid ""
"This special script only execute lilo with sending all output to E<gt>$2"
msgstr ""
"Ce script spécifique exécute simplement lilo en envoyant toutes les sorties "
"vers E<gt>\\ $2."

#. type: IX
#: debian-unstable
#, no-wrap
msgid "COPYRIGHT and LICENSE"
msgstr "DROIT D’AUTEUR et LICENCE"

#. type: Plain text
#: debian-unstable
msgid "Copyright (C) 2010-2013 Joachim Wiedorn"
msgstr "Copyright © 2010-2013 Joachim Wiedorn"

#. type: Plain text
#: debian-unstable
msgid ""
"This script is free software; you can redistribute it and/or modify it under "
"the terms of the \\s-1GNU\\s0 General Public License as published by the "
"Free Software Foundation; either version 2 of the License, or (at your "
"option) any later version."
msgstr ""
"Ce script est libre, vous pouvez le redistribuer ou le modifier selon les "
"termes de la Licence Publique Générale \\s-1GNU\\s0 publiée par la Free "
"Software Foundation (version 2 ou bien toute autre version ultérieure "
"choisie par vous)."

#. type: Plain text
#: debian-unstable
msgid ""
"On Debian systems, the complete text of the \\s-1GNU\\s0 General Public "
"License version 2 can be found in `/usr/share/common-licenses/GPL-2'."
msgstr ""
"Sur les systèmes Debian, le texte complet de la Licence Publique Générale "
"\\s-1GNU\\s0 version 2 est disponible en I</usr/share/common-licenses/GPL-2>."

#. type: IX
#: debian-unstable
#, no-wrap
msgid "AUTHOR"
msgstr "AUTEUR"

#. type: Plain text
#: debian-unstable
msgid "\\&B<update-lilo> was written by Joachim Wiedorn."
msgstr "\\&B<update-lilo> a été écrit par Joachim Wiedorn."

#. type: Plain text
#: debian-unstable
msgid ""
"This manual page was written by Joachim Wiedorn E<lt>joodebian at joonet."
"deE<gt> for the Debian project (and may be used by others)."
msgstr ""
"Cette page de manuel a été écrite par Joachim Wiedorn E<lt>I<joodebian at "
"joonet.de>E<gt> pour le projet Debian (et peut être utilisée par d'autres)."

#. type: IX
#: debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: debian-unstable
msgid "\\&B<lilo>(8), B<lilo-uuid-diskid>(1), B<liloconfig>(8)"
msgstr "\\&B<lilo>(8), B<lilo-uuid-diskid>(1), B<liloconfig>(8)"
