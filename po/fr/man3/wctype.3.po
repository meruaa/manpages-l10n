# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006.
# Denis Barbier <barbier@debian.org>, 2006, 2010.
# David Prévot <david@tilapin.org>, 2010, 2012.
msgid ""
msgstr ""
"Project-Id-Version: perkamon\n"
"POT-Creation-Date: 2023-02-20 20:41+0100\n"
"PO-Revision-Date: 2020-07-24 11:08+0200\n"
"Last-Translator: Thomas Vincent <tvincent@debian.org>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Poedit 1.8.11\n"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<wctype>()"
msgid "wctype"
msgstr "B<wctype>()"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-12-15"
msgstr "15 décembre 2022"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pages du manuel de Linux 6.03"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "wctype - wide-character classification"
msgstr "wctype - Classification des caractères larges"

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHÈQUE"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Bibliothèque C standard (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>wctype.hE<gt>>\n"
msgstr "B<#include E<lt>wctype.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<wctype_t wctype(const char *>I<name>B<);>\n"
msgstr "B<wctype_t wctype(const char *>I<nom>B<);>\n"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The I<wctype_t> type represents a property which a wide character may or may "
"not have.  In other words, it represents a class of wide characters.  This "
"type's nature is implementation-dependent, but the special value "
"I<(wctype_t) 0> denotes an invalid property.  Nonzero I<wctype_t> values can "
"be passed to the B<iswctype>(3)  function to actually test whether a given "
"wide character has the property."
msgstr ""
"Le type I<wctype_t> représente une propriété qu'un caractère large peut "
"avoir ou non. En d'autres termes, il représente une classe de caractères "
"larges. La nature exacte de ce type dépend de l'implémentation, mais la "
"valeur spéciale I<(wctype_t) 0> correspond à une propriété non valable. Un "
"I<wctype_t> non nul peut être transmis à la fonction B<iswctype>(3) pour "
"vérifier si un caractère large a, ou n'a pas, la propriété correspondante."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The B<wctype>()  function returns a property, given by its name.  The set of "
"valid names depends on the B<LC_CTYPE> category of the current locale, but "
"the following names are valid in all locales."
msgstr ""
"La fonction B<wctype>() renvoie une propriété à partir de son nom. Les noms "
"valables dépendent de la catégorie B<LC_CTYPE> de la locale utilisée, mais "
"les noms suivants sont toujours valables quelle que soit la localisation."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"    \"alnum\" - realizes the B<isalnum>(3) classification function\n"
"    \"alpha\" - realizes the B<isalpha>(3) classification function\n"
"    \"blank\" - realizes the B<isblank>(3) classification function\n"
"    \"cntrl\" - realizes the B<iscntrl>(3) classification function\n"
"    \"digit\" - realizes the B<isdigit>(3) classification function\n"
"    \"graph\" - realizes the B<isgraph>(3) classification function\n"
"    \"lower\" - realizes the B<islower>(3) classification function\n"
"    \"print\" - realizes the B<isprint>(3) classification function\n"
"    \"punct\" - realizes the B<ispunct>(3) classification function\n"
"    \"space\" - realizes the B<isspace>(3) classification function\n"
"    \"upper\" - realizes the B<isupper>(3) classification function\n"
"    \"xdigit\" - realizes the B<isxdigit>(3) classification function\n"
msgstr ""
"  \"alnum\" – correspond à la fonction de classification B<isalnum>(3)\n"
"  \"alpha\" – correspond à la fonction de classification B<isalpha>(3)\n"
"  \"blank\" – correspond à la fonction de classification B<isblank>(3)\n"
"  \"cntrl\" – correspond à la fonction de classification B<iscntrl>(3)\n"
"  \"digit\" – correspond à la fonction de classification B<isdigit>(3)\n"
"  \"graph\" – correspond à la fonction de classification B<isgraph>(3)\n"
"  \"lower\" – correspond à la fonction de classification B<islower>(3)\n"
"  \"print\" – correspond à la fonction de classification B<isprint>(3)\n"
"  \"punct\" – correspond à la fonction de classification B<ispunct>(3)\n"
"  \"space\" – correspond à la fonction de classification B<isspace>(3)\n"
"  \"upper\" – correspond à la fonction de classification B<isupper>(3)\n"
"  \"xdigit\" – correspond à la fonction de classification B<isxdigit>(3)\n"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALEUR RENVOYÉE"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The B<wctype>()  function returns a property descriptor if the I<name> is "
"valid.  Otherwise, it returns I<(wctype_t) 0>."
msgstr ""
"La fonction B<wctype>() renvoie un descripteur de propriété si le I<nom> est "
"valable. Sinon, elle renvoie I<(wctype_t) 0>."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATTRIBUTS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Pour une explication des termes utilisés dans cette section, consulter "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interface"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Attribut"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valeur"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<wctype>()"
msgstr "B<wctype>()"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Sécurité des threads"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe locale"
msgstr "MT-Safe locale"

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "POSIX.1-2001, POSIX.1-2008, C99."
msgstr "POSIX.1-2001, POSIX.1-2008, C99."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The behavior of B<wctype>()  depends on the B<LC_CTYPE> category of the "
"current locale."
msgstr ""
"Le comportement de la fonction B<wctype>() dépend de la catégorie "
"B<LC_CTYPE> de la locale utilisée."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<iswctype>(3)"
msgstr "B<iswctype>(3)"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "WCTYPE"
msgstr "WCTYPE"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "2015-08-08"
msgstr "8 août 2015"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "GNU"
msgstr "GNU"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manuel du programmeur Linux"

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORMITÉ"

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "COLOPHON"

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 5.10 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 4.16 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.02"
msgstr "Pages du manuel de Linux 6.02"
