# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006.
# Denis Barbier <barbier@debian.org>, 2006, 2010.
# David Prévot <david@tilapin.org>, 2010-2014.
# Frédéric Hantrais <fhantrais@gmail.com>, 2013, 2014.
msgid ""
msgstr ""
"Project-Id-Version: perkamon\n"
"POT-Creation-Date: 2023-02-20 20:02+0100\n"
"PO-Revision-Date: 2022-10-26 21:22+0200\n"
"Last-Translator: Weblate Admin <jean-baptiste@holcroft.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 3.1.1\n"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<ctermid>()"
msgid "ctermid"
msgstr "B<ctermid>()"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-12-15"
msgstr "15 décembre 2022"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pages du manuel de Linux 6.03"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "ctermid - get controlling terminal name"
msgstr "ctermid - Obtenir le nom du terminal de contrôle"

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHÈQUE"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Bibliothèque C standard (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#.  POSIX also requires this function to be declared in <unistd.h>,
#.  and glibc does so if suitable feature test macros are defined.
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>stdio.hE<gt>>\n"
msgstr "B<#include E<lt>stdio.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<char *ctermid(char *>I<s>B<);>\n"
msgstr "B<char *ctermid(char *>I<s>B<);>\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Exigences de macros de test de fonctionnalités pour la glibc (consulter "
"B<feature_test_macros>(7)) :"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "B<ctermid>():"
msgstr "B<ctermid>() :"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "    _POSIX_C_SOURCE\n"
msgstr "    _POSIX_C_SOURCE\n"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<ctermid>()  returns a string which is the pathname for the current "
"controlling terminal for this process.  If I<s> is NULL, a static buffer is "
"used, otherwise I<s> points to a buffer used to hold the terminal pathname.  "
"The symbolic constant B<L_ctermid> is the maximum number of characters in "
"the returned pathname."
msgstr ""
"La fonction B<ctermid>() renvoie une chaîne de caractères correspondant au "
"chemin d'accès du terminal contrôlant le processus en cours. Si I<s> vaut "
"NULL, un tampon statique est utilisé pour renvoyer la chaîne, sinon I<s> "
"doit pointer sur le tampon à remplir avec le nom du terminal. La constante "
"symbolique B<L_ctermid> représente le nombre maximal de caractères dans le "
"nom renvoyé."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALEUR RENVOYÉE"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The pointer to the pathname."
msgstr "Un pointeur sur le nom du terminal."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATTRIBUTS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Pour une explication des termes utilisés dans cette section, consulter "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interface"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Attribut"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valeur"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ctermid>()"
msgstr "B<ctermid>()"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Sécurité des threads"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "POSIX.1-2001, POSIX.1-2008, Svr4."
msgstr "POSIX.1-2001, POSIX.1-2008, Svr4."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "BOGUES"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The returned pathname may not uniquely identify the controlling terminal; it "
"may, for example, be I</dev/tty>."
msgstr ""
"Le chemin renvoyé n'identifie pas obligatoirement le terminal de contrôle de "
"manière unique. Ce peut être, par exemple, I</dev/tty>."

#. #-#-#-#-#  archlinux: ctermid.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  in glibc 2.3.x, x >= 4, the glibc headers threw an error
#.  if ctermid() was given an argument; fixed in glibc 2.4.
#. type: Plain text
#. #-#-#-#-#  debian-bullseye: ctermid.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  in glibc 2.3.x, x >= 4, the glibc headers threw an error
#.  if ctermid() was given an argument; fixed in 2.4.
#. type: Plain text
#. #-#-#-#-#  debian-unstable: ctermid.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  in glibc 2.3.x, x >= 4, the glibc headers threw an error
#.  if ctermid() was given an argument; fixed in glibc 2.4.
#. type: Plain text
#. #-#-#-#-#  fedora-38: ctermid.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  in glibc 2.3.x, x >= 4, the glibc headers threw an error
#.  if ctermid() was given an argument; fixed in glibc 2.4.
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: ctermid.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  in glibc 2.3.x, x >= 4, the glibc headers threw an error
#.  if ctermid() was given an argument; fixed in glibc 2.4.
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: ctermid.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  in glibc 2.3.x, x >= 4, the glibc headers threw an error
#.  if ctermid() was given an argument; fixed in glibc 2.4.
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-15-5: ctermid.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  in glibc 2.3.x, x >= 4, the glibc headers threw an error
#.  if ctermid() was given an argument; fixed in 2.4.
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: ctermid.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  in glibc 2.3.x, x >= 4, the glibc headers threw an error
#.  if ctermid() was given an argument; fixed in glibc 2.4.
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "It is not assured that the program can open the terminal."
msgstr "Il n'est pas garanti que le programme puisse ouvrir le terminal."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<ttyname>(3)"
msgstr "B<ttyname>(3)"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "CTERMID"
msgstr "CTERMID"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "2019-03-06"
msgstr "6 mars 2019"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "GNU"
msgstr "GNU"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manuel du programmeur Linux"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "B<ctermid>(): _POSIX_C_SOURCE"
msgstr "B<ctermid>() : _POSIX_C_SOURCE"

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORMITÉ"

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "COLOPHON"

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 5.10 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2016-03-15"
msgstr "15 mars 2016"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"The path returned may not uniquely identify the controlling terminal; it "
"may, for example, be I</dev/tty>."
msgstr ""
"Le chemin renvoyé n'identifie pas obligatoirement le terminal de contrôle de "
"manière unique. Ce peut être, par exemple, I</dev/tty>."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 4.16 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.02"
msgstr "Pages du manuel de Linux 6.02"
