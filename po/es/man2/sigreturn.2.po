# Spanish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Gerardo Aburruzaga García <gerardo.aburruzaga@uca.es>, 1998.
# Juan Piernas <piernas@ditec.um.es>, 1999.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-02-20 20:30+0100\n"
"PO-Revision-Date: 1999-01-26 19:53+0200\n"
"Last-Translator: Juan Piernas <piernas@ditec.um.es>\n"
"Language-Team: Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.04.1\n"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<return>"
msgid "sigreturn"
msgstr "B<return>"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 Febrero 2023"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Páginas de manual de Linux 6.03"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOMBRE"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"sigreturn, rt_sigreturn - return from signal handler and cleanup stack frame"
msgstr ""
"sigreturn, rt_sigreturn - regresa desde el manejador de señales y limpia el "
"marco de pila"

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Biblioteca Estándar C (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<int sigreturn(...);>"
msgid "B<int sigreturn(...);>\n"
msgstr "B<int sigreturn(...);>"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPCIÓN"

#. #-#-#-#-#  archlinux: sigreturn.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  See arch/x86/kernel/signal.c::__setup_frame() [in Linux 3.17 source code]
#. type: Plain text
#. #-#-#-#-#  debian-bullseye: sigreturn.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  See arch/x86/kernel/signal.c::__setup_frame() [in 3.17 source code]
#. type: Plain text
#. #-#-#-#-#  debian-unstable: sigreturn.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  See arch/x86/kernel/signal.c::__setup_frame() [in Linux 3.17 source code]
#. type: Plain text
#. #-#-#-#-#  fedora-38: sigreturn.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  See arch/x86/kernel/signal.c::__setup_frame() [in Linux 3.17 source code]
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: sigreturn.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  See arch/x86/kernel/signal.c::__setup_frame() [in Linux 3.17 source code]
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: sigreturn.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  See arch/x86/kernel/signal.c::__setup_frame() [in Linux 3.17 source code]
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-15-5: sigreturn.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  See arch/x86/kernel/signal.c::__setup_frame() [in 3.17 source code]
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: sigreturn.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  See arch/x86/kernel/signal.c::__setup_frame() [in Linux 3.17 source code]
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If the Linux kernel determines that an unblocked signal is pending for a "
"process, then, at the next transition back to user mode in that process (e."
"g., upon return from a system call or when the process is rescheduled onto "
"the CPU), it creates a new frame on the user-space stack where it saves "
"various pieces of process context (processor status word, registers, signal "
"mask, and signal stack settings)."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The kernel also arranges that, during the transition back to user mode, the "
"signal handler is called, and that, upon return from the handler, control "
"passes to a piece of user-space code commonly called the \"signal "
"trampoline\".  The signal trampoline code in turn calls B<sigreturn>()."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"This B<sigreturn>()  call undoes everything that was done\\[em]changing the "
"process's signal mask, switching signal stacks (see "
"B<sigaltstack>(2))\\[em]in order to invoke the signal handler.  Using the "
"information that was earlier saved on the user-space stack B<sigreturn>()  "
"restores the process's signal mask, switches stacks, and restores the "
"process's context (processor flags and registers, including the stack "
"pointer and instruction pointer), so that the process resumes execution at "
"the point where it was interrupted by the signal."
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALOR DEVUELTO"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<sigreturn>()  never returns."
msgstr "B<sigreturn>() nunca regresa."

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "ESTÁNDARES"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Many UNIX-type systems have a B<sigreturn>()  system call or near "
"equivalent.  However, this call is not specified in POSIX, and details of "
"its behavior vary across systems."
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTAS"

#.  See sysdeps/unix/sysv/linux/sigreturn.c and
#.  signal/sigreturn.c in the glibc source
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<sigreturn>()  exists only to allow the implementation of signal handlers.  "
"It should B<never> be called directly.  (Indeed, a simple B<sigreturn>()  "
"wrapper in the GNU C library simply returns -1, with I<errno> set to "
"B<ENOSYS>.)  Details of the arguments (if any) passed to B<sigreturn>()  "
"vary depending on the architecture.  (On some architectures, such as x86-64, "
"B<sigreturn>()  takes no arguments, since all of the information that it "
"requires is available in the stack frame that was previously created by the "
"kernel on the user-space stack.)"
msgstr ""

#.  See, for example, sysdeps/unix/sysv/linux/i386/sigaction.c and
#.  sysdeps/unix/sysv/linux/x86_64/sigaction.c in the glibc (2.20) source.
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Once upon a time, UNIX systems placed the signal trampoline code onto the "
"user stack.  Nowadays, pages of the user stack are protected so as to "
"disallow code execution.  Thus, on contemporary Linux systems, depending on "
"the architecture, the signal trampoline code lives either in the B<vdso>(7)  "
"or in the C library.  In the latter case, the C library's B<sigaction>(2)  "
"wrapper function informs the kernel of the location of the trampoline code "
"by placing its address in the I<sa_restorer> field of the I<sigaction> "
"structure, and sets the B<SA_RESTORER> flag in the I<sa_flags> field."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The saved process context information is placed in a I<ucontext_t> structure "
"(see I<E<lt>sys/ucontext.hE<gt>>).  That structure is visible within the "
"signal handler as the third argument of a handler established via "
"B<sigaction>(2)  with the B<SA_SIGINFO> flag."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"On some other UNIX systems, the operation of the signal trampoline differs a "
"little.  In particular, on some systems, upon transitioning back to user "
"mode, the kernel passes control to the trampoline (rather than the signal "
"handler), and the trampoline code calls the signal handler (and then calls "
"B<sigreturn>()  once the handler returns)."
msgstr ""

#. type: SS
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "C library/kernel differences"
msgstr "Diferencias núcleo / biblioteca C"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The original Linux system call was named B<sigreturn>().  However, with the "
"addition of real-time signals in Linux 2.2, a new system call, "
"B<rt_sigreturn>()  was added to support an enlarged I<sigset_t> type.  The "
"GNU C library hides these details from us, transparently employing "
"B<rt_sigreturn>()  when the kernel provides it."
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VÉASE TAMBIÉN"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<kill>(2), B<restart_syscall>(2), B<sigaltstack>(2), B<signal>(2), "
"B<getcontext>(3), B<signal>(7), B<vdso>(7)"
msgstr ""
"B<kill>(2), B<restart_syscall>(2), B<sigaltstack>(2), B<signal>(2), "
"B<getcontext>(3), B<signal>(7), B<vdso>(7)"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "SIGRETURN"
msgstr "SIGRETURN"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15 Septiembre 2017"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manual del Programador de Linux"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "B<int sigreturn(...);>"
msgstr "B<int sigreturn(...);>"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"This B<sigreturn>()  call undoes everything that was done\\(emchanging the "
"process's signal mask, switching signal stacks (see "
"B<sigaltstack>(2))\\(emin order to invoke the signal handler.  Using the "
"information that was earlier saved on the user-space stack B<sigreturn>()  "
"restores the process's signal mask, switches stacks, and restores the "
"process's context (processor flags and registers, including the stack "
"pointer and instruction pointer), so that the process resumes execution at "
"the point where it was interrupted by the signal."
msgstr ""

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORME A"

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "COLOFÓN"

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Esta página es parte de la versión 5.10 del proyecto Linux I<man-pages>. "
"Puede encontrar una descripción del proyecto, información sobre cómo "
"informar errores y la última versión de esta página en \\%https://www.kernel."
"org/doc/man-pages/."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Esta página es parte de la versión 4.16 del proyecto Linux I<man-pages>. "
"Puede encontrar una descripción del proyecto, información sobre cómo "
"informar errores y la última versión de esta página en \\%https://www.kernel."
"org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-12-04"
msgstr "4 Diciembre 2022"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.02"
msgstr "Páginas de manual de Linux 6.02"
