# Spanish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Gerardo Aburruzaga García <gerardo.aburruzaga@uca.es>, 1998.
# Marcos Fouces <marcos@debian.org>, 2021-2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-02-20 20:11+0100\n"
"PO-Revision-Date: 2023-01-12 24:00+0100\n"
"Last-Translator: Marcos Fouces <marcos@debian.org>\n"
"Language-Team: Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "intro"
msgstr "intro"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 Febrero 2023"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Páginas de manual de Linux 6.03"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOMBRE"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "intro - introduction to library functions"
msgstr "intro - introducción a las funciones de biblioteca"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPCIÓN"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, fuzzy
#| msgid ""
#| "Section 3 of the manual describes all library functions excluding the "
#| "library functions (system call wrappers) described in Section 2, which "
#| "implement system calls."
msgid ""
"Section 3 of the manual describes all library functions excluding the "
"library functions (system call wrappers)  described in Section 2, which "
"implement system calls."
msgstr ""
"La sección 3 de este manual describe todas las funciones de biblioteca salvo "
"las descritas en la sección 2 que implementan las llamadas del sistema."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Many of the functions described in the section are part of the Standard C "
"Library (I<libc>).  Some functions are part of other libraries (e.g., the "
"math library, I<libm>, or the real-time library, I<librt>)  in which case "
"the manual page will indicate the linker option needed to link against the "
"required library (e.g., I<-lm> and I<-lrt>, respectively, for the "
"aforementioned libraries)."
msgstr ""
"Gran parte de las funciones aquí descritas forman parte de la biblioteca "
"estándar de C (I<libc>). Algunas forman parte de otras bibliotecas (p.ej:la "
"biblioteca matemática I<libm> o la de tiempo real I<librt>), en cuyo caso la "
"paǵina de manual indicará la forma de enlazarse a la biblioteca requerida, p."
"ej.: I<-lm> y I<-lrt> respectivamente para las bibliotecas previamente "
"citadas"

#
#.  There
#.  are various function groups which can be identified by a letter which
#.  is appended to the chapter number:
#.  .IP (3C)
#.  These functions,
#.  the functions from chapter 2 and from chapter 3S are
#.  contained in the C standard library libc,
#.  which will be used by
#.  .BR cc (1)
#.  by default.
#.  .IP (3S)
#.  These functions are parts of the
#.  .BR stdio (3)
#.  library.  They are contained in the standard C library libc.
#.  .IP (3M)
#.  These functions are contained in the arithmetic library libm.  They are
#.  used by the
#.  .BR f77 (1)
#.  FORTRAN compiler by default,
#.  but not by the
#.  .BR cc (1)
#.  C compiler,
#.  which needs the option \fI\-lm\fP.
#.  .IP (3F)
#.  These functions are part of the FORTRAN library libF77.  There are no
#.  special compiler flags needed to use these functions.
#.  .IP (3X)
#.  Various special libraries.  The manual pages documenting their functions
#.  specify the library names.
#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, fuzzy
#| msgid ""
#| "In some cases, the programmer must define a feature test macro in order "
#| "to obtain the declaration of a function from the header file specified in "
#| "the man page SYNOPSIS section.  (Where required, these feature test "
#| "macros must be defined before including I<any> header files.)  In such "
#| "cases, the required macro is described in the man page.  For further "
#| "information on feature test macros, see B<feature_test_macros>(7)."
msgid ""
"In some cases, the programmer must define a feature test macro in order to "
"obtain the declaration of a function from the header file specified in the "
"man page SYNOPSIS section.  (Where required, these I<feature test macros> "
"must be defined before including I<any> header files.)  In such cases, the "
"required macro is described in the man page.  For further information on "
"feature test macros, see B<feature_test_macros>(7)."
msgstr ""
"En algunos casos, el programador debe definir una macro que pruebe ciertas "
"características para obtener la declaración de una función del archivo de "
"encabezado especificado en la sección SINOPSIS de la página de manual. "
"Cuando sea necesario, estas macros de prueba deben definirse antes de "
"incluir cualquier archivo de encabezado. En tales casos, la macro requerida "
"se describirá en la página de manual. Consulte B<feature_test_macros>(7)."

#. type: SS
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "Subsections"
msgstr "Subsecciones"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Section 3 of this manual is organized into subsections that reflect the "
"complex structure of the standard C library and its many implementations:"
msgstr ""
"La sección 3 de este manual se organiza en subsecciones que reflejan la "
"complejidad de la librería estándar de C y sus múltiples implementaciones."

#. type: IP
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "\\[bu]"
msgstr "\\[bu]"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "3const"
msgstr "3const"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "3head"
msgstr "3head"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "3type"
msgstr "3type"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"This difficult history frequently makes it a poor example to follow in "
"design, implementation, and presentation."
msgstr ""
"Esta compleja historia hace que muchas veces sea un pobre ejemplo a seguir "
"en cuanto a diseño, implementación y presentación."

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, fuzzy
#| msgid ""
#| "Ideally, a library for the C language is designed such that each header "
#| "file presents the interface to a coherent software module.  It provides a "
#| "small number of function declarations and exposes only data types and "
#| "constants that are required for use of those functions.  Together, these "
#| "are termed an API or I<application program interface>.  Types and "
#| "constants to be shared among multiple APIs shopuld be placed in header "
#| "files that declare no functions.  This organization permits a C library "
#| "module to be documented concisely with one header file per manual page.  "
#| "Such an approach improves the readability and accessibility of library "
#| "documentation, and thereby the usability of the software."
msgid ""
"Ideally, a library for the C language is designed such that each header file "
"presents the interface to a coherent software module.  It provides a small "
"number of function declarations and exposes only data types and constants "
"that are required for use of those functions.  Together, these are termed an "
"API or I<application program interface>.  Types and constants to be shared "
"among multiple APIs should be placed in header files that declare no "
"functions.  This organization permits a C library module to be documented "
"concisely with one header file per manual page.  Such an approach improves "
"the readability and accessibility of library documentation, and thereby the "
"usability of the software."
msgstr ""
"Idealmente, en el diseño de una librería de C cada archivo de encabezado "
"contiene la interfaz de un módulo coherente de software. Debería "
"proporcionar unas pocas declaraciones de funciones y mostrar sólo tipos y "
"constantes requeridas por dichas funciones. En conjunto, esto es lo que se "
"denomina una API (acrónimo del inglés I<application program interface>. Los "
"tipos y constantes que se compartan entre distintas API deben situarse en "
"archivos de encabezado que no declaren funciones. Esta estructura "
"organizativa permite que una librería de C se pueda documentar de forma "
"concisa con un archivo de encabezado en cada página de manual. Esto permite "
"que la documentación de la librería sea más accesible y legible mejorando "
"la  usabilidad del software."

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "ESTÁNDARES"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Certain terms and abbreviations are used to indicate UNIX variants and "
"standards to which calls in this section conform.  See B<standards>(7)."
msgstr ""
"Algunos términos y abreviaturas se emplean para señalar variantes de UNIX y "
"estándares que siguen las llamadas de esta sección. Consulte B<standards>(7)."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTAS"

#. type: SS
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Authors and copyright conditions"
msgstr "Autores y derechos de autor"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Look at the header of the manual page source for the author(s) and copyright "
"conditions.  Note that these can be different from page to page!"
msgstr ""
"En el encabezado del código fuente de cada página de manual se encuentran "
"los datos del autor de la misma y de los derechos de autor. Tenga en cuenta "
"que esa información puede ser distinta entre cada página."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VÉASE TAMBIÉN"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"B<intro>(2), B<errno>(3), B<capabilities>(7), B<credentials>(7), "
"B<environ>(7), B<feature_test_macros>(7), B<libc>(7), B<math_error>(7), "
"B<path_resolution>(7), B<pthreads>(7), B<signal>(7), B<standards>(7), "
"B<system_data_types>(7)"
msgstr ""
"B<intro>(2), B<errno>(3), B<capabilities>(7), B<credentials>(7), "
"B<environ>(7), B<feature_test_macros>(7), B<libc>(7), B<math_error>(7), "
"B<path_resolution>(7), B<pthreads>(7), B<signal>(7), B<standards>(7), "
"B<system_data_types>(7)"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "INTRO"
msgstr "INTRO"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "2020-11-01"
msgstr "1 Noviembre 2020"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manual del Programador de Linux"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Section 3 of the manual describes all library functions excluding the "
"library functions (system call wrappers) described in Section 2, which "
"implement system calls."
msgstr ""
"La sección 3 de este manual describe todas las funciones de biblioteca salvo "
"las descritas en la sección 2 que implementan las llamadas del sistema."

#
#.  There
#.  are various function groups which can be identified by a letter which
#.  is appended to the chapter number:
#.  .IP (3C)
#.  These functions, the functions from chapter 2 and from chapter 3S are
#.  contained in the C standard library libc, which will be used by
#.  .BR cc (1)
#.  by default.
#.  .IP (3S)
#.  These functions are parts of the
#.  .BR stdio (3)
#.  library.  They are contained in the standard C library libc.
#.  .IP (3M)
#.  These functions are contained in the arithmetic library libm.  They are
#.  used by the
#.  .BR f77 (1)
#.  FORTRAN compiler by default, but not by the
#.  .BR cc (1)
#.  C compiler, which needs the option \fI\-lm\fP.
#.  .IP (3F)
#.  These functions are part of the FORTRAN library libF77.  There are no
#.  special compiler flags needed to use these functions.
#.  .IP (3X)
#.  Various special libraries.  The manual pages documenting their functions
#.  specify the library names.
#. type: Plain text
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"In some cases, the programmer must define a feature test macro in order to "
"obtain the declaration of a function from the header file specified in the "
"man page SYNOPSIS section.  (Where required, these feature test macros must "
"be defined before including I<any> header files.)  In such cases, the "
"required macro is described in the man page.  For further information on "
"feature test macros, see B<feature_test_macros>(7)."
msgstr ""
"En algunos casos, el programador debe definir una macro que pruebe ciertas "
"características para obtener la declaración de una función del archivo de "
"encabezado especificado en la sección SINOPSIS de la página de manual. "
"Cuando sea necesario, estas macros de prueba deben definirse antes de "
"incluir cualquier archivo de encabezado. En tales casos, la macro requerida "
"se describirá en la página de manual. Consulte B<feature_test_macros>(7)."

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORME A"

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "COLOFÓN"

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Esta página es parte de la versión 5.10 del proyecto Linux I<man-pages>. "
"Puede encontrar una descripción del proyecto, información sobre cómo "
"informar errores y la última versión de esta página en \\%https://www.kernel."
"org/doc/man-pages/."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15 Septiembre 2017"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"B<intro>(2), B<errno>(3), B<capabilities>(7), B<credentials>(7), "
"B<environ>(7), B<feature_test_macros>(7), B<libc>(7), B<math_error>(7), "
"B<path_resolution>(7), B<pthreads>(7), B<signal>(7), B<standards>(7)"
msgstr ""
"B<intro>(2), B<errno>(3), B<capabilities>(7), B<credentials>(7), "
"B<environ>(7), B<feature_test_macros>(7), B<libc>(7), B<math_error>(7), "
"B<path_resolution>(7), B<pthreads>(7), B<signal>(7), B<standards>(7)"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Esta página es parte de la versión 4.16 del proyecto Linux I<man-pages>. "
"Puede encontrar una descripción del proyecto, información sobre cómo "
"informar errores y la última versión de esta página en \\%https://www.kernel."
"org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-12-12"
msgstr "12 Diciembre 2022"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.02"
msgstr "Páginas de manual de Linux 6.02"

#. type: IP
#: opensuse-tumbleweed
#, no-wrap
msgid "\\(bu"
msgstr "\\(bu"

#. type: Plain text
#: opensuse-tumbleweed
msgid ""
"Ideally, a library for the C language is designed such that each header file "
"presents the interface to a coherent software module.  It provides a small "
"number of function declarations and exposes only data types and constants "
"that are required for use of those functions.  Together, these are termed an "
"API or I<application program interface>.  Types and constants to be shared "
"among multiple APIs shopuld be placed in header files that declare no "
"functions.  This organization permits a C library module to be documented "
"concisely with one header file per manual page.  Such an approach improves "
"the readability and accessibility of library documentation, and thereby the "
"usability of the software."
msgstr ""
"Idealmente, en el diseño de una librería de C cada archivo de encabezado "
"contiene la interfaz de un módulo coherente de software. Debería "
"proporcionar unas pocas declaraciones de funciones y mostrar sólo tipos y "
"constantes requeridas por dichas funciones. En conjunto, esto es lo que se "
"denomina una API (acrónimo del inglés I<application program interface>. Los "
"tipos y constantes que se compartan entre distintas API deben situarse en "
"archivos de encabezado que no declaren funciones. Esta estructura "
"organizativa permite que una librería de C se pueda documentar de forma "
"concisa con un archivo de encabezado en cada página de manual. Esto permite "
"que la documentación de la librería sea más accesible y legible mejorando "
"la  usabilidad del software."
