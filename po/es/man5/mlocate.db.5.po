# Spanish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Vicente Pastor Gómez <vpastorg@santandersupernet.com>, 1998.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2022-06-16 17:16+0200\n"
"PO-Revision-Date: 1998-05-26 19:53+0200\n"
"Last-Translator: Vicente Pastor Gómez <vpastorg@santandersupernet.com>\n"
"Language-Team: Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.04.1\n"

#. type: TH
#: archlinux
#, no-wrap
msgid "mlocate.db"
msgstr "mlocate.db"

#. type: TH
#: archlinux
#, no-wrap
msgid "Jan 2007"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "mlocate"
msgstr "mlocate"

#. type: SH
#: archlinux
#, no-wrap
msgid "NAME"
msgstr "NOMBRE"

#. type: Plain text
#: archlinux
#, fuzzy
msgid "mlocate.db - a mlocate database"
msgstr "locatedb - base de datos de cabecera comprimida de nombres de fichero"

#. type: SH
#: archlinux
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPCIÓN"

#. type: Plain text
#: archlinux
msgid ""
"A mlocate database starts with a file header: 8 bytes for a magic number "
"(B<\"\\e0mlocate\"> like a C literal), 4 bytes for the I<configuration "
"block> size in big endian, 1 byte for file format version (B<0>), 1 byte for "
"the ``require visibility'' flag (B<0> or B<1>), 2 bytes padding, and a "
"\\f(SMNUL\\fR-terminated path name of the root of the database."
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"The header is followed by a I<configuration block>, included to ensure "
"databases are not reused if some configuration changes could affect their "
"contents.  The size of the configuration block in bytes is stored in the "
"file header.  The configuration block is a sequence of I<variable "
"assignments>, ordered by variable name.  Each I<variable assignment> "
"consists of a \\f(SMNUL\\fR-terminated variable name and an ordered list of "
"\\f(SMNUL\\fR-terminated values.  The value list is terminated by one more "
"E<.SM NUL> character.  The ordering used is defined by the B<strcmp ()> "
"function."
msgstr ""

#. type: Plain text
#: archlinux
msgid "Currently defined variables are:"
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "B<prune_bind_mounts>"
msgstr "B<prune_bind_mounts>"

#. type: Plain text
#: archlinux
msgid ""
"A single entry, the value of B<PRUNE_BIND_MOUNTS>; one of the strings B<0> "
"or B<1>."
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "B<prunefs>"
msgstr "B<prunefs>"

#. type: Plain text
#: archlinux
msgid "The value of B<PRUNEFS>, each entry is converted to uppercase."
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "B<prunepaths>"
msgstr "B<prunepaths>"

#. type: Plain text
#: archlinux
msgid "The value of B<PRUNEPATHS>."
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"The rest of the file until E<.SM EOF> describes directories and their "
"contents.  Each directory starts with a header: 8 bytes for I<directory "
"time> (seconds) in big endian, 4 bytes for I<directory time> (nanoseconds) "
"in big endian (0 if unknown, less than 1,000,000,000), 4 bytes padding, and "
"a \\f(SMNUL\\fR-terminated path name of the the directory.  Directory "
"contents, a sequence of I<file entries> sorted by name, follow."
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"I<Directory time> is the maximum of B<st_ctime> and B<st_mtime> of the "
"directory.  B<updatedb>(8)  uses the original data if the I<directory time> "
"in the database and in the file system match exactly.  I<Directory time> "
"equal to 0 always causes rescanning of the directory: this is necessary to "
"handle directories which were being updated while building the database."
msgstr ""

#. type: Plain text
#: archlinux
msgid "Each I<file entry> starts with a single byte, marking its type:"
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "B<0>"
msgstr "B<0>"

#. type: Plain text
#: archlinux
msgid ""
"A non-directory file.  Followed by a \\f(SMNUL\\fR-terminated file (not "
"path) name."
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "B<1>"
msgstr "B<1>"

#. type: Plain text
#: archlinux
msgid ""
"A subdirectory.  Followed by a \\f(SMNUL\\fR-terminated file (not path) name."
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "B<2>"
msgstr "B<2>"

#. type: Plain text
#: archlinux
msgid "Marks the end of the current directory."
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"B<locate(1)> only reports file entries, directory names are not reported "
"because they are reported as an entry in their parent directory.  The only "
"exception is the root directory of the database, which is stored in the file "
"header."
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: archlinux
msgid "Miloslav Trmac E<lt>mitr@redhat.comE<gt>"
msgstr "Miloslav Trmac E<lt>mitr@redhat.comE<gt>"

#. type: SH
#: archlinux
#, no-wrap
msgid "SEE ALSO"
msgstr "VÉASE TAMBIÉN"

#. type: Plain text
#: archlinux
msgid "B<locate>(1), B<updatedb.conf>(5), B<updatedb>(8)"
msgstr "B<locate>(1), B<updatedb.conf>(5), B<updatedb>(8)"
