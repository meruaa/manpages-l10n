# Spanish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Diego Sevilla Ruiz <dsevilla@ditec.um.es>, 1999.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2020-11-24 18:45+01:00\n"
"PO-Revision-Date: 1999-01-22 00:21+0100\n"
"Last-Translator: Diego Sevilla Ruiz <dsevilla@ditec.um.es>\n"
"Language-Team: Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: man8/psupdate.8:8
#, no-wrap
msgid "PSUPDATE"
msgstr "PSUPDATE"

#. type: TH
#: man8/psupdate.8:8
#, no-wrap
msgid "1996/01/06 "
msgstr "6 Enero 1996 "

#. type: TH
#: man8/psupdate.8:8
#, no-wrap
msgid "Cohesive Systems & NRAO"
msgstr "Cohesive Systems & NRAO"

#. type: TH
#: man8/psupdate.8:8
#, no-wrap
msgid "Linux Programming Manual"
msgstr "Manual del Programador de Linux"

#. type: SH
#: man8/psupdate.8:9
#, no-wrap
msgid "NAME"
msgstr "NOMBRE"

#. type: Plain text
#: man8/psupdate.8:11
msgid "...01"
msgstr ""
"psupdate - actualiza la base de datos de desplazamientos de kernel de ps"

#. type: SH
#: man8/psupdate.8:11
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: man8/psupdate.8:13
msgid "...02"
msgstr "psupdate [path a la imagen descomprimida del kernel]"

#. type: SH
#: man8/psupdate.8:13
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPCIÓN"

#. type: Plain text
#: man8/psupdate.8:17
msgid "...03"
msgstr ""
"B<psupdate> actualiza el fichero /etc/psdatabase para que corresponda al "
"mapa del sistema del kernel actual, por defecto /usr/src/linux/vmlinux."

#. type: SS
#: man8/psupdate.8:17
#, no-wrap
msgid "Options"
msgstr "Opciones"

#. type: Plain text
#: man8/psupdate.8:21
msgid "...04"
msgstr ""
"Si su fichero de mapa del sistema no es /usr/src/linux/vmlinux, debe "
"especificar el nombre de un fichero de mapa alternativo en la línea de "
"comandos.  Este es el único argumento actualmente soportado."

#. type: SH
#: man8/psupdate.8:22
#, no-wrap
msgid "FILES"
msgstr "FICHEROS"

#. type: Plain text
#: man8/psupdate.8:25
msgid "/etc/psdatabase"
msgstr "/etc/psdatabase"

#. type: Plain text
#: man8/psupdate.8:27
msgid "/usr/src/linux/vmlinux"
msgstr "/usr/src/linux/vmlinux"

#. type: SH
#: man8/psupdate.8:29
#, no-wrap
msgid "NOTE"
msgstr "NOTA"

#. type: Plain text
#: man8/psupdate.8:41
msgid "...05"
msgstr ""
"La base de datos producida por esta versión de psupdate no es compatible con "
"la que produce el psupdate basado en kmem. La versión procps no necesita "
"algunas de las cosas que contiene la base de datos kmemps, por lo que la "
"base de datos procps es ligeramente menor y tiene una cadena \"mágica\" "
"(\"magic\" string) diferente. Además, la versión basada en kmem (v1.2.9) no "
"está tan actualizada en algunos aspectos.  Si, por alguna oscura razón, "
"quiere utilizar ambas versiones (basada en proc y basada en kmem) de ps, "
"debe mantener dos bases de datos diferentes y actualizar ambas al arrancar "
"(o al compilar el kernel). (Por favor, note que kmemps, por defecto, pone su "
"base de datos en un directorio diferente: /var/run)."

#. type: Plain text
#: man8/psupdate.8:47
msgid "...06"
msgstr ""
"El comando ps no necesita en absoluto el fichero /etc/psdatabase. Si no lo "
"encuentra, entonces ps examinará el fichero System.map, si puede encontrarlo "
"(véase B<ps(1)>).  Si no puede encontrar ni psdatabase ni el fichero "
"System.map, simplemente no podrá poner información simbólica en el campo "
"WCHAN."

#. type: SH
#: man8/psupdate.8:48
#, no-wrap
msgid "SEE ALSO"
msgstr "VÉASE TAMBIÉN"

#. type: Plain text
#: man8/psupdate.8:50
msgid "B<ps>(1)"
msgstr "B<ps>(1)"

#. type: SH
#: man8/psupdate.8:51
#, no-wrap
msgid "AUTORES"
msgstr "AUHTORS"

#. type: Plain text
#: man8/psupdate.8:56
msgid "...07"
msgstr ""
"Código original escrito por Branko Lankaster, horriblemente trastocado por "
"Michael L. Johnson en un intento desesperado para añadir soporte WCHAN a "
"procps.  Jeff Uphoff añadió el código para manipular kernels compilados con "
"formato ELF y reescribió mucho del código restante.  David Mossberger-Tang "
"escribió el soporte para BFD."
