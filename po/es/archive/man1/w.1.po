# Spanish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Claudio S. Suárez <csuarez@ctv.es>, 1999.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2021-08-27 16:45+0200\n"
"PO-Revision-Date: 2021-06-07 00:18+0200\n"
"Last-Translator: Claudio S. Suárez <csuarez@ctv.es>\n"
"Language-Team: Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "W"
msgstr "W"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "2020-06-04"
msgstr "4 Junio 2020"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "procps-ng"
msgstr "procps-ng"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "User Commands"
msgstr "Órdenes de usuario"

#. type: SH
#: debian-bullseye
#, no-wrap
msgid "NAME"
msgstr "NOMBRE"

#. type: Plain text
#: debian-bullseye
msgid "w - Show who is logged on and what they are doing."
msgstr "w - Muestra quienes están conectados y qué están haciendo."

#. type: SH
#: debian-bullseye
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: debian-bullseye
msgid "B<w> [I<options>] I<user> [...]"
msgstr "B<w> [I<opciones>] I<usuario> [...]"

#. type: SH
#: debian-bullseye
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPCIÓN"

#. type: Plain text
#: debian-bullseye
msgid ""
"B<w> displays information about the users currently on the machine, and "
"their processes.  The header shows, in this order, the current time, how "
"long the system has been running, how many users are currently logged on, "
"and the system load averages for the past 1, 5, and 15 minutes."
msgstr ""
"B<w> muestra información sobre los usuarios que están conectados en ese "
"momento a la máquina y sobre sus procesos.  La cabecera muestra, en este "
"orden, el tiempo actual, cuanto lleva el sistema funcionando, cuantos "
"usuarios están conectados y las cargas medias en los anteriores 1, 5 y 15 "
"minutos."

#. type: Plain text
#: debian-bullseye
msgid ""
"The following entries are displayed for each user: login name, the tty name, "
"the remote host, login time, idle time, JCPU, PCPU, and the command line of "
"their current process."
msgstr ""
"Para cada usuario se muestran los siguientes datos: nombre de login, nombre "
"de tty, nodo remoto, tiempo de conexión, tiempo inactivo, JCPU, PCPU y la "
"línea de órdenes del proceso en curso."

#. type: Plain text
#: debian-bullseye
msgid ""
"The JCPU time is the time used by all processes attached to the tty.  It "
"does not include past background jobs, but does include currently running "
"background jobs."
msgstr ""
"JCPU es el tiempo usado por todos los procesos bajo el terminal tty.  No "
"incluye los procesos en segundo plano ya finalizados, aunque incluye los "
"procesos de segundo plano en curso."

#. type: Plain text
#: debian-bullseye
msgid ""
"The PCPU time is the time used by the current process, named in the \"what\" "
"field."
msgstr ""
"PCPU es el tiempo usado por el proceso en curso, nombrado en el campo \"what"
"\"."

#. type: SH
#: debian-bullseye
#, no-wrap
msgid "COMMAND-LINE OPTIONS"
msgstr "OPCIONES DE LÍNEA DE ÓRDENES"

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-h>, B<--no-header>"
msgstr "B<-h>, B<--no-header>"

#. type: Plain text
#: debian-bullseye
msgid "Don't print the header."
msgstr "No escribe la cabecera."

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-u>, B<--no-current>"
msgstr "B<-u>, B<--no-current>"

#. type: Plain text
#: debian-bullseye
#, fuzzy
#| msgid ""
#| "Ignores the username while figuring out the current process and cpu "
#| "times.  To demonstrate this, do a \"su\" and do a \"w\" and a \"w -u\"."
msgid ""
"Ignores the username while figuring out the current process and cpu times.  "
"To demonstrate this, do a B<su> and do a B<w> and a B<w -u>."
msgstr ""
"No tiene en cuenta el nombre de usuario cuando se comprueba el tiempo del "
"proceso actual y de cpu. Para mostrar esto, haga un \"su\" y haga un \"w\" y "
"un \"w -u\"."

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-s>, B<--short>"
msgstr "B<-s>, B<--short>"

#. type: Plain text
#: debian-bullseye
msgid "Use the short format.  Don't print the login time, JCPU or PCPU times."
msgstr ""
"Usa el formato corto.  No escribe el tiempo de conexión, ni JCPU, ni PCPU."

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-f>, B<--from>"
msgstr "B<-f>, B<--from>"

#. type: Plain text
#: debian-bullseye
msgid ""
"Toggle printing the B<from> (remote hostname) field.  The default as "
"released is for the B<from> field to not be printed, although your system "
"administrator or distribution maintainer may have compiled a version in "
"which the B<from> field is shown by default."
msgstr ""
"Cambia la escritura del campo B<from> (nombre del nodo remoto). El "
"comportamiento por defecto es que el campo B<from> no se escriba, pero el "
"administrador de su sistema o el supervisor de la distribución puede haber "
"compilado una versión en la que el campo B<from> se muestre por defecto."

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: debian-bullseye
msgid "Display help text and exit."
msgstr "Mostrar texto de ayuda y finalizar."

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-i>, B<--ip-addr>"
msgstr "B<-i>, B<--ip-addr>"

#. type: Plain text
#: debian-bullseye
msgid "Display IP address instead of hostname for B<from> field."
msgstr ""

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: debian-bullseye
msgid "Display version information."
msgstr "Mostrar información de versión."

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-o>, B<--old-style>"
msgstr "B<-o>, B<--old-style>"

#. type: Plain text
#: debian-bullseye
msgid ""
"Old style output.  Prints blank space for idle times less than one minute."
msgstr ""

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<user >"
msgstr "B<usuario >"

#. type: Plain text
#: debian-bullseye
msgid "Show information about the specified user only."
msgstr "Muestra solamente información sobre el usuario especificado."

#. type: SH
#: debian-bullseye
#, no-wrap
msgid "ENVIRONMENT"
msgstr "ENTORNO"

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "PROCPS_USERLEN"
msgstr "PROCPS_USERLEN"

#. type: Plain text
#: debian-bullseye
msgid "Override the default width of the username column.  Defaults to 8."
msgstr ""

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "PROCPS_FROMLEN"
msgstr "PROCPS_FROMLEN"

#. type: Plain text
#: debian-bullseye
msgid "Override the default width of the from column.  Defaults to 16."
msgstr ""

#. type: SH
#: debian-bullseye
#, no-wrap
msgid "FILES"
msgstr "ARCHIVOS"

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "I</var/run/utmp>"
msgstr "I</var/run/utmp>"

#. type: Plain text
#: debian-bullseye
msgid "information about who is currently logged on"
msgstr "información sobre quien está conectado en el momento"

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "I</proc>"
msgstr "I</proc>"

#. type: Plain text
#: debian-bullseye
msgid "process information"
msgstr "información sobre procesos"

#. type: SH
#: debian-bullseye
#, no-wrap
msgid "SEE ALSO"
msgstr "VÉASE TAMBIÉN"

#. type: Plain text
#: debian-bullseye
msgid "B<free>(1), B<ps>(1), B<top>(1), B<uptime>(1), B<utmp>(5), B<who>(1)"
msgstr "B<free>(1), B<ps>(1), B<top>(1), B<uptime>(1), B<utmp>(5), B<who>(1)"

#. type: SH
#: debian-bullseye
#, no-wrap
msgid "AUTHORS"
msgstr "AUTORES"

#. type: Plain text
#: debian-bullseye
msgid ""
"B<w> was re-written almost entirely by Charles Blake, based on the version "
"by E<.UR greenfie@\\:gauss.\\:rutgers.\\:edu> Larry Greenfield E<.UE> and E<."
"UR johnsonm@\\:redhat.\\:com> Michael K. Johnson E<.UE>"
msgstr ""
"B<w> ha sido re-escrito casi enteramente por Charles Blake, basándose en la "
"versión de E<.UR greenfie@\\:gauss.\\:rutgers.\\:edu> Larry Greenfield E<."
"UE> y E<.UR johnsonm@\\:redhat.\\:com> Michael K. Johnson E<.UE>"

#. type: SH
#: debian-bullseye
#, no-wrap
msgid "REPORTING BUGS"
msgstr "INFORMAR DE ERRORES"

#. type: Plain text
#: debian-bullseye
msgid "Please send bug reports to E<.UR procps@freelists.org> E<.UE>"
msgstr ""
"Por favor, notifique cualquier error a E<.UR procps@freelists.org> E<.UE>"
