# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Mario Blättermann <mario.blaettermann@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2022-10-03 15:50+0200\n"
"PO-Revision-Date: 2019-11-10 21:23+0100\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 19.08.2\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "RPMKEYS"
msgstr "RPMKEYS"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "29 October 2010"
msgstr "29. Oktober 2010"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Red Hat, Inc"
msgstr "Red Hat, Inc"

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "rpmkeys - RPM Keyring"
msgstr "rpmkeys - RPM-Schlüsselbund"

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "B<rpmkeys> {B<--import|--checksig>}"
msgstr "B<rpmkeys> {B<--import|--checksig>}"

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "The general forms of rpm digital signature commands are"
msgstr "Die allgemeine Form der Digitalsignatur-Befehle für B<rpm> ist"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "B<rpmkeys> B<--import> I<PUBKEY>I< ...>"
msgstr "B<rpmkeys> B<--import> I<ÖFFENTLICHER_SCHLÜSSEL>I< …>"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "B<rpmkeys> {B<-K|--checksig>} I<PACKAGE_FILE>I< ...>"
msgstr "B<rpmkeys> {B<-K|--checksig>} I<PAKETDATEI>I< …>"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"The B<--checksig> option checks all the digests and signatures contained in "
"I<PACKAGE_FILE> to ensure the integrity and origin of the package. Note that "
"signatures are now verified whenever a package is read, and B<--checksig> is "
"useful to verify all of the digests and signatures associated with a package."
msgstr ""
"Die Option B<--checksig> überprüft alle Prüfsummen und Signaturen in der "
"angegebenen I<PAKETDATEI>, um die Integrität und die Herkunft des Pakets "
"sicherzustellen. Beachten Sie, dass Signaturen nun immer dann überprüft "
"werden, wenn ein Paket gelesen wird. B<--checksig> ist zum Überprüfen aller "
"einem Paket zugeordneten Prüfsummen und Signaturen nützlich."

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"Digital signatures cannot be verified without a public key.  An ASCII "
"armored public key can be added to the B<rpm> database using B<--import>. An "
"imported public key is carried in a header, and key ring management is "
"performed exactly like package management. For example, all currently "
"imported public keys can be displayed by:"
msgstr ""
"Digitale Signaturen können ohne öffentlichen Schlüssel nicht überprüft "
"werden. Ein öffentlicher, gepanzerter ASCII-Schlüssel kann mit der Option "
"B<--import> zur Datenbank von B<rpm> hinzugefügt werden. Ein importierter "
"öffentlicher Schlüssel wird in Kopfzeilen gespeichert; die "
"Schlüsselbundverwaltung funktioniert exakt genauso wie die Paketverwaltung. "
"Zum Beispiel können Sie alle gegenwärtig importierten öffentlichen Schlüssel "
"folgendermaßen anzeigen:"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "B<rpm -qa gpg-pubkey*>"
msgstr "B<rpm -qa gpg-pubkey*>"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"Details about a specific public key, when imported, can be displayed by "
"querying.  Here's information about the Red Hat GPG/DSA key:"
msgstr ""
"Details zu einem bestimmten öffentlichen Schlüssel, der importiert wurde, "
"können durch eine Abfrage angezeigt werden. Hier werden die Informationen "
"zum GPG/DSA-Schlüssel von Red Hat angezeigt:"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "B<rpm -qi gpg-pubkey-db42a60e>"
msgstr "B<rpm -qi gpg-pubkey-db42a60e>"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"Finally, public keys can be erased after importing just like packages. "
"Here's how to remove the Red Hat GPG/DSA key"
msgstr ""
"Schließlich können öffentliche Schlüssel nach dem Importieren genau wie "
"Pakete gelöscht werden. So entfernen Sie den GPG/DSA-Schlüssel von Red Hat:"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "B<rpm -e gpg-pubkey-db42a60e>"
msgstr "B<rpm -e gpg-pubkey-db42a60e>"

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid ""
"B<popt>(3),\n"
"B<rpm>(8),\n"
"B<rpmdb>(8),\n"
"B<rpmsign>(8),\n"
"B<rpm2cpio>(8),\n"
"B<rpmbuild>(8),\n"
"B<rpmspec>(8),\n"
msgstr ""
"B<popt>(3),\n"
"B<rpm>(8),\n"
"B<rpmdb>(8),\n"
"B<rpmsign>(8),\n"
"B<rpm2cpio>(8),\n"
"B<rpmbuild>(8),\n"
"B<rpmspec>(8),\n"

# FIXME formatting of rpm
#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"B<rpmkeys --help> - as rpm supports customizing the options via popt aliases "
"it's impossible to guarantee that what's described in the manual matches "
"what's available."
msgstr ""
"B<rpmkeys --help> - da B<rpm> benutzerdefinierte Optionen über Popt-Aliase "
"unterstützt, können wir unmöglich garantieren, dass die Beschreibungen in "
"diesem Handbuch exakt dem entsprechen, was verfügbar ist."

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "B<http://www.rpm.org/ E<lt>URL:http://www.rpm.org/E<gt>>"
msgstr "B<http://www.rpm.org/ E<lt>URL:http://www.rpm.org/E<gt>>"

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "AUTHORS"
msgstr "AUTOREN"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid ""
"Marc Ewing E<lt>marc@redhat.comE<gt>\n"
"Jeff Johnson E<lt>jbj@redhat.comE<gt>\n"
"Erik Troan E<lt>ewt@redhat.comE<gt>\n"
"Panu Matilainen E<lt>pmatilai@redhat.comE<gt>\n"
msgstr ""
"Marc Ewing E<lt>marc@redhat.comE<gt>\n"
"Jeff Johnson E<lt>jbj@redhat.comE<gt>\n"
"Erik Troan E<lt>ewt@redhat.comE<gt>\n"
"Panu Matilainen E<lt>pmatilai@redhat.comE<gt>\n"
