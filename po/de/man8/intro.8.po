# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Chris Leick <c.leick@vollbio.de>, 2010, 2013.
# Mario Blättermann <mario.blaettermann@gmail.com>, 2016.
msgid ""
msgstr ""
"Project-Id-Version: manpages-de\n"
"POT-Creation-Date: 2023-02-20 20:11+0100\n"
"PO-Revision-Date: 2020-04-18 15:21+0200\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: de <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.6\n"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "intro"
msgstr "intro"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-10-30"
msgstr "30. Oktober 2022"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "intro - introduction to administration and privileged commands"
msgstr "intro - Einführung in administrative und privilegierte Befehle"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Section 8 of the manual describes commands which either can be or are used "
"only by the superuser, like system-administration commands, daemons, and "
"hardware-related commands."
msgstr ""
"Der Abschnitt 8 des Handbuches beschreibt Befehle, die entweder nur für den "
"Administrator gedacht sind oder nur von ihm benutzt werden, wie "
"Systemverwaltungsbefehle, Daemons und hardwarebezogene Befehle."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"As with the commands described in Section 1, the commands described in this "
"section terminate with an exit status that indicates whether the command "
"succeeded or failed.  See B<intro>(1)  for more information."
msgstr ""
"Wie auch die Befehle aus Abschnitt 1 liefern die Befehle dieses Abschnitts "
"abschließend einen Beendigungsstatus (Exit-Status), der eine erfolgreiche "
"Ausführung des Befehls oder aufgetretene Fehler aufzeigt. Lesen Sie "
"B<intro>(1), um weitere Informationen zu erhalten."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ANMERKUNGEN"

#. type: SS
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Authors and copyright conditions"
msgstr "Autoren und Copyright-Bedingungen"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Look at the header of the manual page source for the author(s) and copyright "
"conditions.  Note that these can be different from page to page!"
msgstr ""
"Den oder die Verfasser und die Copyright-Bedingungen finden Sie im Kopf des "
"Quelltextes der englischen Ausgabe der Handbuchseite. Beachten Sie, dass sie "
"sich von Seite zu Seite unterscheiden können! Hinweise zu den Copyright-"
"Bedingungen der Übersetzung finden Sie weiter unten."

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "INTRO"
msgstr "INTRO"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "2007-10-23"
msgstr "23. Oktober 2007"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Linux-Programmierhandbuch"

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "KOLOPHON"

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Diese Seite ist Teil der Veröffentlichung 5.10 des Projekts Linux-I<man-"
"pages>. Eine Beschreibung des Projekts, Informationen, wie Fehler gemeldet "
"werden können, sowie die aktuelle Version dieser Seite finden sich unter \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Diese Seite ist Teil der Veröffentlichung 4.16 des Projekts Linux-I<man-"
"pages>. Eine Beschreibung des Projekts, Informationen, wie Fehler gemeldet "
"werden können, sowie die aktuelle Version dieser Seite finden sich unter \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.02"
msgstr "Linux-Handbuchseiten 6.02"
