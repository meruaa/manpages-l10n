# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Andreas D. Preissig <andreas@sanix.ruhr.de>
# Chris Leick <c.leick@vollbio.de>, 2010-2014.
# Mario Blättermann <mario.blaettermann@gmail.com>, 2014.
# Dr. Tobias Quathamer <toddy@debian.org>, 2016.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.17.0\n"
"POT-Creation-Date: 2023-02-20 20:17+0100\n"
"PO-Revision-Date: 2023-02-16 21:06+0100\n"
"Last-Translator: Dr. Tobias Quathamer <toddy@debian.org>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "mkstemp"
msgstr "mkstemp"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5. Februar 2023"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "mkstemp, mkostemp, mkstemps, mkostemps - create a unique temporary file"
msgstr ""
"mkstemp, mkostemp, mkstemps, mkostemps - eine einzigartige temporäre Datei "
"erstellen"

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHEK"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Standard-C-Bibliothek (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>stdlib.hE<gt>>\n"
msgstr "B<#include E<lt>stdlib.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int mkstemp(char *>I<template>B<);>\n"
"B<int mkostemp(char *>I<template>B<, int >I<flags>B<);>\n"
"B<int mkstemps(char *>I<template>B<, int >I<suffixlen>B<);>\n"
"B<int mkostemps(char *>I<template>B<, int >I<suffixlen>B<, int >I<flags>B<);>\n"
msgstr ""
"B<int mkstemp(char *>I<schablone>B<);>\n"
"B<int mkostemp(char *>I<schablone>B<, int >I<schalter>B<);>\n"
"B<int mkstemps(char *>I<schablone>B<, int >I<endungslaenge>B<);>\n"
"B<int mkostemps(char *>I<schablone>B<, int >I<endungslaenge>B<, int >I<schalter>B<);>\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Mit Glibc erforderliche Feature-Test-Makros (siehe "
"B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<mkstemp>():"
msgstr "B<mkstemp>():"

#.     || _XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED
#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid ""
"    _XOPEN_SOURCE E<gt>= 500\n"
"        || /* glibc E<gt>= 2.12: */ _POSIX_C_SOURCE E<gt>= 200809L\n"
"        || /* glibc E<lt>= 2.19: */ _SVID_SOURCE || _BSD_SOURCE\n"
msgstr ""
"    _XOPEN_SOURCE E<gt>= 500\n"
"        || /* Glibc E<gt>= 2.12: */ _POSIX_C_SOURCE E<gt>= 200809L\n"
"        || /* Glibc E<lt>= 2.19: */ _SVID_SOURCE || _BSD_SOURCE\n"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "B<mkostemp>():"
msgstr "B<mkostemp>():"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "    _GNU_SOURCE\n"
msgstr "    _GNU_SOURCE\n"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "B<mkstemps>():"
msgstr "B<mkstemps>():"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid ""
"    /* glibc E<gt>= 2.19: */ _DEFAULT_SOURCE\n"
"        || /* glibc E<lt>= 2.19: */ _SVID_SOURCE || _BSD_SOURCE\n"
msgstr ""
"    /* Glibc E<gt>= 2.19: */ _DEFAULT_SOURCE\n"
"        || /* Glibc E<lt>= 2.19: */ _SVID_SOURCE || _BSD_SOURCE\n"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "B<mkostemps>():"
msgstr "B<mkostemps>():"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The B<mkstemp>()  function generates a unique temporary filename from "
"I<template>, creates and opens the file, and returns an open file descriptor "
"for the file."
msgstr ""
"Die Funktion B<mkstemp>() erstellt einen eindeutigen temporären Dateinamen "
"aus I<schablone>, erstellt und öffnet die Datei und gibt einen Deskriptor "
"für für die offene Datei zurück."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The last six characters of I<template> must be \"XXXXXX\" and these are "
"replaced with a string that makes the filename unique.  Since it will be "
"modified, I<template> must not be a string constant, but should be declared "
"as a character array."
msgstr ""
"Die letzten sechs Zeichen von I<schablone> müssen »XXXXXX« sein. Diese "
"werden durch eine Zeichenkette ersetzt, die den Dateinamen eindeutig macht. "
"Da sie verändert wird, darf I<schablone> keine Zeichenkettenkonstante sein, "
"sondern als Zeichenfeld deklariert werden."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The file is created with permissions 0600, that is, read plus write for "
"owner only.  The returned file descriptor provides both read and write "
"access to the file.  The file is opened with the B<open>(2)  B<O_EXCL> flag, "
"guaranteeing that the caller is the process that creates the file."
msgstr ""
"Diese Datei wird mit den Rechten 0600 erstellt, das heißt, nur der Besitzer "
"darf sie lesen und schreiben. Der zurückgegebene Dateideskriptor ist zum "
"Lesen und Schreiben geöffnet. Die Datei wird mit B<open>(2) und dem Schalter "
"B<O_EXCL> geöffnet, was gewährleistet, dass die Datei vom aufrufenden "
"Prozess erzeugt wurde."

#.  Reportedly, FreeBSD
#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"The B<mkostemp>()  function is like B<mkstemp>(), with the difference that "
"the following bits\\[em]with the same meaning as for B<open>(2)\\[em]may be "
"specified in I<flags>: B<O_APPEND>, B<O_CLOEXEC>, and B<O_SYNC>.  Note that "
"when creating the file, B<mkostemp>()  includes the values B<O_RDWR>, "
"B<O_CREAT>, and B<O_EXCL> in the I<flags> argument given to B<open>(2); "
"including these values in the I<flags> argument given to B<mkostemp>()  is "
"unnecessary, and produces errors on some systems."
msgstr ""
"Die Funktion B<mkostemp>() ist B<mkstemp>() ähnlich, mit dem Unterschied, "
"dass die folgenden Bits – mit der selben Bedeutung wie für B<open>(2) – in "
"I<schalter> angegeben werden können: B<O_APPEND>, B<O_CLOEXEC> und "
"B<O_SYNC>. Beachten Sie, dass B<mkostemp>() beim Erstellen der Datei bereits "
"die Werte B<O_RDWR>, B<O_CREAT> und B<O_EXCL> im Argument I<schalter> "
"beinhaltet, das an B<open>(2) übergeben wird. Es ist nicht nötig, diese "
"Werte in das Argument I<schalter> einzubeziehen und ruft auf einigen System "
"Fehler hervor."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The B<mkstemps>()  function is like B<mkstemp>(), except that the string in "
"I<template> contains a suffix of I<suffixlen> characters.  Thus, I<template> "
"is of the form I<prefixXXXXXXsuffix>, and the string XXXXXX is modified as "
"for B<mkstemp>()."
msgstr ""
"Die Funktion B<mkstemps>() unterscheidet sich von B<mkstemp>() nur dadurch, "
"dass die Zeichenkette in I<schablone> eine Endung von I<endungslaenge> "
"Zeichen enthält. Daher hat I<schablone> die Form I<praefixXXXXXXendung> und "
"die Zeichenkette XXXXXX wird wie für B<mkstemp>() verändert."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The B<mkostemps>()  function is to B<mkstemps>()  as B<mkostemp>()  is to "
"B<mkstemp>()."
msgstr ""
"Die Funktion B<mkostemp>() verhält sich zu B<mkstemps>() wie B<mkostemp>() "
"zu B<mkstemp>()."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "RÜCKGABEWERT"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"On success, these functions return the file descriptor of the temporary "
"file.  On error, -1 is returned, and I<errno> is set to indicate the error."
msgstr ""
"Bei Erfolg geben diese Funktionen den Dateideskriptor der temporären Datei "
"zurück. Im Fehlerfall wird -1 zurückgegeben und I<errno> gesetzt, um den "
"Fehler anzuzeigen."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "FEHLER"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EEXIST>"
msgstr "B<EEXIST>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Could not create a unique temporary filename.  Now the contents of "
"I<template> are undefined."
msgstr ""
"Es konnte kein eindeutiger temporärer Dateiname erstellt werden. Der Inhalt "
"von I<schablone> ist nun undefiniert."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For B<mkstemp>()  and B<mkostemp>(): The last six characters of I<template> "
"were not XXXXXX; now I<template> is unchanged."
msgstr ""
"Für B<mkstemp>() und B<mkostemp>(): Die letzten sechs Buchstaben von "
"I<schablone> waren nicht XXXXXX;I<schablone> ist unverändert."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For B<mkstemps>()  and B<mkostemps>(): I<template> is less than I<(6 + "
"suffixlen)> characters long, or the last 6 characters before the suffix in "
"I<template> were not XXXXXX."
msgstr ""
"Für B<mkstemps>() und B<mkostemps>(): I<schablone> ist weniger als I<(6 + "
"endungslaenge)> Zeichen lang oder die letzten 6 Zeichen vor der Endung in "
"I<schablone> waren nicht XXXXXX."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"These functions may also fail with any of the errors described for "
"B<open>(2)."
msgstr ""
"Diese Funktionen könnten auch mit einem der für B<open>(2) beschriebenen "
"Fehler fehlschlagen."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIONEN"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<mkostemp>()  is available since glibc 2.7.  B<mkstemps>()  and "
"B<mkostemps>()  are available since glibc 2.11."
msgstr ""
"B<mkostemp>() ist seit Glibc 2.7 verfügbar. B<mkstemps>() und B<mkostemps>() "
"sind seit Glibc 2.11 verfügbar."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATTRIBUTE"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Siehe B<attributes>(7) für eine Erläuterung der in diesem Abschnitt "
"verwandten Ausdrücke."

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Schnittstelle"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Attribut"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Wert"

#. type: tbl table
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"B<mkstemp>(),\n"
"B<mkostemp>(),\n"
"B<mkstemps>(),\n"
"B<mkostemps>()"
msgstr ""
"B<mkstemp>(),\n"
"B<mkostemp>(),\n"
"B<mkstemps>(),\n"
"B<mkostemps>()"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Multithread-Fähigkeit"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<mkstemp>(): 4.3BSD, POSIX.1-2001."
msgstr "B<mkstemp>(): 4.3BSD, POSIX.1-2001."

#.  mkstemps() appears to be at least on the BSDs, Mac OS X, Solaris,
#.  and Tru64.
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<mkstemps>(): unstandardized, but appears on several other systems."
msgstr ""
"B<mkstemps>(): nicht standardisiert, erscheint aber auf mehreren anderen "
"Systemen"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<mkostemp>()  and B<mkostemps>(): are glibc extensions."
msgstr "B<mkostemp>() und B<mkostemps>() sind Glibc-Erweiterungen."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ANMERKUNGEN"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"In glibc versions 2.06 and earlier, the file is created with permissions "
"0666, that is, read and write for all users.  This old behavior may be a "
"security risk, especially since other UNIX flavors use 0600, and somebody "
"might overlook this detail when porting programs.  POSIX.1-2008 adds a "
"requirement that the file be created with mode 0600."
msgstr ""
"In den Glibc-Versionen bis einschließlich 2.06 wurde die Datei mit den "
"Rechten 0666 erstellt, das heißt, alle Benutzer dürfen sie lesen und "
"schreiben. Dieses frühere Verhalten könnte ein Sicherheitsrisiko darstellen, "
"besonders seit andere UNIX-Varianten 0600 benutzen und jemand diese "
"Einzelheit bei der Portierung von Programmen übersehen könnte. POSIX.1-2008 "
"fügt eine Anforderung hinzu, dass die Datei mit dem Modus 0600 erstellt wird."

#.  The prototype for
#.  .BR mkstemp ()
#.  is in
#.  .I <unistd.h>
#.  for libc4, libc5, glibc1; glibc2 follows POSIX.1 and has the prototype in
#.  .IR <stdlib.h> .
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"More generally, the POSIX specification of B<mkstemp>()  does not say "
"anything about file modes, so the application should make sure its file mode "
"creation mask (see B<umask>(2))  is set appropriately before calling "
"B<mkstemp>()  (and B<mkostemp>())."
msgstr ""
"Allgemeiner ausgedrückt, sagt die POSIX-Spezifikation von B<mkstemp>() "
"nichts über die Dateimodi, daher sollte die Anwendung sicherstellen, dass "
"ihre Dateimodus-Erstellungsmaske (siehe B<umask>(2)) vor dem Aufruf von "
"B<mkstemp>() (und B<mkostemp>()) entsprechend gesetzt ist."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<mkdtemp>(3), B<mktemp>(3), B<tempnam>(3), B<tmpfile>(3), B<tmpnam>(3)"
msgstr ""
"B<mkdtemp>(3), B<mktemp>(3), B<tempnam>(3), B<tmpfile>(3), B<tmpnam>(3)"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "MKSTEMP"
msgstr "MKSTEMP"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15. September 2017"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "GNU"
msgstr "GNU"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Linux-Programmierhandbuch"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "B<int mkstemp(char *>I<template>B<);>\n"
msgstr "B<int mkstemp(char *>I<schablone>B<);>\n"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "B<int mkostemp(char *>I<template>B<, int >I<flags>B<);>\n"
msgstr "B<int mkostemp(char *>I<schablone>B<, int >I<schalter>B<);>\n"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "B<int mkstemps(char *>I<template>B<, int >I<suffixlen>B<);>\n"
msgstr "B<int mkstemps(char *>I<schablone>B<, int >I<endungslaenge>B<);>\n"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "B<int mkostemps(char *>I<template>B<, int >I<suffixlen>B<, int >I<flags>B<);>\n"
msgstr "B<int mkostemps(char *>I<schablone>B<, int >I<endungslaenge>B<, int >I<schalter>B<);>\n"

#.     || _XOPEN_SOURCE\ &&\ _XOPEN_SOURCE_EXTENDED
#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid ""
"_XOPEN_SOURCE\\ E<gt>=\\ 500\n"
"    || /* Since glibc 2.12: */ _POSIX_C_SOURCE\\ E<gt>=\\ 200809L\n"
"    || /* Glibc versions E<lt>= 2.19: */ _SVID_SOURCE || _BSD_SOURCE\n"
msgstr ""
"_XOPEN_SOURCE\\ E<gt>=\\ 500\n"
"    || /* Seit Glibc 2.12: */ _POSIX_C_SOURCE\\ E<gt>=\\ 200809L\n"
"    || /* Glibc-Versionen E<lt>= 2.19: */ _SVID_SOURCE || _BSD_SOURCE\n"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "B<mkostemp>(): _GNU_SOURCE"
msgstr "B<mkostemp>(): _GNU_SOURCE"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid ""
"B<mkstemps>():\n"
"    /* Glibc since 2.19: */ _DEFAULT_SOURCE\n"
"        || /* Glibc versions E<lt>= 2.19: */ _SVID_SOURCE || _BSD_SOURCE\n"
msgstr ""
"B<mkstemps>():\n"
"    /* Glibc seit 2.19: */ _DEFAULT_SOURCE\n"
"        || /* Glibc-Versionen E<lt>= 2.19: */ _SVID_SOURCE || _BSD_SOURCE\n"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "B<mkostemps>(): _GNU_SOURCE"
msgstr "B<mkostemps>(): _GNU_SOURCE"

#.  Reportedly, FreeBSD
#. type: Plain text
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The B<mkostemp>()  function is like B<mkstemp>(), with the difference that "
"the following bits\\(emwith the same meaning as for B<open>(2)\\(emmay be "
"specified in I<flags>: B<O_APPEND>, B<O_CLOEXEC>, and B<O_SYNC>.  Note that "
"when creating the file, B<mkostemp>()  includes the values B<O_RDWR>, "
"B<O_CREAT>, and B<O_EXCL> in the I<flags> argument given to B<open>(2); "
"including these values in the I<flags> argument given to B<mkostemp>()  is "
"unnecessary, and produces errors on some systems."
msgstr ""
"Die Funktion B<mkostemp>() ist B<mkstemp>() ähnlich, mit dem Unterschied, "
"dass die folgenden Bits – mit der selben Bedeutung wie für B<open>(2) – in "
"I<schalter> angegeben werden können: B<O_APPEND>, B<O_CLOEXEC> und "
"B<O_SYNC>. Beachten Sie, dass B<mkostemp>() beim Erstellen der Datei bereits "
"die Werte B<O_RDWR>, B<O_CREAT> und B<O_EXCL> im Argument I<schalter> "
"beinhaltet, das an B<open>(2) übergeben wird. Es ist nicht nötig, diese "
"Werte in das Argument I<schalter> einzubeziehen und ruft auf einigen System "
"Fehler hervor."

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"On success, these functions return the file descriptor of the temporary "
"file.  On error, -1 is returned, and I<errno> is set appropriately."
msgstr ""
"Bei Erfolg geben diese Funktionen den Dateideskriptor der temporären Datei "
"zurück. Im Fehlerfall wird -1 zurückgegeben und I<errno> entsprechend "
"gesetzt."

#. type: tbl table
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid ""
"B<mkstemp>(),\n"
"B<mkostemp>(),\n"
msgstr ""
"B<mkstemp>(),\n"
"B<mkostemp>(),\n"

#. type: tbl table
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid ".br\n"
msgstr ".br\n"

#. type: tbl table
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid ""
"B<mkstemps>(),\n"
"B<mkostemps>()"
msgstr ""
"B<mkstemps>(),\n"
"B<mkostemps>()"

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "KONFORM ZU"

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "KOLOPHON"

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Diese Seite ist Teil der Veröffentlichung 5.10 des Projekts Linux-I<man-"
"pages>. Eine Beschreibung des Projekts, Informationen, wie Fehler gemeldet "
"werden können, sowie die aktuelle Version dieser Seite finden sich unter \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Diese Seite ist Teil der Veröffentlichung 4.16 des Projekts Linux-I<man-"
"pages>. Eine Beschreibung des Projekts, Informationen, wie Fehler gemeldet "
"werden können, sowie die aktuelle Version dieser Seite finden sich unter \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-12-15"
msgstr "15. Dezember 2022"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.02"
msgstr "Linux-Handbuchseiten 6.02"

#.     || _XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED
#. type: Plain text
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    _XOPEN_SOURCE E<gt>= 500\n"
"        || /* Glibc E<gt>= 2.12: */ _POSIX_C_SOURCE E<gt>= 200809L\n"
"        || /* Glibc E<lt>= 2.19: */ _SVID_SOURCE || _BSD_SOURCE\n"
msgstr ""
"    _XOPEN_SOURCE E<gt>= 500\n"
"        || /* Glibc E<gt>= 2.12: */ _POSIX_C_SOURCE E<gt>= 200809L\n"
"        || /* Glibc E<lt>= 2.19: */ _SVID_SOURCE || _BSD_SOURCE\n"

#. type: Plain text
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    /* Glibc E<gt>= 2.19: */ _DEFAULT_SOURCE\n"
"        || /* Glibc E<lt>= 2.19: */ _SVID_SOURCE || _BSD_SOURCE\n"
msgstr ""
"    /* Glibc E<gt>= 2.19: */ _DEFAULT_SOURCE\n"
"        || /* Glibc E<lt>= 2.19: */ _SVID_SOURCE || _BSD_SOURCE\n"
