# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Helge Kreutzmann <debian@helgefjell.de>, 2012.
# Mario Blättermann <mario.blaettermann@gmail.com>, 2014.
# Dr. Tobias Quathamer <toddy@debian.org>, 2016.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.17.0\n"
"POT-Creation-Date: 2023-02-20 20:11+0100\n"
"PO-Revision-Date: 2023-02-19 08:07+0100\n"
"Last-Translator: Dr. Tobias Quathamer <toddy@debian.org>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 2.0\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "isalpha"
msgstr "isalpha"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5. Februar 2023"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"isalnum, isalpha, isascii, isblank, iscntrl, isdigit, isgraph, islower, "
"isprint, ispunct, isspace, isupper, isxdigit, isalnum_l, isalpha_l, "
"isascii_l, isblank_l, iscntrl_l, isdigit_l, isgraph_l, islower_l, isprint_l, "
"ispunct_l, isspace_l, isupper_l, isxdigit_l - character classification "
"functions"
msgstr ""
"isalnum, isalpha, isascii, isblank, iscntrl, isdigit, isgraph, islower, "
"isprint, ispunct, isspace, isupper, isxdigit, isalnum_l, isalpha_l, "
"isascii_l, isblank_l, iscntrl_l, isdigit_l, isgraph_l, islower_l, isprint_l, "
"ispunct_l, isspace_l, isupper_l, isxdigit_l - Zeichenklassifizierungs-"
"Funktionen"

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHEK"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Standard-C-Bibliothek (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>ctype.hE<gt>>\n"
msgstr "B<#include E<lt>ctype.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int isalnum(int >I<c>B<);>\n"
"B<int isalpha(int >I<c>B<);>\n"
"B<int iscntrl(int >I<c>B<);>\n"
"B<int isdigit(int >I<c>B<);>\n"
"B<int isgraph(int >I<c>B<);>\n"
"B<int islower(int >I<c>B<);>\n"
"B<int isprint(int >I<c>B<);>\n"
"B<int ispunct(int >I<c>B<);>\n"
"B<int isspace(int >I<c>B<);>\n"
"B<int isupper(int >I<c>B<);>\n"
"B<int isxdigit(int >I<c>B<);>\n"
msgstr ""
"B<int isalnum(int >I<c>B<);>\n"
"B<int isalpha(int >I<c>B<);>\n"
"B<int iscntrl(int >I<c>B<);>\n"
"B<int isdigit(int >I<c>B<);>\n"
"B<int isgraph(int >I<c>B<);>\n"
"B<int islower(int >I<c>B<);>\n"
"B<int isprint(int >I<c>B<);>\n"
"B<int ispunct(int >I<c>B<);>\n"
"B<int isspace(int >I<c>B<);>\n"
"B<int isupper(int >I<c>B<);>\n"
"B<int isxdigit(int >I<c>B<);>\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int isascii(int >I<c>B<);>\n"
"B<int isblank(int >I<c>B<);>\n"
msgstr ""
"B<int isascii(int >I<c>B<);>\n"
"B<int isblank(int >I<c>B<);>\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int isalnum_l(int >I<c>B<, locale_t >I<locale>B<);>\n"
"B<int isalpha_l(int >I<c>B<, locale_t >I<locale>B<);>\n"
"B<int isblank_l(int >I<c>B<, locale_t >I<locale>B<);>\n"
"B<int iscntrl_l(int >I<c>B<, locale_t >I<locale>B<);>\n"
"B<int isdigit_l(int >I<c>B<, locale_t >I<locale>B<);>\n"
"B<int isgraph_l(int >I<c>B<, locale_t >I<locale>B<);>\n"
"B<int islower_l(int >I<c>B<, locale_t >I<locale>B<);>\n"
"B<int isprint_l(int >I<c>B<, locale_t >I<locale>B<);>\n"
"B<int ispunct_l(int >I<c>B<, locale_t >I<locale>B<);>\n"
"B<int isspace_l(int >I<c>B<, locale_t >I<locale>B<);>\n"
"B<int isupper_l(int >I<c>B<, locale_t >I<locale>B<);>\n"
"B<int isxdigit_l(int >I<c>B<, locale_t >I<locale>B<);>\n"
msgstr ""
"B<int isalnum_l(int >I<c>B<, locale_t >I<locale>B<);>\n"
"B<int isalpha_l(int >I<c>B<, locale_t >I<locale>B<);>\n"
"B<int isblank_l(int >I<c>B<, locale_t >I<locale>B<);>\n"
"B<int iscntrl_l(int >I<c>B<, locale_t >I<locale>B<);>\n"
"B<int isdigit_l(int >I<c>B<, locale_t >I<locale>B<);>\n"
"B<int isgraph_l(int >I<c>B<, locale_t >I<locale>B<);>\n"
"B<int islower_l(int >I<c>B<, locale_t >I<locale>B<);>\n"
"B<int isprint_l(int >I<c>B<, locale_t >I<locale>B<);>\n"
"B<int ispunct_l(int >I<c>B<, locale_t >I<locale>B<);>\n"
"B<int isspace_l(int >I<c>B<, locale_t >I<locale>B<);>\n"
"B<int isupper_l(int >I<c>B<, locale_t >I<locale>B<);>\n"
"B<int isxdigit_l(int >I<c>B<, locale_t >I<locale>B<);>\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<int isascii_l(int >I<c>B<, locale_t >I<locale>B<);>\n"
msgstr "B<int isascii_l(int >I<c>B<, locale_t >I<locale>B<);>\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Mit Glibc erforderliche Feature-Test-Makros (siehe "
"B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<isascii>():"
msgstr "B<isascii>():"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid ""
"    _XOPEN_SOURCE\n"
"        || /* glibc E<gt>= 2.19: */ _DEFAULT_SOURCE\n"
"        || /* glibc E<lt>= 2.19: */ _SVID_SOURCE\n"
msgstr ""
"    _XOPEN_SOURCE\n"
"        || /* Glibc E<gt>= 2.19: */ _DEFAULT_SOURCE\n"
"        || /* Glibc E<lt>= 2.19: */ _SVID_SOURCE\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<isblank>():"
msgstr "B<isblank>():"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "    _ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L\n"
msgstr "    _ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<isalnum_l>(), B<isalpha_l>(), B<isblank_l>(), B<iscntrl_l>(), "
"B<isdigit_l>(), B<isgraph_l>(), B<islower_l>(), B<isprint_l>(), "
"B<ispunct_l>(), B<isspace_l>(), B<isupper_l>(), B<isxdigit_l>():"
msgstr ""
"B<isalnum_l>(), B<isalpha_l>(), B<isblank_l>(), B<iscntrl_l>(), "
"B<isdigit_l>(), B<isgraph_l>(), B<islower_l>(), B<isprint_l>(), "
"B<ispunct_l>(), B<isspace_l>(), B<isupper_l>(), B<isxdigit_l>():"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    Since glibc 2.10:\n"
"        _XOPEN_SOURCE E<gt>= 700\n"
"    Before glibc 2.10:\n"
"        _GNU_SOURCE\n"
msgstr ""
"    Seit Glibc 2.10:\n"
"        _XOPEN_SOURCE E<gt>= 700\n"
"    Vor Glibc 2.10:\n"
"        _GNU_SOURCE\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<isascii_l>():"
msgstr "B<isascii_l>():"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    Since glibc 2.10:\n"
"        _XOPEN_SOURCE E<gt>= 700 && (_SVID_SOURCE || _BSD_SOURCE)\n"
"    Before glibc 2.10:\n"
"        _GNU_SOURCE\n"
msgstr ""
"    Seit Glibc 2.10:\n"
"        _XOPEN_SOURCE E<gt>= 700 && (_SVID_SOURCE || _BSD_SOURCE)\n"
"    Vor Glibc 2.10:\n"
"        _GNU_SOURCE\n"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"These functions check whether I<c>, which must have the value of an "
"I<unsigned char> or B<EOF>, falls into a certain character class according "
"to the specified locale.  The functions without the \"_l\" suffix perform "
"the check based on the current locale."
msgstr ""
"Diese Funktionen prüfen, ob I<c>, das den Wert eines B<unsigned char> haben "
"oder B<EOF> sein muss, in eine Zeichenklasse entsprechend den aktuellen "
"Einstellungen für die Spracheinstellung (locale) passt. Die Funktionen ohne "
"das Suffix »_l« führen die Überprüfung basierend auf der aktuellen Locale "
"aus."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The functions with the \"_l\" suffix perform the check based on the locale "
"specified by the locale object I<locale>.  The behavior of these functions "
"is undefined if I<locale> is the special locale object B<LC_GLOBAL_LOCALE> "
"(see B<duplocale>(3))  or is not a valid locale object handle."
msgstr ""
"Die Funktionen mit dem Suffix »_l« führen die Überprüfung basierend auf der "
"im Locale-Objekt I<locale> angegebenen Locale aus. Das Verhalten dieser "
"Funktionen ist nicht definiert, falls I<locale> das spezielle Locale-Objekt "
"B<LC_GLOBAL_LOCALE> (siehe B<duplocale>(3)) oder kein gültiges Locale-Objekt-"
"Handle ist."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The list below explains the operation of the functions without the \"_l\" "
"suffix; the functions with the \"_l\" suffix differ only in using the locale "
"object I<locale> instead of the current locale."
msgstr ""
"die folgende Liste erläutert die Wirkungsweise der Funktionen ohne das "
"Suffix »_l«. Das Verhalten der Funktionen mit dem Suffix »_l« unterscheidet "
"sich davon nur dadurch, dass das Locale-Objekt I<locale> anstelle der "
"aktuellen Locale verwendet wird."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<isalnum>()"
msgstr "B<isalnum>()"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"checks for an alphanumeric character; it is equivalent to "
"B<(isalpha(>I<c>B<) || isdigit(>I<c>B<))>."
msgstr ""
"prüft auf alphanumerische Zeichen, es ist äquivalent zu B<(isalpha(>I<c>B<) "
"|| isdigit(>I<c>B<))>."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<isalpha>()"
msgstr "B<isalpha>()"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"checks for an alphabetic character; in the standard B<\"C\"> locale, it is "
"equivalent to B<(isupper(>I<c>B<) || islower(>I<c>B<))>.  In some locales, "
"there may be additional characters for which B<isalpha>()  is "
"true\\[em]letters which are neither uppercase nor lowercase."
msgstr ""
"prüft auf alphanumerische Zeichen, in der standard B<\"C\">-Locale ist es "
"äquivalent zu B<(isupper(>I<c>B<) || islower(>I<c>B<))>. In anderen Locales "
"kann es weitere Zeichen geben, für die B<isalpha()> wahr ist - Zeichen, die "
"weder Groß- noch Kleinbuchstaben sind."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<isascii>()"
msgstr "B<isascii>()"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"checks whether I<c> is a 7-bit I<unsigned char> value that fits into the "
"ASCII character set."
msgstr ""
"prüft, ob I<c> ein 7-bit I<unsigned char>-Wert ist, der in den ASCII-"
"Zeichensatz passt."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<isblank>()"
msgstr "B<isblank>()"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "checks for a blank character; that is, a space or a tab."
msgstr "prüft auf ein Leerzeichen, also ein Leerzeichen oder einen Tabulator"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<iscntrl>()"
msgstr "B<iscntrl>()"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "checks for a control character."
msgstr "prüft auf ein Steuerzeichen"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<isdigit>()"
msgstr "B<isdigit>()"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "checks for a digit (0 through 9)."
msgstr "prüft auf eine Ziffer (0 bis 9)"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<isgraph>()"
msgstr "B<isgraph>()"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "checks for any printable character except space."
msgstr "prüft auf druckbare Zeichen außer Leerzeichen"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<islower>()"
msgstr "B<islower>()"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "checks for a lowercase character."
msgstr "prüft auf einen Kleinbuchstaben"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<isprint>()"
msgstr "B<isprint>()"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "checks for any printable character including space."
msgstr "prüft auf druckbare Zeichen inklusive Leerzeichen"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ispunct>()"
msgstr "B<ispunct>()"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"checks for any printable character which is not a space or an alphanumeric "
"character."
msgstr ""
"prüft auf druckbare Zeichen, das kein Leerzeichen und kein alphanumerisches "
"Zeichen ist"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<isspace>()"
msgstr "B<isspace>()"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"checks for white-space characters.  In the B<\"C\"> and B<\"POSIX\"> "
"locales, these are: space, form-feed (B<\\[aq]\\ef\\[aq]>), newline "
"(B<\\[aq]\\en\\[aq]>), carriage return (B<\\[aq]\\er\\[aq]>), horizontal tab "
"(B<\\[aq]\\et\\[aq]>), and vertical tab (B<\\[aq]\\ev\\[aq]>)."
msgstr ""
"prüft auf Leerraumzeichen. In den B<\"C\">- und B<\"POSIX\">-Locales sind "
"dies: Leerzeichen, Seitenvorschub (B<»\\ef«>), Zeilenumbruch "
"(B<\\(aq\\en\\(aq>), Wagenrücklauf (B<»\\er«>), horizontaler Tabulator "
"(B<»\\et«>) und vertikaler Tabulator (B<»\\ev«>)."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<isupper>()"
msgstr "B<isupper>()"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "checks for an uppercase letter."
msgstr "prüft auf einen Großbuchstaben"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<isxdigit>()"
msgstr "B<isxdigit>()"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "checks for hexadecimal digits, that is, one of"
msgstr "prüft, auf hexadezimale Ziffern, also eine von"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<0 1 2 3 4 5 6 7 8 9 a b c d e f A B C D E F>."
msgstr "B<0 1 2 3 4 5 6 7 8 9 a b c d e f A B C D E F>."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "RÜCKGABEWERT"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The values returned are nonzero if the character I<c> falls into the tested "
"class, and zero if not."
msgstr ""
"Falls das Zeichen I<c> in die geprüfte Klasse fällt, wird eine Zahl ungleich "
"null zurückgegeben, ansonsten null."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIONEN"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<isalnum_l>(), B<isalpha_l>(), B<isblank_l>(), B<iscntrl_l>(), "
"B<isdigit_l>(), B<isgraph_l>(), B<islower_l>(), B<isprint_l>(), "
"B<ispunct_l>(), B<isspace_l>(), B<isupper_l>(), B<isxdigit_l>(), and "
"B<isascii_l>()  are available since glibc 2.3."
msgstr ""
"B<isalnum_l>(), B<isalpha_l>(), B<isblank_l>(), B<iscntrl_l>(), "
"B<isdigit_l>(), B<isgraph_l>(), B<islower_l>(), B<isprint_l>(), "
"B<ispunct_l>(), B<isspace_l>(), B<isupper_l>(), B<isxdigit_l>() und "
"B<isascii_l>() sind seit Glibc 2.3 verfügbar."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATTRIBUTE"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Siehe B<attributes>(7) für eine Erläuterung der in diesem Abschnitt "
"verwandten Ausdrücke."

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Schnittstelle"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Attribut"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Wert"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<isalnum>(),\n"
"B<isalpha>(),\n"
"B<isascii>(),\n"
"B<isblank>(),\n"
"B<iscntrl>(),\n"
"B<isdigit>(),\n"
"B<isgraph>(),\n"
"B<islower>(),\n"
"B<isprint>(),\n"
"B<ispunct>(),\n"
"B<isspace>(),\n"
"B<isupper>(),\n"
"B<isxdigit>()"
msgstr ""
"B<isalnum>(),\n"
"B<isalpha>(),\n"
"B<isascii>(),\n"
"B<isblank>(),\n"
"B<iscntrl>(),\n"
"B<isdigit>(),\n"
"B<isgraph>(),\n"
"B<islower>(),\n"
"B<isprint>(),\n"
"B<ispunct>(),\n"
"B<isspace>(),\n"
"B<isupper>(),\n"
"B<isxdigit>()"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Multithread-Fähigkeit"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#.  FIXME: need a thread-safety statement about the *_l functions
#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"POSIX.1-2001 specifies B<isalnum>(), B<isalpha>(), B<isblank>(), "
"B<iscntrl>(), B<isdigit>(), B<isgraph>(), B<islower>(), B<isprint>(), "
"B<ispunct>(), B<isspace>(), B<isupper>(), and B<isxdigit>(), and also "
"B<isascii>()  (as an XSI extension).  C99 specifies all of the preceding "
"functions, except B<isascii>()."
msgstr ""
"POSIX.1-2001 spezifiziert B<isalnum>(), B<isalpha>(), B<isblank>(), "
"B<iscntrl>(), B<isdigit>(), B<isgraph>(), B<islower>(), B<isprint>(), "
"B<ispunct>(), B<isspace>(), B<isupper>() und B<isxdigit>() und auch "
"B<isascii>() (als eine XSI-Erweiterung). C99 spezifiziert alle der "
"vorhergehenden Funktionen außer B<isascii>()."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"POSIX.1-2008 marks B<isascii>()  as obsolete, noting that it cannot be used "
"portably in a localized application."
msgstr ""
"POSIX.1-2008 markiert B<isascii>() als veraltet, daher sollte darauf "
"hingewiesen werden, dass es in einer lokalisierten Anwendung nicht portabel "
"verwendet werden kann."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"POSIX.1-2008 specifies B<isalnum_l>(), B<isalpha_l>(), B<isblank_l>(), "
"B<iscntrl_l>(), B<isdigit_l>(), B<isgraph_l>(), B<islower_l>(), "
"B<isprint_l>(), B<ispunct_l>(), B<isspace_l>(), B<isupper_l>(), and "
"B<isxdigit_l>()."
msgstr ""
"POSIX.1-2008 spezifiziert B<isalnum_l>(), B<isalpha_l>(), B<isblank_l>(), "
"B<iscntrl_l>(), B<isdigit_l>(), B<isgraph_l>(), B<islower_l>(), "
"B<isprint_l>(), B<ispunct_l>(), B<isspace_l>(), B<isupper_l>() und "
"B<isxdigit_l>()."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<isascii_l>()  is a GNU extension."
msgstr "B<isascii_l>() ist eine GNU-Erweiterung."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ANMERKUNGEN"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The standards require that the argument I<c> for these functions is either "
"B<EOF> or a value that is representable in the type I<unsigned char>.  If "
"the argument I<c> is of type I<char>, it must be cast to I<unsigned char>, "
"as in the following example:"
msgstr ""
"Die Standards verlangen, dass das Argument I<c> für diese Funktionen "
"entweder B<EOF> oder ein Wert, der in dem Typ I<unsigned char> darstellbar "
"ist, sein muss. Falls das Argument I<c> vom Typ I<char> ist, muss es in "
"I<unsigned char> umgewandelt werden, wie in dem folgenden Beispiel:"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"char c;\n"
"\\&...\n"
"res = toupper((unsigned char) c);\n"
msgstr ""
"char c;\n"
"…\n"
"res = toupper((unsigned char) c);\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"This is necessary because I<char> may be the equivalent of I<signed char>, "
"in which case a byte where the top bit is set would be sign extended when "
"converting to I<int>, yielding a value that is outside the range of "
"I<unsigned char>."
msgstr ""
"Dies ist notwendig, da I<char> äquivalent zu I<signed char> sein kann. In "
"diesem Fall würde ein Byte, bei dem das höchste Bit gesetzt ist, mit einem "
"Vorzeichen erweitert, wenn es in ein I<int> konvertiert würde. Dies würde zu "
"einem Wert führen, der außerhalb des Bereichs von I<unsigned char> wäre."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The details of what characters belong to which class depend on the locale.  "
"For example, B<isupper>()  will not recognize an A-umlaut (\\(:A) as an "
"uppercase letter in the default B<C> locale."
msgstr ""
"Die Details, welche Zeichen zu welcher Klasse gehören, sind von der Locale "
"abhängig. Zum Beispiel wird B<isupper()> in der Standard-B<C>-Locale kein Ä "
"als Großbuchstaben erkennen."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<iswalnum>(3), B<iswalpha>(3), B<iswblank>(3), B<iswcntrl>(3), "
"B<iswdigit>(3), B<iswgraph>(3), B<iswlower>(3), B<iswprint>(3), "
"B<iswpunct>(3), B<iswspace>(3), B<iswupper>(3), B<iswxdigit>(3), "
"B<newlocale>(3), B<setlocale>(3), B<toascii>(3), B<tolower>(3), "
"B<toupper>(3), B<uselocale>(3), B<ascii>(7), B<locale>(7)"
msgstr ""
"B<iswalnum>(3), B<iswalpha>(3), B<iswblank>(3), B<iswcntrl>(3), "
"B<iswdigit>(3), B<iswgraph>(3), B<iswlower>(3), B<iswprint>(3), "
"B<iswpunct>(3), B<iswspace>(3), B<iswupper>(3), B<iswxdigit>(3), "
"B<newlocale>(3), B<setlocale>(3), B<toascii>(3), B<tolower>(3), "
"B<toupper>(3), B<uselocale>(3), B<ascii>(7), B<locale>(7)"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "ISALPHA"
msgstr "ISALPHA"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15. September 2017"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "GNU"
msgstr "GNU"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Linux-Programmierhandbuch"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid ""
"_XOPEN_SOURCE\n"
"    || /* Glibc since 2.19: */ _DEFAULT_SOURCE\n"
"    || /* Glibc versions E<lt>= 2.19: */ _SVID_SOURCE\n"
msgstr ""
"_XOPEN_SOURCE\n"
"    || /* Glibc seit 2.19: */ _DEFAULT_SOURCE\n"
"    || /* Glibc-Versionen E<lt>= 2.19: */ _SVID_SOURCE\n"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "_ISOC99_SOURCE || _POSIX_C_SOURCE\\ E<gt>=\\ 200112L"
msgstr "_ISOC99_SOURCE || _POSIX_C_SOURCE\\ E<gt>=\\ 200112L"

#. type: TP
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Since glibc 2.10:"
msgstr "Seit Glibc 2.10:"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "_XOPEN_SOURCE\\ E<gt>=\\ 700"
msgstr "_XOPEN_SOURCE\\ E<gt>=\\ 700"

#. type: TP
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Before glibc 2.10:"
msgstr "Vor Glibc 2.10:"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "_GNU_SOURCE"
msgstr "_GNU_SOURCE"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "_XOPEN_SOURCE\\ E<gt>=\\ 700 && (_SVID_SOURCE || _BSD_SOURCE)"
msgstr "_XOPEN_SOURCE\\ E<gt>=\\ 700 && (_SVID_SOURCE || _BSD_SOURCE)"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"checks for an alphabetic character; in the standard B<\"C\"> locale, it is "
"equivalent to B<(isupper(>I<c>B<) || islower(>I<c>B<))>.  In some locales, "
"there may be additional characters for which B<isalpha>()  is "
"true\\(emletters which are neither uppercase nor lowercase."
msgstr ""
"prüft auf alphanumerische Zeichen, in der standard B<\"C\">-Locale ist es "
"äquivalent zu B<(isupper(>I<c>B<) || islower(>I<c>B<))>. In anderen Locales "
"kann es weitere Zeichen geben, für die B<isalpha()> wahr ist - Zeichen, die "
"weder Groß- noch Kleinbuchstaben sind."

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"checks for white-space characters.  In the B<\"C\"> and B<\"POSIX\"> "
"locales, these are: space, form-feed (B<\\(aq\\ef\\(aq>), newline "
"(B<\\(aq\\en\\(aq>), carriage return (B<\\(aq\\er\\(aq>), horizontal tab "
"(B<\\(aq\\et\\(aq>), and vertical tab (B<\\(aq\\ev\\(aq>)."
msgstr ""
"prüft auf Leeraumzeichen. In den B<\"C\">- und B<\"POSIX\">-Locales sind "
"dies: Leerzeichen, Seitenvorschub (B<»\\ef«>), Zeilenumbruch (B<»\\en«>), "
"Wagenrücklauf (B<»\\er«>), horizontaler Tabulator (B<»\\et«>) und vertikaler "
"Tabulator (B<»\\ev«>)."

#.  FIXME: need a thread-safety statement about the *_l functions
#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "KONFORM ZU"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"C89 specifies B<isalnum>(), B<isalpha>(), B<iscntrl>(), B<isdigit>(), "
"B<isgraph>(), B<islower>(), B<isprint>(), B<ispunct>(), B<isspace>(), "
"B<isupper>(), and B<isxdigit>(), but not B<isascii>()  and B<isblank>().  "
"POSIX.1-2001 also specifies those functions, and also B<isascii>()  (as an "
"XSI extension)  and B<isblank>().  C99 specifies all of the preceding "
"functions, except B<isascii>()."
msgstr ""
"C89 spezifiziert B<isalnum>(), B<isalpha>(), B<iscntrl>(), B<isdigit>(), "
"B<isgraph>(), B<islower>(), B<isprint>(), B<ispunct>(), B<isspace>(), "
"B<isupper>() und B<isxdigit>(), aber nicht B<isascii>() und B<isblank>(). "
"POSIX.1-2001 spezifiziert auch diese Funktionen und auch B<isascii>() (als "
"eine XSI-Erweiterung) und B<isblank>(). C99 spezifiziert alle der "
"vorhergehenden Funktionen außer B<isascii>()."

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "KOLOPHON"

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Diese Seite ist Teil der Veröffentlichung 5.10 des Projekts Linux-I<man-"
"pages>. Eine Beschreibung des Projekts, Informationen, wie Fehler gemeldet "
"werden können, sowie die aktuelle Version dieser Seite finden sich unter \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Diese Seite ist Teil der Veröffentlichung 4.16 des Projekts Linux-I<man-"
"pages>. Eine Beschreibung des Projekts, Informationen, wie Fehler gemeldet "
"werden können, sowie die aktuelle Version dieser Seite finden sich unter \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-12-15"
msgstr "15. Dezember 2022"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.02"
msgstr "Linux-Handbuchseiten 6.02"

#. type: Plain text
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    _XOPEN_SOURCE\n"
"        || /* Glibc E<gt>= 2.19: */ _DEFAULT_SOURCE\n"
"        || /* Glibc E<lt>= 2.19: */ _SVID_SOURCE\n"
msgstr ""
"    _XOPEN_SOURCE\n"
"        || /* Glibc E<gt>= 2.19: */ _DEFAULT_SOURCE\n"
"        || /* Glibc E<lt>= 2.19: */ _SVID_SOURCE\n"
