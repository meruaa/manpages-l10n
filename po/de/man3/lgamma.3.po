# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Markus Schmitt <fw@math.uni-sb.de>
# Chris Leick <c.leick@vollbio.de>, 2010.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.17.0\n"
"POT-Creation-Date: 2023-02-20 20:13+0100\n"
"PO-Revision-Date: 2023-02-16 18:02+0100\n"
"Last-Translator: Chris Leick <c.leick@vollbio.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "lgamma"
msgstr "lgamma"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5. Februar 2023"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"lgamma, lgammaf, lgammal, lgamma_r, lgammaf_r, lgammal_r, signgam - log "
"gamma function"
msgstr ""
"lgamma, lgammaf, lgammal, lgamma_r, lgammaf_r, lgammal_r, signgam - "
"Gammafunktion protokollieren"

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHEK"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Math library (I<libm>, I<-lm>)"
msgstr "Mathematik-Bibliothek (I<libm>, I<-lm>)"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>math.hE<gt>>\n"
msgstr "B<#include E<lt>math.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<double lgamma(double >I<x>B<);>\n"
"B<float lgammaf(float >I<x>B<);>\n"
"B<long double lgammal(long double >I<x>B<);>\n"
msgstr ""
"B<double lgamma(double >I<x>B<);>\n"
"B<float lgammaf(float >I<x>B<);>\n"
"B<long double lgammal(long double >I<x>B<);>\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<double lgamma_r(double >I<x>B<, int *>I<signp>B<);>\n"
"B<float lgammaf_r(float >I<x>B<, int *>I<signp>B<);>\n"
"B<long double lgammal_r(long double >I<x>B<, int *>I<signp>B<);>\n"
msgstr ""
"B<double lgamma_r(double >I<x>B<, int *>I<signp>B<);>\n"
"B<float lgammaf_r(float >I<x>B<, int *>I<signp>B<);>\n"
"B<long double lgammal_r(long double >I<x>B<, int *>I<signp>B<);>\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<extern int >I<signgam>B<;>\n"
msgstr "B<extern int >I<signgam>B<;>\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Mit Glibc erforderliche Feature-Test-Makros (siehe "
"B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid ""
"B<lgamma>():\n"
"    _ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L || _XOPEN_SOURCE\n"
"        || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"B<lgamma>():\n"
"    _ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L || _XOPEN_SOURCE\n"
"        || /* Seit Glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* Glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<lgammaf>(), B<lgammal>():"
msgstr "B<lgammaf>(), B<lgammal>():"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid ""
"    _ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L\n"
"        || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"    _ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L\n"
"        || /* Seit Glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* Glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<lgamma_r>(), B<lgammaf_r>(), B<lgammal_r>():"
msgstr "B<lgamma_r>(), B<lgammaf_r>(), B<lgammal_r>():"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid ""
"    /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"    /* Since Glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* Glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "I<signgam>:"
msgstr "I<signgam>:"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid ""
"    _XOPEN_SOURCE\n"
"        || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"    _XOPEN_SOURCE\n"
"        || /* Seit Glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* Glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "For the definition of the Gamma function, see B<tgamma>(3)."
msgstr "Die Definition der Gammafunktion finden Sie unter B<tgamma>(3)."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The B<lgamma>(), B<lgammaf>(), and B<lgammal>()  functions return the "
"natural logarithm of the absolute value of the Gamma function.  The sign of "
"the Gamma function is returned in the external integer I<signgam> declared "
"in I<E<lt>math.hE<gt>>.  It is 1 when the Gamma function is positive or "
"zero, -1 when it is negative."
msgstr ""
"Die Funktionen B<lgamma>(), B<lgammaf>() und B<lgammal>() geben den "
"natürlichen Logarithmus des Betrages der Gammafunktion zurück. Das "
"Vorzeichen der Gammafunktion wird in der externen Ganzzahl I<signgam> "
"zurückgegeben, die in I<E<lt>math.hE<gt>> deklariert ist. Sie ist 1, wenn "
"die Gammafunktion positiv oder Null ist und -1, wenn sie negativ ist."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Since using a constant location I<signgam> is not thread-safe, the functions "
"B<lgamma_r>(), B<lgammaf_r>(), and B<lgammal_r>()  have been introduced; "
"they return the sign via the argument I<signp>."
msgstr ""
"Da die Benutzung eines konstanten I<signgam>-Ortes nicht multithread-fähig "
"ist, wurden die Funktionen B<lgamma_r>(), B<lgammaf_r>() und B<lgammal_r>() "
"eingeführt; sie geben das Vorzeichen über das Argument I<signp> zurück."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "RÜCKGABEWERT"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "On success, these functions return the natural logarithm of Gamma(x)."
msgstr ""
"Bei Erfolg geben diese Funktionen den natürlichen Logarithmus von Gamma(x) "
"zurück."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "If I<x> is a NaN, a NaN is returned."
msgstr "Falls I<x> keine zulässige Zahl (»NaN«) ist, wird »NaN« zurückgegeben."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "If I<x> is 1 or 2, +0 is returned."
msgstr "Falls I<x> 1 oder 2 ist, wird +0 zurückgegeben."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If I<x> is positive infinity or negative infinity, positive infinity is "
"returned."
msgstr ""
"Falls I<x> positiv unendlich oder negativ unendlich ist, wird positiv "
"unendlich zurückgegeben."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If I<x> is a nonpositive integer, a pole error occurs, and the functions "
"return +B<HUGE_VAL>, +B<HUGE_VALF>, or +B<HUGE_VALL>, respectively."
msgstr ""
"Falls I<x> eine nicht positive Ganzzahl ist, tritt ein Polstellenfehler auf "
"und die Funktionen geben +B<HUGE_VAL>, +B<HUGE_VALF> beziehungsweise "
"+B<HUGE_VALL> zurück."

#.  e.g., lgamma(DBL_MAX)
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If the result overflows, a range error occurs, and the functions return "
"B<HUGE_VAL>, B<HUGE_VALF>, or B<HUGE_VALL>, respectively, with the correct "
"mathematical sign."
msgstr ""
"Falls das Ergebnis überläuft, tritt ein Bereichsfehler auf und die "
"Funktionen geben B<HUGE_VAL>, B<HUGE_VALF> beziehungsweise B<HUGE_VALL> mit "
"dem korrekten mathematischen Vorzeichen zurück."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "FEHLER"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"See B<math_error>(7)  for information on how to determine whether an error "
"has occurred when calling these functions."
msgstr ""
"In B<math_error>(7) erfahren Sie, wie Sie Fehler bei der Ausführung dieser "
"Funktionen erkennen."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The following errors can occur:"
msgstr "Die folgenden Fehler können auftreten:"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Pole error: I<x> is a nonpositive integer"
msgstr "Polstellenfehler: I<x> ist eine nicht positive Ganzzahl."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"I<errno> is set to B<ERANGE> (but see BUGS).  A divide-by-zero floating-"
"point exception (B<FE_DIVBYZERO>)  is raised."
msgstr ""
"I<errno> wird auf B<ERANGE> gesetzt (siehe aber FEHLER). Es wird der "
"Fließkomma-Ausnahmefehler »Division durch Null« (B<FE_DIVBYZERO>) ausgelöst."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Range error: result overflow"
msgstr "Bereichsfehler: Ergebnisüberlauf"

#.  glibc (as at 2.8) also supports an inexact
#.  exception for various cases.
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"I<errno> is set to B<ERANGE>.  An overflow floating-point exception "
"(B<FE_OVERFLOW>)  is raised."
msgstr ""
"I<errno> wird auf B<ERANGE> gesetzt. Es wird der Fließkomma-Ausnahmefehler "
"»Überlauf« (B<FE_OVERFLOW>) ausgelöst."

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The B<lgamma>()  functions are specified in C99, POSIX.1-2001, and "
"POSIX.1-2008.  I<signgam> is specified in POSIX.1-2001 and POSIX.1-2008, but "
"not in C99.  The B<lgamma_r>()  functions are nonstandard, but present on "
"several other systems."
msgstr ""
"Die B<lgamma>()-Funktionen sind in C99, POSIX.1-2001 und POSIX.1-2008 "
"spezifiziert. I<signgam> ist in POSIX.1-2001 und POSIX.1-2008 spezifiziert, "
"aber nicht in C99. Die B<lgamma_r>()-Funktionen sind nicht Standard, aber "
"auf mehreren anderen Systemen vorhanden."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "FEHLER"

#.  http://sources.redhat.com/bugzilla/show_bug.cgi?id=6777
#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"In glibc 2.9 and earlier, when a pole error occurs, I<errno> is set to "
"B<EDOM>; instead of the POSIX-mandated B<ERANGE>.  Since glibc 2.10, glibc "
"does the right thing."
msgstr ""
"Bis einschließlich Glibc 2.9 wurde, wenn ein Polstellenfehler auftrat, "
"I<errno> auf B<EDOM> statt des von POSIX vorgeschriebenen B<ERANGE> gesetzt. "
"Seit Glibc 2.10 tut Glibc das Richtige."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<tgamma>(3)"
msgstr "B<tgamma>(3)"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "LGAMMA"
msgstr "LGAMMA"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15. September 2017"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Linux-Programmierhandbuch"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "Link with I<-lm>."
msgstr "Linken Sie mit der Option I<-lm>."

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "B<lgamma>():"
msgstr "B<lgamma>():"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid ""
"_ISOC99_SOURCE || _POSIX_C_SOURCE\\ E<gt>=\\ 200112L || _XOPEN_SOURCE\n"
"    || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"    || /* Glibc versions E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"_ISOC99_SOURCE || _POSIX_C_SOURCE\\ E<gt>=\\ 200112L || _XOPEN_SOURCE\n"
"    || /* Seit Glibc 2.19: */ _DEFAULT_SOURCE\n"
"    || /* Glibc-Versionen E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid ""
"_ISOC99_SOURCE || _POSIX_C_SOURCE\\ E<gt>=\\ 200112L\n"
"    || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"    || /* Glibc versions E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"_ISOC99_SOURCE || _POSIX_C_SOURCE\\ E<gt>=\\ 200112L\n"
"    || /* Seit Glibc 2.19: */ _DEFAULT_SOURCE\n"
"    || /* Glibc-Versionen E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid ""
"/* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"    || /* Glibc versions E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"/* Seit Glibc 2.19: */ _DEFAULT_SOURCE\n"
"    || /* Glibc-Versionen E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid ""
"_XOPEN_SOURCE\n"
"    || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"    || /* Glibc versions E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"_XOPEN_SOURCE\n"
"    || /* Seit Glibc 2.19: */ _DEFAULT_SOURCE\n"
"    || /* Glibc-Versionen E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "KONFORM ZU"

#.  http://sources.redhat.com/bugzilla/show_bug.cgi?id=6777
#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"In glibc 2.9 and earlier, when a pole error occurs, I<errno> is set to "
"B<EDOM>; instead of the POSIX-mandated B<ERANGE>.  Since version 2.10, glibc "
"does the right thing."
msgstr ""
"Bis einschließlich Glibc 2.9 wurde, wenn ein Polstellenfehler auftrat, "
"I<errno> auf B<EDOM> statt des von POSIX vorgeschriebenen B<ERANGE> gesetzt. "
"Seit Version 2.10 tut Glibc das Richtige."

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "KOLOPHON"

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Diese Seite ist Teil der Veröffentlichung 5.10 des Projekts Linux-I<man-"
"pages>. Eine Beschreibung des Projekts, Informationen, wie Fehler gemeldet "
"werden können, sowie die aktuelle Version dieser Seite finden sich unter \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Diese Seite ist Teil der Veröffentlichung 4.16 des Projekts Linux-I<man-"
"pages>. Eine Beschreibung des Projekts, Informationen, wie Fehler gemeldet "
"werden können, sowie die aktuelle Version dieser Seite finden sich unter \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-12-04"
msgstr "4. Dezember 2022"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.02"
msgstr "Linux-Handbuchseiten 6.02"

#. type: Plain text
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"B<lgamma>():\n"
"    _ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L || _XOPEN_SOURCE\n"
"        || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* Glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"B<lgamma>():\n"
"    _ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L || _XOPEN_SOURCE\n"
"        || /* Seit Glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* Glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"

#. type: Plain text
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    _ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L\n"
"        || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* Glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"    _ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L\n"
"        || /* Seit Glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* Glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"

#. type: Plain text
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* Glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"    /* Since Glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* Glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"

#. type: Plain text
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    _XOPEN_SOURCE\n"
"        || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* Glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"    _XOPEN_SOURCE\n"
"        || /* Seit Glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* Glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
