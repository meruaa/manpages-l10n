# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Helge Kreutzmann <debian@helgefjell.de>, 2018-2020,2022,2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.17.0\n"
"POT-Creation-Date: 2023-02-20 20:36+0100\n"
"PO-Revision-Date: 2023-02-05 07:45+0100\n"
"Last-Translator: Helge Kreutzmann <debian@helgefjell.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYSTEMD\\&.SCOPE"
msgstr "SYSTEMD\\&.SCOPE"

#. type: TH
#: archlinux fedora-38 fedora-rawhide
#, no-wrap
msgid "systemd 253"
msgstr "systemd 253"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "systemd.scope"
msgstr "systemd.scope"

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "systemd.scope - Scope unit configuration"
msgstr "systemd.scope - Bereichs-Unit-Konfiguration"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "I<scope>\\&.scope"
msgstr "I<Bereich>\\&.scope"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Scope units are not configured via unit configuration files, but are only "
"created programmatically using the bus interfaces of systemd\\&. They are "
"named similar to filenames\\&. A unit whose name ends in \"\\&.scope\" "
"refers to a scope unit\\&. Scopes units manage a set of system processes\\&. "
"Unlike service units, scope units manage externally created processes, and "
"do not fork off processes on its own\\&."
msgstr ""
"Bereichs-Units werden nicht über Unit-Konfigurationsdateien konfiguriert, "
"sondern werden nur programmatisch mittels der Bus-Schnittstellen von Systemd "
"erstellt\\&. Sie sind ähnlich zu Dateinamen benannt\\&. Eine Unit, deren "
"Namen auf »&.scope« endet, bezieht sich auf eine Bereichs-Unit\\&. Bereichs-"
"Units verwalten eine Gruppe von Systemprozessen\\&. Anders als Dienste-Units "
"verwalten Bereichs-Units extern erstellte Prozesse und erstellen selbst "
"keine Prozesse mittels »fork«\\&."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The main purpose of scope units is grouping worker processes of a system "
"service for organization and for managing resources\\&."
msgstr ""
"Der Hauptzweck von Bereichs-Units ist die Gruppierung von Arbeitsprozessen "
"eines Systemdienstes für die Organisation und die Verwaltung von "
"Ressourcen\\&."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<systemd-run >B<--scope> may be used to easily launch a command in a new "
"scope unit from the command line\\&."
msgstr ""
"B<systemd-run >B<--scope> kann zum leichten Starten eines Befehls in einer "
"neuen Bereichs-Unit von der Befehlszeile aus verwandt werden\\&."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"See the \\m[blue]B<New Control Group Interfaces>\\m[]\\&\\s-2\\u[1]\\d\\s+2 "
"for an introduction on how to make use of scope units from programs\\&."
msgstr ""
"Siehe die \\m[blue]B<Neue Control-Gruppen-"
"Schnittstelle>\\m[]\\&\\s-2\\u[1]\\d\\s+2 für eine Einführung, wie für "
"Programme Bereichs-Units eingesetzt werden können."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Note that, unlike service units, scope units have no \"main\" process: all "
"processes in the scope are equivalent\\&. The lifecycle of the scope unit is "
"thus not bound to the lifetime of one specific process, but to the existence "
"of at least one process in the scope\\&. This also means that the exit "
"statuses of these processes are not relevant for the scope unit failure "
"state\\&. Scope units may still enter a failure state, for example due to "
"resource exhaustion or stop timeouts being reached, but not due to programs "
"inside of them terminating uncleanly\\&. Since processes managed as scope "
"units generally remain children of the original process that forked them "
"off, it is also the job of that process to collect their exit statuses and "
"act on them as needed\\&."
msgstr ""
"Beachten Sie, dass Bereichs-Units, anders als Dienste-Units, keinen "
"»Hauptprozess« haben, alle Prozesse im Bereich sind äquivalent\\&. Der "
"Lebenszyklus der Bereichs-Unit ist nicht an die Lebensdauer eines bestimmten "
"Prozesses gebunden, sondern an die Existenz von mindestestens einem Prozess "
"im Bereich\\&. Das bedeutet auch, dass der Exit-Status eines Prozesses nicht "
"relevant für den Fehlerzustand der Bereichs-Unit ist\\&. Bereichs-Units "
"können weiterhin einen Fehlerzustand einnehmen, beispielsweise aufgrund von "
"Ressourcenerschöpfung und dem Erreichen von Zeitüberschreitungen, aber nicht "
"aufgrund unsauberer Beendigungen von Programmen innerhalb der Unit\\&. Da "
"die Prozesse, die als Bereichs-Unit verwaltet werden, im Allgemeinen "
"Kindprozess des ursprünglichen Prozesses, der sie per Fork gestartet hat, "
"bleiben, ist es auch die Aufgabe dieses Prozesses, ihre Exit-Status "
"einzusammeln und entsprechend zu reagieren\\&."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "AUTOMATIC DEPENDENCIES"
msgstr "AUTOMATISCHE ABHÄNGIGKEITEN"

#. type: SS
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Implicit Dependencies"
msgstr "Implizite Abhängigkeiten"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Implicit dependencies may be added as result of resource control parameters "
"as documented in B<systemd.resource-control>(5)\\&."
msgstr ""
"Wie in B<systemd.resource-control>(5) dokumentiert, können implizite "
"Abhängigkeiten als Ergebnis von Ressourcensteuerungsparametern hinzugefügt "
"werden\\&."

#. type: SS
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Default Dependencies"
msgstr "Standardabhängigkeiten"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The following dependencies are added unless I<DefaultDependencies=no> is set:"
msgstr ""
"Die folgenden Abhängigkeiten werden hinzugefügt, es sei denn, "
"I<DefaultDependencies=no> ist gesetzt:"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Scope units will automatically have dependencies of type I<Conflicts=> and "
"I<Before=> on shutdown\\&.target\\&. These ensure that scope units are "
"removed prior to system shutdown\\&. Only scope units involved with early "
"boot or late system shutdown should disable I<DefaultDependencies=> "
"option\\&."
msgstr ""
"Bereichs-Units erhalten automatisch Abhängigkeiten vom Typ I<Conflicts=> und "
"I<Before=> von shutdown\\&.target\\&. Dies stellt sicher, dass Bereichs-"
"Units vor dem Herunterfahren entfernt werden\\&. Nur Bereichs-Units, die in "
"der frühen Systemstartphase oder im späten Herunterfahren involviert sind, "
"sollten die Option I<DefaultDependencies=> deaktivieren\\&."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONEN"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Scope files may include a [Unit] section, which is described in B<systemd."
"unit>(5)\\&."
msgstr ""
"Bereichs-Dateien können einen Abschnit [Unit] enthalten, der in B<systemd."
"unit>(5) beschrieben ist\\&."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Scope files may include a [Scope] section, which carries information about "
"the scope and the units it contains\\&. A number of options that may be used "
"in this section are shared with other unit types\\&. These options are "
"documented in B<systemd.kill>(5)  and B<systemd.resource-control>(5)\\&. The "
"options specific to the [Scope] section of scope units are the following:"
msgstr ""
"Bereichs-Units können einen Abschnitt »[Scope]« enthalten, der Informationen "
"über den Bereich und die darin enthaltenen Units weitergibt\\&. Eine Reihe "
"von Optionen, die in diesem Abschnitt verwandt werden, werden auch von "
"anderen Unit-Typen verwendet\\&. Diese Optionen sind in B<systemd.kill>(5) "
"und B<systemd.resource-control>(5) dokumentiert\\&. Folgende Optionen sind "
"spezifisch für den Abschnitt »[Scope]« von Bereichs-Units:"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "I<OOMPolicy=>"
msgstr "I<OOMPolicy=>"

# FIXME systemd-oomd\\&.service → B<systemd-oomd\\&.service>(8)
#. type: Plain text
#: archlinux fedora-38 fedora-rawhide
msgid ""
"Configure the out-of-memory (OOM) killing policy for the kernel and the "
"userspace OOM killer B<systemd-oomd.service>(8)\\&. On Linux, when memory "
"becomes scarce to the point that the kernel has trouble allocating memory "
"for itself, it might decide to kill a running process in order to free up "
"memory and reduce memory pressure\\&. Note that systemd-oomd\\&.service is a "
"more flexible solution that aims to prevent out-of-memory situations for the "
"userspace too, not just the kernel, by attempting to terminate services "
"earlier, before the kernel would have to act\\&."
msgstr ""
"Konfiguriert die Richtlinie für die Speichererschöpfungs- (OOM-)Beendigung "
"für den Kernel und den OOM-Klller im Anwendungsraum B<systemd-oomd."
"service>(8)\\&. Wenn unter Linux der Speicher so knapp wird, dass der Kernel "
"Schwierigkeiten bekommt, Speicher für sich selbst zu reservieren, dann kann "
"er sich entscheiden, laufende Prozesse zu beenden, um Speicher freizugeben "
"und den Speicherdruck zu reduzieren\\&. Beachten Sie, dass B<systemd-oomd."
"service>(8) eine flexiblere Lösung ist, die zu verhindern versucht, dass "
"Speichererschöpfungssituationen im Anwendungsraum auftreten, nicht nur im "
"Kernel, indem versucht wird, Dienste früher zu beenden, bevor der Kernel "
"agieren müsste\\&."

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide
msgid ""
"This setting takes one of B<continue>, B<stop> or B<kill>\\&. If set to "
"B<continue> and a process in the unit is killed by the OOM killer, this is "
"logged but the unit continues running\\&. If set to B<stop> the event is "
"logged but the unit is terminated cleanly by the service manager\\&. If set "
"to B<kill> and one of the unit\\*(Aqs processes is killed by the OOM killer "
"the kernel is instructed to kill all remaining processes of the unit too, by "
"setting the memory\\&.oom\\&.group attribute to B<1>; also see "
"\\m[blue]B<kernel documentation>\\m[]\\&\\s-2\\u[2]\\d\\s+2\\&."
msgstr ""
"Diese Einstellung akzeptiert entweder B<continue>, B<stop> oder B<kill>\\&. "
"Falls auf B<continue> gesetzt und ein Prozess in der Unit vom OOM-Killer "
"beendet wird, wird dies protokolliert aber die Unit läuft weiter\\&. Falls "
"auf B<stop> gesetzt, wird das Ereignis protokolliert, aber die Unit wird "
"sauber durch den Diensteverwalter beendet\\&. Falls auf B<kill> gesetzt und "
"einer der Prozesse der Unit wird durch den OOM-Killer beendet, wird der "
"Kernel angewiesen, auch alle verbleibenden Prozesse der Unit durch Setzen "
"des Attributes memory\\&.oom\\&.group auf B<1> durch den OOM-Killer zu "
"beenden; siehe auch die "
"\\m[blue]B<Kerneldokumentation>\\m[]\\&\\s-2\\u[2]\\d\\s+2\\&."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Defaults to the setting I<DefaultOOMPolicy=> in B<systemd-system.conf>(5)  "
"is set to, except for units where I<Delegate=> is turned on, where it "
"defaults to B<continue>\\&."
msgstr ""
"Standardmäßig der Wert, auf den die Einstellung I<DefaultOOMPolicy=> in "
"B<systemd-system.conf>(5) gesetzt ist, außer bei Units, bei denen "
"I<Delegate=> eingeschaltet ist, wo die Vorgabe B<continue> ist\\&."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Use the I<OOMScoreAdjust=> setting to configure whether processes of the "
"unit shall be considered preferred or less preferred candidates for process "
"termination by the Linux OOM killer logic\\&. See B<systemd.exec>(5)  for "
"details\\&."
msgstr ""
"Verwenden Sie die Einstellung I<OOMScoreAdjust=>, um zu konfigurieren, ob "
"Prozesse der Unit als bevorzugte oder weniger bevorzugte Kandidaten für "
"Prozessbeendigungen durch die Logik des OOM-Killers von Linux betrachtet "
"werden sollen\\&. Siehe B<systemd.exec>(5) für Details\\&."

# FIXME B<systemd-oomd> → B<systemd-oomd>(8)
# FIMXE OOM kills → OOM killer
# FIXME kills a → killed a
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"This setting also applies to B<systemd-oomd>\\&. Similarly to the kernel OOM "
"kills, this setting determines the state of the unit after B<systemd-oomd> "
"kills a cgroup associated with it\\&."
msgstr ""
"Diese Einstellung gilt auch für B<systemd-oomd>(8)\\&. Ähnlich wie beim "
"Kernel-OOM-Killer bestimmt diese Einstellung den Zustand der Unit, nachdem "
"B<systemd-oomd>(8) eine ihr zugeordnete Cgroup beendet hat\\&."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "I<RuntimeMaxSec=>"
msgstr "I<RuntimeMaxSec=>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Configures a maximum time for the scope to run\\&. If this is used and the "
"scope has been active for longer than the specified time it is terminated "
"and put into a failure state\\&. Pass \"infinity\" (the default) to "
"configure no runtime limit\\&."
msgstr ""
"Konfiguriert eine maximale Laufzeit für den Bereich\\&. Falls dies verwandt "
"wird und die Unit länger als die festgelegte Zeit aktiv war, wird sie "
"beendet und in einen Fehlerzustand gebracht\\&. Übergeben Sie "
"»infinity« (die Vorgabe), um keine Laufzeitbegrenzung zu konfigurieren\\&."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "I<RuntimeRandomizedExtraSec=>"
msgstr "I<RuntimeRandomizedExtraSec=>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"This option modifies I<RuntimeMaxSec=> by increasing the maximum runtime by "
"an evenly distributed duration between 0 and the specified value (in "
"seconds)\\&. If I<RuntimeMaxSec=> is unspecified, then this feature will be "
"disabled\\&."
msgstr ""
"Diese Option verändert I<RuntimeMaxSec=> durch Erhöhung der maximalen "
"Laufzeit mit einer gleichverteilten Dauer zwischen 0 und dem festgelegten "
"Wert (in Sekunden)\\&. Falls I<RuntimeMaxSec=> nicht festgelegt ist, wird "
"diese Funktionalität deaktiviert\\&."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Check B<systemd.unit>(5), B<systemd.exec>(5), and B<systemd.kill>(5)  for "
"more settings\\&."
msgstr ""
"Lesen Sie B<systemd.unit>(5), B<systemd.exec>(5) und B<systemd.kill>(5) für "
"weitere Einstellungen\\&."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<systemd>(1), B<systemd-run>(1), B<systemd.unit>(5), B<systemd.resource-"
"control>(5), B<systemd.service>(5), B<systemd.directives>(7)\\&."
msgstr ""
"B<systemd>(1), B<systemd-run>(1), B<systemd.unit>(5), B<systemd.resource-"
"control>(5), B<systemd.service>(5), B<systemd.directives>(7)\\&."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ANMERKUNGEN"

#. type: IP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid " 1."
msgstr " 1."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "New Control Group Interfaces"
msgstr "Neue Control-Gruppen-Schnittstellen"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"\\%https://www.freedesktop.org/wiki/Software/systemd/ControlGroupInterface"
msgstr ""
"\\%https://www.freedesktop.org/wiki/Software/systemd/ControlGroupInterface"

#. type: IP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid " 2."
msgstr " 2."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "kernel documentation"
msgstr "Kernel-Dokumentation"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "\\%https://docs.kernel.org/admin-guide/cgroup-v2.html"
msgstr "\\%https://docs.kernel.org/admin-guide/cgroup-v2.html"

#. type: TH
#: debian-bullseye debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "systemd 252"
msgstr "systemd 252"

#. type: Plain text
#: debian-bullseye debian-unstable mageia-cauldron opensuse-tumbleweed
msgid ""
"Configure the out-of-memory (OOM) kernel killer policy\\&. Note that the "
"userspace OOM killer B<systemd-oomd.service>(8)  is a more flexible solution "
"that aims to prevent out-of-memory situations for the userspace, not just "
"the kernel\\&."
msgstr ""
"Konfiguriert die Richtlinie für den Speichererschöpfungs- (OOM-)Killer\\&. "
"Beachten Sie, dass der OOM-Killer im Anwendungsraum B<systemd-oomd."
"service>(8) eine flexiblere Lösung ist, die zu verhindern versucht, dass "
"Speichererschöpfungssituationen im Anwendungsraum auftreten, nicht nur im "
"Kernel\\&."

#. type: Plain text
#: debian-bullseye debian-unstable mageia-cauldron opensuse-tumbleweed
msgid ""
"On Linux, when memory becomes scarce to the point that the kernel has "
"trouble allocating memory for itself, it might decide to kill a running "
"process in order to free up memory and reduce memory pressure\\&. This "
"setting takes one of B<continue>, B<stop> or B<kill>\\&. If set to "
"B<continue> and a process of the service is killed by the OOM killer, this "
"is logged but the unit continues running\\&. If set to B<stop> the event is "
"logged but the unit is terminated cleanly by the service manager\\&. If set "
"to B<kill> and one of the unit\\*(Aqs processes is killed by the OOM killer "
"the kernel is instructed to kill all remaining processes of the unit too, by "
"setting the memory\\&.oom\\&.group attribute to B<1>; also see "
"\\m[blue]B<kernel documentation>\\m[]\\&\\s-2\\u[2]\\d\\s+2\\&."
msgstr ""
"Wenn unter Linux der Speicher so knapp wird, dass der Kernel Schwierigkeiten "
"bekommt, Speicher für sich selbst zu reservieren, dann kann er sich "
"entscheiden, laufende Prozesse zu beenden, um Speicher freizugeben und den "
"Speicherdruck zu reduzieren\\&. Diese Einstellung akzeptiert entweder "
"B<continue>, B<stop> oder B<kill>\\&. Falls auf B<continue> gesetzt und ein "
"Prozess des Dienstes vom OOM-Killer beendet wird, wird dies protokolliert "
"aber die Unit läuft weiter\\&. Falls auf B<stop> gesetzt, wird das Ereignis "
"protokolliert, aber die Unit wird sauber durch den Diensteverwalter "
"beendet\\&. Falls auf B<kill> gesetzt und einer der Prozesse der Unit wird "
"durch den OOM-Killer beendet, wird der Kernel angewiesen, auch alle "
"verbleibenden Prozesse der Unit durch Setzen des Attributes memory\\&.oom\\&."
"group auf B<1> durch den OOM-Killer zu beenden; siehe auch die "
"\\m[blue]B<Kerneldokumentation>\\m[]\\&\\s-2\\u[2]\\d\\s+2\\&."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "systemd 249"
msgstr "systemd 249"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"\\%https://www.freedesktop.org/wiki/Software/systemd/ControlGroupInterface/"
msgstr ""
"\\%https://www.freedesktop.org/wiki/Software/systemd/ControlGroupInterface/"
