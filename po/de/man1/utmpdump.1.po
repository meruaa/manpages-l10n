# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Mario Blättermann <mario.blaettermann@gmail.com>, 2014, 2019, 2021.
# Helge Kreutzmann <debian@helgefjell.de>, 2016.
msgid ""
msgstr ""
"Project-Id-Version: manpages-de\n"
"POT-Creation-Date: 2023-02-15 19:24+0100\n"
"PO-Revision-Date: 2021-05-22 21:16+0200\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 21.04.1\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "UTMPDUMP"
msgstr "UTMPDUMP"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-05-11"
msgstr "11. Mai 2022"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "util-linux 2.38.1"
msgstr "util-linux 2.38.1"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Dienstprogramme für Benutzer"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "utmpdump - dump UTMP and WTMP files in raw format"
msgstr "utmpdump - UTMP- und WTMP-Dateien im Rohformat ausgeben"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<utmpdump> [options] I<filename>"
msgstr "B<utmpdump> [Optionen] I<Dateiname>"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<utmpdump> is a simple program to dump UTMP and WTMP files in raw format, "
"so they can be examined. B<utmpdump> reads from stdin unless a I<filename> "
"is passed."
msgstr ""
"B<utmpdump> ist ein einfaches Programm zum Ausgeben von UTMP- und WTMP-"
"Dateien im Rohformat, so dass diese untersucht werden können. B<utmpdump> "
"liest aus der Standardeingabe, wenn kein I<Dateiname> übergeben wird."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONEN"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<-f>, B<--follow>"
msgstr "B<-f>, B<--follow>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Output appended data as the file grows."
msgstr "gibt angehängte Daten aus, wenn die Datei wächst."

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<-o>, B<--output> I<file>"
msgstr "B<-o>, B<--output> I<Datei>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Write command output to I<file> instead of standard output."
msgstr "schreibt das Ergebnis in I<Datei> statt in die Standardausgabe."

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<-r>, B<--reverse>"
msgstr "B<-r>, B<--reverse>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Undump, write back edited login information into the utmp or wtmp files."
msgstr ""
"kehrt den Vorgang um. Die bearbeiteten Anmeldeinformationen werden in die "
"UTMP- oder WTMP-Dateien zurück geschrieben"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Display help text and exit."
msgstr "zeigt einen Hilfetext an und beendet das Programm."

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Print version and exit."
msgstr "zeigt die Versionsnummer an und beendet das Programm."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ANMERKUNGEN"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<utmpdump> can be useful in cases of corrupted utmp or wtmp entries. It can "
"dump out utmp/wtmp to an ASCII file, which can then be edited to remove "
"bogus entries, and reintegrated using:"
msgstr ""
"B<utmpdump> kann im Falle beschädigter UTMP- oder WTMP-Einträge hilfreich "
"sein. Es kann die Daten in eine ASCII-Datei ausgeben, die Sie bearbeiten "
"können, um die falschen Einträge zu entfernen. Folgender Befehl führt dies "
"aus:"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<utmpdump -r E<lt> ascii_file E<gt> wtmp>"
msgstr "B<utmpdump -r E<lt> ASCII-Datei E<gt> wtmp>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "But be warned, B<utmpdump> was written for debugging purposes only."
msgstr ""
"Seien Sie gewarnt, B<utmpdump> wurde nur für Debugging-Zwecke geschrieben."

#. type: SS
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "File formats"
msgstr "Dateiformate"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Only the binary version of the B<utmp>(5) is standardised. Textual dumps may "
"become incompatible in future."
msgstr ""
"Nur die binäre Version von B<utmp>(5) ist standardisiert. Auszüge in "
"Textform können in der Zukunft inkompatibel werden."

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The version 2.28 was the last one that printed text output using B<ctime>(3) "
"timestamp format. Newer dumps use millisecond precision ISO-8601 timestamp "
"format in UTC-0 timezone. Conversion from former timestamp format can be "
"made to binary, although attempt to do so can lead the timestamps to drift "
"amount of timezone offset."
msgstr ""
"Die Version 2.28 war die letzte, die die Textausgabe mittels des "
"Zeitstempelformats von B<ctime>(3) ausgab. Neuere Auszüge verwenden "
"Millisekundengenauigkeit im Zeitstempelformat von ISO-8601 in der UTC-0-"
"Zeitzone. Eine Umwandlung vom früheren Zeitstempelformat kann in das binäre "
"erfolgen. Allerdings können solche Versuche dazu führen, dass der "
"Zeitstempel aufgrund von Zeitzonen-Versätzen abweicht."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "FEHLER"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"You may B<not> use the B<-r> option, as the format for the utmp/wtmp files "
"strongly depends on the input format. This tool was B<not> written for "
"normal use, but for debugging only."
msgstr ""
"Sie dürfen die Option B<-r> B<nicht> verwenden, da das Format der utmp/wtmp-"
"Dateien stark vom Eingabeformat abhängt. Dieses Werkzeug wurde B<nicht> für "
"normale Verwendung geschrieben, sondern lediglich für Debugging-Zwecke."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "AUTHORS"
msgstr "AUTOREN"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Michael Krapp"
msgstr "Michael Krapp"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<last>(1), B<w>(1), B<who>(1), B<utmp>(5)"
msgstr "B<last>(1), B<w>(1), B<who>(1), B<utmp>(5)"

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "FEHLER MELDEN"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "For bug reports, use the issue tracker at"
msgstr "Verwenden Sie zum Melden von Fehlern das Fehlererfassungssystem auf"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "AVAILABILITY"
msgstr "VERFÜGBARKEIT"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The B<utmpdump> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""
"Der Befehl B<utmpdump> ist Teil des Pakets util-linux, welches "
"heruntergeladen werden kann von:"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "July 2014"
msgstr "Juli 2014"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "util-linux"
msgstr "util-linux"

#. type: Plain text
#: debian-bullseye
msgid "B<utmpdump> [options] [I<filename>]"
msgstr "B<utmpdump> [Optionen] [I<Dateiname>]"

#. type: Plain text
#: debian-bullseye
msgid ""
"B<utmpdump> is a simple program to dump UTMP and WTMP files in raw format, "
"so they can be examined.  B<utmpdump> reads from stdin unless a I<filename> "
"is passed."
msgstr ""
"B<utmpdump> ist ein einfaches Programm zum Ausgeben von UTMP- und WTMP-"
"Dateien im Rohformat, so dass diese untersucht werden können. B<utmpdump> "
"liest aus der Standardeingabe, wenn kein I<Dateiname> übergeben wird."

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-f>,B< --follow>"
msgstr "B<-f>,B< --follow>"

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-o>,B< --output >I<file>"
msgstr "B<-o>,B< --output >I<Datei>"

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-r>,B< --reverse>"
msgstr "B<-r>,B< --reverse>"

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-V>,B< --version>"
msgstr "B<-V>,B< --version>"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "Display version information and exit."
msgstr "zeigt Versionsinformationen an und beendet das Programm."

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-h>,B< --help>"
msgstr "B<-h>,B< --help>"

#. type: Plain text
#: debian-bullseye
msgid ""
"B<utmpdump> can be useful in cases of corrupted utmp or wtmp entries.  It "
"can dump out utmp/wtmp to an ASCII file, which can then be edited to remove "
"bogus entries, and reintegrated using:"
msgstr ""
"B<utmpdump> kann im Falle beschädigter UTMP- oder WTMP-Einträge hilfreich "
"sein. Es kann die Daten in eine ASCII-Datei ausgeben, die Sie bearbeiten "
"können, um die falschen Einträge zu entfernen. Folgender Befehl führt dies "
"aus:"

#. type: Plain text
#: debian-bullseye
msgid ""
"Only the binary version of the B<utmp>(5)  is standardised.  Textual dumps "
"may become incompatible in future."
msgstr ""
"Nur die binäre Version von B<utmp>(5) ist standardisiert. Auszüge in "
"Textform können in der Zukunft inkompatibel werden."

#. type: Plain text
#: debian-bullseye
msgid ""
"The version 2.28 was the last one that printed text output using "
"B<ctime>(3)  timestamp format.  Newer dumps use millisecond precision "
"ISO-8601 timestamp format in UTC-0 timezone.  Conversion from former "
"timestamp format can be made to binary, although attempt to do so can lead "
"the timestamps to drift amount of timezone offset."
msgstr ""
"Die Version 2.28 war die letzte, die die Textausgabe mittels des "
"Zeitstempelformats von B<ctime>(3) ausgab. Neuere Auszüge verwenden "
"Millisekundengenauigkeit im Zeitstempelformat von ISO-8601 in der UTC-0-"
"Zeitzone. Eine Umwandlung vom früheren Zeitstempelformat kann in das binäre "
"erfolgen. Allerdings können solche Versuche dazu führen, dass der "
"Zeitstempel aufgrund von Zeitzonen-Versätzen abweicht."

#. type: Plain text
#: debian-bullseye
msgid ""
"You may B<not> use the B<-r> option, as the format for the utmp/wtmp files "
"strongly depends on the input format.  This tool was B<not> written for "
"normal use, but for debugging only."
msgstr ""
"Sie dürfen die Option B<-r> B<nicht> verwenden, da das Format der utmp/wtmp-"
"Dateien stark vom Eingabeformat abhängt. Dieses Werkzeug wurde B<nicht> für "
"normale Verwendung geschrieben, sondern lediglich für Debugging-Zwecke."

#. type: Plain text
#: debian-bullseye
msgid ""
"The utmpdump command is part of the util-linux package and is available from "
"E<.UR https://\\:www.kernel.org\\:/pub\\:/linux\\:/utils\\:/util-linux/> "
"Linux Kernel Archive E<.UE .>"
msgstr ""
"Der Befehl utmpdump ist Teil des Pakets util-linux, welches aus dem E<.UR "
"https://\\:www.kernel.org\\:/pub\\:/linux\\:/utils\\:/util-linux/> Linux "
"Kernel-Archiv E<.UE .> heruntergeladen werden kann."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2022-02-14"
msgstr "14. Februar 2022"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "util-linux 2.37.4"
msgstr "util-linux 2.37.4"
