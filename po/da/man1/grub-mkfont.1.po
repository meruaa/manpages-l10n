# Danish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Joe Hansen <joedalton2@yahoo.dk>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-02-15 18:53+0100\n"
"PO-Revision-Date: 2022-06-18 15:09+0200\n"
"Last-Translator: Joe Hansen <joedalton2@yahoo.dk>\n"
"Language-Team: Danish <debian-l10n-danish@lists.debian.org>\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 20.12.2\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "GRUB-MKFONT"
msgstr "GRUB-MKFONT"

#. type: TH
#: archlinux debian-unstable
#, no-wrap
msgid "February 2023"
msgstr "februar 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "GRUB 2:2.06.r456.g65bc45963-1"
msgstr "GRUB 2:2.06.r456.g65bc45963-1"

#. type: TH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "User Commands"
msgstr "Brugerkommandoer"

#. type: SH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "NAME"
msgstr "NAVN"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "grub-mkfont - make GRUB font files"
msgstr "grub-mkfont - lav GRUB-skrifttypefiler"

#. type: SH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid ""
"B<grub-mkfont> [I<\\,OPTION\\/>...] [I<\\,OPTIONS\\/>] I<\\,FONT_FILES\\/>"
msgstr ""
"B<grub-mkfont> [I<\\,TILVALG\\/>...] [I<\\,TILVALG\\/>] I<\\,"
"SKRIFTTYPEFILER\\/>"

#. type: SH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESKRIVELSE"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "Convert common font file formats into PF2"
msgstr "Konverter almindelige skrifttypefilformater til PF2"

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "B<-a>, B<--force-autohint>"
msgstr "B<-a>, B<--force-autohint>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "force autohint"
msgstr "tving autohint"

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "B<-b>, B<--bold>"
msgstr "B<-b>, B<--bold>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "convert to bold font"
msgstr "konverter til fed skrift"

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "B<-c>, B<--asce>=I<\\,NUM\\/>"
msgstr "B<-c>, B<--asce>=I<\\,NUM\\/>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "set font ascent"
msgstr "angiv overlængde for skrifttype"

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "B<-d>, B<--desc>=I<\\,NUM\\/>"
msgstr "B<-d>, B<--desc>=I<\\,NUM\\/>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "set font descent"
msgstr "angiv underlængde for skrifttype"

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "B<-i>, B<--index>=I<\\,NUM\\/>"
msgstr "B<-i>, B<--index>=I<\\,NUM\\/>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "select face index"
msgstr "vælg skrifttypeindeks"

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "B<--no-bitmap>"
msgstr "B<--no-bitmap>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "ignore bitmap strikes when loading"
msgstr "ignorer bitmap-dele ved indlæsning"

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "B<--no-hinting>"
msgstr "B<--no-hinting>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "disable hinting"
msgstr "deaktiver hinting"

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "B<-n>, B<--name>=I<\\,NAME\\/>"
msgstr "B<-n>, B<--name>=I<\\,NAVN\\/>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "set font family name"
msgstr "angiv skriftfamilienavn"

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "B<-o>, B<--output>=I<\\,FILE\\/>"
msgstr "B<-o>, B<--output>=I<\\,FIL\\/>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "save output in FILE [required]"
msgstr "gem uddata i FIL [påkrævet]"

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "B<-r>, B<--range>=I<\\,FROM-TO[\\/>,FROM-TO]"
msgstr "B<-r>, B<--range>=I<\\,FRA-TIL[\\/>,FRA-TIL]"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "set font range"
msgstr "angiv skrifttypeinterval"

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "B<-s>, B<--size>=I<\\,SIZE\\/>"
msgstr "B<-s>, B<--size>=I<\\,STØRRELSE\\/>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "set font size"
msgstr "angiv skriftstørrelse"

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "print verbose messages."
msgstr "udskriv uddybende meddelelser."

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "-?, B<--help>"
msgstr "-?, B<--help>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "give this help list"
msgstr "vis denne hjælpeliste"

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "B<--usage>"
msgstr "B<--usage>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "give a short usage message"
msgstr "vis en kort besked om brug af programmet"

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "print program version"
msgstr "udskriv programversion"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid ""
"Mandatory or optional arguments to long options are also mandatory or "
"optional for any corresponding short options."
msgstr ""
"Obligatoriske eller valgfri argumenter til lange tilvalg er også "
"obligatoriske henholdsvis valgfri til de tilsvarende korte."

#. type: SH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "REPORTING BUGS"
msgstr "FEJLRAPPORTER"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "Report bugs to E<lt>bug-grub@gnu.orgE<gt>."
msgstr ""
"Rapporter programfejl på engelsk til E<lt>bug-grub@gnu.orgE<gt>.\n"
"Oversættelsesfejl rapporteres til E<lt>dansk@dansk-gruppen.dkE<gt>."

#. type: SH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr "SE OGSÅ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "B<grub-mkconfig>(8)"
msgstr "B<grub-mkconfig>(8)"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid ""
"The full documentation for B<grub-mkfont> is maintained as a Texinfo "
"manual.  If the B<info> and B<grub-mkfont> programs are properly installed "
"at your site, the command"
msgstr ""
"Den fulde dokumentation for B<grub-mkfont> vedligeholdes som Texinfo-manual. "
"Hvis B<info> og B<grub-mkfont> programmerne er korrekt installeret på din "
"side, bør kommandoen"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "B<info grub-mkfont>"
msgstr "B<info grub-mkfont>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "should give you access to the complete manual."
msgstr "give dig adgang til den fulde manual."

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "November 2022"
msgstr "november 2022"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "GRUB 2.06-3~deb11u5"
msgstr "GRUB 2.06-3~deb11u5"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "GRUB 2.06-8"
msgstr "GRUB 2.06-8"
