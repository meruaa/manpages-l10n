# Persian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.10.0\n"
"POT-Creation-Date: 2023-02-20 20:10+0100\n"
"PO-Revision-Date: 2021-09-12 19:03+0430\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Persian <mksafavi@gmail.com>\n"
"Language: fa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "intro"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "نام"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "intro - introduction to user commands"
msgstr "مقدمه - مقدمه ای بر فرمان های کاربر"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "توضیح"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Section 1 of the manual describes user commands and tools, for example, file "
"manipulation tools, shells, compilers, web browsers, file and image viewers "
"and editors, and so on."
msgstr ""
"بخش اول راهنما فرمان های کاربر و ابزار ها را توضیح میدهد. برای مثال، ابزار "
"های دستکاری فایل، shellها، کامپایلگر ها، مرورگرهای شبکه، نمایشگر فایل و عکس "
"و ویرایشگر متن و غیره."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "یادداشت ها"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Linux is a flavor of UNIX, and as a first approximation all user commands "
"under UNIX work precisely the same under Linux (and FreeBSD and lots of "
"other UNIX-like systems)."
msgstr ""
"لینوکس یک طمع از سیستم عامل یونیکس می باشد. در یک تخمین کلی غالب فرمان های "
"یونیکس در عملکرد مشابهی را در لینوکس(و FreeBSD و سایر سیستم های شبه-یونیکس) "
"خواهند داشت."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Under Linux, there are GUIs (graphical user interfaces), where you can point "
"and click and drag, and hopefully get work done without first reading lots "
"of documentation.  The traditional UNIX environment is a CLI (command line "
"interface), where you type commands to tell the computer what to do.  That "
"is faster and more powerful, but requires finding out what the commands "
"are.  Below a bare minimum, to get started."
msgstr ""
"در لینوکس، رابط های کاربری گرافیکی(GUI) موجود هستند که در آنها می توان با "
"اشاره و کلیک، امیدوار بود که کار های مورد نظر بدون خواندن مستندات زیادی "
"انجام شوند.  محیط مرسوم یونیکس رابط خط فرمان(CLI) است، که در آنها شما به "
"رایانه می گویید که چه کار کند.  سریع تر و قدرتمند تر است، اما نیاز مند این "
"می باشد که بدانید فرمان ها چه هستند.  در ادامه با حداقل فرمان های مورد نیاز "
"برای شروع آشنا می شوید."

#. type: SS
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Login"
msgstr "ورود"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"In order to start working, you probably first have to open a session by "
"giving your username and password.  The program B<login>(1)  now starts a "
"I<shell> (command interpreter) for you.  In case of a graphical login, you "
"get a screen with menus or icons and a mouse click will start a shell in a "
"window.  See also B<xterm>(1)."
msgstr ""

#. type: SS
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy, no-wrap
msgid "The shell"
msgstr "پوسته(shell)"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"One types commands to the I<shell>, the command interpreter.  It is not "
"built-in, but is just a program and you can change your shell.  Everybody "
"has their own favorite one.  The standard one is called I<sh>.  See also "
"B<ash>(1), B<bash>(1), B<chsh>(1), B<csh>(1), B<dash>(1), B<ksh>(1), "
"B<zsh>(1)."
msgstr ""
"کاربر فرمان هایش را در مفسر فرمان I<shell> می نویسد. شل یک برنامه درونی "
"سیستم عامل نیست و می توانید آن را عوض کنید.  هرکسی یک شل مورد علاقه دارد.  "
"شل استاندارد I<sh> می باشد.  همچنین ببینید B<ash>(1), B<bash>(1), "
"B<chsh>(1), B<csh>(1), B<dash>(1), B<ksh>(1), B<zsh>(1)."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "A session might go like:"
msgstr "یک جلسه کاری می تواند شبیه به این باشد:"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"knuth login: B<aeb>\n"
"Password: B<********>\n"
"$ B<date>\n"
"Tue Aug  6 23:50:44 CEST 2002\n"
"$ B<cal>\n"
"     August 2002\n"
"Su Mo Tu We Th Fr Sa\n"
"             1  2  3\n"
" 4  5  6  7  8  9 10\n"
"11 12 13 14 15 16 17\n"
"18 19 20 21 22 23 24\n"
"25 26 27 28 29 30 31\n"
msgstr ""
"knuth login: B<aeb>\n"
"Password: B<********>\n"
"$ B<date>\n"
"Tue Aug  6 23:50:44 CEST 2002\n"
"$ B<cal>\n"
"     August 2002\n"
"Su Mo Tu We Th Fr Sa\n"
"             1  2  3\n"
" 4  5  6  7  8  9 10\n"
"11 12 13 14 15 16 17\n"
"18 19 20 21 22 23 24\n"
"25 26 27 28 29 30 31\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"$ B<ls>\n"
"bin  tel\n"
"$ B<ls -l>\n"
"total 2\n"
"drwxrwxr-x   2 aeb       1024 Aug  6 23:51 bin\n"
"-rw-rw-r--   1 aeb         37 Aug  6 23:52 tel\n"
"$ B<cat tel>\n"
"maja    0501-1136285\n"
"peter   0136-7399214\n"
"$ B<cp tel tel2>\n"
"$ B<ls -l>\n"
"total 3\n"
"drwxr-xr-x   2 aeb       1024 Aug  6 23:51 bin\n"
"-rw-r--r--   1 aeb         37 Aug  6 23:52 tel\n"
"-rw-r--r--   1 aeb         37 Aug  6 23:53 tel2\n"
"$ B<mv tel tel1>\n"
"$ B<ls -l>\n"
"total 3\n"
"drwxr-xr-x   2 aeb       1024 Aug  6 23:51 bin\n"
"-rw-r--r--   1 aeb         37 Aug  6 23:52 tel1\n"
"-rw-r--r--   1 aeb         37 Aug  6 23:53 tel2\n"
"$ B<diff tel1 tel2>\n"
"$ B<rm tel1>\n"
"$ B<grep maja tel2>\n"
"maja    0501-1136285\n"
"$\n"
msgstr ""
"$ B<ls>\n"
"bin  tel\n"
"$ B<ls -l>\n"
"total 2\n"
"drwxrwxr-x   2 aeb       1024 Aug  6 23:51 bin\n"
"-rw-rw-r--   1 aeb         37 Aug  6 23:52 tel\n"
"$ B<cat tel>\n"
"maja    0501-1136285\n"
"peter   0136-7399214\n"
"$ B<cp tel tel2>\n"
"$ B<ls -l>\n"
"total 3\n"
"drwxr-xr-x   2 aeb       1024 Aug  6 23:51 bin\n"
"-rw-r--r--   1 aeb         37 Aug  6 23:52 tel\n"
"-rw-r--r--   1 aeb         37 Aug  6 23:53 tel2\n"
"$ B<mv tel tel1>\n"
"$ B<ls -l>\n"
"total 3\n"
"drwxr-xr-x   2 aeb       1024 Aug  6 23:51 bin\n"
"-rw-r--r--   1 aeb         37 Aug  6 23:52 tel1\n"
"-rw-r--r--   1 aeb         37 Aug  6 23:53 tel2\n"
"$ B<diff tel1 tel2>\n"
"$ B<rm tel1>\n"
"$ B<grep maja tel2>\n"
"maja    0501-1136285\n"
"$\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy
msgid "Here typing Control-D ended the session."
msgstr "با زدن کلیدهای Control-D جلسه خاتمه می یابد."

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, fuzzy
msgid ""
"The B<$> here was the command prompt\\[em]it is the shell's way of "
"indicating that it is ready for the next command.  The prompt can be "
"customized in lots of ways, and one might include stuff like username, "
"machine name, current directory, time, and so on.  An assignment PS1=\"What "
"next, master? \" would change the prompt as indicated."
msgstr ""
"نشانگر B<$> پیام واره فرمان بود \\(emit که روش شل برای نشان دادن آماده بودن "
"برای فرمان بعدیست.  پیام واره می تواند به شکل های مختلفی شخصی سازی شود و "
"اطلاعاتی مانند نام، نام دستگاه، دایرکتوری فعلی، زمان و غیره را در بر بگیرد.  "
"به عنوان تمرین PS1=\"فرمان بعدی چیست؟ آقا \"، پیام واره را به آن تغییر می "
"دهند."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"We see that there are commands I<date> (that gives date and time), and "
"I<cal> (that gives a calendar)."
msgstr ""
"می بینیم که فرمان هایی هستن که تاریخ و زمان  I<date> و تقویم I<cal> را نشان "
"می دهند."

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, fuzzy
#| msgid ""
#| "The command I<ls> lists the contents of the current directory\\(emit "
#| "tells you what files you have.  With a I<-l> option it gives a long "
#| "listing, that includes the owner and size and date of the file, and the "
#| "permissions people have for reading and/or changing the file.  For "
#| "example, the file \"tel\" here is 37 bytes long, owned by aeb and the "
#| "owner can read and write it, others can only read it.  Owner and "
#| "permissions can be changed by the commands I<chown> and I<chmod>."
msgid ""
"The command I<ls> lists the contents of the current directory\\[em]it tells "
"you what files you have.  With a I<-l> option it gives a long listing, that "
"includes the owner and size and date of the file, and the permissions people "
"have for reading and/or changing the file.  For example, the file \"tel\" "
"here is 37 bytes long, owned by aeb and the owner can read and write it, "
"others can only read it.  Owner and permissions can be changed by the "
"commands I<chown> and I<chmod>."
msgstr ""
"فرمان I<ls> لیست محتوای دایرکتوری فعلی را نمایش می دهد. گزینه  I<-l> نمایش "
"کامل لیست، صاحب و اندازه و زمان فایل و سطح دسترسی کاربران برای خواندن ویا "
"نوشتن فایل را شامل میشود. برای مثال، فایل \"tel\" به اندازه ۳۷ بایت میباشد و "
"صاحب آن aeb میباشد و صاحب دسترسی خواندن و نوشتن دارد و بقیه فقط دسترسی "
"خواندن دارند. صاحب و سطح دسترسی میتواند با chown و chmod تغییر کند."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The command I<cat> will show the contents of a file.  (The name is from "
"\"concatenate and print\": all files given as parameters are concatenated "
"and sent to \"standard output\" (see B<stdout>(3)), here the terminal "
"screen.)"
msgstr ""
"فرمان I<cat> محتویات یک فایل را نمایش می دهد.  نام آن از concatenate (به هم "
"پیوستن) و print (چاپ) گرفته شده است که فایل هایی که به عنوان پارامتر داده "
"شوند ابتدا به هم متصل شده و سپس در \"خروجی استاندارد\" (ببینید B<stdout>(3)) "
"در صفحه ترمینال نمایش داده می شود."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The command I<cp> (from \"copy\") will copy a file."
msgstr "فرمان I<cp> (گرفته شده از کپی \"copy\") می تواند فایلی را کپی کند."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The command I<mv> (from \"move\"), on the other hand, only renames it."
msgstr ""
"فرمان I<mv> (گرفته شده از انتقال \"move\") تنها نام فایل را تغییر می دهد."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The command I<diff> lists the differences between two files.  Here there was "
"no output because there were no differences."
msgstr ""
"فرمان I<diff> اختلافات بین دو فایل را لیست میکند. در اینجا خروجی ای ندارد "
"چون اختلافی بین دو فایل نیست."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The command I<rm> (from \"remove\") deletes the file, and be careful! it is "
"gone.  No wastepaper basket or anything.  Deleted means lost."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The command I<grep> (from \"g/re/p\") finds occurrences of a string in one "
"or more files.  Here it finds Maja's telephone number."
msgstr ""

#. type: SS
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Pathnames and the current directory"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Files live in a large tree, the file hierarchy.  Each has a I<pathname> "
"describing the path from the root of the tree (which is called I</>)  to the "
"file.  For example, such a full pathname might be I</home/aeb/tel>.  Always "
"using full pathnames would be inconvenient, and the name of a file in the "
"current directory may be abbreviated by giving only the last component.  "
"That is why I</home/aeb/tel> can be abbreviated to I<tel> when the current "
"directory is I</home/aeb>."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The command I<pwd> prints the current directory."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The command I<cd> changes the current directory."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"Try alternatively I<cd> and I<pwd> commands and explore I<cd> usage: \"cd\", "
"\"cd .\", \"cd ..\", \"cd /\", and \"cd \\[ti]\"."
msgstr ""

#. type: SS
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Directories"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The command I<mkdir> makes a new directory."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The command I<rmdir> removes a directory if it is empty, and complains "
"otherwise."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The command I<find> (with a rather baroque syntax) will find files with "
"given name or other properties.  For example, \"find . -name tel\" would "
"find the file I<tel> starting in the present directory (which is called I<."
">).  And \"find / -name tel\" would do the same, but starting at the root of "
"the tree.  Large searches on a multi-GB disk will be time-consuming, and it "
"may be better to use B<locate>(1)."
msgstr ""

#. type: SS
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Disks and filesystems"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The command I<mount> will attach the filesystem found on some disk (or "
"floppy, or CDROM or so)  to the big filesystem hierarchy.  And I<umount> "
"detaches it again.  The command I<df> will tell you how much of your disk is "
"still free."
msgstr ""

#. type: SS
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Processes"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"On a UNIX system many user and system processes run simultaneously.  The one "
"you are talking to runs in the I<foreground>, the others in the "
"I<background>.  The command I<ps> will show you which processes are active "
"and what numbers these processes have.  The command I<kill> allows you to "
"get rid of them.  Without option this is a friendly request: please go "
"away.  And \"kill -9\" followed by the number of the process is an immediate "
"kill.  Foreground processes can often be killed by typing Control-C."
msgstr ""

#. type: SS
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Getting information"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"There are thousands of commands, each with many options.  Traditionally "
"commands are documented on I<man pages>, (like this one), so that the "
"command \"man kill\" will document the use of the command \"kill\" (and "
"\"man man\" document the command \"man\").  The program I<man> sends the "
"text through some I<pager>, usually I<less>.  Hit the space bar to get the "
"next page, hit q to quit."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"In documentation it is customary to refer to man pages by giving the name "
"and section number, as in B<man>(1).  Man pages are terse, and allow you to "
"find quickly some forgotten detail.  For newcomers an introductory text with "
"more examples and explanations is useful."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"A lot of GNU/FSF software is provided with info files.  Type \"info info\" "
"for an introduction on the use of the program I<info>."
msgstr ""

#
#.  Actual examples? Separate section for each of cat, cp, ...?
#.  gzip, bzip2, tar, rpm
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Special topics are often treated in HOWTOs.  Look in I</usr/share/doc/howto/"
"en> and use a browser if you find HTML files there."
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "همچنین ببینید"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<ash>(1), B<bash>(1), B<chsh>(1), B<csh>(1), B<dash>(1), B<ksh>(1), "
"B<locate>(1), B<login>(1), B<man>(1), B<xterm>(1), B<zsh>(1), B<wait>(2), "
"B<stdout>(3), B<man-pages>(7), B<standards>(7)"
msgstr ""
"B<ash>(1), B<bash>(1), B<chsh>(1), B<csh>(1), B<dash>(1), B<ksh>(1), "
"B<locate>(1), B<login>(1), B<man>(1), B<xterm>(1), B<zsh>(1), B<wait>(2), "
"B<stdout>(3), B<man-pages>(7), B<standards>(7)"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "INTRO"
msgstr "INTRO"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "2020-08-13"
msgstr ""

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux"
msgstr "لینوکس"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux User's Manual"
msgstr "راهنمای برنامه نویس لینوکس"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy
msgid ""
"The B<$> here was the command prompt\\(emit is the shell's way of indicating "
"that it is ready for the next command.  The prompt can be customized in lots "
"of ways, and one might include stuff like username, machine name, current "
"directory, time, and so on.  An assignment PS1=\"What next, master? \" would "
"change the prompt as indicated."
msgstr ""
"نشانگر B<$> پیام واره فرمان بود \\(emit که روش شل برای نشان دادن آماده بودن "
"برای فرمان بعدیست.  پیام واره می تواند به شکل های مختلفی شخصی سازی شود و "
"اطلاعاتی مانند نام، نام دستگاه، دایرکتوری فعلی، زمان و غیره را در بر بگیرد.  "
"به عنوان تمرین PS1=\"فرمان بعدی چیست؟ آقا \"، پیام واره را به آن تغییر می "
"دهند."

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The command I<ls> lists the contents of the current directory\\(emit tells "
"you what files you have.  With a I<-l> option it gives a long listing, that "
"includes the owner and size and date of the file, and the permissions people "
"have for reading and/or changing the file.  For example, the file \"tel\" "
"here is 37 bytes long, owned by aeb and the owner can read and write it, "
"others can only read it.  Owner and permissions can be changed by the "
"commands I<chown> and I<chmod>."
msgstr ""
"فرمان I<ls> لیست محتوای دایرکتوری فعلی را نمایش می دهد. گزینه  I<-l> نمایش "
"کامل لیست، صاحب و اندازه و زمان فایل و سطح دسترسی کاربران برای خواندن ویا "
"نوشتن فایل را شامل میشود. برای مثال، فایل \"tel\" به اندازه ۳۷ بایت میباشد و "
"صاحب آن aeb میباشد و صاحب دسترسی خواندن و نوشتن دارد و بقیه فقط دسترسی "
"خواندن دارند. صاحب و سطح دسترسی میتواند با chown و chmod تغییر کند."

#. type: Plain text
#: debian-bullseye
msgid ""
"Try alternatively I<cd> and I<pwd> commands and explore I<cd> usage: \"cd\", "
"\"cd .\", \"cd ..\", \"cd /\" and \"cd \\(ti\"."
msgstr ""

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "پایان نگاشت"

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"این صفحه بخشی از انتشار ۵.۱۰ پروژه صفحات راهنما لینوکس I<man-pages> میباشد."
"توضیح ای از پروژه، اطلاعات لازم برای اعلام خطا و آخرین نسخه این صفحه را در \\"
"%https://www.kernel.org/doc/man-pages/ بیابید."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2015-07-23"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"One types commands to the I<shell>, the command interpreter.  It is not "
"built-in, but is just a program and you can change your shell.  Everybody "
"has her own favorite one.  The standard one is called I<sh>.  See also "
"B<ash>(1), B<bash>(1), B<chsh>(1), B<csh>(1), B<dash>(1), B<ksh>(1), "
"B<zsh>(1)."
msgstr ""
"کاربر فرمان هایش را در مفسر فرمان I<shell> می نویسد. شل یک برنامه درونی "
"سیستم عامل نیست و می توانید آن را عوض کنید.  هرکسی یک شل مورد علاقه دارد.  "
"شل استاندارد I<sh> می باشد.  همچنین ببینید B<ash>(1), B<bash>(1), "
"B<chsh>(1), B<csh>(1), B<dash>(1), B<ksh>(1), B<zsh>(1)."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"Try alternatively I<cd> and I<pwd> commands and explore I<cd> usage: \"cd\", "
"\"cd .\", \"cd ..\", \"cd /\" and \"cd ~\"."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"این صفحه بخشی از انتشار ۴.۱۶ پروژه صفحات راهنما لینوکس I<man-pages> میباشد."
"توضیح ای از پروژه، اطلاعات لازم برای اعلام خطا و آخرین نسخه این صفحه را در \\"
"%https://www.kernel.org/doc/man-pages/ بیابید."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-10-30"
msgstr ""

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.02"
msgstr ""

#. type: Plain text
#: opensuse-tumbleweed
msgid ""
"Try alternatively I<cd> and I<pwd> commands and explore I<cd> usage: \"cd\", "
"\"cd .\", \"cd ..\", \"cd /\", and \"cd \\(ti\"."
msgstr ""
