# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2013, 2016.
# Dmitriy Ovchinnikov <dmitriyxt5@gmail.com>, 2012.
# Dmitry Bolkhovskikh <d20052005@yandex.ru>, 2017.
# Katrin Kutepova <blackkatelv@gmail.com>, 2018.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-02-20 20:39+0100\n"
"PO-Revision-Date: 2019-10-15 18:59+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "umount"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-12-04"
msgstr "4 декабря 2022 г."

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "umount, umount2 - unmount filesystem"
msgstr "umount, umount2 - размонтирует файловую систему"

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>sys/mount.hE<gt>>\n"
msgstr "B<#include E<lt>sys/mount.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int umount(const char *>I<target>B<);>\n"
"B<int umount2(const char *>I<target>B<, int >I<flags>B<);>\n"
msgstr ""
"B<int umount(const char *>I<target>B<);>\n"
"B<int umount2(const char *>I<target>B<, int >I<flags>B<);>\n"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#.  Note: the kernel naming differs from the glibc naming
#.  umount2 is the glibc name for what the kernel now calls umount
#.  and umount is the glibc name for oldumount
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<umount>()  and B<umount2>()  remove the attachment of the (topmost) "
"filesystem mounted on I<target>."
msgstr ""
"Вызовы B<umount>() и B<umount2>() удаляют подключение (самого верхнего "
"уровня) к файловой системе, примонтированной к I<target>."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Appropriate privilege (Linux: the B<CAP_SYS_ADMIN> capability) is required "
"to unmount filesystems."
msgstr ""
"Для размонтирования файловых систем требуются права (Linux: мандат "
"B<CAP_SYS_ADMIN>)."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Linux 2.1.116 added the B<umount2>()  system call, which, like B<umount>(), "
"unmounts a target, but allows additional I<flags> controlling the behavior "
"of the operation:"
msgstr ""
"В Linux 2.1.116 добавлен системный вызов B<umount2>(), который, подобно "
"B<umount>(), размонтирует заданный объект, но позволяет указать "
"дополнительные флаги I<flags>, контролирующие поведение операции:"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<MNT_FORCE> (since Linux 2.1.116)"
msgstr "B<MNT_FORCE> (начиная с Linux 2.1.116)"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Ask the filesystem to abort pending requests before attempting the unmount.  "
"This may allow the unmount to complete without waiting for an inaccessible "
"server, but could cause data loss.  If, after aborting requests, some "
"processes still have active references to the filesystem, the unmount will "
"still fail.  As at Linux 4.12, B<MNT_FORCE> is supported only on the "
"following filesystems: 9p (since Linux 2.6.16), ceph (since Linux 2.6.34), "
"cifs (since Linux 2.6.12), fuse (since Linux 2.6.16), lustre (since Linux "
"3.11), and NFS (since Linux 2.1.116)."
msgstr ""
"Попросить файловую систему прервать ожидающие запросы перед попыткой "
"размонтирования. Это может позволить выполнить размонтирование без ожидания "
"недоступного сервера, но может привести к потере данных. Если после "
"прерывания запросов некоторые процессы продолжат обращаться к файловой "
"системе, размонтирование не будет выполнено. В Linux 4.12 B<MNT_FORCE> "
"поддерживается только в следующих файловых системах: 9p (начиная с Linux "
"2.6.16), ceph (начиная с Linux 2.6.34), cifs (начиная с Linux 2.6.12), fuse "
"(начиная с Linux 2.6.16), lustre (начиная с Linux 3.11) и NFS (начиная с "
"Linux 2.1.116)."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<MNT_DETACH> (since Linux 2.4.11)"
msgstr "B<MNT_DETACH> (начиная с Linux 2.4.11)"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Perform a lazy unmount: make the mount point unavailable for new "
#| "accesses, immediately disconnect the filesystem and all filesystems "
#| "mounted below it from each other and from the mount table, and actually "
#| "perform the unmount when the mount point ceases to be busy."
msgid ""
"Perform a lazy unmount: make the mount unavailable for new accesses, "
"immediately disconnect the filesystem and all filesystems mounted below it "
"from each other and from the mount table, and actually perform the unmount "
"when the mount ceases to be busy."
msgstr ""
"Выполнить отложенное размонтирование: сделать точку монтирования недоступной "
"для новых подключений, немедленно размонтировать файловую систему и все "
"файловые системы, смонтированные ниже по дереву и по таблице монтирования, и "
"выполнить настоящее размонтирование только когда точка доступа станет "
"свободной."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<MNT_EXPIRE> (since Linux 2.6.8)"
msgstr "B<MNT_EXPIRE> (начиная Linux 2.6.8)"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Mark the mount point as expired.  If a mount point is not currently in "
#| "use, then an initial call to B<umount2>()  with this flag fails with the "
#| "error B<EAGAIN>, but marks the mount point as expired.  The mount point "
#| "remains expired as long as it isn't accessed by any process.  A second "
#| "B<umount2>()  call specifying B<MNT_EXPIRE> unmounts an expired mount "
#| "point.  This flag cannot be specified with either B<MNT_FORCE> or "
#| "B<MNT_DETACH>."
msgid ""
"Mark the mount as expired.  If a mount is not currently in use, then an "
"initial call to B<umount2>()  with this flag fails with the error B<EAGAIN>, "
"but marks the mount as expired.  The mount remains expired as long as it "
"isn't accessed by any process.  A second B<umount2>()  call specifying "
"B<MNT_EXPIRE> unmounts an expired mount.  This flag cannot be specified with "
"either B<MNT_FORCE> or B<MNT_DETACH>."
msgstr ""
"Пометить точку монтирования как недействительную. Если точка монтирования в "
"это время не используется, то первоначальный вызов B<umount2>() с этим "
"флагом завершится с ошибкой B<EAGAIN>, но пометит точку монтирования как "
"недействительную. Точка монтирования остаётся недействительной до тех пор, "
"пока какой-нибудь процесс не запросит к ней доступ. Второй вызов "
"B<umount2>() с флагом B<MNT_EXPIRE> размонтирует недействительную точку "
"монтирования. Этот флаг нельзя указывать вместе с B<MNT_FORCE> или "
"B<MNT_DETACH>."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<UMOUNT_NOFOLLOW> (since Linux 2.6.34)"
msgstr "B<UMOUNT_NOFOLLOW> (начиная с Linux 2.6.34)"

#. #-#-#-#-#  archlinux: umount.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  Later added to Linux 2.6.33-stable
#. type: Plain text
#. #-#-#-#-#  debian-bullseye: umount.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  Later added to 2.6.33-stable
#. type: Plain text
#. #-#-#-#-#  debian-unstable: umount.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  Later added to Linux 2.6.33-stable
#. type: Plain text
#. #-#-#-#-#  fedora-38: umount.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  Later added to Linux 2.6.33-stable
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: umount.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  Later added to Linux 2.6.33-stable
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: umount.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  Later added to Linux 2.6.33-stable
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-15-5: umount.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  Later added to 2.6.33-stable
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: umount.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  Later added to Linux 2.6.33-stable
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Don't dereference I<target> if it is a symbolic link.  This flag allows "
"security problems to be avoided in set-user-ID-I<root> programs that allow "
"unprivileged users to unmount filesystems."
msgstr ""
"Не следовать по ссылке, если I<target> является символьной ссылкой. Это флаг "
"помогает избежать проблем с безопасностью в программах принадлежащих root и "
"с установленным битом set-user-ID, которые позволяют непривилегированным "
"пользователям размонтировать файловые системы."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "On success, zero returned.  On failure, -1 is returned and I<errno> is "
#| "set to indicate the error."
msgid ""
"On success, zero is returned.  On error, -1 is returned, and I<errno> is set "
"to indicate the error."
msgstr ""
"При успешном выполнении возвращается 0. При ошибке возвращается -1, а "
"I<errno> присваивается значение ошибки."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ОШИБКИ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The error values given below result from filesystem type independent "
"errors.  Each filesystem type may have its own special errors and its own "
"special behavior.  See the Linux kernel source code for details."
msgstr ""
"Коды ошибок, описанные ниже, не зависят от типа файловой системы. У каждой "
"файловой системы могут быть свои коды ошибок и своё собственное поведение. "
"Подробности смотрите в исходном коде ядра Linux."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EAGAIN>"
msgstr "B<EAGAIN>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"A call to B<umount2>()  specifying B<MNT_EXPIRE> successfully marked an "
"unbusy filesystem as expired."
msgstr ""
"Вызов B<umount2>() с флагом B<MNT_EXPIRE> успешно пометил незанятую файловую "
"систему как недействительную."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EBUSY>"
msgstr "B<EBUSY>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "I<target> could not be unmounted because it is busy."
msgstr "Объект I<target> не может быть размонтирован, так как он занят."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "I<target> points outside the user address space."
msgstr ""
"Объект I<target> указывает вне адресного пространства, доступного "
"пользователю."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "I<target> is not a mount point."
msgstr "Значение I<target> не является точкой монтирования."

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "I<target> is locked; see B<mount_namespaces>(7)."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<umount2>()  was called with B<MNT_EXPIRE> and either B<MNT_DETACH> or "
"B<MNT_FORCE>."
msgstr ""
"Вызов B<umount2>() был вызван с флагом B<MNT_EXPIRE> и с B<MNT_DETACH> или "
"B<MNT_FORCE>."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL> (since Linux 2.6.34)"
msgstr "B<EINVAL> (начиная с Linux 2.6.34)"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<umount2>()  was called with an invalid flag value in I<flags>."
msgstr "Вызов B<umount2>() был сделан с неверным значением флага I<flags>."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ENAMETOOLONG>"
msgstr "B<ENAMETOOLONG>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "A pathname was longer than B<MAXPATHLEN>."
msgstr "Значение пути длиннее чем B<MAXPATHLEN>."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOENT>"
msgstr "B<ENOENT>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "A pathname was empty or had a nonexistent component."
msgstr "Указан пустой путь или одна из его частей не существует."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOMEM>"
msgstr "B<ENOMEM>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The kernel could not allocate a free page to copy filenames or data into."
msgstr ""
"Ядро не может выделить свободную страницу для копирования имени файла или "
"данных."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM>"
msgstr "B<EPERM>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The caller does not have the required privileges."
msgstr "Вызывающий процесс не имеет требуемых привилегий."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "ВЕРСИИ"

#.  http://sourceware.org/bugzilla/show_bug.cgi?id=10092
#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<MNT_DETACH> and B<MNT_EXPIRE> are available in glibc since version 2.11."
msgid "B<MNT_DETACH> and B<MNT_EXPIRE> are available since glibc 2.11."
msgstr ""
"Флаги B<MNT_DETACH> и B<MNT_EXPIRE> доступны в glibc начиная с версии 2.11."

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"These functions are Linux-specific and should not be used in programs "
"intended to be portable."
msgstr ""
"Эти функции есть только в Linux, и они не должны использоваться в "
"переносимых программах."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ЗАМЕЧАНИЯ"

#. type: SS
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "umount() and shared mount points"
msgid "umount() and shared mounts"
msgstr "Вызов umount() и общие точки монтирования"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Shared mount points cause any mount activity on a mount point, including "
#| "B<umount>()  operations, to be forwarded to every shared mount point in "
#| "the peer group and every slave mount of that peer group.  This means that "
#| "B<umount>()  of any peer in a set of shared mounts will cause all of its "
#| "peers to be unmounted and all of their slaves to be unmounted as well."
msgid ""
"Shared mounts cause any mount activity on a mount, including B<umount>()  "
"operations, to be forwarded to every shared mount in the peer group and "
"every slave mount of that peer group.  This means that B<umount>()  of any "
"peer in a set of shared mounts will cause all of its peers to be unmounted "
"and all of their slaves to be unmounted as well."
msgstr ""
"Действия на точку монтирования, являющуюся общей, включая операции "
"B<umount>(), будут переданы каждой общей точке монтирования в этой группе и "
"каждой подчинённой точке монтирования в этой группе. Это означает, что "
"B<umount>() члена из набора общих точек монтирования приведёт к "
"размонтированию всех в его членов, а также всех их подчинённых."

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "This propagation of unmount activity can be particularly surprising on "
#| "systems where every mount point is shared by default.  On such systems, "
#| "recursively bind mounting the root directory of the filesystem onto a "
#| "subdirectory and then later unmounting that subdirectory with "
#| "B<MNT_DETACH> will cause every mount in the mount namespace to be lazily "
#| "unmounted."
msgid ""
"This propagation of unmount activity can be particularly surprising on "
"systems where every mount is shared by default.  On such systems, "
"recursively bind mounting the root directory of the filesystem onto a "
"subdirectory and then later unmounting that subdirectory with B<MNT_DETACH> "
"will cause every mount in the mount namespace to be lazily unmounted."
msgstr ""
"Это распространение действия размонтирования может быть особенно "
"удивительным в системах, где каждая точка монтирования является общей по "
"умолчанию. В таких системах рекурсивно привязываемое монтирование корневого "
"каталога файловой системы в подкаталог, а затем размонтирование этого "
"подкаталога с B<MNT_DETACH> приведёт к тому, что каждая точка монтирования в "
"пространстве имён монтирования будет размонтирована в отложенном режиме."

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "To ensure B<umount>()  does not propagate in this fashion, the mount "
#| "point may be remounted using a B<mount>()  call with a I<mount_flags> "
#| "argument that includes both B<MS_REC> and B<MS_PRIVATE> prior to "
#| "B<umount>()  being called."
msgid ""
"To ensure B<umount>()  does not propagate in this fashion, the mount may be "
"remounted using a B<mount>(2)  call with a I<mount_flags> argument that "
"includes both B<MS_REC> and B<MS_PRIVATE> prior to B<umount>()  being called."
msgstr ""
"Чтобы B<umount>() также не делал, перед вызовом B<umount>() точка "
"монтирования может быть перемонтирована с помощью вызова B<mount>() с "
"аргументом I<mount_flags>, в который добавлены B<MS_REC> и B<MS_PRIVATE>."

#. type: SS
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Historical details"
msgstr "Историческая справка"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The original B<umount>()  function was called as I<umount(device)> and would "
"return B<ENOTBLK> when called with something other than a block device.  In "
"Linux 0.98p4, a call I<umount(dir)> was added, in order to support anonymous "
"devices.  In Linux 2.3.99-pre7, the call I<umount(device)> was removed, "
"leaving only I<umount(dir)> (since now devices can be mounted in more than "
"one place, so specifying the device does not suffice)."
msgstr ""
"Изначально функция B<umount>() вызывалась как I<umount(device)> и возвращала "
"B<ENOTBLK> при попытке работы с любым не блочным устройством. В Linux 0.98p4 "
"был добавлен вызов I<umount(dir)> для поддержки анонимных устройств. В Linux "
"2.3.99-pre7 был убран вызов I<umount(device)>, остался только I<umount(dir)> "
"(теперь устройства могут быть подключены более чем к одной точке, поэтому "
"указания только устройства недостаточно)."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<mount>(2), B<mount_namespaces>(7), B<path_resolution>(7), B<mount>(8), "
"B<umount>(8)"
msgstr ""
"B<mount>(2), B<mount_namespaces>(7), B<path_resolution>(7), B<mount>(8), "
"B<umount>(8)"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "UMOUNT"
msgstr "UMOUNT"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "2020-06-09"
msgstr "9 июня 2020 г."

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Руководство программиста Linux"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "B<int umount(const char *>I<target>B<);>\n"
msgstr "B<int umount(const char *>I<target>B<);>\n"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "B<int umount2(const char *>I<target>B<, int >I<flags>B<);>\n"
msgstr "B<int umount2(const char *>I<target>B<, int >I<flags>B<);>\n"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"Perform a lazy unmount: make the mount point unavailable for new accesses, "
"immediately disconnect the filesystem and all filesystems mounted below it "
"from each other and from the mount table, and actually perform the unmount "
"when the mount point ceases to be busy."
msgstr ""
"Выполнить отложенное размонтирование: сделать точку монтирования недоступной "
"для новых подключений, немедленно размонтировать файловую систему и все "
"файловые системы, смонтированные ниже по дереву и по таблице монтирования, и "
"выполнить настоящее размонтирование только когда точка доступа станет "
"свободной."

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"Mark the mount point as expired.  If a mount point is not currently in use, "
"then an initial call to B<umount2>()  with this flag fails with the error "
"B<EAGAIN>, but marks the mount point as expired.  The mount point remains "
"expired as long as it isn't accessed by any process.  A second B<umount2>()  "
"call specifying B<MNT_EXPIRE> unmounts an expired mount point.  This flag "
"cannot be specified with either B<MNT_FORCE> or B<MNT_DETACH>."
msgstr ""
"Пометить точку монтирования как недействительную. Если точка монтирования в "
"это время не используется, то первоначальный вызов B<umount2>() с этим "
"флагом завершится с ошибкой B<EAGAIN>, но пометит точку монтирования как "
"недействительную. Точка монтирования остаётся недействительной до тех пор, "
"пока какой-нибудь процесс не запросит к ней доступ. Второй вызов "
"B<umount2>() с флагом B<MNT_EXPIRE> размонтирует недействительную точку "
"монтирования. Этот флаг нельзя указывать вместе с B<MNT_FORCE> или "
"B<MNT_DETACH>."

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"On success, zero is returned.  On error, -1 is returned, and I<errno> is set "
"appropriately."
msgstr ""
"При успешном выполнении возвращается 0. В случае ошибки возвращается -1, а "
"I<errno> устанавливается в соответствующее значение."

#.  http://sourceware.org/bugzilla/show_bug.cgi?id=10092
#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"B<MNT_DETACH> and B<MNT_EXPIRE> are available in glibc since version 2.11."
msgstr ""
"Флаги B<MNT_DETACH> и B<MNT_EXPIRE> доступны в glibc начиная с версии 2.11."

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "СООТВЕТСТВИЕ СТАНДАРТАМ"

#. type: SS
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "umount() and shared mount points"
msgstr "Вызов umount() и общие точки монтирования"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"Shared mount points cause any mount activity on a mount point, including "
"B<umount>()  operations, to be forwarded to every shared mount point in the "
"peer group and every slave mount of that peer group.  This means that "
"B<umount>()  of any peer in a set of shared mounts will cause all of its "
"peers to be unmounted and all of their slaves to be unmounted as well."
msgstr ""
"Действия на точку монтирования, являющуюся общей, включая операции "
"B<umount>(), будут переданы каждой общей точке монтирования в этой группе и "
"каждой подчинённой точке монтирования в этой группе. Это означает, что "
"B<umount>() члена из набора общих точек монтирования приведёт к "
"размонтированию всех в его членов, а также всех их подчинённых."

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"This propagation of unmount activity can be particularly surprising on "
"systems where every mount point is shared by default.  On such systems, "
"recursively bind mounting the root directory of the filesystem onto a "
"subdirectory and then later unmounting that subdirectory with B<MNT_DETACH> "
"will cause every mount in the mount namespace to be lazily unmounted."
msgstr ""
"Это распространение действия размонтирования может быть особенно "
"удивительным в системах, где каждая точка монтирования является общей по "
"умолчанию. В таких системах рекурсивно привязываемое монтирование корневого "
"каталога файловой системы в подкаталог, а затем размонтирование этого "
"подкаталога с B<MNT_DETACH> приведёт к тому, что каждая точка монтирования в "
"пространстве имён монтирования будет размонтирована в отложенном режиме."

#. type: Plain text
#: debian-bullseye
msgid ""
"To ensure B<umount>()  does not propagate in this fashion, the mount point "
"may be remounted using a B<mount>(2)  call with a I<mount_flags> argument "
"that includes both B<MS_REC> and B<MS_PRIVATE> prior to B<umount>()  being "
"called."
msgstr ""
"Чтобы B<umount>() также не делал, перед вызовом B<umount>() точка "
"монтирования может быть перемонтирована с помощью вызова B<mount>(2) с "
"аргументом I<mount_flags>, в который добавлены B<MS_REC> и B<MS_PRIVATE>."

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 5.10. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15 сентября 2017 г."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"To ensure B<umount>()  does not propagate in this fashion, the mount point "
"may be remounted using a B<mount>()  call with a I<mount_flags> argument "
"that includes both B<MS_REC> and B<MS_PRIVATE> prior to B<umount>()  being "
"called."
msgstr ""
"Чтобы B<umount>() также не делал, перед вызовом B<umount>() точка "
"монтирования может быть перемонтирована с помощью вызова B<mount>() с "
"аргументом I<mount_flags>, в который добавлены B<MS_REC> и B<MS_PRIVATE>."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 4.16. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.02"
msgstr "Linux man-pages 6.02"
