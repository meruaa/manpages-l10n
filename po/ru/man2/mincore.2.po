# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# aereiae <aereiae@gmail.com>, 2014.
# Alexey <a.chepugov@gmail.com>, 2015.
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2013-2017.
# Dmitriy S. Seregin <dseregin@59.ru>, 2013.
# Dmitry Bolkhovskikh <d20052005@yandex.ru>, 2017.
# ITriskTI <ITriskTI@gmail.com>, 2013.
# Max Is <ismax799@gmail.com>, 2016.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017.
# Малянов Евгений Викторович <maljanow@outlook.com>, 2014.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-02-20 20:16+0100\n"
"PO-Revision-Date: 2019-10-06 08:59+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<mincore>():"
msgid "mincore"
msgstr "B<mincore>():"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 февраля 2023 г."

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "mincore - determine whether pages are resident in memory"
msgstr "mincore - определяет, хранятся ли страницы в памяти"

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>sys/mman.hE<gt>>\n"
msgstr "B<#include E<lt>sys/mman.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<int mincore(void *>I<addr>B<, size_t >I<length>B<, unsigned char *>I<vec>B<);>\n"
msgid "B<int mincore(void >I<addr>B<[.>I<length>B<], size_t >I<length>B<, unsigned char *>I<vec>B<);>\n"
msgstr "B<int mincore(void *>I<addr>B<, size_t >I<length>B<, unsigned char *>I<vec>B<);>\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Требования макроса тестирования свойств для glibc (см. "
"B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "B<mincore>():"
msgstr "B<mincore>():"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, fuzzy, no-wrap
#| msgid ""
#| "    Since glibc 2.19:\n"
#| "        _DEFAULT_SOURCE\n"
#| "    Glibc 2.19 and earlier:\n"
#| "        _BSD_SOURCE || _SVID_SOURCE\n"
msgid ""
"    Since glibc 2.19:\n"
"        _DEFAULT_SOURCE\n"
"    glibc 2.19 and earlier:\n"
"        _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"    начиная с glibc 2.19:\n"
"        _DEFAULT_SOURCE\n"
"    в glibc 2.19 и старее:\n"
"        _BSD_SOURCE || _SVID_SOURCE\n"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<mincore>()  returns a vector that indicates whether pages of the calling "
"process's virtual memory are resident in core (RAM), and so will not cause a "
"disk access (page fault) if referenced.  The kernel returns residency "
"information about the pages starting at the address I<addr>, and continuing "
"for I<length> bytes."
msgstr ""
"B<mincore>() возвращает вектор, описывающий страницы виртуальной памяти "
"вызывающего процесса, которые находятся в физической памяти (core, RAM) и "
"поэтому доступ к ним не приводит к обращению к дискам. Ядро возвращает "
"информацию о страницах, которые расположены начиная с адреса I<addr> и "
"занимают I<length> байт."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The I<addr> argument must be a multiple of the system page size.  The "
"I<length> argument need not be a multiple of the page size, but since "
"residency information is returned for whole pages, I<length> is effectively "
"rounded up to the next multiple of the page size.  One may obtain the page "
"size (B<PAGE_SIZE>)  using I<sysconf(_SC_PAGESIZE)>."
msgstr ""
"Значение аргумента I<addr> должно быть кратно размеру системной страницы. "
"Значение аргумента I<length> может быть не кратно размеру страницы, но так "
"как возвращается информация по целым страницам, I<length> округляется до "
"следующего значения, кратного размеру страницы. Размер страницы "
"(B<PAGE_SIZE>) может быть получен с помощью I<sysconf(_SC_PAGESIZE)>."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The I<vec> argument must point to an array containing at least "
"I<(length+PAGE_SIZE-1) / PAGE_SIZE> bytes.  On return, the least significant "
"bit of each byte will be set if the corresponding page is currently resident "
"in memory, and be clear otherwise.  (The settings of the other bits in each "
"byte are undefined; these bits are reserved for possible later use.)  Of "
"course the information returned in I<vec> is only a snapshot: pages that are "
"not locked in memory can come and go at any moment, and the contents of "
"I<vec> may already be stale by the time this call returns."
msgstr ""
"Аргумент I<vec> должен указывать на массив размером не менее "
"I<(length+PAGE_SIZE-1) / PAGE_SIZE> байт. При возврате самый младший "
"значимый бит каждого байта будет установлен, если соответствующая страница "
"находится в памяти, и будет сброшен, если это не так. (Значения других битов "
"каждого байта не определено; эти биты зарезервированы для использования в "
"будущем.) Естественно, информация, возвращаемая в I<vec>, носит моментальный "
"характер: страницы, которые не заблокированы в памяти, могут быть удалены из "
"неё или добавлены в любой момент, и содержимое I<vec> может стать "
"неактуальным уже на момент возврата из вызова."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "On success, B<mbind>()  returns 0; on error, -1 is returned and I<errno> "
#| "is set to indicate the error."
msgid ""
"On success, B<mincore>()  returns zero.  On error, -1 is returned, and "
"I<errno> is set to indicate the error."
msgstr ""
"При успешном выполнении B<mbind>() возвращается 0. При ошибке возвращается "
"-1, а в I<errno> содержится код ошибки."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ОШИБКИ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<EAGAIN> kernel is temporarily out of resources."
msgstr "B<EAGAIN> ядру временно не хватает ресурсов."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "I<vec> points to an invalid address."
msgstr "I<vec> указывает на неправильный адрес."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "I<addr> is not a multiple of the page size."
msgstr "Значение I<addr> не кратно размеру страницы."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOMEM>"
msgstr "B<ENOMEM>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"I<length> is greater than (I<TASK_SIZE> - I<addr>).  (This could occur if a "
"negative value is specified for I<length>, since that value will be "
"interpreted as a large unsigned integer.)  In Linux 2.6.11 and earlier, the "
"error B<EINVAL> was returned for this condition."
msgstr ""
"Значение I<length> больше чем (I<TASK_SIZE> - I<addr>). (Это может "
"произойти, если в I<length> указано отрицательное значение, так как это "
"значение воспринимается как большое беззнаковое целое.) В Linux 2.6.11 и "
"более ранних версиях в этом случае возвращалась ошибка B<EINVAL>."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "I<addr> to I<addr> + I<length> contained unmapped memory."
msgstr ""
"С адреса I<addr> по адрес I<addr> + I<length> содержится память без "
"отображения."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "ВЕРСИИ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Available since Linux 2.3.99pre1 and glibc 2.2."
msgstr "Доступен начиная с Linux 2.3.99pre1 и glibc 2.2."

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#.  It is on at least NetBSD, FreeBSD, OpenBSD, Solaris 8,
#.  AIX 5.1, SunOS 4.1
#.  .SH HISTORY
#.  The
#.  .BR mincore ()
#.  function first appeared in 4.4BSD.
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<mincore>()  is not specified in POSIX.1, and it is not available on all "
"UNIX implementations."
msgstr ""
"Системный вызов B<mincore>() не определён в стандарте POSIX.1 и отсутствует "
"во всех реализациях UNIX."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "ДЕФЕКТЫ"

#.  Linux (up to now, 2.6.5),
#.  .B mincore
#.  does not return correct information for MAP_PRIVATE mappings:
#.  for a MAP_PRIVATE file mapping,
#.  .B mincore
#.  returns the residency of the file pages, rather than any
#.  modified process-private pages that have been copied on write;
#.  for a MAP_PRIVATE mapping of
#.  .IR /dev/zero ,
#.  .B mincore
#.  always reports pages as nonresident;
#.  and for a MAP_PRIVATE, MAP_ANONYMOUS mapping,
#.  .B mincore
#.  always fails with the error
#.  .BR ENOMEM .
#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Before kernel 2.6.21, B<mincore>()  did not return correct information "
#| "for B<MAP_PRIVATE> mappings, or for nonlinear mappings (established using "
#| "B<remap_file_pages>(2))."
msgid ""
"Before Linux 2.6.21, B<mincore>()  did not return correct information for "
"B<MAP_PRIVATE> mappings, or for nonlinear mappings (established using "
"B<remap_file_pages>(2))."
msgstr ""
"До ядра версии 2.6.21, B<mincore>() не возвращал правильные данные для "
"отображений B<MAP_PRIVATE> или для нелинейных отображений (заданных с "
"помощью B<remap_file_pages>(2))."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<fincore>(1), B<madvise>(2), B<mlock>(2), B<mmap>(2), B<posix_fadvise>(2), "
"B<posix_madvise>(3)"
msgstr ""
"B<fincore>(1), B<madvise>(2), B<mlock>(2), B<mmap>(2), B<posix_fadvise>(2), "
"B<posix_madvise>(3)"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "MINCORE"
msgstr "MINCORE"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15 сентября 2017 г."

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Руководство программиста Linux"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "B<#include E<lt>unistd.hE<gt>>"
msgstr "B<#include E<lt>unistd.hE<gt>>"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "B<#include E<lt>sys/mman.hE<gt>>"
msgstr "B<#include E<lt>sys/mman.hE<gt>>"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"B<int mincore(void *>I<addr>B<, size_t >I<length>B<, unsigned char "
"*>I<vec>B<);>"
msgstr ""
"B<int mincore(void *>I<addr>B<, size_t >I<length>B<, unsigned char "
"*>I<vec>B<);>"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid ""
"B<mincore>():\n"
"    Since glibc 2.19:\n"
"        _DEFAULT_SOURCE\n"
"    Glibc 2.19 and earlier:\n"
"        _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"B<mincore>():\n"
"    начиная с glibc 2.19:\n"
"        _DEFAULT_SOURCE\n"
"    в glibc 2.19 и старее:\n"
"        _BSD_SOURCE || _SVID_SOURCE\n"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"On success, B<mincore>()  returns zero.  On error, -1 is returned, and "
"I<errno> is set appropriately."
msgstr ""
"При нормальном завершении работы B<mincore>()  возвращает ноль. При ошибках "
"возвращается -1, а переменной I<errno> присваивается соответствующее "
"значение."

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "СООТВЕТСТВИЕ СТАНДАРТАМ"

#.  Linux (up to now, 2.6.5),
#.  .B mincore
#.  does not return correct information for MAP_PRIVATE mappings:
#.  for a MAP_PRIVATE file mapping,
#.  .B mincore
#.  returns the residency of the file pages, rather than any
#.  modified process-private pages that have been copied on write;
#.  for a MAP_PRIVATE mapping of
#.  .IR /dev/zero ,
#.  .B mincore
#.  always reports pages as nonresident;
#.  and for a MAP_PRIVATE, MAP_ANONYMOUS mapping,
#.  .B mincore
#.  always fails with the error
#.  .BR ENOMEM .
#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"Before kernel 2.6.21, B<mincore>()  did not return correct information for "
"B<MAP_PRIVATE> mappings, or for nonlinear mappings (established using "
"B<remap_file_pages>(2))."
msgstr ""
"До ядра версии 2.6.21, B<mincore>() не возвращал правильные данные для "
"отображений B<MAP_PRIVATE> или для нелинейных отображений (заданных с "
"помощью B<remap_file_pages>(2))."

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 5.10. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 4.16. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-12-04"
msgstr "4 декабря 2022 г."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.02"
msgstr "Linux man-pages 6.02"

#. type: Plain text
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    Since glibc 2.19:\n"
"        _DEFAULT_SOURCE\n"
"    Glibc 2.19 and earlier:\n"
"        _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"    начиная с glibc 2.19:\n"
"        _DEFAULT_SOURCE\n"
"    в glibc 2.19 и старее:\n"
"        _BSD_SOURCE || _SVID_SOURCE\n"
