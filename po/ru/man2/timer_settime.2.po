# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2014, 2016-2017.
# Dmitry Bolkhovskikh <d20052005@yandex.ru>, 2017.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017, 2019.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-02-20 20:38+0100\n"
"PO-Revision-Date: 2019-09-16 18:46+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<timer_settime>(2)"
msgid "timer_settime"
msgstr "B<timer_settime>(2)"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-12-03"
msgstr "3 декабря 2022 г."

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"timer_settime, timer_gettime - arm/disarm and fetch state of POSIX per-"
"process timer"
msgstr ""
"timer_settime, timer_gettime - запускает/останавливает и возвращает "
"состояние таймера POSIX некоторого процесса"

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Real-time library (I<librt>, I<-lrt>)"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>time.hE<gt>>\n"
msgstr "B<#include E<lt>time.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "B<int timer_settime(timer_t >I<timerid>B<, int >I<flags>B<,>\n"
#| "B<                  const struct itimerspec *restrict >I<new_value>B<,>\n"
#| "B<                  struct itimerspec *restrict >I<old_value>B<);>\n"
#| "B<int timer_gettime(timer_t >I<timerid>B<, struct itimerspec *>I<curr_value>B<);>\n"
msgid ""
"B<int timer_settime(timer_t >I<timerid>B<, int >I<flags>B<,>\n"
"B<                  const struct itimerspec *restrict >I<new_value>B<,>\n"
"B<                  struct itimerspec *_Nullable restrict >I<old_value>B<);>\n"
"B<int timer_gettime(timer_t >I<timerid>B<, struct itimerspec *>I<curr_value>B<);>\n"
msgstr ""
"B<int timer_settime(timer_t >I<timerid>B<, int >I<flags>B<,>\n"
"B<                  const struct itimerspec *restrict >I<new_value>B<,>\n"
"B<                  struct itimerspec *restrict >I<old_value>B<);>\n"
"B<int timer_gettime(timer_t >I<timerid>B<, struct itimerspec *>I<curr_value>B<);>\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Требования макроса тестирования свойств для glibc (см. "
"B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "B<timer_settime>(), B<timer_gettime>():"
msgstr "B<timer_settime>(), B<timer_gettime>():"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "    _POSIX_C_SOURCE E<gt>= 199309L\n"
msgstr "    _POSIX_C_SOURCE E<gt>= 199309L\n"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<timer_settime>()  arms or disarms the timer identified by I<timerid>.  "
#| "The I<new_value> argument is pointer to an I<itimerspec> structure that "
#| "specifies the new initial value and the new interval for the timer.  The "
#| "I<itimerspec> structure is defined as follows:"
msgid ""
"B<timer_settime>()  arms or disarms the timer identified by I<timerid>.  The "
"I<new_value> argument is pointer to an I<itimerspec> structure that "
"specifies the new initial value and the new interval for the timer.  The "
"I<itimerspec> structure is described in B<itimerspec>(3type)."
msgstr ""
"Вызов B<timer_settime>() запускает или останавливает таймер, указанный в "
"I<timerid>. Аргумент I<new_value> — это указатель на структуру "
"I<itimerspec>, которая определяет новое начальное значение и новый интервал "
"таймера. Структура I<itimerspec> определена следующим образом:"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Each of the substructures of the I<itimerspec> structure is a "
"B<timespec>(3)  structure that allows a time value to be specified in "
"seconds and nanoseconds.  These time values are measured according to the "
"clock that was specified when the timer was created by B<timer_create>(2)."
msgstr ""
"Каждая подструктура структуры I<itimerspec> представляет собой структуру "
"B<timespec>(3), которая позволяет задавать значение времени в секундах и "
"наносекундах. Эти значения времени отсчитываются по часам, которые были "
"указаны при создании таймера с помощью B<timer_create>(2)."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If I<new_value-E<gt>it_value> specifies a nonzero value (i.e., either "
"subfield is nonzero), then B<timer_settime>()  arms (starts) the timer, "
"setting it to initially expire at the given time.  (If the timer was already "
"armed, then the previous settings are overwritten.)  If I<new_value-"
"E<gt>it_value> specifies a zero value (i.e., both subfields are zero), then "
"the timer is disarmed."
msgstr ""
"Если I<new_value-E<gt>it_value> равно ненулевому значению (т. е., любое из "
"подполей не равно нулю), то B<timer_settime>() запускает таймер, "
"устанавливая ему первое время срабатывания (если таймер уже запущен, то "
"предыдущие параметры перезаписываются). Если I<new_value-E<gt>it_value> "
"равно нулю (т. е., оба подполя равны нулю), то таймер выключается."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The I<new_value-E<gt>it_interval> field specifies the period of the timer, "
"in seconds and nanoseconds.  If this field is nonzero, then each time that "
"an armed timer expires, the timer is reloaded from the value specified in "
"I<new_value-E<gt>it_interval>.  If I<new_value-E<gt>it_interval> specifies a "
"zero value, then the timer expires just once, at the time specified by "
"I<it_value>."
msgstr ""
"В поле I<new_value-E<gt>it_interval> указывается период таймера в секундах и "
"наносекундах. Если это поле равно нулю, то каждый раз, когда таймер "
"срабатывает, он перезапускается со значением, указанным в I<new_value-"
"E<gt>it_interval>. Если I<new_value-E<gt>it_interval> равно нулю, то таймер "
"срабатывает только один раз, согласно заданному в I<it_value> времени."

#.  By experiment: the overrun count is set correctly, for CLOCK_REALTIME.
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"By default, the initial expiration time specified in I<new_value-"
"E<gt>it_value> is interpreted relative to the current time on the timer's "
"clock at the time of the call.  This can be modified by specifying "
"B<TIMER_ABSTIME> in I<flags>, in which case I<new_value-E<gt>it_value> is "
"interpreted as an absolute value as measured on the timer's clock; that is, "
"the timer will expire when the clock value reaches the value specified by "
"I<new_value-E<gt>it_value>.  If the specified absolute time has already "
"passed, then the timer expires immediately, and the overrun count (see "
"B<timer_getoverrun>(2))  will be set correctly."
msgstr ""
"По умолчанию, начальное время срабатывания, указанное в I<new_value-"
"E<gt>it_value>, считается относительно текущего времени на часах таймера на "
"момент вызова. Это можно изменить, указав B<TIMER_ABSTIME> в I<flags>; в "
"этом случае I<new_value-E<gt>it_value> рассматривается как абсолютное "
"значение по часам таймера; то есть таймер сработает, когда значение часов "
"достигнет значения, указанного в I<new_value-E<gt>it_value>. Если указанное "
"абсолютное время уже прошло, то таймер срабатывает немедленно и счётчик "
"переполнения изменяется соответствующим образом (смотрите "
"B<timer_getoverrun>(2))."

#.  Similar remarks might apply with respect to process and thread CPU time
#.  clocks, but these clocks are not currently (2.6.28) settable on Linux.
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If the value of the B<CLOCK_REALTIME> clock is adjusted while an absolute "
"timer based on that clock is armed, then the expiration of the timer will be "
"appropriately adjusted.  Adjustments to the B<CLOCK_REALTIME> clock have no "
"effect on relative timers based on that clock."
msgstr ""
"Если значение часов B<CLOCK_REALTIME> корректируется (adjusted) и указано "
"абсолютное значение у включённого таймера с этими часами, то срабатывание "
"таймера будет скорректировано соответствующим образом. Корректировка часов "
"B<CLOCK_REALTIME> не влияет на относительные таймеры, использующие эти часы."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If I<old_value> is not NULL, then it points to a buffer that is used to "
"return the previous interval of the timer (in I<old_value-"
"E<gt>it_interval>)  and the amount of time until the timer would previously "
"have next expired (in I<old_value-E<gt>it_value>)."
msgstr ""
"Если значение I<old_value> не равно NULL, то оно указывает на буфер, который "
"используется для возврата предыдущего интервала таймера (в I<old_value-"
"E<gt>it_interval>) и количества времени, которое осталось таймеру до "
"срабатывания (в I<old_value-E<gt>it_value>)."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<timer_gettime>()  returns the time until next expiration, and the "
"interval, for the timer specified by I<timerid>, in the buffer pointed to by "
"I<curr_value>.  The time remaining until the next timer expiration is "
"returned in I<curr_value-E<gt>it_value>; this is always a relative value, "
"regardless of whether the B<TIMER_ABSTIME> flag was used when arming the "
"timer.  If the value returned in I<curr_value-E<gt>it_value> is zero, then "
"the timer is currently disarmed.  The timer interval is returned in "
"I<curr_value-E<gt>it_interval>.  If the value returned in I<curr_value-"
"E<gt>it_interval> is zero, then this is a \"one-shot\" timer."
msgstr ""
"Вызов B<timer_gettime>() возвращает время до следующего срабатывания таймера "
"I<timerid> и интервал в буфер I<curr_value>. Оставшееся время до следующего "
"срабатывания возвращается в I<curr_value-E<gt>it_value>; это всегда "
"относительное значение, независимо от того, указывался ли флаг "
"B<TIMER_ABSTIME> при включении таймера. Если значение I<curr_value-"
"E<gt>it_value> равно нулю, то таймер в данный момент выключен. Интервал "
"таймера возвращается в I<curr_value-E<gt>it_interval>. Если значение "
"I<curr_value-E<gt>it_interval> равно нулю, то это «одноразовый» таймер."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"On success, B<timer_settime>()  and B<timer_gettime>()  return 0.  On error, "
"-1 is returned, and I<errno> is set to indicate the error."
msgstr ""
"При успешном выполнении B<timer_settime>() и B<timer_gettime>() возвращается "
"0. При ошибке возвращается -1, а в I<errno> содержится код ошибки."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ОШИБКИ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "These functions may fail with the following errors:"
msgstr "Эти функции могут завершиться со следующими ошибками:"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "I<new_value>, I<old_value>, or I<curr_value> is not a valid pointer."
msgstr "Некорректный указатель I<new_value>, I<old_value> или I<curr_value>."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#.  FIXME . eventually: invalid value in flags
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "I<timerid> is invalid."
msgstr "Неверное значение I<timerid>."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<timer_settime>()  may fail with the following errors:"
msgstr "B<timer_settime>() может завершиться со следующими ошибками:"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"I<new_value.it_value> is negative; or I<new_value.it_value.tv_nsec> is "
"negative or greater than 999,999,999."
msgstr ""
"Значение I<new_value.it_value> отрицательно; или I<new_value.it_value."
"tv_nsec> отрицательно или больше 999999999."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "ВЕРСИИ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "These system calls are available since Linux 2.6."
msgstr "Данные системные вызовы появились в Linux 2.6."

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "POSIX.1-2001, POSIX.1-2008."
msgstr "POSIX.1-2001, POSIX.1-2008."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "ПРИМЕРЫ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "See B<timer_create>(2)."
msgstr "Смотрите B<timer_create>(2)."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "B<timer_create>(2), B<timer_getoverrun>(2), B<timespec>(3), B<time>(7)"
msgstr "B<timer_create>(2), B<timer_getoverrun>(2), B<timespec>(3), B<time>(7)"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "TIMER_SETTIME"
msgstr "TIMER_SETTIME"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "2020-06-09"
msgstr "9 июня 2020 г."

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Руководство программиста Linux"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid ""
"B<int timer_settime(timer_t >I<timerid>B<, int >I<flags>B<,>\n"
"B<                  const struct itimerspec *>I<new_value>B<,>\n"
"B<                  struct itimerspec *>I<old_value>B<);>\n"
"B<int timer_gettime(timer_t >I<timerid>B<, struct itimerspec *>I<curr_value>B<);>\n"
msgstr ""
"B<int timer_settime(timer_t >I<timerid>B<, int >I<flags>B<,>\n"
"B<                  const struct itimerspec *>I<new_value>B<,>\n"
"B<                  struct itimerspec *>I<old_value>B<);>\n"
"B<int timer_gettime(timer_t >I<timerid>B<, struct itimerspec *>I<curr_value>B<);>\n"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "Link with I<-lrt>."
msgstr "Компонуется при указании параметра I<-lrt>."

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"B<timer_settime>(), B<timer_gettime>(): _POSIX_C_SOURCE\\ E<gt>=\\ 199309L"
msgstr ""
"B<timer_settime>(), B<timer_gettime>(): _POSIX_C_SOURCE\\ E<gt>=\\ 199309L"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"B<timer_settime>()  arms or disarms the timer identified by I<timerid>.  The "
"I<new_value> argument is pointer to an I<itimerspec> structure that "
"specifies the new initial value and the new interval for the timer.  The "
"I<itimerspec> structure is defined as follows:"
msgstr ""
"Вызов B<timer_settime>() запускает или останавливает таймер, указанный в "
"I<timerid>. Аргумент I<new_value> — это указатель на структуру "
"I<itimerspec>, которая определяет новое начальное значение и новый интервал "
"таймера. Структура I<itimerspec> определена следующим образом:"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid ""
"struct timespec {\n"
"    time_t tv_sec;                /* Seconds */\n"
"    long   tv_nsec;               /* Nanoseconds */\n"
"};\n"
msgstr ""
"struct timespec {\n"
"    time_t tv_sec;                /* секунды */\n"
"    long   tv_nsec;               /* наносекунды */\n"
"};\n"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid ""
"struct itimerspec {\n"
"    struct timespec it_interval;  /* Timer interval */\n"
"    struct timespec it_value;     /* Initial expiration */\n"
"};\n"
msgstr ""
"struct itimerspec {\n"
"    struct timespec it_interval;  /* интервал таймера */\n"
"    struct timespec it_value;     /* первое срабатывание */\n"
"};\n"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"Each of the substructures of the I<itimerspec> structure is a I<timespec> "
"structure that allows a time value to be specified in seconds and "
"nanoseconds.  These time values are measured according to the clock that was "
"specified when the timer was created by B<timer_create>(2)."
msgstr ""
"Каждая подструктура структуры I<itimerspec> представляет собой структуру "
"I<timespec>, которая позволяет задавать значение времени в секундах и "
"наносекундах. Эти значения времени отсчитываются по часам, которые были "
"указаны при создании таймера с помощью B<timer_create>(2)."

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "СООТВЕТСТВИЕ СТАНДАРТАМ"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "B<timer_create>(2), B<timer_getoverrun>(2), B<time>(7)"
msgstr "B<timer_create>(2), B<timer_getoverrun>(2), B<time>(7)"

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 5.10. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15 сентября 2017 г."

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "EXAMPLE"
msgstr "ПРИМЕР"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 4.16. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.02"
msgstr "Linux man-pages 6.02"
