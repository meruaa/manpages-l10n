# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2013-2014, 2017.
# Dmitriy S. Seregin <dseregin@59.ru>, 2013.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017, 2019.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-02-20 20:11+0100\n"
"PO-Revision-Date: 2019-10-05 08:17+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "io_cancel"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-10-30"
msgstr "30 октября 2022 г."

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "io_cancel - cancel an outstanding asynchronous I/O operation"
msgstr "io_cancel - отменяет невыполненную асинхронную операцию ввода-вывода"

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Alternatively, Asynchronous I/O library (I<libaio>, I<-laio>); see NOTES."
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "B<#include E<lt>fcntl.hE<gt>           >/* Definition of AT_* constants */\n"
#| "B<#include E<lt>unistd.hE<gt>>\n"
msgid ""
"B<#include E<lt>linux/aio_abi.hE<gt>>    /* Definition of needed types */\n"
"B<#include E<lt>sys/syscall.hE<gt>>      /* Definition of B<SYS_*> constants */\n"
"B<#include E<lt>unistd.hE<gt>>\n"
msgstr ""
"B<#include E<lt>fcntl.hE<gt>           >/* определения констант of AT_* */\n"
"B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int syscall(SYS_io_cancel, aio_context_t >I<ctx_id>B<, struct iocb *>I<iocb>B<,>\n"
"B<            struct io_event *>I<result>B<);>\n"
msgstr ""
"B<int syscall(SYS_io_cancel, aio_context_t >I<ctx_id>B<, struct iocb *>I<iocb>B<,>\n"
"B<            struct io_event *>I<result>B<);>\n"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"I<Note>: this page describes the raw Linux system call interface.  The "
"wrapper function provided by I<libaio> uses a different type for the "
"I<ctx_id> argument.  See NOTES."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The B<io_cancel>()  system call attempts to cancel an asynchronous I/O "
"operation previously submitted with B<io_submit>(2).  The I<iocb> argument "
"describes the operation to be canceled and the I<ctx_id> argument is the AIO "
"context to which the operation was submitted.  If the operation is "
"successfully canceled, the event will be copied into the memory pointed to "
"by I<result> without being placed into the completion queue."
msgstr ""
"Системный вызов B<io_cancel>() пытается отменить асинхронную операцию ввода-"
"вывода, ранее отправленную системным вызовом B<io_submit>(2). В В параметре "
"I<iocb> указывается отменяемая операция, а в параметре I<ctx_id> задаётся "
"идентификатор контекста AIO отправленной операции. Если операция успешно "
"отменена, то событие будет скопировано в память, куда указывает I<result>, "
"без помещения в очередь выполнения."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"On success, B<io_cancel>()  returns 0.  For the failure return, see NOTES."
msgstr ""
"В случае успешного завершения B<io_cancel>() возвращает 0. В случае ошибки "
"смотрите ЗАМЕЧАНИЯ."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ОШИБКИ"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EAGAIN>"
msgstr "B<EAGAIN>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The I<iocb> specified was not canceled."
msgstr "Указанный I<iocb> не был отменен."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "One of the data structures points to invalid data."
msgstr "Одна из структур данных указывает на некорректные данные."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The AIO context specified by I<ctx_id> is invalid."
msgstr "Некорректен контекст AIO, указанный I<ctx_id>."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOSYS>"
msgstr "B<ENOSYS>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<io_cancel>()  is not implemented on this architecture."
msgstr "Вызов B<io_cancel>() не реализован для этой архитектуры."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "ВЕРСИИ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The asynchronous I/O system calls first appeared in Linux 2.5."
msgstr ""
"Асинхронные системные вызовы ввода-вывода впервые появились в Linux 2.5."

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<io_cancel>()  is Linux-specific and should not be used in programs that "
"are intended to be portable."
msgstr ""
"Вызов B<io_cancel>() есть только в Linux, и он не должен использоваться в "
"переносимых программах."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ЗАМЕЧАНИЯ"

#.  http://git.fedorahosted.org/git/?p=libaio.git
#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"You probably want to use the B<io_cancel>()  wrapper function provided by "
"I<libaio>."
msgstr ""

#.  But glibc is confused, since <libaio.h> uses 'io_context_t' to declare
#.  the system call.
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Note that the I<libaio> wrapper function uses a different type "
"(I<io_context_t>)  for the I<ctx_id> argument.  Note also that the I<libaio> "
"wrapper does not follow the usual C library conventions for indicating "
"errors: on error it returns a negated error number (the negative of one of "
"the values listed in ERRORS).  If the system call is invoked via "
"B<syscall>(2), then the return value follows the usual conventions for "
"indicating an error: -1, with I<errno> set to a (positive) value that "
"indicates the error."
msgstr ""
"Заметим, что в обёрточной функции I<libaio> используется другой тип "
"(I<io_context_t>) аргумента I<ctx_id>. Также заметим, что I<libaio> не "
"следует соглашениям обычной библиотеки C для возврата ошибок: при ошибке она "
"возвращает отрицательный номер ошибки (из списка в разделе ОШИБКИ). Если "
"системный вызов вызывается с помощью B<syscall>(2), то возвращаемое значение "
"следует обычным соглашениям для указания на ошибку: возвращается -1 и в "
"I<errno> записывается (положительное) значение возникшей ошибки."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. #-#-#-#-#  archlinux: io_cancel.2.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  debian-bullseye: io_cancel.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .SH AUTHOR
#.  Kent Yoder.
#. type: Plain text
#. #-#-#-#-#  debian-unstable: io_cancel.2.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  fedora-38: io_cancel.2.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: io_cancel.2.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: io_cancel.2.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-15-5: io_cancel.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .SH AUTHOR
#.  Kent Yoder.
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: io_cancel.2.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<io_destroy>(2), B<io_getevents>(2), B<io_setup>(2), B<io_submit>(2), "
"B<aio>(7)"
msgstr ""
"B<io_destroy>(2), B<io_getevents>(2), B<io_setup>(2), B<io_submit>(2), "
"B<aio>(7)"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "IO_CANCEL"
msgstr "IO_CANCEL"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "2020-12-21"
msgstr "21 декабря 2020 г."

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Руководство программиста Linux"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "B<#include E<lt>linux/aio_abi.hE<gt>>          /* Defines needed types */\n"
msgstr "B<#include E<lt>linux/aio_abi.hE<gt>>          /* определяет необходимые типы */\n"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid ""
"B<int io_cancel(aio_context_t >I<ctx_id>B<, struct iocb *>I<iocb>B<,>\n"
"B<              struct io_event *>I<result>B<);>\n"
msgstr ""
"B<int io_cancel(aio_context_t >I<ctx_id>B<, struct iocb *>I<iocb>B<,>\n"
"B<              struct io_event *>I<result>B<);>\n"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "I<Note>: There is no glibc wrapper for this system call; see NOTES."
msgstr ""
"I<Замечание>: В glibc нет обёрточной функции для данного системного вызова; "
"смотрите ЗАМЕЧАНИЯ."

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "СООТВЕТСТВИЕ СТАНДАРТАМ"

#.  http://git.fedorahosted.org/git/?p=libaio.git
#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"Glibc does not provide a wrapper function for this system call.  You could "
"invoke it using B<syscall>(2).  But instead, you probably want to use the "
"B<io_cancel>()  wrapper function provided by I<libaio>."
msgstr ""
"В glibc нет обёрточной функции для данного системного вызова. Вы можете "
"вызвать его с помощью B<syscall>(2). Но лучше воспользоваться обёрточной "
"функцией B<io_cancel>() из библиотеки I<libaio>."

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 5.10. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15 сентября 2017 г."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 4.16. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.02"
msgstr "Linux man-pages 6.02"
