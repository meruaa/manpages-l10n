# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2013-2014, 2017.
# Dmitriy S. Seregin <dseregin@59.ru>, 2013.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017, 2019.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-02-20 20:11+0100\n"
"PO-Revision-Date: 2019-10-05 08:17+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "iopl"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 февраля 2023 г."

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "iopl - change I/O privilege level"
msgstr "iopl - меняет уровень привилегий ввода-вывода"

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>sys/io.hE<gt>>\n"
msgstr "B<#include E<lt>sys/io.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<int iopl(int >I<level>B<);>"
msgid "B<[[deprecated]] int iopl(int >I<level>B<);>\n"
msgstr "B<int iopl(int >I<level>B<);>"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<iopl>()  changes the I/O privilege level of the calling process, as "
#| "specified by the two least significant bits in I<level>."
msgid ""
"B<iopl>()  changes the I/O privilege level of the calling thread, as "
"specified by the two least significant bits in I<level>."
msgstr ""
"B<iopl>() изменяет уровень привилегий ввода/вывода вызывающего процесса, "
"задаваемый двумя младшими битами в значении I<level>."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid "The I/O privilege level for a normal process is 0."
msgid ""
"The I/O privilege level for a normal thread is 0.  Permissions are inherited "
"from parents to children."
msgstr "Уровень привилегий ввода/вывода обычного процесса равен 0."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "This call is mostly for the i386 architecture.  On many other "
#| "architectures it does not exist or will always return an error."
msgid ""
"This call is deprecated, is significantly slower than B<ioperm>(2), and is "
"only provided for older X servers which require access to all 65536 I/O "
"ports.  It is mostly for the i386 architecture.  On many other architectures "
"it does not exist or will always return an error."
msgstr ""
"Данный вызов, в основном, предназначен для архитектуры i386. На большинстве "
"других архитектур он не существует или будет возвращать ошибку."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "On success, B<ioprio_set>()  returns 0.  On error, -1 is returned, and "
#| "I<errno> is set to indicate the error."
msgid ""
"On success, zero is returned.  On error, -1 is returned, and I<errno> is set "
"to indicate the error."
msgstr ""
"При успешном выполнении B<ioprio_set>() возвращается 0. При ошибке "
"возвращается -1, а в I<errno> содержится код ошибки."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ОШИБКИ"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "I<level> is greater than 3."
msgstr "Значение I<level> больше 3."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOSYS>"
msgstr "B<ENOSYS>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "This call is unimplemented."
msgstr "Этот вызов не реализован."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM>"
msgstr "B<EPERM>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The calling process has insufficient privilege to call B<iopl>(); the "
#| "B<CAP_SYS_RAWIO> capability is required to raise the I/O privilege level "
#| "above its current value."
msgid ""
"The calling thread has insufficient privilege to call B<iopl>(); the "
"B<CAP_SYS_RAWIO> capability is required to raise the I/O privilege level "
"above its current value."
msgstr ""
"У вызывающего процесса недостаточно прав вызвать B<iopl>(); для повышения "
"уровня привилегий ввода-вывода выше текущего значения требуется мандат "
"B<CAP_SYS_RAWIO>."

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<iopl>()  is Linux-specific and should not be used in programs that are "
"intended to be portable."
msgstr ""
"Вызов B<iopl>() есть только в Linux, и он не должен использоваться в "
"переносимых программах."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ЗАМЕЧАНИЯ"

#.  Libc5 treats it as a system call and has a prototype in
#.  .IR <unistd.h> .
#.  glibc1 does not have a prototype.
#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, fuzzy
#| msgid ""
#| "Glibc2 has a prototype both in I<E<lt>sys/io.hE<gt>> and in I<E<lt>sys/"
#| "perm.hE<gt>>.  Avoid the latter, it is available on i386 only."
msgid ""
"glibc2 has a prototype both in I<E<lt>sys/io.hE<gt>> and in I<E<lt>sys/perm."
"hE<gt>>.  Avoid the latter, it is available on i386 only."
msgstr ""
"В glibc2 прототип расположен в I<E<lt>sys/io.hE<gt>> и I<E<lt>sys/perm."
"hE<gt>>. Не используйте последний вариант, он существует только для i386."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "In addition to granting unrestricted I/O port access, running at a higher "
#| "I/O privilege level also allows the process to disable interrupts.  This "
#| "will probably crash the system, and is not recommended."
msgid ""
"Prior to Linux 5.5 B<iopl>()  allowed the thread to disable interrupts while "
"running at a higher I/O privilege level.  This will probably crash the "
"system, and is not recommended."
msgstr ""
"В дополнение к неограниченному доступу к портам ввода-вывода работа на "
"высоком уровне привилегий также позволяет процессу отключать прерывания. "
"Скорее всего, это приведет к сбою системы, поэтому использование этой "
"возможности не рекомендуется."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Prior to Linux 3.7, on some architectures (such as i386), permissions "
"I<were> inherited by the child produced by B<fork>(2)  and were preserved "
"across B<execve>(2).  This behavior was inadvertently changed in Linux 3.7, "
"and won't be reinstated."
msgstr ""
"До Linux 3.7 на некоторых архитектурах (например, i386), права "
"I<наследовались> потомком, созданным B<fork>(2), и сохранялись при "
"B<execve>(2). Такое поведение было неумышленно изменено в Linux 3.7, и не "
"будет возвращено назад."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<ioperm>(2), B<outb>(2), B<capabilities>(7)"
msgstr "B<ioperm>(2), B<outb>(2), B<capabilities>(7)"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "IOPL"
msgstr "IOPL"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "2020-08-13"
msgstr "13 августа 2020 г."

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Руководство программиста Linux"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "B<#include E<lt>sys/io.hE<gt>>"
msgstr "B<#include E<lt>sys/io.hE<gt>>"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "B<int iopl(int >I<level>B<);>"
msgstr "B<int iopl(int >I<level>B<);>"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"On success, zero is returned.  On error, -1 is returned, and I<errno> is set "
"appropriately."
msgstr ""
"При успешном выполнении возвращается 0. В случае ошибки возвращается -1, а "
"I<errno> устанавливается в соответствующее значение."

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "СООТВЕТСТВИЕ СТАНДАРТАМ"

#.  Libc5 treats it as a system call and has a prototype in
#.  .IR <unistd.h> .
#.  Glibc1 does not have a prototype.
#. type: Plain text
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Glibc2 has a prototype both in I<E<lt>sys/io.hE<gt>> and in I<E<lt>sys/perm."
"hE<gt>>.  Avoid the latter, it is available on i386 only."
msgstr ""
"В glibc2 прототип расположен в I<E<lt>sys/io.hE<gt>> и I<E<lt>sys/perm."
"hE<gt>>. Не используйте последний вариант, он существует только для i386."

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 5.10. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15 сентября 2017 г."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"B<iopl>()  changes the I/O privilege level of the calling process, as "
"specified by the two least significant bits in I<level>."
msgstr ""
"B<iopl>() изменяет уровень привилегий ввода/вывода вызывающего процесса, "
"задаваемый двумя младшими битами в значении I<level>."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This call is necessary to allow 8514-compatible X servers to run under "
"Linux.  Since these X servers require access to all 65536 I/O ports, the "
"B<ioperm>(2)  call is not sufficient."
msgstr ""
"Этот вызов необходим для того, чтобы 8514-совместимые X-серверы могли "
"работать под управлением Linux. Этим X-серверам необходим доступ ко всем "
"65536-и портам ввода/вывода, вызова B<ioperm>(2) для этого недостаточно."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"In addition to granting unrestricted I/O port access, running at a higher I/"
"O privilege level also allows the process to disable interrupts.  This will "
"probably crash the system, and is not recommended."
msgstr ""
"В дополнение к неограниченному доступу к портам ввода-вывода работа на "
"высоком уровне привилегий также позволяет процессу отключать прерывания. "
"Скорее всего, это приведет к сбою системы, поэтому использование этой "
"возможности не рекомендуется."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"Permissions are not inherited by the child process created by B<fork>(2)  "
"and are not preserved across B<execve>(2)  (but see NOTES)."
msgstr ""
"Права не наследуются дочерним процессом, созданным B<fork>(2) и не "
"сохраняются при вызове B<execve>(2) (но смотрите ЗАМЕЧАНИЯ)."

#. type: Plain text
#: opensuse-leap-15-5
msgid "The I/O privilege level for a normal process is 0."
msgstr "Уровень привилегий ввода/вывода обычного процесса равен 0."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This call is mostly for the i386 architecture.  On many other architectures "
"it does not exist or will always return an error."
msgstr ""
"Данный вызов, в основном, предназначен для архитектуры i386. На большинстве "
"других архитектур он не существует или будет возвращать ошибку."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"The calling process has insufficient privilege to call B<iopl>(); the "
"B<CAP_SYS_RAWIO> capability is required to raise the I/O privilege level "
"above its current value."
msgstr ""
"У вызывающего процесса недостаточно прав вызвать B<iopl>(); для повышения "
"уровня привилегий ввода-вывода выше текущего значения требуется мандат "
"B<CAP_SYS_RAWIO>."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 4.16. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-10-30"
msgstr "30 октября 2022 г."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.02"
msgstr "Linux man-pages 6.02"
