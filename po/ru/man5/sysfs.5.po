# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Alexander Golubev <fatzer2@gmail.com>, 2018.
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2011, 2014-2016.
# Hotellook, 2014.
# Nikita <zxcvbnm3230@mail.ru>, 2014.
# Spiros Georgaras <sng@hellug.gr>, 2016.
# Vladislav <ivladislavefimov@gmail.com>, 2015.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-02-20 20:33+0100\n"
"PO-Revision-Date: 2019-10-15 18:55+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "sysfs"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-10-30"
msgstr "30 октября 2022 г."

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "sysfs - a filesystem for exporting kernel objects"
msgstr "sysfs - файловая система для экспортируемых объектов ядра"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The B<sysfs> filesystem is a pseudo-filesystem which provides an interface "
"to kernel data structures.  (More precisely, the files and directories in "
"B<sysfs> provide a view of the I<kobject> structures defined internally "
"within the kernel.)  The files under B<sysfs> provide information about "
"devices, kernel modules, filesystems, and other kernel components."
msgstr ""
"Файловая система B<sysfs> — это псевдофайловая система, предоставляющая "
"интерфейс к структурам данных ядра (точнее, файлы и каталоги в B<sysfs> "
"позволяют видеть структуры I<kobject>, определённые внутри ядра). Файлы в "
"B<sysfs> показывают информацию об устройствах, модулях ядра, файловых "
"системах и других компонентах ядра."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The B<sysfs> filesystem is commonly mounted at I</sys>.  Typically, it is "
"mounted automatically by the system, but it can also be mounted manually "
"using a command such as:"
msgstr ""
"Обычно, файловая система B<sysfs> монтируется в I</sys>. Это выполняется "
"системой автоматически, но также можно монтировать её вручную с помощью "
"команды:"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "mount -t sysfs sysfs /sys\n"
msgstr "mount -t sysfs sysfs /sys\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Many of the files in the B<sysfs> filesystem are read-only, but some files "
"are writable, allowing kernel variables to be changed.  To avoid redundancy, "
"symbolic links are heavily used to connect entries across the filesystem "
"tree."
msgstr ""
"Многие из файлов файловой системы B<sysfs> доступны только для чтения, но "
"есть и доступные на запись, через которые можно изменять переменные ядра. В "
"целях сокращения избыточности для связи элементов в дереве файловой системы "
"широко применяются символьные ссылки."

#. type: SS
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Files and directories"
msgstr "Файлы и каталоги"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The following list describes some of the files and directories under the I</"
"sys> hierarchy."
msgstr ""
"В следующем списке описаны некоторые файлы и каталоги в иерархии I</sys>."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I</sys/block>"
msgstr "I</sys/block>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"This subdirectory contains one symbolic link for each block device that has "
"been discovered on the system.  The symbolic links point to corresponding "
"directories under I</sys/devices>."
msgstr ""
"В этом подкаталоге содержится по одной символьной ссылке на каждое блочное "
"устройство, обнаруженное в системе. Символьные ссылки указывают на "
"соответствующие каталоги в I</sys/devices>."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I</sys/bus>"
msgstr "I</sys/bus>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"This directory contains one subdirectory for each of the bus types in the "
"kernel.  Inside each of these directories are two subdirectories:"
msgstr ""
"В этом каталоге содержится по одному подкаталогу на каждый тип шины ядра. "
"Внутри каждого каталога есть два подкаталога:"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I<devices>"
msgstr "I<devices>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"This subdirectory contains symbolic links to entries in I</sys/devices> that "
"correspond to the devices discovered on this bus."
msgstr ""
"Этот подкаталог содержит символьные ссылки на элементы в I</sys/devices>, "
"которые соответствуют устройствам, обнаруженным на этой шине."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I<drivers>"
msgstr "I<drivers>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"This subdirectory contains one subdirectory for each device driver that is "
"loaded on this bus."
msgstr ""
"Этот подкаталог содержит по одному подкаталогу на каждый драйвер устройства, "
"загруженный для этой шины."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I</sys/class>"
msgstr "I</sys/class>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"This subdirectory contains a single layer of further subdirectories for each "
"of the device classes that have been registered on the system (e.g., "
"terminals, network devices, block devices, graphics devices, sound devices, "
"and so on).  Inside each of these subdirectories are symbolic links for each "
"of the devices in this class.  These symbolic links refer to entries in the "
"I</sys/devices> directory."
msgstr ""
"Этот подкаталог содержит по одному слою дополнительных подкаталогов на "
"каждый класс устройств, зарегистрированных в системе (например, терминалы, "
"сетевые устройства, блочные устройства, графические устройства, звуковые "
"устройства и т. д.). Внутри каждого из этих подкаталогов находятся "
"символьные ссылки на каждое из устройств этого класса. Данные символьные "
"ссылки указывают на элементы в каталоге I</sys/devices>."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I</sys/class/net>"
msgstr "I</sys/class/net>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Each of the entries in this directory is a symbolic link representing one of "
"the real or virtual networking devices that are visible in the network "
"namespace of the process that is accessing the directory.  Each of these "
"symbolic links refers to entries in the I</sys/devices> directory."
msgstr ""
"Каждый элемент этого каталога представляет собой символьную ссылку на одно "
"из  реальных или виртуальных сетевых устройств, видимых в сетевом "
"пространстве имён процесса, обратившегося к каталогу. Эти символьные ссылки "
"указывают на элементы в каталоге I</sys/devices>."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I</sys/dev>"
msgstr "I</sys/dev>"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"This directory contains two subdirectories I<block/> and I<char/>, "
"corresponding, respectively, to the block and character devices on the "
"system.  Inside each of these subdirectories are symbolic links with names "
"of the form I<major-ID>:I<minor-ID>, where the ID values correspond to the "
"major and minor ID of a specific device.  Each symbolic link points to the "
"B<sysfs> directory for a device.  The symbolic links inside I</sys/dev> thus "
"provide an easy way to look up the B<sysfs> interface using the device IDs "
"returned by a call to B<stat>(2)  (or similar)."
msgstr ""
"Этот каталог содержит два подкаталога — I<block/> и I<char/>, "
"соответствующие блочным и символьным устройствам в системе. Внутри каждого "
"подкаталога находятся символьные ссылки с именами в виде I<основной-ID>:"
"I<дополнительный-ID>, где значения ID соответствуют основному и "
"дополнительному идентификаторами определённого устройства. Каждая символьная "
"ссылка указывает на каталог B<sysfs> для устройства. Таким образом, "
"символьные ссылки в I</sys/dev> предоставляют простой способ просмотра "
"интерфейса B<sysfs> по идентификаторам устройств, возвращаемых вызовом "
"B<stat>(2) (или подобным)."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The following shell session shows an example from I</sys/dev>:"
msgstr "Пример сеанса оболочки с выводом I</sys/dev>:"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"$ B<stat -c \"%t %T\" /dev/null>\n"
"1 3\n"
"$ B<readlink /sys/dev/char/1\\e:3>\n"
"\\&../../devices/virtual/mem/null\n"
"$ B<ls -Fd /sys/devices/virtual/mem/null>\n"
"/sys/devices/virtual/mem/null/\n"
"$ B<ls -d1 /sys/devices/virtual/mem/null/*>\n"
"/sys/devices/virtual/mem/null/dev\n"
"/sys/devices/virtual/mem/null/power/\n"
"/sys/devices/virtual/mem/null/subsystem@\n"
"/sys/devices/virtual/mem/null/uevent\n"
msgstr ""
"$ B<stat -c \"%t %T\" /dev/null>\n"
"1 3\n"
"$ B<readlink /sys/dev/char/1\\e:3>\n"
"\\&../../devices/virtual/mem/null\n"
"$ B<ls -Fd /sys/devices/virtual/mem/null>\n"
"/sys/devices/virtual/mem/null/\n"
"$ B<ls -d1 /sys/devices/virtual/mem/null/*>\n"
"/sys/devices/virtual/mem/null/dev\n"
"/sys/devices/virtual/mem/null/power/\n"
"/sys/devices/virtual/mem/null/subsystem@\n"
"/sys/devices/virtual/mem/null/uevent\n"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I</sys/devices>"
msgstr "I</sys/devices>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"This is a directory that contains a filesystem representation of the kernel "
"device tree, which is a hierarchy of I<device> structures within the kernel."
msgstr ""
"В данном каталоге содержится представление в виде файловой системы для "
"дерева устройств ядра, которое является иерархией структур I<device> внутри "
"ядра."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I</sys/firmware>"
msgstr "I</sys/firmware>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"This subdirectory contains interfaces for viewing and manipulating firmware-"
"specific objects and attributes."
msgstr ""
"В этом подкаталоге содержатся интерфейсы для просмотра и изменения объектов "
"и атрибутов микропрограмм."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I</sys/fs>"
msgstr "I</sys/fs>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"This directory contains subdirectories for some filesystems.  A filesystem "
"will have a subdirectory here only if it chose to explicitly create the "
"subdirectory."
msgstr ""
"В этом каталоге содержатся подкаталоги для некоторых файловых систем. Здесь "
"файловая система будет иметь подкаталог только, если она явно создаст "
"подкаталог."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I</sys/fs/cgroup>"
msgstr "I</sys/fs/cgroup>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"This directory conventionally is used as a mount point for a B<tmpfs>(5)  "
"filesystem containing mount points for B<cgroups>(7)  filesystems."
msgstr ""
"Обычно, этот каталог используется как точка монтирования файловой системы "
"B<tmpfs>(5), содержащей точки монтирования для файловых систем B<cgroups>(7)."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I</sys/fs/smackfs>"
msgstr "I</sys/fs/smackfs>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The directory contains configuration files for the SMACK LSM.  See the "
"kernel source file I<Documentation/admin-guide/LSM/Smack.rst>."
msgstr ""
"В каталоге содержатся файлы настройки SMACK LSM. Смотрите файл исходного "
"кода ядра I<Documentation/admin-guide/LSM/Smack.rst>."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I</sys/hypervisor>"
msgstr "I</sys/hypervisor>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "[To be documented]"
msgstr "[Будет описано]"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I</sys/kernel>"
msgstr "I</sys/kernel>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"This subdirectory contains various files and subdirectories that provide "
"information about the running kernel."
msgstr ""
"В этом подкаталоге содержатся различные файлы и подкаталоги, предоставляющие "
"информацию о работающем ядре."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I</sys/kernel/cgroup/>"
msgstr "I</sys/kernel/cgroup/>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "For information about the files in this directory, see B<cgroups>(7)."
msgstr "Файлы этого каталога описаны в B<cgroups>(7)."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I</sys/kernel/debug/tracing>"
msgstr "I</sys/kernel/debug/tracing>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Mount point for the I<tracefs> filesystem used by the kernel's I<ftrace> "
"facility.  (For information on I<ftrace>, see the kernel source file "
"I<Documentation/trace/ftrace.txt>.)"
msgstr ""
"Точка монтирования файловой системы I<tracefs>, используемая свойством ядра "
"I<ftrace> (информацию по I<ftrace> смотрите в файле исходного кода ядра "
"I<Documentation/trace/ftrace.txt>)."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I</sys/kernel/mm>"
msgstr "I</sys/kernel/mm>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"This subdirectory contains various files and subdirectories that provide "
"information about the kernel's memory management subsystem."
msgstr ""
"В этом подкаталоге содержатся различные файлы и подкаталоги, предоставляющие "
"информацию о подсистеме ядра управления памятью."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I</sys/kernel/mm/hugepages>"
msgstr "I</sys/kernel/mm/hugepages>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"This subdirectory contains one subdirectory for each of the huge page sizes "
"that the system supports.  The subdirectory name indicates the huge page "
"size (e.g., I<hugepages-2048kB>).  Within each of these subdirectories is a "
"set of files that can be used to view and (in some cases) change settings "
"associated with that huge page size.  For further information, see the "
"kernel source file I<Documentation/admin-guide/mm/hugetlbpage.rst>."
msgstr ""
"Этот подкаталог содержит по одному подкаталогу на каждый размер огромных "
"страниц ядра, поддерживаемых системой. В имени подкаталога показан размер "
"огромной страницы (например, I<hugepages-2048kB>). Внутри каждого "
"подкаталога содержится набор файлов, которые могут быть использованы для "
"просмотра и (в некоторых случаях) изменения настроек, относящихся к размеру "
"огромных страниц. Дополнительную информацию смотрите в файле исходного кода "
"ядра I<Documentation/admin-guide/mm/hugetlbpage.rst>."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I</sys/module>"
msgstr "I</sys/module>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"This subdirectory contains one subdirectory for each module that is loaded "
"into the kernel.  The name of each directory is the name of the module.  In "
"each of the subdirectories, there may be following files:"
msgstr ""
"Этот подкаталог содержит по одному подкаталогу на каждый модуль, загруженный "
"в ядро. Подкаталоги называются по имени модулей. В каждом подкаталоге могут "
"быть следующие файлы:"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I<coresize>"
msgstr "I<coresize>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "[to be documented]"
msgstr "[Будет описано]"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I<initsize>"
msgstr "I<initsize>"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I<initstate>"
msgstr "I<initstate>"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I<refcnt>"
msgstr "I<refcnt>"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I<srcversion>"
msgstr "I<srcversion>"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I<taint>"
msgstr "I<taint>"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I<uevent>"
msgstr "I<uevent>"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I<version>"
msgstr "I<version>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "In each of the subdirectories, there may be following subdirectories:"
msgstr "В каждом подкаталоге могут быть следующие подкаталоги:"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I<holders>"
msgstr "I<holders>"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I<notes>"
msgstr "I<notes>"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I<parameters>"
msgstr "I<parameters>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"This directory contains one file for each module parameter, with each file "
"containing the value of the corresponding parameter.  Some of these files "
"are writable, allowing the"
msgstr ""
"Этот каталог содержит по одному файлу на каждый параметр модуля; в каждом "
"файле хранится значение соответствующего параметра. Некоторые файлы доступны "
"на запись."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I<sections>"
msgstr "I<sections>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"This subdirectories contains files with information about module sections.  "
"This information is mainly used for debugging."
msgstr ""
"В этих подкаталогах содержатся файла с информацией о разделах модуля. Эта "
"информацию, в основном, используется для отладки."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I<[To be documented]>"
msgstr "I<[Будет описано]>"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I</sys/power>"
msgstr "I</sys/power>"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "ВЕРСИИ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The B<sysfs> filesystem first appeared in Linux 2.6.0."
msgstr "Файловая система B<sysfs> впервые появилась в Linux 2.6.0."

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The B<sysfs> filesystem is Linux-specific."
msgstr "Файловая система B<sysfs> есть только в Linux."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"This manual page is incomplete, possibly inaccurate, and is the kind of "
"thing that needs to be updated very often."
msgstr ""
"Данная справочная страница неполна, в ней могут быть неточности и является "
"одной из страниц, которую требуется обновлять очень часто."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<proc>(5), B<udev>(7)"
msgstr "B<proc>(5), B<udev>(7)"

#.  https://www.kernel.org/pub/linux/kernel/people/mochel/doc/papers/ols-2005/mochel.pdf
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"P.\\& Mochel. (2005).  I<The sysfs filesystem>.  Proceedings of the 2005 "
"Ottawa Linux Symposium."
msgstr ""
"P.\\& Mochel. (2005).  I<Файловая система sysfs>. Представлен на симпозиуме "
"Linux в Оттаве в 2005 году."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The kernel source file I<Documentation/filesystems/sysfs.txt> and various "
"other files in I<Documentation/ABI> and I<Documentation/*/sysfs.txt>"
msgstr ""
"Файл исходного кода ядра I<Documentation/filesystems/sysfs.txt> и другие "
"различные файлы в I<Documentation/ABI> и I<Documentation/*/sysfs.txt>"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "SYSFS"
msgstr "SYSFS"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "2018-04-30"
msgstr "30 апреля 2018 г."

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Руководство программиста Linux"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"This directory contains two subdirectories I<block>/ and I<char/>, "
"corresponding, respectively, to the block and character devices on the "
"system.  Inside each of these subdirectories are symbolic links with names "
"of the form I<major-ID>:I<minor-ID>, where the ID values correspond to the "
"major and minor ID of a specific device.  Each symbolic link points to the "
"B<sysfs> directory for a device.  The symbolic links inside I</sys/dev> thus "
"provide an easy way to look up the B<sysfs> interface using the device IDs "
"returned by a call to B<stat>(2)  (or similar)."
msgstr ""
"Этот каталог содержит два подкаталога — I<block>/ и I<char/>, "
"соответствующие блочным и символьным устройствам в системе. Внутри каждого "
"подкаталога находятся символьные ссылки с именами в виде I<основной-ID>:"
"I<дополнительный-ID>, где значения ID соответствуют основному и "
"дополнительному идентификаторами определённого устройства. Каждая символьная "
"ссылка указывает на каталог B<sysfs> для устройства. Таким образом, "
"символьные ссылки в I</sys/dev> предоставляют простой способ просмотра "
"интерфейса B<sysfs> по идентификаторам устройств, возвращаемых вызовом "
"B<stat>(2) (или подобным)."

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "СООТВЕТСТВИЕ СТАНДАРТАМ"

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 5.10. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This subdirectory contains one subdirectory for each of the huge page sizes "
"that the system supports.  The subdirectory name indicates the huge page "
"size (e.g., I<hugepages-2048kB>).  Within each of these subdirectories is a "
"set of files that can be used to view and (in some cases) change settings "
"associated with that huge page size.  For further information, see the "
"kernel source file I<Documentation/vm/hugetlbpage.txt>."
msgstr ""
"Этот подкаталог содержит по одному подкаталогу на каждый размер огромных "
"страниц ядра, поддерживаемых системой. В имени подкаталога показан размер "
"огромной страницы (например, I<hugepages-2048kB>). Внутри каждого "
"подкаталога содержится набор файлов, которые могут быть использованы для "
"просмотра и (в некоторых случаях) изменения настроек, относящихся к размеру "
"огромных страниц. Дополнительную информацию смотрите в файле исходного кода "
"ядра I<Documentation/vm/hugetlbpage.txt>."

#.  https://www.kernel.org/pub/linux/kernel/people/mochel/doc/papers/ols-2005/mochel.pdf
#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"P. Mochel. (2005).  I<The sysfs filesystem>.  Proceedings of the 2005 Ottawa "
"Linux Symposium."
msgstr ""
"P. Mochel. (2005).  I<Файловая система sysfs>. Представлен на симпозиуме "
"Linux в Оттаве в 2005 году."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 4.16. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.02"
msgstr "Linux man-pages 6.02"
