# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2016.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2015, 2017-2019.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-02-20 20:41+0100\n"
"PO-Revision-Date: 2019-10-15 19:01+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<wcsnlen>()"
msgid "wcsnlen"
msgstr "B<wcsnlen>()"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 февраля 2023 г."

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "wcsnlen - determine the length of a fixed-size wide-character string"
msgstr "wcsnlen - определяет длину широкосимвольной строки постоянного размера"

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>wchar.hE<gt>>\n"
msgstr "B<#include E<lt>wchar.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<size_t wcsnlen(const wchar_t *>I<s>B<, size_t >I<maxlen>B<);>\n"
msgid "B<size_t wcsnlen(const wchar_t >I<s>B<[.>I<maxlen>B<], size_t >I<maxlen>B<);>\n"
msgstr "B<size_t wcsnlen(const wchar_t *>I<s>B<, size_t >I<maxlen>B<);>\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Требования макроса тестирования свойств для glibc (см. "
"B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<wcsnlen>():"
msgstr "B<wcsnlen>():"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "    Since glibc 2.19:\n"
#| "        _DEFAULT_SOURCE\n"
#| "    In glibc up to and including 2.19:\n"
#| "        _BSD_SOURCE\n"
msgid ""
"    Since glibc 2.10:\n"
"        _POSIX_C_SOURCE E<gt>= 200809L\n"
"    Before glibc 2.10:\n"
"        _GNU_SOURCE\n"
msgstr ""
"    начиная с glibc 2.19:\n"
"        _DEFAULT_SOURCE\n"
"    в glibc до версии 2.19 включительно:\n"
"        _BSD_SOURCE\n"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, fuzzy
#| msgid ""
#| "The B<wcsnlen>()  function is the wide-character equivalent of the "
#| "B<strnlen>(3)  function.  It returns the number of wide-characters in the "
#| "string pointed to by I<s>, not including the terminating null wide "
#| "character (L\\(aq\\e0\\(aq), but at most I<maxlen> wide characters (note: "
#| "this parameter is not a byte count).  In doing this, B<wcsnlen>()  looks "
#| "at only the first I<maxlen> wide characters at I<s> and never beyond "
#| "I<s+maxlen>."
msgid ""
"The B<wcsnlen>()  function is the wide-character equivalent of the "
"B<strnlen>(3)  function.  It returns the number of wide-characters in the "
"string pointed to by I<s>, not including the terminating null wide character "
"(L\\[aq]\\e0\\[aq]), but at most I<maxlen> wide characters (note: this "
"parameter is not a byte count).  In doing this, B<wcsnlen>()  looks at only "
"the first I<maxlen> wide characters at I<s> and never beyond I<s[maxlen-1]>."
msgstr ""
"Функция B<wcsnlen>() — это эквивалент B<strnlen>(3), но работает с широкими "
"символами. Она возвращает количество широких символов в строке, указанной в "
"I<s>, не считая завершающего широкого символа  null (L\\(aq\\e0\\(aq) и не "
"более чем I<maxlen> широких символов (примечание: данный параметр не "
"количество байт). Для этого B<wcsnlen>() просматривает только первые "
"I<maxlen> широких символов начиная с I<s> и никогда не учитывает символы, "
"расположенные за I<s+maxlen>."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The B<wcsnlen>()  function returns I<wcslen(s)>, if that is less than "
"I<maxlen>, or I<maxlen> if there is no null wide character among the first "
"I<maxlen> wide characters pointed to by I<s>."
msgstr ""
"Функция B<wcsnlen>() возвращает I<wcslen(s)>, если оно меньше I<maxlen>, или "
"I<maxlen>, если среди первых I<maxlen> широких символов строки, на которую "
"указывает I<s>, не встретился широкий символ null."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "ВЕРСИИ"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy
#| msgid "The B<wcsnlen>()  function is provided in glibc since version 2.1."
msgid "The B<wcsnlen>()  function is provided since glibc 2.1."
msgstr "Функция B<wcsnlen>() определена в glibc начиная с версии 2.1."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "АТРИБУТЫ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr "Описание терминов данного раздела смотрите в B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Интерфейс"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Атрибут"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Значение"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<wcsnlen>()"
msgstr "B<wcsnlen>()"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Безвредность в нитях"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<strnlen>(3), B<wcslen>(3)"
msgstr "B<strnlen>(3), B<wcslen>(3)"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "WCSNLEN"
msgstr "WCSNLEN"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "2019-03-06"
msgstr "6 марта 2019 г."

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "GNU"
msgstr "GNU"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Руководство программиста Linux"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "B<size_t wcsnlen(const wchar_t *>I<s>B<, size_t >I<maxlen>B<);>\n"
msgstr "B<size_t wcsnlen(const wchar_t *>I<s>B<, size_t >I<maxlen>B<);>\n"

#. type: TP
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Since glibc 2.10:"
msgstr "Начиная с glibc 2.10:"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "_POSIX_C_SOURCE\\ E<gt>=\\ 200809L"
msgstr "_POSIX_C_SOURCE\\ E<gt>=\\ 200809L"

#. type: TP
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Before glibc 2.10:"
msgstr "До glibc 2.10:"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "_GNU_SOURCE"
msgstr "_GNU_SOURCE"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"The B<wcsnlen>()  function is the wide-character equivalent of the "
"B<strnlen>(3)  function.  It returns the number of wide-characters in the "
"string pointed to by I<s>, not including the terminating null wide character "
"(L\\(aq\\e0\\(aq), but at most I<maxlen> wide characters (note: this "
"parameter is not a byte count).  In doing this, B<wcsnlen>()  looks at only "
"the first I<maxlen> wide characters at I<s> and never beyond I<s+maxlen>."
msgstr ""
"Функция B<wcsnlen>() — это эквивалент B<strnlen>(3), но работает с широкими "
"символами. Она возвращает количество широких символов в строке, указанной в "
"I<s>, не считая завершающего широкого символа  null (L\\(aq\\e0\\(aq) и не "
"более чем I<maxlen> широких символов (примечание: данный параметр не "
"количество байт). Для этого B<wcsnlen>() просматривает только первые "
"I<maxlen> широких символов начиная с I<s> и никогда не учитывает символы, "
"расположенные за I<s+maxlen>."

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "The B<wcsnlen>()  function is provided in glibc since version 2.1."
msgstr "Функция B<wcsnlen>() определена в glibc начиная с версии 2.1."

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "СООТВЕТСТВИЕ СТАНДАРТАМ"

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 5.10. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2016-03-15"
msgstr "15 марта 2016 г."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 4.16. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-12-15"
msgstr "15 декабря 2022 г."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.02"
msgstr "Linux man-pages 6.02"

#. type: Plain text
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The B<wcsnlen>()  function is the wide-character equivalent of the "
#| "B<strnlen>(3)  function.  It returns the number of wide-characters in the "
#| "string pointed to by I<s>, not including the terminating null wide "
#| "character (L\\(aq\\e0\\(aq), but at most I<maxlen> wide characters (note: "
#| "this parameter is not a byte count).  In doing this, B<wcsnlen>()  looks "
#| "at only the first I<maxlen> wide characters at I<s> and never beyond "
#| "I<s+maxlen>."
msgid ""
"The B<wcsnlen>()  function is the wide-character equivalent of the "
"B<strnlen>(3)  function.  It returns the number of wide-characters in the "
"string pointed to by I<s>, not including the terminating null wide character "
"(L\\(aq\\e0\\(aq), but at most I<maxlen> wide characters (note: this "
"parameter is not a byte count).  In doing this, B<wcsnlen>()  looks at only "
"the first I<maxlen> wide characters at I<s> and never beyond I<s[maxlen-1]>."
msgstr ""
"Функция B<wcsnlen>() — это эквивалент B<strnlen>(3), но работает с широкими "
"символами. Она возвращает количество широких символов в строке, указанной в "
"I<s>, не считая завершающего широкого символа  null (L\\(aq\\e0\\(aq) и не "
"более чем I<maxlen> широких символов (примечание: данный параметр не "
"количество байт). Для этого B<wcsnlen>() просматривает только первые "
"I<maxlen> широких символов начиная с I<s> и никогда не учитывает символы, "
"расположенные за I<s+maxlen>."
