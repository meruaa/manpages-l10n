# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Alexander Golubev <fatzer2@gmail.com>, 2018.
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2011, 2014-2016.
# Hotellook, 2014.
# Nikita <zxcvbnm3230@mail.ru>, 2014.
# Spiros Georgaras <sng@hellug.gr>, 2016.
# Vladislav <ivladislavefimov@gmail.com>, 2015.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-02-20 20:31+0100\n"
"PO-Revision-Date: 2019-10-15 18:55+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "SLIST"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-10-30"
msgstr "30 октября 2022 г."

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. SLIST_FOREACH_FROM,
#. SLIST_FOREACH_FROM_SAFE,
#. SLIST_FOREACH_SAFE,
#. SLIST_REMOVE_AFTER,
#. SLIST_SWAP
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"SLIST_EMPTY, SLIST_ENTRY, SLIST_FIRST, SLIST_FOREACH, SLIST_HEAD, "
"SLIST_HEAD_INITIALIZER, SLIST_INIT, SLIST_INSERT_AFTER, SLIST_INSERT_HEAD, "
"SLIST_NEXT, SLIST_REMOVE, SLIST_REMOVE_HEAD - implementation of a singly "
"linked list"
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<#include E<lt>mqueue.hE<gt>>\n"
msgid "B<#include E<lt>sys/queue.hE<gt>>\n"
msgstr "B<#include E<lt>mqueue.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<SLIST_ENTRY(TYPE);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"B<SLIST_HEAD(HEADNAME, TYPE);>\n"
"B<SLIST_HEAD SLIST_HEAD_INITIALIZER(SLIST_HEAD >I<head>B<);>\n"
"B<void SLIST_INIT(SLIST_HEAD *>I<head>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<int SLIST_EMPTY(SLIST_HEAD *>I<head>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"B<void SLIST_INSERT_HEAD(SLIST_HEAD *>I<head>B<,>\n"
"B<                        struct TYPE *>I<elm>B<, SLIST_ENTRY >I<NAME>B<);>\n"
"B<void SLIST_INSERT_AFTER(struct TYPE *>I<listelm>B<,>\n"
"B<                        struct TYPE *>I<elm>B<, SLIST_ENTRY >I<NAME>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"B<struct TYPE *SLIST_FIRST(SLIST_HEAD *>I<head>B<);>\n"
"B<struct TYPE *SLIST_NEXT(struct TYPE *>I<elm>B<, SLIST_ENTRY >I<NAME>B<);>\n"
msgstr ""

#. #-#-#-#-#  archlinux: slist.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .BI "SLIST_FOREACH_FROM(struct TYPE *" var ", SLIST_HEAD *" head ,
#.  .BI "                        SLIST_ENTRY " NAME );
#.  .PP
#.  .BI "SLIST_FOREACH_SAFE(struct TYPE *" var ", SLIST_HEAD *" head ,
#.  .BI "                        SLIST_ENTRY " NAME ", struct TYPE *" temp_var );
#.  .BI "SLIST_FOREACH_FROM_SAFE(struct TYPE *" var ", SLIST_HEAD *" head ,
#.  .BI "                        SLIST_ENTRY " NAME ", struct TYPE *" temp_var );
#. type: Plain text
#. #-#-#-#-#  debian-bullseye: slist.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. .PP
#. .BI "SLIST_FOREACH_FROM(struct TYPE *" var ", SLIST_HEAD *" head ", SLIST_ENTRY " NAME ");"
#. .PP
#. .BI "SLIST_FOREACH_FROM_SAFE(struct TYPE *" var ", SLIST_HEAD *" head ", SLIST_ENTRY " NAME ", struct TYPE *" temp_var ");"
#. .PP
#. .BI "SLIST_FOREACH_SAFE(struct TYPE *" var ", SLIST_HEAD *" head ", SLIST_ENTRY " NAME ", struct TYPE *" temp_var ");"
#. type: Plain text
#. #-#-#-#-#  debian-unstable: slist.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .BI "SLIST_FOREACH_FROM(struct TYPE *" var ", SLIST_HEAD *" head ,
#.  .BI "                        SLIST_ENTRY " NAME );
#.  .PP
#.  .BI "SLIST_FOREACH_SAFE(struct TYPE *" var ", SLIST_HEAD *" head ,
#.  .BI "                        SLIST_ENTRY " NAME ", struct TYPE *" temp_var );
#.  .BI "SLIST_FOREACH_FROM_SAFE(struct TYPE *" var ", SLIST_HEAD *" head ,
#.  .BI "                        SLIST_ENTRY " NAME ", struct TYPE *" temp_var );
#. type: Plain text
#. #-#-#-#-#  fedora-38: slist.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .BI "SLIST_FOREACH_FROM(struct TYPE *" var ", SLIST_HEAD *" head ,
#.  .BI "                        SLIST_ENTRY " NAME );
#.  .PP
#.  .BI "SLIST_FOREACH_SAFE(struct TYPE *" var ", SLIST_HEAD *" head ,
#.  .BI "                        SLIST_ENTRY " NAME ", struct TYPE *" temp_var );
#.  .BI "SLIST_FOREACH_FROM_SAFE(struct TYPE *" var ", SLIST_HEAD *" head ,
#.  .BI "                        SLIST_ENTRY " NAME ", struct TYPE *" temp_var );
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: slist.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .BI "SLIST_FOREACH_FROM(struct TYPE *" var ", SLIST_HEAD *" head ,
#.  .BI "                        SLIST_ENTRY " NAME );
#.  .PP
#.  .BI "SLIST_FOREACH_SAFE(struct TYPE *" var ", SLIST_HEAD *" head ,
#.  .BI "                        SLIST_ENTRY " NAME ", struct TYPE *" temp_var );
#.  .BI "SLIST_FOREACH_FROM_SAFE(struct TYPE *" var ", SLIST_HEAD *" head ,
#.  .BI "                        SLIST_ENTRY " NAME ", struct TYPE *" temp_var );
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: slist.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .BI "SLIST_FOREACH_FROM(struct TYPE *" var ", SLIST_HEAD *" head ,
#.  .BI "                        SLIST_ENTRY " NAME );
#.  .PP
#.  .BI "SLIST_FOREACH_SAFE(struct TYPE *" var ", SLIST_HEAD *" head ,
#.  .BI "                        SLIST_ENTRY " NAME ", struct TYPE *" temp_var );
#.  .BI "SLIST_FOREACH_FROM_SAFE(struct TYPE *" var ", SLIST_HEAD *" head ,
#.  .BI "                        SLIST_ENTRY " NAME ", struct TYPE *" temp_var );
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: slist.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .BI "SLIST_FOREACH_FROM(struct TYPE *" var ", SLIST_HEAD *" head ,
#.  .BI "                        SLIST_ENTRY " NAME );
#.  .PP
#.  .BI "SLIST_FOREACH_SAFE(struct TYPE *" var ", SLIST_HEAD *" head ,
#.  .BI "                        SLIST_ENTRY " NAME ", struct TYPE *" temp_var );
#.  .BI "SLIST_FOREACH_FROM_SAFE(struct TYPE *" var ", SLIST_HEAD *" head ,
#.  .BI "                        SLIST_ENTRY " NAME ", struct TYPE *" temp_var );
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<SLIST_FOREACH(struct TYPE *>I<var>B<, SLIST_HEAD *>I<head>B<, SLIST_ENTRY >I<NAME>B<);>\n"
msgstr ""

#.  .BI "void SLIST_REMOVE_AFTER(struct TYPE *" elm ,
#.  .BI "                        SLIST_ENTRY " NAME );
#.  .PP
#.  .BI "void SLIST_SWAP(SLIST_HEAD *" head1 ", SLIST_HEAD *" head2 ,
#.  .BI "                        SLIST_ENTRY " NAME );
#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"B<void SLIST_REMOVE(SLIST_HEAD *>I<head>B<, struct TYPE *>I<elm>B<,>\n"
"B<                        SLIST_ENTRY >I<NAME>B<);>\n"
"B<void SLIST_REMOVE_HEAD(SLIST_HEAD *>I<head>B<,>\n"
"B<                        SLIST_ENTRY >I<NAME>B<);>\n"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "These macros define and operate on doubly linked lists."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"In the macro definitions, I<TYPE> is the name of a user-defined structure, "
"that must contain a field of type I<SLIST_ENTRY>, named I<NAME>.  The "
"argument I<HEADNAME> is the name of a user-defined structure that must be "
"declared using the macro B<SLIST_HEAD>()."
msgstr ""

#. type: SS
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "action"
msgid "Creation"
msgstr "действие"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"A singly linked list is headed by a structure defined by the "
"B<SLIST_HEAD>()  macro.  This structure contains a single pointer to the "
"first element on the list.  The elements are singly linked for minimum space "
"and pointer manipulation overhead at the expense of O(n) removal for "
"arbitrary elements.  New elements can be added to the list after an existing "
"element or at the head of the list.  An I<SLIST_HEAD> structure is declared "
"as follows:"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "SLIST_HEAD(HEADNAME, TYPE) head;\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"where I<struct HEADNAME> is the structure to be defined, and I<struct TYPE> "
"is the type of the elements to be linked into the list.  A pointer to the "
"head of the list can later be declared as:"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "struct HEADNAME *headp;\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "(The names I<head> and I<headp> are user selectable.)"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"B<SLIST_ENTRY>()  declares a structure that connects the elements in the "
"list."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"B<SLIST_HEAD_INITIALIZER>()  evaluates to an initializer for the list "
"I<head>."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "B<SLIST_INIT>()  initializes the list referenced by I<head>."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"B<SLIST_EMPTY>()  evaluates to true if there are no elements in the list."
msgstr ""

#. type: SS
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "I<sections>"
msgid "Insertion"
msgstr "I<sections>"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy
#| msgid "Insertion of a new entry at the head of the list."
msgid ""
"B<SLIST_INSERT_HEAD>()  inserts the new element I<elm> at the head of the "
"list."
msgstr "Вставка нового элемента в начало списка."

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"B<SLIST_INSERT_AFTER>()  inserts the new element I<elm> after the element "
"I<listelm>."
msgstr ""

#. type: SS
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "Traversal"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"B<SLIST_FIRST>()  returns the first element in the list, or NULL if the list "
"is empty."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "B<SLIST_NEXT>()  returns the next element in the list."
msgstr ""

#.  .PP
#.  .BR SLIST_FOREACH_FROM ()
#.  behaves identically to
#.  .BR SLIST_FOREACH ()
#.  when
#.  .I var
#.  is NULL, else it treats
#.  .I var
#.  as a previously found SLIST element and begins the loop at
#.  .I var
#.  instead of the first element in the SLIST referenced by
#.  .IR head .
#.  .Pp
#.  .BR SLIST_FOREACH_SAFE ()
#.  traverses the list referenced by
#.  .I head
#.  in the forward direction, assigning each element in
#.  turn to
#.  .IR var .
#.  However, unlike
#.  .BR SLIST_FOREACH ()
#.  here it is permitted to both remove
#.  .I var
#.  as well as free it from within the loop safely without interfering with the
#.  traversal.
#.  .PP
#.  .BR SLIST_FOREACH_FROM_SAFE ()
#.  behaves identically to
#.  .BR SLIST_FOREACH_SAFE ()
#.  when
#.  .I var
#.  is NULL, else it treats
#.  .I var
#.  as a previously found SLIST element and begins the loop at
#.  .I var
#.  instead of the first element in the SLIST referenced by
#.  .IR head .
#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"B<SLIST_FOREACH>()  traverses the list referenced by I<head> in the forward "
"direction, assigning each element in turn to I<var>."
msgstr ""

#. type: SS
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "Removal"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "B<SLIST_REMOVE>()  removes the element I<elm> from the list."
msgstr ""

#.  .PP
#.  .BR SLIST_REMOVE_AFTER ()
#.  removes the element after
#.  .I elm
#.  from the list.
#.  Unlike
#.  .IR SLIST_REMOVE ,
#.  this macro does not traverse the entire list.
#.  .SS Other features
#.  .BR SLIST_SWAP ()
#.  swaps the contents of
#.  .I head1
#.  and
#.  .IR head2 .
#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"B<SLIST_REMOVE_HEAD>()  removes the element I<elm> from the head of the "
"list.  For optimum efficiency, elements being removed from the head of the "
"list should explicitly use this macro instead of the generic "
"B<SLIST_REMOVE>()."
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"B<SLIST_EMPTY>()  returns nonzero if the list is empty, and zero if the list "
"contains at least one entry."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"B<SLIST_FIRST>(), and B<SLIST_NEXT>()  return a pointer to the first or next "
"I<TYPE> structure, respectively."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"B<SLIST_HEAD_INITIALIZER>()  returns an initializer that can be assigned to "
"the list I<head>."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "POSIX.1-2001, POSIX.1-2008, 4.4BSD (B<shutdown>()  first appeared in "
#| "4.2BSD)."
msgid ""
"Not in POSIX.1, POSIX.1-2001, or POSIX.1-2008.  Present on the BSDs (SLIST "
"macros first appeared in 4.4BSD)."
msgstr ""
"POSIX.1-2001, POSIX.1-2008, 4.4BSD, (B<shutdown>() впервые появился в "
"4.2BSD)."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "ДЕФЕКТЫ"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"B<SLIST_FOREACH>()  doesn't allow I<var> to be removed or freed within the "
"loop, as it would interfere with the traversal.  B<SLIST_FOREACH_SAFE>(), "
"which is present on the BSDs but is not present in glibc, fixes this "
"limitation by allowing I<var> to safely be removed from the list and freed "
"from within the loop without interfering with the traversal."
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "ПРИМЕРЫ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"#include E<lt>stddef.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>sys/queue.hE<gt>\n"
msgstr ""
"#include E<lt>stddef.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>sys/queue.hE<gt>\n"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"struct entry {\n"
"    int data;\n"
"    SLIST_ENTRY(entry) entries;             /* Singly linked list */\n"
"};\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "SLIST_HEAD(slisthead, entry);\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"int\n"
"main(void)\n"
"{\n"
"    struct entry *n1, *n2, *n3, *np;\n"
"    struct slisthead head;                  /* Singly linked list\n"
"                                               head */\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "    SLIST_INIT(&head);                      /* Initialize the queue */\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    n1 = malloc(sizeof(struct entry));      /* Insert at the head */\n"
"    SLIST_INSERT_HEAD(&head, n1, entries);\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    n2 = malloc(sizeof(struct entry));      /* Insert after */\n"
"    SLIST_INSERT_AFTER(n1, n2, entries);\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    SLIST_REMOVE(&head, n2, entry, entries);/* Deletion */\n"
"    free(n2);\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    n3 = SLIST_FIRST(&head);\n"
"    SLIST_REMOVE_HEAD(&head, entries);      /* Deletion from the head */\n"
"    free(n3);\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    for (unsigned int i = 0; i E<lt> 5; i++) {\n"
"        n1 = malloc(sizeof(struct entry));\n"
"        SLIST_INSERT_HEAD(&head, n1, entries);\n"
"        n1-E<gt>data = i;\n"
"    }\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"                                            /* Forward traversal */\n"
"    SLIST_FOREACH(np, &head, entries)\n"
"        printf(\"%i\\en\", np-E<gt>data);\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    while (!SLIST_EMPTY(&head)) {           /* List deletion */\n"
"        n1 = SLIST_FIRST(&head);\n"
"        SLIST_REMOVE_HEAD(&head, entries);\n"
"        free(n1);\n"
"    }\n"
"    SLIST_INIT(&head);\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. #-#-#-#-#  archlinux: slist.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  debian-bullseye: slist.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  debian-unstable: slist.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  fedora-38: slist.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  fedora-rawhide: slist.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  mageia-cauldron: slist.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  opensuse-tumbleweed: slist.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<insque>(3), B<queue>(7)"
msgstr "B<insque>(3), B<queue>(7)"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "2020-10-21"
msgstr "21 октября 2020 г."

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "GNU"
msgstr "GNU"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Руководство программиста Linux"

#. type: Plain text
#: debian-bullseye
#, no-wrap
msgid "B<struct TYPE *SLIST_FIRST(SLIST_HEAD *>I<head>B<);>\n"
msgstr ""

#. type: Plain text
#: debian-bullseye
#, no-wrap
msgid "B<SLIST_HEAD(HEADNAME, TYPE);>\n"
msgstr ""

#. type: Plain text
#: debian-bullseye
#, no-wrap
msgid "B<SLIST_HEAD SLIST_HEAD_INITIALIZER(SLIST_HEAD >I<head>B<);>\n"
msgstr ""

#. type: Plain text
#: debian-bullseye
#, no-wrap
msgid "B<void SLIST_INIT(SLIST_HEAD *>I<head>B<);>\n"
msgstr ""

#. type: Plain text
#: debian-bullseye
#, no-wrap
msgid ""
"B<void SLIST_INSERT_AFTER(struct TYPE *>I<listelm>B<, struct TYPE *>I<elm>B<,>\n"
"B<                SLIST_ENTRY >I<NAME>B<);>\n"
msgstr ""

#. type: Plain text
#: debian-bullseye
#, no-wrap
msgid ""
"B<void SLIST_INSERT_HEAD(SLIST_HEAD *>I<head>B<, struct TYPE *>I<elm>B<,>\n"
"B<                SLIST_ENTRY >I<NAME>B<);>\n"
msgstr ""

#. type: Plain text
#: debian-bullseye
#, no-wrap
msgid "B<struct TYPE *SLIST_NEXT(struct TYPE *>I<elm>B<, SLIST_ENTRY >I<NAME>B<);>\n"
msgstr ""

#. .PP
#. .BI "void SLIST_REMOVE_AFTER(struct TYPE *" elm ", SLIST_ENTRY " NAME ");"
#. type: Plain text
#: debian-bullseye
#, no-wrap
msgid "B<void SLIST_REMOVE(SLIST_HEAD *>I<head>B<, struct TYPE *>I<elm>B<, SLIST_ENTRY >I<NAME>B<);>\n"
msgstr ""

#. .PP
#. .BI "void SLIST_SWAP(SLIST_HEAD *" head1 ", SLIST_HEAD *" head2 ", SLIST_ENTRY " NAME ");"
#. type: Plain text
#: debian-bullseye
#, no-wrap
msgid "B<void SLIST_REMOVE_HEAD(SLIST_HEAD *>I<head>B<, SLIST_ENTRY >I<NAME>B<);>\n"
msgstr ""

#. type: Plain text
#: debian-bullseye
msgid ""
"The macro B<SLIST_HEAD_INITIALIZER>()  evaluates to an initializer for the "
"list I<head>."
msgstr ""

#. type: Plain text
#: debian-bullseye
msgid ""
"The macro B<SLIST_EMPTY>()  evaluates to true if there are no elements in "
"the list."
msgstr ""

#. type: Plain text
#: debian-bullseye
msgid ""
"The macro B<SLIST_ENTRY>()  declares a structure that connects the elements "
"in the list."
msgstr ""

#. type: Plain text
#: debian-bullseye
msgid ""
"The macro B<SLIST_FIRST>()  returns the first element in the list or NULL if "
"the list is empty."
msgstr ""

#.  .PP
#.  The macro
#.  .BR SLIST_FOREACH_FROM ()
#.  behaves identically to
#.  .BR SLIST_FOREACH ()
#.  when
#.  .I var
#.  is NULL, else it treats
#.  .I var
#.  as a previously found SLIST element and begins the loop at
#.  .I var
#.  instead of the first element in the SLIST referenced by
#.  .IR head .
#.  .Pp
#.  The macro
#.  .BR SLIST_FOREACH_SAFE ()
#.  traverses the list referenced by
#.  .I head
#.  in the forward direction, assigning each element in
#.  turn to
#.  .IR var .
#.  However, unlike
#.  .BR SLIST_FOREACH ()
#.  here it is permitted to both remove
#.  .I var
#.  as well as free it from within the loop safely without interfering with the
#.  traversal.
#.  .PP
#.  The macro
#.  .BR SLIST_FOREACH_FROM_SAFE ()
#.  behaves identically to
#.  .BR SLIST_FOREACH_SAFE ()
#.  when
#.  .I var
#.  is NULL, else it treats
#.  .I var
#.  as a previously found SLIST element and begins the loop at
#.  .I var
#.  instead of the first element in the SLIST referenced by
#.  .IR head .
#. type: Plain text
#: debian-bullseye
msgid ""
"The macro B<SLIST_FOREACH>()  traverses the list referenced by I<head> in "
"the forward direction, assigning each element in turn to I<var>."
msgstr ""

#. type: Plain text
#: debian-bullseye
msgid "The macro B<SLIST_INIT>()  initializes the list referenced by I<head>."
msgstr ""

#. type: Plain text
#: debian-bullseye
msgid ""
"The macro B<SLIST_INSERT_HEAD>()  inserts the new element I<elm> at the head "
"of the list."
msgstr ""

#. type: Plain text
#: debian-bullseye
msgid ""
"The macro B<SLIST_INSERT_AFTER>()  inserts the new element I<elm> after the "
"element I<listelm>."
msgstr ""

#.  .PP
#.  The macro
#.  .BR SLIST_REMOVE_AFTER ()
#.  removes the element after
#.  .I elm
#.  from the list.
#.  Unlike
#.  .IR SLIST_REMOVE ,
#.  this macro does not traverse the entire list.
#. type: Plain text
#: debian-bullseye
msgid "The macro B<SLIST_NEXT>()  returns the next element in the list."
msgstr ""

#. type: Plain text
#: debian-bullseye
msgid ""
"The macro B<SLIST_REMOVE_HEAD>()  removes the element I<elm> from the head "
"of the list.  For optimum efficiency, elements being removed from the head "
"of the list should explicitly use this macro instead of the generic "
"I<SLIST_REMOVE> macro."
msgstr ""

#.  .PP
#.  The macro
#.  .BR SLIST_SWAP ()
#.  swaps the contents of
#.  .I head1
#.  and
#.  .IR head2 .
#. type: Plain text
#: debian-bullseye
msgid "The macro B<SLIST_REMOVE>()  removes the element I<elm> from the list."
msgstr ""

#. type: SH
#: debian-bullseye
#, no-wrap
msgid "CONFORMING TO"
msgstr "СООТВЕТСТВИЕ СТАНДАРТАМ"

#. type: Plain text
#: debian-bullseye
#, fuzzy
#| msgid ""
#| "POSIX.1-2001, POSIX.1-2008, 4.4BSD (B<shutdown>()  first appeared in "
#| "4.2BSD)."
msgid ""
"Not in POSIX.1, POSIX.1-2001 or POSIX.1-2008.  Present on the BSDs (SLIST "
"macros first appeared in 4.4BSD)."
msgstr ""
"POSIX.1-2001, POSIX.1-2008, 4.4BSD, (B<shutdown>() впервые появился в "
"4.2BSD)."

#. type: Plain text
#: debian-bullseye
msgid ""
"The macro B<SLIST_FOREACH>()  doesn't allow I<var> to be removed or freed "
"within the loop, as it would interfere with the traversal.  The macro "
"B<SLIST_FOREACH_SAFE>(), which is present on the BSDs but is not present in "
"glibc, fixes this limitation by allowing I<var> to safely be removed from "
"the list and freed from within the loop without interfering with the "
"traversal."
msgstr ""

#. type: Plain text
#: debian-bullseye
#, no-wrap
msgid ""
"struct entry {\n"
"    int data;\n"
"    SLIST_ENTRY(entry) entries;             /* Singly linked List. */\n"
"};\n"
msgstr ""

#. type: Plain text
#: debian-bullseye
#, no-wrap
msgid ""
"int\n"
"main(void)\n"
"{\n"
"    struct entry *n1, *n2, *n3, *np;\n"
"    struct slisthead head;                  /* Singly linked List\n"
"                                               head. */\n"
msgstr ""

#. type: Plain text
#: debian-bullseye
#, no-wrap
msgid "    SLIST_INIT(&head);                      /* Initialize the queue. */\n"
msgstr ""

#. type: Plain text
#: debian-bullseye
#, no-wrap
msgid ""
"    n1 = malloc(sizeof(struct entry));      /* Insert at the head. */\n"
"    SLIST_INSERT_HEAD(&head, n1, entries);\n"
msgstr ""

#. type: Plain text
#: debian-bullseye
#, no-wrap
msgid ""
"    n2 = malloc(sizeof(struct entry));      /* Insert after. */\n"
"    SLIST_INSERT_AFTER(n1, n2, entries);\n"
msgstr ""

#. type: Plain text
#: debian-bullseye
#, no-wrap
msgid ""
"    SLIST_REMOVE(&head, n2, entry, entries);/* Deletion. */\n"
"    free(n2);\n"
msgstr ""

#. type: Plain text
#: debian-bullseye
#, no-wrap
msgid ""
"    n3 = SLIST_FIRST(&head);\n"
"    SLIST_REMOVE_HEAD(&head, entries);      /* Deletion from the head. */\n"
"    free(n3);\n"
msgstr ""

#. type: Plain text
#: debian-bullseye
#, no-wrap
msgid ""
"    for (int i = 0; i E<lt> 5; i++) {\n"
"        n1 = malloc(sizeof(struct entry));\n"
"        SLIST_INSERT_HEAD(&head, n1, entries);\n"
"        n1-E<gt>data = i;\n"
"    }\n"
msgstr ""

#. type: Plain text
#: debian-bullseye
#, no-wrap
msgid ""
"                                            /* Forward traversal. */\n"
"    SLIST_FOREACH(np, &head, entries)\n"
"        printf(\"%i\\en\", np-E<gt>data);\n"
msgstr ""

#. type: Plain text
#: debian-bullseye
#, no-wrap
msgid ""
"    while (!SLIST_EMPTY(&head)) {           /* List Deletion. */\n"
"        n1 = SLIST_FIRST(&head);\n"
"        SLIST_REMOVE_HEAD(&head, entries);\n"
"        free(n1);\n"
"    }\n"
"    SLIST_INIT(&head);\n"
msgstr ""

#. type: SH
#: debian-bullseye
#, no-wrap
msgid "COLOPHON"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 5.10. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.02"
msgstr "Linux man-pages 6.02"
