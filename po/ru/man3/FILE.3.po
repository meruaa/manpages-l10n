# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2017.
# Dmitry Bolkhovskikh <d20052005@yandex.ru>, 2017.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2022-12-19 18:36+0100\n"
"PO-Revision-Date: 2019-09-27 19:31+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "FILE"
msgstr "FILE"

#. type: TH
#: archlinux debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2022-10-09"
msgstr "9 октября 2022 г."

#. type: TH
#: archlinux debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.01"
msgstr "Linux man-pages 6.01"

#. type: SH
#: archlinux debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide mageia-cauldron
msgid "FILE - input/output stream"
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide mageia-cauldron
msgid "Standard C library (I<libc>)"
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<#include E<lt>stdio.hE<gt>>\n"
msgstr "B<#include E<lt>stdio.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<typedef> /* ... */ B<FILE;>\n"
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide mageia-cauldron
msgid "An object type used for streams."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide mageia-cauldron
msgid "C99 and later; POSIX.1-2001 and later."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "NOTES"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide mageia-cauldron
msgid "The following header also provides this type: I<E<lt>wchar.hE<gt>>."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide mageia-cauldron
#, fuzzy
#| msgid ""
#| "B<fsync>(2), B<sync>(2), B<write>(2), B<fclose>(3), B<fileno>(3), "
#| "B<fopen>(3), B<setbuf>(3), B<unlocked_stdio>(3)"
msgid ""
"B<fclose>(3), B<flockfile>(3), B<fopen>(3), B<fprintf>(3), B<fread>(3), "
"B<fscanf>(3), B<stdin>(3), B<stdio>(3)"
msgstr ""
"B<fsync>(2), B<sync>(2), B<write>(2), B<fclose>(3), B<fileno>(3), "
"B<fopen>(3), B<setbuf>(3), B<unlocked_stdio>(3)"
