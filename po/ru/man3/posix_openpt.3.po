# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Alexey, 2016.
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2014-2017.
# kogamatranslator49 <r.podarov@yandex.ru>, 2015.
# Kogan, Darima <silverdk99@gmail.com>, 2014.
# Max Is <ismax799@gmail.com>, 2016.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-02-20 20:21+0100\n"
"PO-Revision-Date: 2019-10-12 08:58+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<posix_openpt>()"
msgid "posix_openpt"
msgstr "B<posix_openpt>()"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 февраля 2023 г."

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "posix_openpt - open a pseudoterminal device"
msgstr "posix_openpt - открывает псевдо-терминальное устройство"

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#include E<lt>stdlib.hE<gt>>\n"
"B<#include E<lt>fcntl.hE<gt>>\n"
msgstr ""
"B<#include E<lt>stdlib.hE<gt>>\n"
"B<#include E<lt>fcntl.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<int posix_openpt(int >I<flags>B<);>\n"
msgstr "B<int posix_openpt(int >I<flags>B<);>\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Требования макроса тестирования свойств для glibc (см. "
"B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy
#| msgid "B<posix_openpt>()"
msgid "B<posix_openpt>():"
msgstr "B<posix_openpt>()"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "_XOPEN_SOURCE\\ E<gt>=\\ 500"
msgid "    _XOPEN_SOURCE E<gt>= 600\n"
msgstr "_XOPEN_SOURCE\\ E<gt>=\\ 500"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The B<posix_openpt>()  function opens an unused pseudoterminal master "
"device, returning a file descriptor that can be used to refer to that device."
msgstr ""
"Функция B<posix_openpt>() открывает неиспользуемое главное псевдо-"
"терминальное устройство, возвращая файловый дескриптор, который можно "
"использовать для работы с устройством."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The I<flags> argument is a bit mask that ORs together zero or more of the "
"following flags:"
msgstr ""
"Аргумент I<flags> представляет собой битовую маску из комбинации (OR) нуля "
"или более следующих флагов:"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<O_RDWR>"
msgstr "B<O_RDWR>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Open the device for both reading and writing.  It is usual to specify this "
"flag."
msgstr "Открыть устройство для чтения и записи. Обычно, указывается этот флаг."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<O_NOCTTY>"
msgstr "B<O_NOCTTY>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Do not make this device the controlling terminal for the process."
msgstr "Не делать данное устройство управляющим терминалом процесса."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "On success, B<posix_openpt>()  returns a nonnegative file descriptor "
#| "which is the lowest numbered unused file descriptor.  On failure, -1 is "
#| "returned, and I<errno> is set to indicate the error."
msgid ""
"On success, B<posix_openpt>()  returns a file descriptor (a nonnegative "
"integer) which is the lowest numbered unused file descriptor.  On failure, "
"-1 is returned, and I<errno> is set to indicate the error."
msgstr ""
"При успешном выполнении B<posix_openpt>() возвращает неотрицательный "
"файловый дескриптор с наименьшим номером неиспользуемого файлового "
"дескриптора. При ошибке возвращается -1, и в I<errno> записывается номер "
"ошибки."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ОШИБКИ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "See B<open>(2)."
msgstr "Смотрите B<open>(2)."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "ВЕРСИИ"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, fuzzy
#| msgid ""
#| "Glibc support for B<posix_openpt>()  has been provided since version "
#| "2.2.1."
msgid ""
"glibc support for B<posix_openpt>()  has been provided since glibc 2.2.1."
msgstr "Поддержка B<posix_openpt>() в glibc появилась в версии 2.2.1."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "АТРИБУТЫ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr "Описание терминов данного раздела смотрите в B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Интерфейс"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Атрибут"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Значение"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<posix_openpt>()"
msgstr "B<posix_openpt>()"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Безвредность в нитях"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "POSIX.1-2001, POSIX.1-2008."
msgstr "POSIX.1-2001, POSIX.1-2008."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<posix_openpt>()  is part of the UNIX 98 pseudoterminal support (see "
"B<pts>(4))."
msgstr ""
"Функция B<posix_openpt>() является частью поддержки псевдо-терминалов UNIX "
"98 (смотрите B<pts>(4))."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Some older UNIX implementations that support System V (aka UNIX 98) "
#| "pseudoterminals don't have this function, but it is easy to implement:"
msgid ""
"Some older UNIX implementations that support System V (aka UNIX 98) "
"pseudoterminals don't have this function, but it can be easily implemented "
"by opening the pseudoterminal multiplexor device:"
msgstr ""
"В некоторых старых реализациях UNIX, поддерживающих System V (также "
"называемом UNIX 98), для псевдо-терминалов нет этой функции, но её легко "
"написать:"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"int\n"
"posix_openpt(int flags)\n"
"{\n"
"    return open(\"/dev/ptmx\", flags);\n"
"}\n"
msgstr ""
"int\n"
"posix_openpt(int flags)\n"
"{\n"
"    return open(\"/dev/ptmx\", flags);\n"
"}\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Calling B<posix_openpt>()  creates a pathname for the corresponding "
"pseudoterminal slave device.  The pathname of the slave device can be "
"obtained using B<ptsname>(3).  The slave device pathname exists only as long "
"as the master device is open."
msgstr ""
"При вызове B<posix_openpt>() создаётся путь для соответствующего "
"подчинённого псевдо-терминального устройства. Путь подчинённого устройства "
"можно получить с помощью B<ptsname>(3). Путь подчинённого устройства "
"существует только пока открыто главное устройство."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<open>(2), B<getpt>(3), B<grantpt>(3), B<ptsname>(3), B<unlockpt>(3), "
"B<pts>(4), B<pty>(7)"
msgstr ""
"B<open>(2), B<getpt>(3), B<grantpt>(3), B<ptsname>(3), B<unlockpt>(3), "
"B<pts>(4), B<pty>(7)"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "POSIX_OPENPT"
msgstr "POSIX_OPENPT"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "2020-08-13"
msgstr "13 августа 2020 г."

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Руководство программиста Linux"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "B<posix_openpt>(): _XOPEN_SOURCE\\ E<gt>=\\ 600"
msgstr "B<posix_openpt>(): _XOPEN_SOURCE\\ E<gt>=\\ 600"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"Glibc support for B<posix_openpt>()  has been provided since version 2.2.1."
msgstr "Поддержка B<posix_openpt>() в glibc появилась в версии 2.2.1."

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "СООТВЕТСТВИЕ СТАНДАРТАМ"

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 5.10. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15 сентября 2017 г."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"On success, B<posix_openpt>()  returns a nonnegative file descriptor which "
"is the lowest numbered unused file descriptor.  On failure, -1 is returned, "
"and I<errno> is set to indicate the error."
msgstr ""
"При успешном выполнении B<posix_openpt>() возвращает неотрицательный "
"файловый дескриптор с наименьшим номером неиспользуемого файлового "
"дескриптора. При ошибке возвращается -1, и в I<errno> записывается номер "
"ошибки."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"Some older UNIX implementations that support System V (aka UNIX 98) "
"pseudoterminals don't have this function, but it is easy to implement:"
msgstr ""
"В некоторых старых реализациях UNIX, поддерживающих System V (также "
"называемом UNIX 98), для псевдо-терминалов нет этой функции, но её легко "
"написать:"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 4.16. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-12-15"
msgstr "15 декабря 2022 г."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.02"
msgstr "Linux man-pages 6.02"

#. type: Plain text
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Glibc support for B<posix_openpt>()  has been provided since version "
#| "2.2.1."
msgid ""
"Glibc support for B<posix_openpt>()  has been provided since glibc 2.2.1."
msgstr "Поддержка B<posix_openpt>() в glibc появилась в версии 2.2.1."
