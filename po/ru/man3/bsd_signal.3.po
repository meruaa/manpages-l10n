# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Artyom Kunyov <artkun@guitarplayer.ru>, 2012.
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2012.
# Dmitriy Ovchinnikov <dmitriyxt5@gmail.com>, 2012.
# Dmitry Bolkhovskikh <d20052005@yandex.ru>, 2017.
# ITriskTI <ITriskTI@gmail.com>, 2012.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-02-20 20:00+0100\n"
"PO-Revision-Date: 2019-08-27 07:22+0000\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<bsd_signal>()"
msgid "bsd_signal"
msgstr "B<bsd_signal>()"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 февраля 2023 г."

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "bsd_signal - signal handling with BSD semantics"
msgstr "bsd_signal - обрабатывает сигналы согласно семантике BSD"

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>signal.hE<gt>>\n"
msgstr "B<#include E<lt>signal.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "B<typedef void (*sighandler_t)(int);>\n"
msgstr "B<typedef void (*sighandler_t)(int);>\n"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<sighandler_t bsd_signal(int >I<signum>B<, sighandler_t >I<handler>B<);>"
msgid "B<sighandler_t bsd_signal(int >I<signum>B<, sighandler_t >I<handler>B<);>\n"
msgstr "B<sighandler_t bsd_signal(int >I<signum>B<, sighandler_t >I<handler>B<);>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Требования макроса тестирования свойств для glibc (см. "
"B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<bsd_signal>():"
msgstr "B<bsd_signal>():"

#.     || _XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED
#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, fuzzy, no-wrap
#| msgid ""
#| "Since glibc 2.26:\n"
#| "    _XOPEN_SOURCE E<gt>= 500\n"
#| "        && ! (_POSIX_C_SOURCE\\ E<gt>=\\ 200809L)\n"
msgid ""
"    Since glibc 2.26:\n"
"        _XOPEN_SOURCE E<gt>= 500\n"
"            && ! (_POSIX_C_SOURCE E<gt>= 200809L)\n"
"    glibc 2.25 and earlier:\n"
"        _XOPEN_SOURCE\n"
msgstr ""
"Начиная с glibc 2.26:\n"
"    _XOPEN_SOURCE E<gt>= 500\n"
"        && ! (_POSIX_C_SOURCE\\ E<gt>=\\ 200809L)\n"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The B<bsd_signal>()  function takes the same arguments, and performs the "
"same task, as B<signal>(2)."
msgstr ""
"Функция B<bsd_signal>() ожидает те же аргументы и выполняет ту же задачу, "
"что и B<signal>(2)."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The difference between the two is that B<bsd_signal>()  is guaranteed to "
"provide reliable signal semantics, that is: a) the disposition of the signal "
"is not reset to the default when the handler is invoked; b) delivery of "
"further instances of the signal is blocked while the signal handler is "
"executing; and c) if the handler interrupts a blocking system call, then the "
"system call is automatically restarted.  A portable application cannot rely "
"on B<signal>(2)  to provide these guarantees."
msgstr ""
"Различие между ними в том, что B<bsd_signal>() гарантированно предоставляет "
"надёжную семантику сигналов, то есть: a) обработчик сигнала не сбрасывается "
"в значение по умолчанию, при его вызове; b) доставка последующих экземпляров "
"сигнала блокируется до тех пор, пока выполняется обработчик сигнала; c) если "
"обработчик сигнала прерывает блокирующий системный вызов, то системный вызов "
"автоматически перезапускается. Переносимое приложение не может полагаться на "
"B<signal>(2), если ему нужны такие гарантии."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The B<bsd_signal>()  function returns the previous value of the signal "
"handler, or B<SIG_ERR> on error."
msgstr ""
"Функция B<bsd_signal>() возвращает предыдущее значение обработчика сигнала "
"или B<SIG_ERR> при ошибке."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ОШИБКИ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "As for B<signal>(2)."
msgstr "Как в B<signal>(2)."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "АТРИБУТЫ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr "Описание терминов данного раздела смотрите в B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Интерфейс"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Атрибут"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Значение"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<bsd_signal>()"
msgstr "B<bsd_signal>()"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Безвредность в нитях"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"4.2BSD, POSIX.1-2001.  POSIX.1-2008 removes the specification of "
"B<bsd_signal>(), recommending the use of B<sigaction>(2)  instead."
msgstr ""
"4.2BSD и POSIX.1-2001. В POSIX.1-2008 спецификация B<bsd_signal>() удалена, "
"вместо неё рекомендуется использовать B<sigaction>(2)."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Use of B<bsd_signal>()  should be avoided; use B<sigaction>(2)  instead."
msgstr ""
"Избегайте использования B<bsd_signal>(), используйте вместо неё "
"B<sigaction>(2)."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"On modern Linux systems, B<bsd_signal>()  and B<signal>(2)  are equivalent.  "
"But on older systems, B<signal>(2)  provided unreliable signal semantics; "
"see B<signal>(2)  for details."
msgstr ""
"В современных Linux-системах B<bsd_signal>() и B<signal>(2) эквивалентны. "
"Однако на старых системах B<signal>(2) предоставляет ненадежную семантику "
"сигналов. Подробности см. в B<signal>(2)."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The use of I<sighandler_t> is a GNU extension; this type is defined only if "
"the B<_GNU_SOURCE> feature test macro is defined."
msgstr ""
"Используемый параметр I<sighandler_t> является расширением GNU; данный тип "
"определён только в том случае, если определён макрос B<_GNU_SOURCE>."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<sigaction>(2), B<signal>(2), B<sysv_signal>(3), B<signal>(7)"
msgstr "B<sigaction>(2), B<signal>(2), B<sysv_signal>(3), B<signal>(7)"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "BSD_SIGNAL"
msgstr "BSD_SIGNAL"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "2019-03-06"
msgstr "6 марта 2019 г."

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Руководство программиста Linux"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "B<#include E<lt>signal.hE<gt>>"
msgstr "B<#include E<lt>signal.hE<gt>>"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "B<typedef void (*sighandler_t)(int);>"
msgstr "B<typedef void (*sighandler_t)(int);>"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"B<sighandler_t bsd_signal(int >I<signum>B<, sighandler_t >I<handler>B<);>"
msgstr ""
"B<sighandler_t bsd_signal(int >I<signum>B<, sighandler_t >I<handler>B<);>"

#.     || _XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED
#. type: Plain text
#: debian-bullseye
#, no-wrap
msgid ""
"Since glibc 2.26:\n"
"    _XOPEN_SOURCE E<gt>= 500\n"
"        && ! (_POSIX_C_SOURCE\\ E<gt>=\\ 200809L)\n"
msgstr ""
"Начиная с glibc 2.26:\n"
"    _XOPEN_SOURCE E<gt>= 500\n"
"        && ! (_POSIX_C_SOURCE\\ E<gt>=\\ 200809L)\n"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid ""
"Glibc 2.25 and earlier:\n"
"    _XOPEN_SOURCE\n"
msgstr ""
"glibc 2.25 и старее:\n"
"    _XOPEN_SOURCE\n"

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "СООТВЕТСТВИЕ СТАНДАРТАМ"

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 5.10. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15 сентября 2017 г."

#.     || _XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED
#. type: Plain text
#: opensuse-leap-15-5
#, fuzzy, no-wrap
#| msgid ""
#| "Since glibc 2.26:\n"
#| "    _XOPEN_SOURCE E<gt>= 500\n"
#| "        && ! (_POSIX_C_SOURCE\\ E<gt>=\\ 200809L)\n"
msgid ""
"Since glibc 2.26:\n"
"    _XOPEN_SOURCE E<gt>= 500\n"
"        && ! (_POSIX_C_SOURCE\\ E<gt>=\\ 200112L)\n"
msgstr ""
"Начиная с glibc 2.26:\n"
"    _XOPEN_SOURCE E<gt>= 500\n"
"        && ! (_POSIX_C_SOURCE\\ E<gt>=\\ 200809L)\n"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 4.16. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-12-15"
msgstr "15 декабря 2022 г."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.02"
msgstr "Linux man-pages 6.02"

#.     || _XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED
#. type: Plain text
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "Since glibc 2.26:\n"
#| "    _XOPEN_SOURCE E<gt>= 500\n"
#| "        && ! (_POSIX_C_SOURCE\\ E<gt>=\\ 200809L)\n"
msgid ""
"    Since glibc 2.26:\n"
"        _XOPEN_SOURCE E<gt>= 500\n"
"            && ! (_POSIX_C_SOURCE E<gt>= 200809L)\n"
"    Glibc 2.25 and earlier:\n"
"        _XOPEN_SOURCE\n"
msgstr ""
"Начиная с glibc 2.26:\n"
"    _XOPEN_SOURCE E<gt>= 500\n"
"        && ! (_POSIX_C_SOURCE\\ E<gt>=\\ 200809L)\n"
