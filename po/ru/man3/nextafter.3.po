# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Alex Nik <rage.iz.me@gmail.com>, 2013.
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2013, 2016.
# Dmitry Bolkhovskikh <d20052005@yandex.ru>, 2017.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-02-20 20:19+0100\n"
"PO-Revision-Date: 2019-09-19 18:57+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, fuzzy, no-wrap
#| msgid "B<nextafter>():"
msgid "nextafter"
msgstr "B<nextafter>():"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 февраля 2023 г."

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"nextafter, nextafterf, nextafterl, nexttoward, nexttowardf, nexttowardl - "
"floating-point number manipulation"
msgstr ""
"nextafter, nextafterf, nextafterl, nexttoward, nexttowardf, nexttowardl - "
"операции над числами с плавающей запятой"

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Math library (I<libm>, I<-lm>)"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>math.hE<gt>>\n"
msgstr "B<#include E<lt>math.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "B<double nextup(double >I<x>B<);>\n"
#| "B<float nextupf(float >I<x>B<);>\n"
#| "B<long double nextupl(long double >I<x>B<);>\n"
msgid ""
"B<double nextafter(double >I<x>B<, double >I<y>B<);>\n"
"B<float nextafterf(float >I<x>B<, float >I<y>B<);>\n"
"B<long double nextafterl(long double >I<x>B<, long double >I<y>B<);>\n"
msgstr ""
"B<double nextup(double >I<x>B<);>\n"
"B<float nextupf(float >I<x>B<);>\n"
"B<long double nextupl(long double >I<x>B<);>\n"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "B<double nextdown(double >I<x>B<);>\n"
#| "B<float nextdownf(float >I<x>B<);>\n"
#| "B<long double nextdownl(long double >I<x>B<);>\n"
msgid ""
"B<double nexttoward(double >I<x>B<, long double >I<y>B<);>\n"
"B<float nexttowardf(float >I<x>B<, long double >I<y>B<);>\n"
"B<long double nexttowardl(long double >I<x>B<, long double >I<y>B<);>\n"
msgstr ""
"B<double nextdown(double >I<x>B<);>\n"
"B<float nextdownf(float >I<x>B<);>\n"
"B<long double nextdownl(long double >I<x>B<);>\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Требования макроса тестирования свойств для glibc (см. "
"B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<nextafter>():"
msgstr "B<nextafter>():"

#.     || _XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED
#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, fuzzy, no-wrap
#| msgid ""
#| "    _ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L\n"
#| "        || _XOPEN_SOURCE E<gt>= 500\n"
#| "        || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
#| "        || /* Glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgid ""
"    _ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L\n"
"        || _XOPEN_SOURCE E<gt>= 500\n"
"        || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"    _ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L\n"
"        || _XOPEN_SOURCE E<gt>= 500\n"
"        || /* начиная с glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* Glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<nextafterf>(), B<nextafterl>():"
msgstr "B<nextafterf>(), B<nextafterl>():"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid ""
"    _ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L\n"
"        || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"    _ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L\n"
"        || /* начиная с glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* Glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<nexttoward>(), B<nexttowardf>(), B<nexttowardl>():"
msgstr "B<nexttoward>(), B<nexttowardf>(), B<nexttowardl>():"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "_XOPEN_SOURCE\\ E<gt>=\\ 600 || _ISOC99_SOURCE || _POSIX_C_SOURCE\\ E<gt>=\\ 200112L"
msgid ""
"    _XOPEN_SOURCE E<gt>= 600 || _ISOC99_SOURCE\n"
"        || _POSIX_C_SOURCE E<gt>= 200112L\n"
msgstr "_XOPEN_SOURCE\\ E<gt>=\\ 600 || _ISOC99_SOURCE || _POSIX_C_SOURCE\\ E<gt>=\\ 200112L"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The B<nextafter>(), B<nextafterf>(), and B<nextafterl>()  functions return "
"the next representable floating-point value following I<x> in the direction "
"of I<y>.  If I<y> is less than I<x>, these functions will return the largest "
"representable number less than I<x>."
msgstr ""
"Функции B<nextafter>(), B<nextafterf>() и B<nextafterl>() возвращают "
"следующее представимое значение с плавающей запятой после I<x> по "
"направлению к I<y>. Если I<y> меньше I<x>, то эти функции возвращают "
"наибольшее представимое число, которое меньше I<x>."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "If I<x> equals I<y>, the functions return I<y>."
msgstr "Если I<x> = I<y>, то функции возвращают I<y>."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The B<nexttoward>(), B<nexttowardf>(), and B<nexttowardl>()  functions do "
"the same as the corresponding B<nextafter>()  functions, except that they "
"have a I<long double> second argument."
msgstr ""
"Функции B<nexttoward>(), B<nexttowardf>() и B<nexttowardl>() делают то же, "
"что и функции B<nextafter>(), за исключением того, что у них второй аргумент "
"имеет тип I<long double>."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"On success, these functions return the next representable floating-point "
"value after I<x> in the direction of I<y>."
msgstr ""
"При успешном выполнении данные функции возвращают представимое значение с "
"плавающей запятой следующее за I<x> по направлению к I<y>."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If I<x> equals I<y>, then I<y> (cast to the same type as I<x>)  is returned."
msgstr ""
"Если I<x> = I<y>, то возвращается значение I<y> (приведённое к типу I<x>)."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "If I<x> or I<y> is a NaN, a NaN is returned."
msgstr "Если значение I<x> или I<y> равно NaN, будет возвращено NaN."

#.  e.g., DBL_MAX
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If I<x> is finite, and the result would overflow, a range error occurs, and "
"the functions return B<HUGE_VAL>, B<HUGE_VALF>, or B<HUGE_VALL>, "
"respectively, with the correct mathematical sign."
msgstr ""
"Если I<x> имеет конечное значение и происходит переполнение результата, "
"возникает ошибка диапазона, то функции возвращают B<HUGE_VAL>, B<HUGE_VALF> "
"или B<HUGE_VALL>, соответственно, с математически правильным знаком."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If I<x> is not equal to I<y>, and the correct function result would be "
"subnormal, zero, or underflow, a range error occurs, and either the correct "
"value (if it can be represented), or 0.0, is returned."
msgstr ""
"Если I<x> не равно I<y>, и правильный результат функции был бы "
"субнормальным, нулём или возникала бы исчерпание степени, ошибка диапазона, "
"то возвращается любое правильное значение (если оно представимо) или 0,0."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ОШИБКИ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"See B<math_error>(7)  for information on how to determine whether an error "
"has occurred when calling these functions."
msgstr ""
"Смотрите B<math_error>(7), чтобы определить, какие ошибки могут возникать "
"при вызове этих функций."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The following errors can occur:"
msgstr "Могут возникать следующие ошибки:"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Range error: result overflow"
msgstr "Ошибка диапазона: результат превысил разрядность"

#.  e.g., nextafter(DBL_MAX, HUGE_VAL);
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"I<errno> is set to B<ERANGE>.  An overflow floating-point exception "
"(B<FE_OVERFLOW>)  is raised."
msgstr ""
"Значение I<errno> устанавливается в B<ERANGE>. Возникает исключение "
"переполнения плавающей запятой (B<FE_OVERFLOW>)."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Range error: result is subnormal or underflows"
msgstr "Ошибка диапазона: результат субнормальный или исчерпание степени"

#.  e.g., nextafter(DBL_MIN, 0.0);
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"I<errno> is set to B<ERANGE>.  An underflow floating-point exception "
"(B<FE_UNDERFLOW>)  is raised."
msgstr ""
"Значение I<errno> устанавливается в B<ERANGE>. Возникает исключение "
"исчерпания степени чисел с плавающей запятой (B<FE_UNDERFLOW>)."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "АТРИБУТЫ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr "Описание терминов данного раздела смотрите в B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Интерфейс"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Атрибут"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Значение"

#. type: tbl table
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<nexttoward>(), B<nexttowardf>(), B<nexttowardl>():"
msgid ""
"B<nextafter>(),\n"
"B<nextafterf>(),\n"
"B<nextafterl>(),\n"
"B<nexttoward>(),\n"
"B<nexttowardf>(),\n"
"B<nexttowardl>()"
msgstr "B<nexttoward>(), B<nexttowardf>(), B<nexttowardl>():"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Безвредность в нитях"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"C99, POSIX.1-2001, POSIX.1-2008.  This function is defined in IEC 559 (and "
"the appendix with recommended functions in IEEE 754/IEEE 854)."
msgstr ""
"C99, POSIX.1-2001, POSIX.1-2008. Эта функция определена в IEC 559 (и в "
"приложении с рекомендуемыми функциями IEEE 754/IEEE 854)."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "ДЕФЕКТЫ"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "In glibc version 2.5 and earlier, these functions do not raise an "
#| "underflow floating-point (B<FE_UNDERFLOW>)  exception when an underflow "
#| "occurs."
msgid ""
"In glibc 2.5 and earlier, these functions do not raise an underflow floating-"
"point (B<FE_UNDERFLOW>)  exception when an underflow occurs."
msgstr ""
"В glibc 2.5 и младше данные функции не вызывают исключение исчерпания "
"степени плавающей запятой (B<FE_UNDERFLOW>) при возникновении ошибки "
"исчерпания степени."

#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6799
#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy
#| msgid "These functions do not set I<errno>."
msgid "Before glibc 2.23 these functions did not set I<errno>."
msgstr "Эти функции не изменяют I<errno>."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<nearbyint>(3)"
msgstr "B<nearbyint>(3)"

#. type: TH
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NEXTAFTER"
msgstr "NEXTAFTER"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "2020-12-21"
msgstr "21 декабря 2020 г."

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "GNU"
msgstr "GNU"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Руководство программиста Linux"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "B<#include E<lt>math.hE<gt>>"
msgstr "B<#include E<lt>math.hE<gt>>"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "B<double nextafter(double >I<x>B<, double >I<y>B<);>"
msgstr "B<double nextafter(double >I<x>B<, double >I<y>B<);>"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "B<float nextafterf(float >I<x>B<, float >I<y>B<);>"
msgstr "B<float nextafterf(float >I<x>B<, float >I<y>B<);>"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "B<long double nextafterl(long double >I<x>B<, long double >I<y>B<);>"
msgstr "B<long double nextafterl(long double >I<x>B<, long double >I<y>B<);>"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "B<double nexttoward(double >I<x>B<, long double >I<y>B<);>"
msgstr "B<double nexttoward(double >I<x>B<, long double >I<y>B<);>"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "B<float nexttowardf(float >I<x>B<, long double >I<y>B<);>"
msgstr "B<float nexttowardf(float >I<x>B<, long double >I<y>B<);>"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "B<long double nexttowardl(long double >I<x>B<, long double >I<y>B<);>"
msgstr "B<long double nexttowardl(long double >I<x>B<, long double >I<y>B<);>"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "Link with I<-lm>."
msgstr "Компонуется при указании параметра I<-lm>."

#.     || _XOPEN_SOURCE\ &&\ _XOPEN_SOURCE_EXTENDED
#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid ""
"_ISOC99_SOURCE || _POSIX_C_SOURCE\\ E<gt>=\\ 200112L\n"
"    || _XOPEN_SOURCE\\ E<gt>=\\ 500\n"
"    || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"    || /* Glibc versions E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"_ISOC99_SOURCE || _POSIX_C_SOURCE\\ E<gt>=\\ 200112L\n"
"    || _XOPEN_SOURCE\\ E<gt>=\\ 500\n"
"    || /* начиная с glibc 2.19: */ _DEFAULT_SOURCE\n"
"    || /* версии glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid ""
"_ISOC99_SOURCE || _POSIX_C_SOURCE\\ E<gt>=\\ 200112L\n"
"    || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"    || /* Glibc versions E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"_ISOC99_SOURCE || _POSIX_C_SOURCE\\ E<gt>=\\ 200112L\n"
"    || /* начиная с glibc 2.19: */ _DEFAULT_SOURCE\n"
"    || /* в версиях glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"_XOPEN_SOURCE\\ E<gt>=\\ 600 || _ISOC99_SOURCE || _POSIX_C_SOURCE\\ E<gt>=\\ "
"200112L"
msgstr ""
"_XOPEN_SOURCE\\ E<gt>=\\ 600 || _ISOC99_SOURCE || _POSIX_C_SOURCE\\ E<gt>=\\ "
"200112L"

#. type: tbl table
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid ""
"B<nextafter>(),\n"
"B<nextafterf>(),\n"
msgstr ""
"B<nextafter>(),\n"
"B<nextafterf>(),\n"

#. type: tbl table
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid ".br\n"
msgstr ".br\n"

#. type: tbl table
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid ""
"B<nextafterl>(),\n"
"B<nexttoward>(),\n"
msgstr ""
"B<nextafterl>(),\n"
"B<nexttoward>(),\n"

#. type: tbl table
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid ""
"B<nexttowardf>(),\n"
"B<nexttowardl>()"
msgstr ""
"B<nexttowardf>(),\n"
"B<nexttowardl>()"

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "СООТВЕТСТВИЕ СТАНДАРТАМ"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"In glibc version 2.5 and earlier, these functions do not raise an underflow "
"floating-point (B<FE_UNDERFLOW>)  exception when an underflow occurs."
msgstr ""
"В glibc 2.5 и младше данные функции не вызывают исключение исчерпания "
"степени плавающей запятой (B<FE_UNDERFLOW>) при возникновении ошибки "
"исчерпания степени."

#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6799
#. type: Plain text
#: debian-bullseye
#, fuzzy
#| msgid "These functions do not set I<errno>."
msgid "Before glibc version 2.23 these functions did not set I<errno>."
msgstr "Эти функции не изменяют I<errno>."

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 5.10. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15 сентября 2017 г."

#.  e.g., nextafter(DBL_MAX, HUGE_VAL);
#.  .I errno
#.  is set to
#.  .BR ERANGE .
#. type: Plain text
#: opensuse-leap-15-5
msgid "An overflow floating-point exception (B<FE_OVERFLOW>)  is raised."
msgstr "Вызывается исключение переполнения плавающей запятой (B<FE_OVERFLOW>)."

#.  e.g., nextafter(DBL_MIN, 0.0);
#.  .I errno
#.  is set to
#.  .BR ERANGE .
#. type: Plain text
#: opensuse-leap-15-5
msgid "An underflow floating-point exception (B<FE_UNDERFLOW>)  is raised."
msgstr ""
"Возникает исключение исчерпания степени чисел с плавающей запятой "
"(B<FE_UNDERFLOW>)."

#.  FIXME . Is it intentional that these functions do not set errno?
#.  Bug raised: http://sources.redhat.com/bugzilla/show_bug.cgi?id=6799
#. type: Plain text
#: opensuse-leap-15-5
msgid "These functions do not set I<errno>."
msgstr "Эти функции не изменяют I<errno>."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 4.16. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-12-15"
msgstr "15 декабря 2022 г."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.02"
msgstr "Linux man-pages 6.02"

#.     || _XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED
#. type: Plain text
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    _ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L\n"
"        || _XOPEN_SOURCE E<gt>= 500\n"
"        || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* Glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"    _ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L\n"
"        || _XOPEN_SOURCE E<gt>= 500\n"
"        || /* начиная с glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* Glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"

#. type: Plain text
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    _ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L\n"
"        || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* Glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"    _ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L\n"
"        || /* начиная с glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* Glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
