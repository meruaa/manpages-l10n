# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2013-2014, 2017.
# Dmitry Bolkhovskikh <d20052005@yandex.ru>, 2017.
# Vladislav <ivladislavefimov@gmail.com>, 2015.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017, 2019.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-02-20 20:08+0100\n"
"PO-Revision-Date: 2019-10-05 08:06+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "getw"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 февраля 2023 г."

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "getw, putw - input and output of words (ints)"
msgstr "getw, putw - ввод и вывод слов (int)"

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>stdio.hE<gt>>\n"
msgstr "B<#include E<lt>stdio.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<int putw(int >I<w>B<, FILE *>I<stream>B<);>\n"
msgid ""
"B<int getw(FILE *>I<stream>B<);>\n"
"B<int putw(int >I<w>B<, FILE *>I<stream>B<);>\n"
msgstr "B<int putw(int >I<w>B<, FILE *>I<stream>B<);>\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Требования макроса тестирования свойств для glibc (см. "
"B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<getw>(), B<putw>():"
msgstr "B<getw>(), B<putw>():"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, fuzzy, no-wrap
#| msgid ""
#| "_XOPEN_SOURCE && ! (_POSIX_C_SOURCE\\ E<gt>=\\ 200112L)\n"
#| "    || /* Glibc since 2.19: */ _DEFAULT_SOURCE\n"
#| "    || /* Glibc versions E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgid ""
"    Since glibc 2.3.3:\n"
"        _XOPEN_SOURCE && ! (_POSIX_C_SOURCE E<gt>= 200112L)\n"
"            || /* glibc E<gt>= 2.19: */ _DEFAULT_SOURCE\n"
"            || /* glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
"    Before glibc 2.3.3:\n"
"        _SVID_SOURCE || _BSD_SOURCE || _XOPEN_SOURCE\n"
msgstr ""
"_XOPEN_SOURCE && ! (_POSIX_C_SOURCE\\ E<gt>=\\ 200112L)\n"
"    || /* начиная с glibc 2.19: */ _DEFAULT_SOURCE\n"
"    || /* в версии glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<getw>()  reads a word (that is, an I<int>) from I<stream>.  It's provided "
"for compatibility with SVr4.  We recommend you use B<fread>(3)  instead."
msgstr ""
"Функция B<getw>() читает слово (то есть, I<int>) из I<stream>. Она "
"предоставляется для совместимости с SVr4. Вместо неё рекомендуется "
"использовать B<fread>(3)."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<putw>()  writes the word I<w> (that is, an I<int>) to I<stream>.  It is "
"provided for compatibility with SVr4, but we recommend you use B<fwrite>(3)  "
"instead."
msgstr ""
"Функция B<putw>() записывает слово I<w> (то есть, I<int>) в I<stream>. Она "
"предоставляется для совместимости с SVr4; вместо неё рекомендуется "
"использовать B<fwrite>(3)."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Normally, B<getw>()  returns the word read, and B<putw>()  returns 0.  On "
"error, they return B<EOF>."
msgstr ""
"Обычно, B<getw>() возвращает прочитанное слово, а B<putw>() возвращает 0. "
"При ошибках они возвращают B<EOF>."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "АТРИБУТЫ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr "Описание терминов данного раздела смотрите в B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Интерфейс"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Атрибут"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Значение"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<getw>(),\n"
"B<putw>()"
msgstr ""
"B<getw>(),\n"
"B<putw>()"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Безвредность в нитях"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "SVr4, SUSv2.  Not present in POSIX.1."
msgstr "SVr4, SUSv2. Отсутствуют в POSIX.1."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "ДЕФЕКТЫ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The value returned on error is also a legitimate data value.  B<ferror>(3)  "
"can be used to distinguish between the two cases."
msgstr ""
"Значение, возвращаемое при ошибке, также является корректным значением. Для "
"точного определения ошибки можно использовать B<ferror>(3)."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<ferror>(3), B<fread>(3), B<fwrite>(3), B<getc>(3), B<putc>(3)"
msgstr "B<ferror>(3), B<fread>(3), B<fwrite>(3), B<getc>(3), B<putc>(3)"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "GETW"
msgstr "GETW"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "2016-03-15"
msgstr "15 марта 2016 г."

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "GNU"
msgstr "GNU"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Руководство программиста Linux"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "B<int getw(FILE *>I<stream>B<);>\n"
msgstr "B<int getw(FILE *>I<stream>B<);>\n"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "B<int putw(int >I<w>B<, FILE *>I<stream>B<);>\n"
msgstr "B<int putw(int >I<w>B<, FILE *>I<stream>B<);>\n"

#. type: TP
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Since glibc 2.3.3:"
msgstr "Начиная с glibc 2.3.3:"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid ""
"_XOPEN_SOURCE && ! (_POSIX_C_SOURCE\\ E<gt>=\\ 200112L)\n"
"    || /* Glibc since 2.19: */ _DEFAULT_SOURCE\n"
"    || /* Glibc versions E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"_XOPEN_SOURCE && ! (_POSIX_C_SOURCE\\ E<gt>=\\ 200112L)\n"
"    || /* начиная с glibc 2.19: */ _DEFAULT_SOURCE\n"
"    || /* в версии glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"

#. type: TP
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Before glibc 2.3.3:"
msgstr "До glibc 2.3.3:"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "_SVID_SOURCE || _BSD_SOURCE || _XOPEN_SOURCE"
msgstr "_SVID_SOURCE || _BSD_SOURCE || _XOPEN_SOURCE"

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "СООТВЕТСТВИЕ СТАНДАРТАМ"

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 5.10. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 4.16. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-12-15"
msgstr "15 декабря 2022 г."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.02"
msgstr "Linux man-pages 6.02"

#. type: Plain text
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "_XOPEN_SOURCE && ! (_POSIX_C_SOURCE\\ E<gt>=\\ 200112L)\n"
#| "    || /* Glibc since 2.19: */ _DEFAULT_SOURCE\n"
#| "    || /* Glibc versions E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgid ""
"    Since glibc 2.3.3:\n"
"        _XOPEN_SOURCE && ! (_POSIX_C_SOURCE E<gt>= 200112L)\n"
"            || /* Glibc E<gt>= 2.19: */ _DEFAULT_SOURCE\n"
"            || /* Glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
"    Before glibc 2.3.3:\n"
"        _SVID_SOURCE || _BSD_SOURCE || _XOPEN_SOURCE\n"
msgstr ""
"_XOPEN_SOURCE && ! (_POSIX_C_SOURCE\\ E<gt>=\\ 200112L)\n"
"    || /* начиная с glibc 2.19: */ _DEFAULT_SOURCE\n"
"    || /* в версии glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
