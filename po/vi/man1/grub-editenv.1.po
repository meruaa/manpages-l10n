# Vietnamese translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-02-15 18:53+0100\n"
"PO-Revision-Date: 2022-01-18 19:49+0100\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Vietnamese <>\n"
"Language: vi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "GRUB-EDITENV"
msgstr "GRUB-EDITENV"

#. type: TH
#: archlinux debian-unstable
#, no-wrap
msgid "February 2023"
msgstr "Tháng 2 năm 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "GRUB 2:2.06.r456.g65bc45963-1"
msgstr "GRUB 2:2.06.r456.g65bc45963-1"

#. type: TH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "User Commands"
msgstr "Các câu lệnh"

#. type: SH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "NAME"
msgstr "TÊN"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
#, fuzzy
#| msgid "Tool to edit environment block."
msgid "grub-editenv - edit GRUB environment block"
msgstr "Công cụ sửa khối biến môi trường."

#. type: SH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr "TÓM TẮT"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "B<grub-editenv> [I<\\,OPTION\\/>...] I<\\,FILENAME COMMAND\\/>"
msgstr "B<grub-editenv> [I<\\,TÙY_CHỌN\\/>…] I<\\,TÊN_TẬP_TIN LỆNH\\/>"

#. type: SH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr "MÔ TẢ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "Tool to edit environment block."
msgstr "Công cụ sửa khối biến môi trường."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "Commands:"
msgstr "Lệnh:"

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "create"
msgstr "create"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "Create a blank environment block file."
msgstr "Tạo khối môi trường có nội dung trắng."

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "list"
msgstr "list"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "List the current variables."
msgstr "Liệt kê các biến sẵn dùng."

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "set [NAME=VALUE ...]"
msgstr "set [TÊN=GIÁ_TRỊ …]"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "Set variables."
msgstr "Đặt các biến."

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "unset [NAME ...]"
msgstr "unset [TÊN …]"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "Delete variables."
msgstr "Xóa các biến."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "Options:"
msgstr "Tùy chọn:"

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "-?, B<--help>"
msgstr "-?, B<--help>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "give this help list"
msgstr "hiển thị trợ giúp này"

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "B<--usage>"
msgstr "B<--usage>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "give a short usage message"
msgstr "hiển thị cách sử dụng dạng ngắn gọn"

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "print verbose messages."
msgstr "hiển thị thông tin chi tiết."

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "print program version"
msgstr "in ra phiên bản chương trình"

#. type: Plain text
#: archlinux
msgid "If FILENAME is `-', the default value //boot/grub/grubenv is used."
msgstr ""
"Nếu TÊN_TẬP_TIN là “-”, thì giá trị mặc định //boot/grub/grubenv sẽ được "
"dùng."

#. type: Plain text
#: archlinux
msgid ""
"There is no `delete' command; if you want to delete the whole environment "
"block, use `rm //boot/grub/grubenv'."
msgstr ""
"Ở đây không có lệnh “delete”; nếu bạn muốn xóa toàn bộ khối môi\n"
"trường thì hãy dùng lệnh “rm //boot/grub/grubenv”."

#. type: SH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "REPORTING BUGS"
msgstr "THÔNG BÁO LỖI"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "Report bugs to E<lt>bug-grub@gnu.orgE<gt>."
msgstr ""
"Hãy thông báo lỗi cho  E<lt>bug-grub@gnu.orgE<gt>. Thông báo lỗi dịch cho: "
"E<lt>http://translationproject.org/team/vi.htmlE<gt>."

#. type: SH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr "XEM THÊM"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "B<grub-reboot>(8), B<grub-set-default>(8)"
msgstr "B<grub-reboot>(8), B<grub-set-default>(8)"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid ""
"The full documentation for B<grub-editenv> is maintained as a Texinfo "
"manual.  If the B<info> and B<grub-editenv> programs are properly installed "
"at your site, the command"
msgstr ""
"Tài liệu hướng dẫn đầy đủ về B<grub-editenv> được bảo trì dưới dạng một sổ "
"tay Texinfo.  Nếu chương trình B<info> và B<grub-editenv> được cài đặt đúng "
"ở địa chỉ của bạn thì câu lệnh"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "B<info grub-editenv>"
msgstr "B<info grub-editenv>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "should give you access to the complete manual."
msgstr "nên cho phép bạn truy cập đến toàn bộ sổ tay."

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "November 2022"
msgstr "Tháng 11 năm 2022"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "GRUB 2.06-3~deb11u5"
msgstr "GRUB 2.06-3~deb11u5"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"If FILENAME is `-', the default value I<\\,/boot/grub/grubenv\\/> is used."
msgstr ""
"Nếu TÊN_TẬP_TIN là “-”, thì giá trị mặc định I<\\,/boot/grub/grubenv\\/> sẽ "
"được dùng."

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"There is no `delete' command; if you want to delete the whole environment "
"block, use `rm /boot/grub/grubenv'."
msgstr ""
"Ở đây không có lệnh “delete”; nếu bạn muốn xóa toàn bộ khối môi trường thì "
"hãy dùng lệnh “rm /boot/grub/grubenv”."

#. type: TH
#: debian-unstable
#, no-wrap
msgid "GRUB 2.06-8"
msgstr "GRUB 2.06-8"
