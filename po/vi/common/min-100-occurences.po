# Common msgids (vi)
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-02-20 20:44+0100\n"
"PO-Revision-Date: 2022-01-18 19:49+0100\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Vietnamese <>\n"
"Language: vi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 debian-bullseye opensuse-tumbleweed
#, no-wrap
msgid " 1."
msgstr " 1."

#: debian-bullseye debian-unstable opensuse-leap-15-5 mageia-cauldron archlinux
#: fedora-38 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "*"
msgstr "*"

#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr ""

#: debian-bullseye debian-unstable fedora-38 fedora-rawhide opensuse-leap-15-5
#: opensuse-tumbleweed mageia-cauldron archlinux opensuse-leap-15-4
#, no-wrap
msgid "AUTHOR"
msgstr "TÁC GIẢ"

#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "AUTHOR"
msgid "AUTHORS"
msgstr "TÁC GIẢ"

#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed debian-bullseye debian-unstable
#, no-wrap
msgid "AVAILABILITY"
msgstr ""

#: mageia-cauldron debian-unstable fedora-38 fedora-rawhide
#, no-wrap
msgid "April 2022"
msgstr "Tháng 4 năm 2022"

#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr ""

#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed fedora-38 fedora-rawhide
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed fedora-38 fedora-rawhide
#, no-wrap
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EBADF>"
msgstr "B<EBADF>"

#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOMEM>"
msgstr "B<ENOMEM>"

#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM>"
msgstr "B<EPERM>"

#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr ""

#: debian-bullseye opensuse-leap-15-5 archlinux debian-unstable fedora-38
#: fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "COLOPHON"
msgstr ""

#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "BẢN QUYỀN"

#: debian-bullseye opensuse-leap-15-5 fedora-38 fedora-rawhide
msgid ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  Giấy phép GPL pb3+ : "
"Giấy phép Công cộng GNU phiên bản 3 hay sau E<lt>https://gnu.org/licenses/"
"gpl.htmlE<gt>."

#: debian-unstable fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#: archlinux
msgid ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  Giấy phép GPL pb3+ : "
"Giấy phép Công cộng GNU phiên bản 3 hay sau E<lt>https://gnu.org/licenses/"
"gpl.htmlE<gt>."

#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed fedora-38 fedora-rawhide opensuse-leap-15-4
#, no-wrap
msgid "DESCRIPTION"
msgstr "MÔ TẢ"

#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed debian-bullseye debian-unstable
#, fuzzy
#| msgid "display this help and exit"
msgid "Display help text and exit."
msgstr "hiển thị trợ giúp này rồi thoát"

#: opensuse-leap-15-5 debian-bullseye archlinux debian-unstable fedora-38
#: fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid "output version information and exit"
msgid "Display version information and exit."
msgstr "đưa ra thông tin phiên bản rồi thoát"

#: debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed archlinux fedora-38 fedora-rawhide
#, no-wrap
msgid "ENVIRONMENT"
msgstr ""

#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr ""

#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed fedora-38 fedora-rawhide
#, no-wrap
msgid "EXAMPLE"
msgstr ""

# type: =head1
#: debian-bullseye debian-unstable mageia-cauldron archlinux fedora-38
#: fedora-rawhide opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "VÍ DỤ"

#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "EXIT STATUS"
msgstr ""

#: debian-bullseye debian-unstable fedora-38 fedora-rawhide opensuse-leap-15-5
#: opensuse-tumbleweed archlinux mageia-cauldron opensuse-leap-15-4
#, no-wrap
msgid "FILES"
msgstr "TẬP TIN"

#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""

#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""

#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed debian-unstable
msgid "For bug reports, use the issue tracker at"
msgstr ""

#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "GNU coreutils 8.32"
msgstr "GNU coreutils 8.32"

#: debian-unstable fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#: archlinux
#, no-wrap
msgid "GNU coreutils 9.1"
msgstr "GNU coreutils 9.1"

#: debian-bullseye debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed archlinux
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"Trợ giúp trực tuyến GNU coreutils: E<lt>https://www.gnu.org/software/"
"coreutils/E<gt>"

#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr ""

#: fedora-38 fedora-rawhide debian-unstable archlinux mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "January 2023"
msgstr "Tháng 1 năm 2023"

#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed debian-bullseye
#, no-wrap
msgid "LIBRARY"
msgstr ""

#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Linux"
msgstr ""

#: debian-bullseye debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed archlinux
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.02"
msgstr ""

#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr ""

#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr ""

#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed fedora-38 fedora-rawhide opensuse-leap-15-4
#, no-wrap
msgid "NAME"
msgstr "TÊN"

#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 debian-bullseye opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr ""

#: archlinux debian-bullseye mageia-cauldron
#, no-wrap
msgid "November 2022"
msgstr "Tháng 11 năm 2022"

#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed fedora-38 fedora-rawhide
#, no-wrap
msgid "OPTIONS"
msgstr "TÙY CHỌN"

#: opensuse-leap-15-5 mageia-cauldron
#, no-wrap
msgid "October 2021"
msgstr "Tháng 10 năm 2021"

#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed mageia-cauldron
msgid "POSIX.1-2001, POSIX.1-2008."
msgstr ""

#: archlinux debian-bullseye debian-unstable opensuse-leap-15-5
#: opensuse-tumbleweed fedora-38 fedora-rawhide mageia-cauldron
#, fuzzy
#| msgid "output version information and exit"
msgid "Print version and exit."
msgstr "đưa ra thông tin phiên bản rồi thoát"

#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Program source"
msgstr ""

#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed fedora-38 fedora-rawhide
#, no-wrap
msgid "REPORTING BUGS"
msgstr "THÔNG BÁO LỖI"

#: debian-bullseye debian-unstable opensuse-leap-15-5 opensuse-tumbleweed
#: archlinux mageia-cauldron fedora-38 fedora-rawhide
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

#: debian-bullseye debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed archlinux
#, fuzzy
#| msgid ""
#| "Report %s translation bugs to <https://translationproject.org/team/>\n"
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"Hãy thông báo lỗi dịch “%s” cho <https://translationproject.org/team/vi."
"html>\n"

#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed fedora-38 fedora-rawhide opensuse-leap-15-4
#, no-wrap
msgid "SEE ALSO"
msgstr "XEM THÊM"

#: debian-bullseye debian-unstable archlinux fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr ""

#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed fedora-38 fedora-rawhide
#, no-wrap
msgid "SYNOPSIS"
msgstr "TÓM TẮT"

#: debian-bullseye
#, no-wrap
msgid "September 2020"
msgstr "Tháng 9 năm 2020"

#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "September 2022"
msgstr "Tháng 9 năm 2022"

#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#: debian-bullseye debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed archlinux
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Đây là phần mềm tự do: bạn có quyền sửa đổi và phát hành lại nó. KHÔNG CÓ "
"BẢO HÀNH GÌ CẢ, với điều khiển được pháp luật cho phép."

#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""

#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""

#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr ""

#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed fedora-38 fedora-rawhide
#, no-wrap
msgid "User Commands"
msgstr "Các câu lệnh"

#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr ""

#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr ""

#: debian-unstable fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed archlinux debian-bullseye
#, no-wrap
msgid "\\(bu"
msgstr "\\(bu"

#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "\\[bu]"
msgstr ""

#: debian-bullseye debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed archlinux
msgid "display this help and exit"
msgstr "hiển thị trợ giúp này rồi thoát"

#: debian-bullseye debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed archlinux
msgid "output version information and exit"
msgstr "đưa ra thông tin phiên bản rồi thoát"

#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed fedora-38 fedora-rawhide
msgid "should give you access to the complete manual."
msgstr "nên cho phép bạn truy cập đến toàn bộ sổ tay."

#: opensuse-leap-15-5
#, no-wrap
msgid "systemd 249"
msgstr "systemd 249"

#: debian-unstable mageia-cauldron debian-bullseye opensuse-tumbleweed
#: archlinux fedora-38 fedora-rawhide
#, no-wrap
msgid "systemd 252"
msgstr "systemd 252"

#: archlinux fedora-38 fedora-rawhide
#, fuzzy, no-wrap
#| msgid "systemd 251"
msgid "systemd 253"
msgstr "systemd 251"

#: debian-bullseye
#, no-wrap
msgid "util-linux"
msgstr "util-linux"

#: opensuse-leap-15-5
#, no-wrap
msgid "util-linux 2.37.4"
msgstr "util-linux 2.37.4"

#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#: debian-unstable
#, no-wrap
msgid "util-linux 2.38.1"
msgstr "util-linux 2.38.1"
